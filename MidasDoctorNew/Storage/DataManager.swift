//
//  DataManager.swift
//  MiDasHMIS
//
//  Created by ramesh prajapati on 12/20/19.
//  Copyright © 2019 Midas. All rights reserved.
//

import UIKit
class DataManager: NSObject, NSCoding {
    
    var gdocid: String?
    var fullname: String?
    var mobile_number: String?
    var pwd: String?
    
    var gdepid: String?
    var islivemerodoctor: String?
    var orgid: String?
    var orgimageurl: String?
    var hospitaldomain: String?
    var address: String?
    var orgname: String?
    
    
    init(json: [String: Any])
    {
        self.gdocid = json["gdocid"] as? String
        self.fullname = json["fullname"] as? String
        self.mobile_number = json["mobile_number"] as? String
        self.pwd = json["pwd"] as? String
        
        self.gdepid                   = json["gdepid"] as? String
        self.islivemerodoctor   = json["islivemerodoctor"] as? String
        self.orgid                     = json["orgid"] as? String
        self.orgimageurl            = json["orgimageurl"] as? String
        self.hospitaldomain     = json["hospitaldomain"] as? String
        self.address                = json["address"] as? String
        self.orgname                = json["orgname"] as? String
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.gdocid, forKey:"gdocid")
        aCoder.encode(self.fullname, forKey:"fullname")
        aCoder.encode(self.mobile_number, forKey:"mobile_number")
        aCoder.encode(self.pwd, forKey:"pwd")
        
        
        aCoder.encode(self.gdepid, forKey: "gdepid")
        aCoder.encode(self.islivemerodoctor, forKey: "islivemerodoctor")
        aCoder.encode(self.orgid, forKey: "orgid")
        aCoder.encode(self.orgimageurl, forKey: "orgimageurl")
        aCoder.encode(self.hospitaldomain, forKey: "hospitaldomain")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encode(self.orgname, forKey: "orgname")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.gdocid = aDecoder.decodeObject(forKey: "gdocid") as? String
        self.fullname = aDecoder.decodeObject(forKey: "fullname") as? String
        self.mobile_number = aDecoder.decodeObject(forKey: "mobile_number") as? String
        self.pwd = aDecoder.decodeObject(forKey: "pwd") as? String
        
        
        self.gdepid = aDecoder.decodeObject(forKey: "gdepid") as? String
        self.islivemerodoctor = aDecoder.decodeObject(forKey: "islivemerodoctor") as? String
        self.orgid = aDecoder.decodeObject(forKey: "orgid") as? String
        self.orgimageurl = aDecoder.decodeObject(forKey: "orgimageurl") as? String
        self.hospitaldomain = aDecoder.decodeObject(forKey: "hospitaldomain") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.orgname = aDecoder.decodeObject(forKey: "orgname") as? String
    }
    
    deinit {
        print("deinit")
        self.gdocid = nil
        self.fullname = nil
        self.mobile_number = nil
        self.pwd = nil
        
        self.gdepid = nil
        self.islivemerodoctor = nil
        self.orgid = nil
        self.orgimageurl = nil
        self.hospitaldomain = nil
        self.address = nil
        self.orgname = nil
    }
    
    class func getUserDetail() -> Array<DataManager> {
        let decodedArray = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "UserDetail") as! Data) as! [DataManager]
        return decodedArray
    }
}
