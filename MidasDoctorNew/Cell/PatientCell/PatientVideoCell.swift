//
//  PatientVideoCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit
protocol videoCellDelegate: class {
    func outPatient()
}
class PatientVideoCell: UITableViewCell {

    let videoConsultationView: UIView = {
        let bv = UIView()
        bv.contentMode = .scaleAspectFit
        bv.clipsToBounds = true
        bv.layer.cornerRadius = 8
        bv.isUserInteractionEnabled = true
        return bv
    }()
    let stackView: UIStackView = {
    let stackView   = UIStackView()
    stackView.axis  = NSLayoutConstraint.Axis.horizontal
    stackView.distribution  = UIStackView.Distribution.equalSpacing
    stackView.alignment = UIStackView.Alignment.center
    stackView.spacing   = 16.0
        return stackView
    }()
     let outPatientBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = Theme.Color.Dark_Red_Color
        btn.titleLabel?.numberOfLines = 2
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.setTitle("OUT \nPATIENT", for: .normal)
        btn.layer.cornerRadius = 4
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    
        btn.tintColor = Theme.Color.white
        btn.clipsToBounds = true
        return btn
    }()
    
    let videoConsultationlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.medium(ofSize: 16)
        lbl.numberOfLines = 1
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        lbl.text = "Video Consultation Room"
        return lbl
    }()
    
    let videoConsultationImage: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = false
        iv.contentMode = .scaleToFill
        iv.image = Images.ic_video_room
        iv.alpha = 0.3
        return iv
    }()
    
    let gradientLayer = CAGradientLayer()
    weak var delegate: videoCellDelegate?
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: self.layer)
        gradientLayer.frame = videoConsultationView.bounds
        videoConsultationView.applyGradient(colors: [Theme.Color.Light_Orange_Color, Theme.Color.Orange_Color], gradient: .horizontal)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray
       

//        stackView.addArrangedSubview(videoConsultationView)
//        stackView.addArrangedSubview(outPatientBtn)
       // stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(videoConsultationView)
        contentView.addSubview(outPatientBtn)
        
        videoConsultationView.addSubview(videoConsultationImage)
        videoConsultationView.addSubview(videoConsultationlbl)
        contentView.addSubview(stackView)
    }
    
    fileprivate func setupConstraint(){
        videoConsultationView.frame = CGRect(x: 10, y: 10, width: SCREEN.WIDTH - 120, height: 60)
        videoConsultationImage.frame = CGRect(x: videoConsultationView.frame.width - 10 - 40, y: 5, width: 50, height: 50)
        videoConsultationlbl.frame = CGRect(x: 8, y: 0, width: videoConsultationView.frame.width, height: videoConsultationView.frame.height)
        outPatientBtn.frame = CGRect(x: (videoConsultationView.frame.width + 10 + 10), y: 13.5, width: 90, height: 50)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
