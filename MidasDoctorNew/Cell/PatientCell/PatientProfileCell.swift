//
//  PatientProfileCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit

class PatientProfileCell: UITableViewCell {
    
    lazy var backView = BackViewProperties()
    
    let infolbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    let profileImg: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        return img
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray
        contentView.addSubview(backView)
        backView.addSubview(infolbl)
        backView.addSubview(profileImg)
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 50)
        profileImg.frame = CGRect(x: backView.frame.width - 10 - 30, y: backView.frame.midY - 15, width: 30, height: 30)
        infolbl.frame = CGRect(x: 10, y: backView.frame.midY - 15, width: backView.frame.width - profileImg.frame.width - 10, height: 20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
