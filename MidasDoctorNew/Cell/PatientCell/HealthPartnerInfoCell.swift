//
//  HealthPartnerInfoCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit

class HealthPartnerInfoCell: UITableViewCell {
    
    lazy var backView = BackViewProperties()
    
    let topView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.colorBlue
//        v.addRoundedCorner(corner: .topLeft, radius: 4)
//        v.roundedButton(round: [.topRight, .topLeft])
        return v
    }()
    
    
    let infolbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.white
        lbl.backgroundColor = Theme.Color.colorBlue
        lbl.font = UIFont.medium(ofSize: 16)
        lbl.text = "Health Partner Information"
        return lbl
    }()
    
    let profileImg: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        return img
    }()
    
    let orglbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.colorGreen
        lbl.font = UIFont.semibold(ofSize: 15)
        return lbl
    }()
    
    let addresslbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 14)
        return lbl
    }()
    
    let namelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 14)
        return lbl
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray
        
        contentView.addSubview(backView)
        backView.addSubview(topView)
        topView.addSubview(infolbl)
        
        backView.addSubview(profileImg)
        backView.addSubview(orglbl)
        backView.addSubview(addresslbl)
        backView.addSubview(namelbl)
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 110)
        topView.frame = CGRect(x: 0, y: 0, width: backView.frame.width, height: 30)
        infolbl.frame = CGRect(x: 10, y: 0, width: topView.frame.width - 20, height: 30)

        profileImg.frame = CGRect(x: 5, y: 40, width: 50, height: 50)
        orglbl.frame = CGRect(x: profileImg.frame.origin.x + profileImg.frame.width + 5, y: 40, width: backView.frame.width - profileImg.frame.origin.x - profileImg.frame.width - 5 - 10, height: 20)
        addresslbl.frame = CGRect(x: orglbl.frame.origin.x, y: orglbl.frame.origin.y + orglbl.frame.height, width: orglbl.frame.width, height: 18)
        namelbl.frame = CGRect(x: orglbl.frame.origin.x, y: addresslbl.frame.origin.y + addresslbl.frame.height, width: orglbl.frame.width, height: 18)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
