//
//  PatientInfoCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit

class PatientInfoCell: UITableViewCell {
    
    lazy var backView = BackViewProperties()
    let topView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.colorBlue
//        v.addRoundedCorner(corner: .topLeft, radius: 4)
//        v.roundedButton(round: [.topRight, .topLeft])
        return v
    }()
    let infolbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.white
        lbl.backgroundColor = Theme.Color.colorBlue
        lbl.font = UIFont.medium(ofSize: 16)
        lbl.text = "Patient Information"
        
        return lbl
    }()
    
    let profileImg: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        return img
    }()
    
    let namelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    let genderlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    let agelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    
    let tokenlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.semibold(ofSize: 15)
        return lbl
    }()
            
    let appDate: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.lightGray
        lbl.font = UIFont.medium(ofSize: 15)
//        lbl.textAlignment = .right
        return lbl
    }()
    
    let appDatelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
//        lbl.textAlignment = .right
        return lbl
    }()
    
    let consultationTime: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.lightGray
        lbl.font = UIFont.medium(ofSize: 15)
//        lbl.textAlignment = .right
        return lbl
    }()
    
    let consultationTimelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
//        lbl.textAlignment = .right
        return lbl
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray
        
        contentView.addSubview(backView)
        backView.addSubview(topView)
        topView.addSubview(infolbl)
        
        backView.addSubview(profileImg)
        backView.addSubview(namelbl)
        backView.addSubview(genderlbl)
        backView.addSubview(agelbl)
        
        backView.addSubview(tokenlbl)
        backView.addSubview(appDate)
        backView.addSubview(appDatelbl)
        backView.addSubview(consultationTime)
        backView.addSubview(consultationTimelbl)
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 170)
        
        topView.frame = CGRect(x: 0, y: 0, width: backView.frame.width, height: 30)
        infolbl.frame = CGRect(x: 10, y: 0, width: topView.frame.width - 20, height: 30)

        profileImg.frame = CGRect(x: 5, y: 40, width: 50, height: 50)
        namelbl.frame = CGRect(x: profileImg.frame.origin.x + profileImg.frame.width + 5, y: 40, width: backView.frame.width - profileImg.frame.origin.x - profileImg.frame.width - 5, height: 20)
        genderlbl.frame = CGRect(x: namelbl.frame.origin.x, y: namelbl.frame.origin.y + namelbl.frame.height, width: namelbl.frame.width, height: 18)
        agelbl.frame = CGRect(x: namelbl.frame.origin.x, y: genderlbl.frame.origin.y + genderlbl.frame.height, width: namelbl.frame.width, height: 18)
        tokenlbl.frame = CGRect(x: 10, y: agelbl.frame.origin.y + agelbl.frame.height + 10, width: backView.frame.width - 20, height: 18)
        
        let appWidth: CGFloat = backView.frame.width / 2
        appDate.frame = CGRect(x: 10, y: tokenlbl.frame.origin.y + tokenlbl.frame.height, width: appWidth - 5, height: 18)
        consultationTime.frame = CGRect(x: 10, y: appDate.frame.origin.y + appDate.frame.height, width: appWidth - 5, height: 18)

        appDatelbl.frame = CGRect(x: appDate.frame.origin.x + appDate.frame.width, y: appDate.frame.origin.y, width: appWidth - 5, height: 18)
        consultationTimelbl.frame = CGRect(x: consultationTime.frame.origin.x + consultationTime.frame.width, y: consultationTime.frame.origin.y, width: appWidth - 5, height: 18)

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
