//
//  HealthPortfolioUploadCell.swift
//  HealthPartner
//
//  Created by ramesh prajapati on 1/20/21.
//

import UIKit

class HealthPortfolioUploadCell: UICollectionViewCell {
    
    fileprivate lazy var backview = BackViewProperties()
    let medicallbl = UILabel()
    
    fileprivate lazy var subBackview = BackViewProperties()
    
    
    fileprivate lazy var imag1 = UIImageView()
    fileprivate lazy var imag2 = UIImageView()
    fileprivate lazy var imag3 = UIImageView()
    fileprivate lazy var titlelbl1 = UILabel()
    fileprivate lazy var titlelbl2 = UILabel()
    fileprivate lazy var titlelbl3 = UILabel()
    lazy var subtitlelbl1 = UILabel()
    lazy var subtitlelbl2 = UILabel()
    lazy var subtitlelbl3 = UILabel()
    lazy var chooseBtn1 = UIButton()
    lazy var chooseBtn2 = UIButton()
    lazy var chooseBtn3 = UIButton()
    
    lazy var editBtn = UIButton()
    lazy var saveBtn = PrimaryActionButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupFrame()
    }
    
    fileprivate func setupView() {
        contentView.addSubview(backview)
        backview.addSubview(medicallbl)
        backview.addSubview(subBackview)

        subBackview.addSubview(imag1)
        subBackview.addSubview(imag2)
        subBackview.addSubview(imag3)
        
        subBackview.addSubview(titlelbl1)
        subBackview.addSubview(titlelbl2)
        subBackview.addSubview(titlelbl3)

        subBackview.addSubview(subtitlelbl1)
        subBackview.addSubview(subtitlelbl2)
        subBackview.addSubview(subtitlelbl3)
        
        subBackview.addSubview(chooseBtn1)
        subBackview.addSubview(chooseBtn2)
        subBackview.addSubview(chooseBtn3)

        subBackview.addSubview(editBtn)
        subBackview.addSubview(saveBtn)

        
        backview.backgroundColor = Theme.Color.dark_Green_Color
        
        medicallbl.text = "Medical Records"
        medicallbl.textColor = .white
        medicallbl.font = UIFont.medium(ofSize: 16)
        
        imag1.image = Images.ic_image
        imag2.image = Images.ic_image
        imag3.image = Images.ic_image

        titlelbl1.text = "Pathology Report (Photo)"
        titlelbl2.text = "X-ray, ECG, Echo, MRI, CT Reports (Photo)"
        titlelbl3.text = "Prescription / Medication (Photo)"
        
        titlelbl1.font = UIFont.medium(ofSize: 16)
        titlelbl2.font = UIFont.medium(ofSize: 16)
        titlelbl3.font = UIFont.medium(ofSize: 16)
        
        titlelbl1.textColor = .black
        titlelbl2.textColor = .black
        titlelbl3.textColor = .black
        
        
        subtitlelbl1.text = "No Photo Selected"
        subtitlelbl2.text = "No Photo Selected"
        subtitlelbl3.text = "No Photo Selected"
        
        subtitlelbl1.font = UIFont.regular(ofSize: 15)
        subtitlelbl2.font = UIFont.regular(ofSize: 15)
        subtitlelbl3.font = UIFont.regular(ofSize: 15)
        
        subtitlelbl1.textColor = .darkGray
        subtitlelbl2.textColor = .darkGray
        subtitlelbl3.textColor = .darkGray
        
        
        chooseBtn1.setImage(Images.ic_folder, for: .normal)
        chooseBtn1.backgroundColor = .clear
        chooseBtn1.clipsToBounds = true
        chooseBtn1.layer.borderWidth = 1.5
        chooseBtn1.layer.borderColor = Theme.Color.lightGray.cgColor
        chooseBtn1.layer.cornerRadius = 20
        
        chooseBtn2.setImage(Images.ic_folder, for: .normal)
        chooseBtn2.backgroundColor = .clear
        chooseBtn2.clipsToBounds = true
        chooseBtn2.layer.borderWidth = 1.5
        chooseBtn2.layer.borderColor = Theme.Color.lightGray.cgColor
        chooseBtn2.layer.cornerRadius = 20
        
        chooseBtn3.setImage(Images.ic_folder, for: .normal)
        chooseBtn3.backgroundColor = .clear
        chooseBtn3.clipsToBounds = true
        chooseBtn3.layer.borderWidth = 1.5
        chooseBtn3.layer.borderColor = Theme.Color.lightGray.cgColor
        chooseBtn3.layer.cornerRadius = 20
        
        editBtn.setTitle("Edit", for: .normal)
        editBtn.setTitleColor(Theme.Color.white, for: .normal)
        editBtn.backgroundColor = Theme.Color.dark_Green_Color
        editBtn.layer.cornerRadius = 8
        
        saveBtn.setTitle("Save", for: .normal)
        saveBtn.layer.cornerRadius = 8

    }
    
    fileprivate func setupFrame(){
        backview.frame = CGRect(x: 10, y: 10, width: SCREEN.WIDTH - 20, height: 295)
        medicallbl.frame = CGRect(x: 10, y: 10, width: backview.frame.width - 20, height: 20)
        subBackview.frame = CGRect(x: 3, y: medicallbl.frame.origin.y + medicallbl.frame.height + 10, width: backview.frame.width - 6, height: backview.frame.height - 10 - 20 - 10 - 3)
        
        imag1.frame = CGRect(x: 10, y: 10, width: 40, height: 40)
        imag2.frame = CGRect(x: 10, y: imag1.frame.origin.y + imag1.frame.height + 20, width: 40, height: 40)
        imag3.frame = CGRect(x: 10, y: imag2.frame.origin.y + imag2.frame.height + 20, width: 40, height: 40)

        chooseBtn1.frame = CGRect(x: subBackview.frame.width - 10 - 40, y: imag1.frame.origin.y , width: 40, height: 40)
        chooseBtn2.frame = CGRect(x: subBackview.frame.width - 10 - 40, y: imag2.frame.origin.y , width: 40, height: 40)
        chooseBtn3.frame = CGRect(x: subBackview.frame.width - 10 - 40, y: imag3.frame.origin.y , width: 40, height: 40)
        
        let titleWidth: CGFloat = subBackview.frame.width - 10 - imag1.frame.width - 5 - 5 - chooseBtn1.frame.width - 10
        titlelbl1.frame = CGRect(x: imag1.frame.origin.x + imag1.frame.width + 5, y: imag1.frame.origin.y + 5, width: titleWidth, height: 20)
        titlelbl2.frame = CGRect(x: titlelbl1.frame.origin.x , y: imag2.frame.origin.y + 5, width: titleWidth, height: 20)
        titlelbl3.frame = CGRect(x: titlelbl1.frame.origin.x, y: imag3.frame.origin.y + 5, width: titleWidth, height: 20)
        
        subtitlelbl1.frame = CGRect(x: titlelbl1.frame.origin.x , y: titlelbl1.frame.origin.y + titlelbl1.frame.height, width: titleWidth, height: 20)
        subtitlelbl2.frame = CGRect(x: titlelbl1.frame.origin.x , y: titlelbl2.frame.origin.y + titlelbl2.frame.height, width: titleWidth, height: 20)
        subtitlelbl3.frame = CGRect(x: titlelbl1.frame.origin.x , y: titlelbl3.frame.origin.y + titlelbl3.frame.height, width: titleWidth, height: 20)
        
        saveBtn.frame = CGRect(x: subBackview.frame.width - 10 - 80, y: subtitlelbl3.frame.origin.y + subtitlelbl3.frame.height + 20, width: 80, height: 35)
        editBtn.frame = CGRect(x: saveBtn.frame.origin.x - 10 - 80, y: saveBtn.frame.origin.y, width: 80, height: 35)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
