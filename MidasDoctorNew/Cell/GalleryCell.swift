//
//  GalleryCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 04/04/2021.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    lazy var profileImage: UIImageView = {
       let iv = UIImageView()
//        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.layer.borderWidth = 1
        iv.layer.borderColor = Theme.Color.lightGray.cgColor
        iv.clipsToBounds = true
        return iv
    }()
    lazy var deleteBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(Images.ic_close_red, for: .normal)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(profileImage)
        contentView.addSubview(deleteBtn)
        
        profileImage.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        deleteBtn.frame = CGRect(x: contentView.frame.width - 20 - 5, y: 0, width: 20, height: 20)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
//    var data: MyCustomData?{
//        didSet{
//            guard  let data = data else{ return }
//            img.image = data.image
//        }
//    }
//
    
        override func layoutSubviews() {
            super.layoutSubviews()
            profileImage.frame = self.bounds
        }
}
