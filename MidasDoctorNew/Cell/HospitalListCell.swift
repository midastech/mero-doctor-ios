//
//  HospitalListCell.swift
//  MeroDoctorNew
//
//  Created by ramesh prajapati on 2/8/21.
//

import UIKit

class HospitalListCell: UITableViewCell {
    let backView: UIView = {
        let bv = BackViewProperties()
        return bv
    }()
    
    fileprivate lazy var profileImg: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.clipsToBounds = true
        iv.dropShadow()
        iv.layer.cornerRadius = 8
        
        return iv
    }()
    
    
    fileprivate lazy var eHospitalNamelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 13)
        tl.textColor = .black
        return tl
    }()
    
    fileprivate lazy var addresslbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.regular(ofSize: 13)
        tl.textColor = .darkGray
        return tl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = Theme.Color.white
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        contentView.addSubview(backView)
        backView.addSubview(profileImg)
        backView.addSubview(eHospitalNamelbl)
        backView.addSubview(addresslbl)
        profileImg.layer.cornerRadius = 4.0
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 70)
        profileImg.frame = CGRect(x: 5, y: 5, width: 60, height: 60)
        eHospitalNamelbl.frame = CGRect(x: profileImg.frame.origin.x + profileImg.frame.width + 5, y: 12, width: backView.frame.width - profileImg.frame.origin.x - profileImg.frame.width - 10, height: 20)
        addresslbl.frame = CGRect(x: eHospitalNamelbl.frame.origin.x, y: eHospitalNamelbl.frame.origin.y + eHospitalNamelbl.frame.height, width: eHospitalNamelbl.frame.width, height: 20)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var data: HospitalListModel?{
        didSet{
            guard let data = data else { return }
            profileImg.sd_setImage(with: URL(string: data.orgimageurl), placeholderImage: Images.ic_hospital)
            eHospitalNamelbl.text = data.orgname
            addresslbl.text = data.address
        }
    }
    
}
