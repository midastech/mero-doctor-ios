//
//  CommonSymptomsFooterCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 05/04/2021.
//

import UIKit

class CommonSymptomsFooterCell: UICollectionViewCell {
    lazy var anylbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 15)
        tl.textColor = .black
        tl.text = "Any other symptoms:"
        return tl
    }()
    
    lazy var anySymptomsTxtField: PrimaryInputField = {
        let tx = PrimaryInputField()
        tx.clipsToBounds = true
        tx.textColor = Theme.Color.darkGray
        tx.layer.borderWidth = 1
        tx.layer.borderColor = UIColor.darkGray.cgColor
        tx.layer.cornerRadius = 4
        tx.attributedPlaceholder = (NSAttributedString(string: "Please specify your symptoms here...",
                                                       attributes: [.foregroundColor: Theme.Color.lightGray, .font:UIFont.medium(ofSize: 14)]))
        
//        tx.placeholder = "Please specify your symptoms here..."
        tx.layer.masksToBounds = true
        return tx
    }()
    
    lazy var otherInfolbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 15)
        tl.textColor = .black
        tl.text = "Other Pertinent Information:"
        return tl
    }()
    
    lazy var otherInfoTxtField: PrimaryInputField = {
        let tx = PrimaryInputField()
        tx.clipsToBounds = true
        tx.textColor = Theme.Color.darkGray
        tx.layer.borderWidth = 1
        tx.layer.borderColor = UIColor.darkGray.cgColor
        tx.layer.cornerRadius = 4
        tx.attributedPlaceholder = (NSAttributedString(string: "Please specify similer information here...",
                                                     attributes: [.foregroundColor: Theme.Color.lightGray, .font:UIFont.medium(ofSize: 14)]))

        
        
        tx.layer.masksToBounds = true
        return tx
    }()
    
    lazy var editBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Edit", for: .normal)
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.backgroundColor = Theme.Color.dark_Green_Color
        btn.layer.cornerRadius = 8
        btn.titleLabel?.font = UIFont.medium(ofSize: 15)
        return btn
    }()
    
    lazy var saveBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Save", for: .normal)
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.backgroundColor = Theme.Color.colorBlue
        btn.layer.cornerRadius = 8
        btn.titleLabel?.font = UIFont.medium(ofSize: 15)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(anylbl)
        contentView.addSubview(anySymptomsTxtField)
        contentView.addSubview(otherInfolbl)
        contentView.addSubview(otherInfoTxtField)
        contentView.addSubview(editBtn)
        contentView.addSubview(saveBtn)

        
        
        anylbl.frame = CGRect(x: 10, y: 8, width: SCREEN.WIDTH - 20, height: 20)
        anySymptomsTxtField.frame = CGRect(x: 10, y: anylbl.frame.origin.y + anylbl.frame.height, width: SCREEN.WIDTH - 20, height: 50)
        otherInfolbl.frame = CGRect(x: 10, y: anySymptomsTxtField.frame.origin.y + anySymptomsTxtField.frame.height + 5, width: SCREEN.WIDTH - 20, height: 20)
        otherInfoTxtField.frame = CGRect(x: 10, y: otherInfolbl.frame.origin.y + otherInfolbl.frame.height, width: SCREEN.WIDTH - 20, height: 50)
        
        saveBtn.frame = CGRect(x: SCREEN.WIDTH - 10 - 80, y: otherInfoTxtField.frame.origin.y + otherInfoTxtField.frame.height + 10, width: 80, height: 36)
        editBtn.frame = CGRect(x: saveBtn.frame.origin.x - 10 - 80, y: saveBtn.frame.origin.y, width: 80, height: 36)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
    }
    
}
