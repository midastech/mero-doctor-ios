//
//  ChiefComplaintsCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 05/04/2021.
//

import UIKit

class ChiefComplaintsCell: UICollectionViewCell {

    fileprivate lazy var backview = BackViewProperties()
    let titlelbl = UILabel()
    
    fileprivate lazy var subBackview = BackViewProperties()
    
    fileprivate lazy var complaintTextField: UITextField = {
        let tx = PrimaryInputField()
        tx.clipsToBounds = true
        tx.textColor = Theme.Color.darkGray
        tx.layer.borderWidth = 1
        tx.layer.borderColor = UIColor.darkGray.cgColor
        tx.layer.cornerRadius = 4
        tx.attributedPlaceholder = (NSAttributedString(string: "Please enter chief complaints.",
                                                     attributes: [.foregroundColor: Theme.Color.lightGray, .font:UIFont.medium(ofSize: 14)]))
        tx.layer.masksToBounds = true
        return tx
    }()
    
    lazy var editBtn = UIButton()
    lazy var saveBtn = PrimaryActionButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupFrame()
    }
    
    
    fileprivate func setupView() {
        contentView.addSubview(backview)
        backview.addSubview(titlelbl)
        backview.addSubview(subBackview)

        subBackview.addSubview(complaintTextField)
        subBackview.addSubview(editBtn)
        subBackview.addSubview(saveBtn)
        
        
        backview.backgroundColor = Theme.Color.dark_Green_Color
        titlelbl.text = "Chief Complaints"
        titlelbl.textColor = .white
        titlelbl.font = UIFont.medium(ofSize: 16)
                
        editBtn.setTitle("Edit", for: .normal)
        editBtn.setTitleColor(Theme.Color.white, for: .normal)
        editBtn.backgroundColor = Theme.Color.dark_Green_Color
        editBtn.layer.cornerRadius = 8
        
        saveBtn.setTitle("Save", for: .normal)
        saveBtn.layer.cornerRadius = 8

    }
    
    fileprivate func setupFrame(){
        backview.frame = CGRect(x: 10, y: 10, width: SCREEN.WIDTH - 20, height: 185)
        titlelbl.frame = CGRect(x: 10, y: 10, width: backview.frame.width - 20, height: 20)
        subBackview.frame = CGRect(x: 3, y: titlelbl.frame.origin.y + titlelbl.frame.height + 10, width: backview.frame.width - 6, height: backview.frame.height - 10 - 20 - 10 - 3)
        
        complaintTextField.frame = CGRect(x: 10, y: 10, width: subBackview.frame.width - 20, height: 60)
        
        saveBtn.frame = CGRect(x: subBackview.frame.width - 10 - 80, y: complaintTextField.frame.origin.y + complaintTextField.frame.height + 20, width: 80, height: 35)
        editBtn.frame = CGRect(x: saveBtn.frame.origin.x - 10 - 80, y: saveBtn.frame.origin.y, width: 80, height: 35)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        
    }

}
