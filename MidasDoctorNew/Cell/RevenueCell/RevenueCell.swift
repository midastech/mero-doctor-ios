//
//  RevenueCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 30/03/2021.
//

import UIKit

class RevenueCell: UITableViewCell {
    fileprivate lazy var backView: UIView = {
        let bv = BackViewProperties()
        bv.contentMode = .scaleAspectFit
        return bv
    }()
 
    
     lazy var titlelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.black
        return tl
    }()
    
     lazy var amountlbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 13)
        tl.textColor = Theme.Color.colorBlue
        tl.textAlignment = .right
        return tl
    }()
    
    
    fileprivate lazy var pgView: UIView = {
        let bv = UIView()
        bv.contentMode = .scaleToFill
        bv.backgroundColor = Theme.Color.lightGray
        bv.layer.cornerRadius = 3
        return bv
    }()
    
     lazy var percentlbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 13)
        tl.textColor = Theme.Color.colorMidas
        tl.textAlignment = .right
        return tl
    }()
   
    
    var presentBarView = UIView()
    var progressBgView = UIView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = Theme.Color.backgroundGray
        
        contentView.addSubview(backView)
        backView.addSubview(titlelbl)
        backView.addSubview(amountlbl)
        backView.addSubview(pgView)
        backView.addSubview(percentlbl)
        
//        amountlbl.backgroundColor = .lightGray
        
        setupFrame()
    }
    
    fileprivate func setupFrame(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 60)
        
        amountlbl.frame = CGRect(x: backView.frame.width - 10 - 100, y: 10, width: 100, height: 20)
        titlelbl.frame = CGRect(x: 10, y: 10, width: amountlbl.frame.origin.x - 10 - 10, height: 20)
        
        percentlbl.frame = CGRect(x: amountlbl.frame.origin.x, y: amountlbl.frame.origin.y + 20 + 5, width: amountlbl.frame.width, height: 20)
        pgView.frame = CGRect(x: 10, y: percentlbl.frame.origin.y, width: titlelbl.frame.width, height: 5)
    }
    
    func ProgressBar(progressPercent value: Float){
        addProgressView(progressFrame: CGRect(x: 0, y: 0, width: pgView.frame.size.width, height: pgView.frame.height))
        
        // Apply colors
        setProgressBgColor(color: Theme.Color.lightGray)
        setProgressBarColor(progressColor: Theme.Color.colorBlue)
        setPercentage(precent: value)
    }
    
    
    func addProgressView(progressFrame: CGRect){
        let progressView = UIView(frame: progressFrame)
        //Progress background Downloaded
        progressBgView = UIView(frame: CGRect(x: 0, y: 0, width: progressView.frame.width, height: progressView.frame.height))
        progressBgView.backgroundColor = Theme.Color.lightGray
        
        //Progress present animation view
        presentBarView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: progressBgView.frame.height))
        presentBarView.backgroundColor = Theme.Color.colorBlue
        
        
        presentBarView.layer.cornerRadius = 3
        progressBgView.layer.cornerRadius = 3
        
        
        progressBgView.addSubview(presentBarView)
        progressView.addSubview(progressBgView)
        pgView.addSubview(progressView)
    }
    
    func setProgressBgColor(color: UIColor){
        progressBgView.backgroundColor = color
    }
    
    func setProgressBarColor(progressColor: UIColor){
        presentBarView.backgroundColor = progressColor
    }
    
    func setPercentage(precent: Float){
        UIView.animate(withDuration: 1.0) {
            self.presentBarView.frame = CGRect(x: 0, y: 0, width: (self.progressBgView.frame.width * CGFloat(precent)) / 100, height: self.progressBgView.frame.height)
        }

    }
  /*
    var data: RevenueModel?{
        didSet{
            guard let data = data else { return }
//            profileImg.sd_setImage(with: URL(string: data.orgimageurl), placeholderImage: Images.ic_hospital)
           
            titlelbl.text = String(format: "%@, (%@)", data.servicedescription, data.qty)
            amountlbl.text = "NRs. " + data.netfraction
        }
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
//43770.79
