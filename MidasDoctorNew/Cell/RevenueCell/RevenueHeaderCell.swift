//
//  RevenueHeaderCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 30/03/2021.
//

import UIKit

class RevenueHeaderCell: UITableViewCell {
    
    let categorylbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.darkGray
        tl.text = "Category"
        return tl
    }()
    
    let incomelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.darkGray
        tl.text = "Net Income"
        return tl
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        contentView.addSubview(categorylbl)
        contentView.addSubview(incomelbl)
        contentView.backgroundColor = Theme.Color.backgroundGray
    }
    
    fileprivate func setupConstraint(){
        incomelbl.frame = CGRect(x: SCREEN.WIDTH - 100, y: 0, width: 90, height: 25)
        categorylbl.frame = CGRect(x: 10, y: 0, width: incomelbl.frame.origin.x, height: 25)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
