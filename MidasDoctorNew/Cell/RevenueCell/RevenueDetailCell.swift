//
//  RevenueDetailCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 30/03/2021.
//

import UIKit

class RevenueDetailCell: UITableViewCell {
    fileprivate lazy var backView: UIView = {
        let bv = BackViewProperties()
        bv.contentMode = .scaleAspectFit
        return bv
    }()
 
    
     lazy var billbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.Orange_Color
        return tl
    }()
    
     lazy var namelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.black
        return tl
    }()
    
    
    lazy var unitlbl: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 13)
       tl.textColor = Theme.Color.colorBlue
       return tl
   }()
    
     lazy var totalFractionlbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.darkGray
        return tl
    }()
    lazy var totalincomelbl: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 13)
       tl.textColor = Theme.Color.lightGray
        tl.text = "Total Income"
       return tl
   }()
    
    lazy var tdsamountlbl: UILabel = {
       let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 13.5)
       tl.textColor = Theme.Color.colorBlue
        tl.textAlignment = .center
       return tl
   }()
    
    lazy var tdsamount: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 14)
       tl.textColor = Theme.Color.lightGray
        tl.text = "TDS Amount"
        tl.textAlignment = .center
       return tl
   }()
    
    lazy var billdatelbl: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 13)
       tl.textColor = Theme.Color.colorPrimary
        tl.textAlignment = .right
       return tl
   }()
    
    lazy var agegenderlbl: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 13)
       tl.textColor = Theme.Color.colorPrimary
        tl.textAlignment = .right
       return tl
   }()
    
    lazy var testnamelbl: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 12)
        tl.textColor = Theme.Color.colorMidas
        tl.textAlignment = .right
       return tl
   }()
    
    lazy var netfractionlbl: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.medium(ofSize: 13)
       tl.textColor = Theme.Color.colorGreen
        tl.textAlignment = .right
       return tl
   }()
    
    lazy var netIncome: UILabel = {
       let tl = UILabel()
       tl.font = UIFont.regular(ofSize: 13)
       tl.textColor = Theme.Color.darkGray
        tl.text = "Net Income"
        tl.textAlignment = .right
       return tl
   }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = Theme.Color.backgroundGray
        
        contentView.addSubview(backView)
        backView.addSubview(billbl)
        backView.addSubview(namelbl)
        backView.addSubview(unitlbl)
        backView.addSubview(totalFractionlbl)
        backView.addSubview(totalincomelbl)

        backView.addSubview(tdsamountlbl)
        backView.addSubview(tdsamount)
        backView.addSubview(billdatelbl)
        backView.addSubview(agegenderlbl)

        backView.addSubview(testnamelbl)
        backView.addSubview(netfractionlbl)
        backView.addSubview(netIncome)

//        billdatelbl.backgroundColor = .lightGray
//        testnamelbl.backgroundColor = .lightGray
//        netIncome.backgroundColor = .lightGray
        setupFrame()
    }
    
    fileprivate func setupFrame(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 110)
        billdatelbl.frame = CGRect(x: backView.frame.width - 8 - 120, y: 8, width: 120, height: 18)
        agegenderlbl.frame = CGRect(x: billdatelbl.frame.origin.x, y: billdatelbl.frame.origin.y + billdatelbl.frame.height, width: billdatelbl.frame.width, height: 18)
        testnamelbl.frame = CGRect(x: billdatelbl.frame.origin.x, y: agegenderlbl.frame.origin.y + agegenderlbl.frame.height, width: billdatelbl.frame.width, height: 18)
        
          
        let billWidth = billdatelbl.frame.origin.x - 8 - 5
        billbl.frame = CGRect(x: 8, y: 8, width: billWidth, height: 18)
        namelbl.frame = CGRect(x: 8, y: billbl.frame.origin.y + billbl.frame.height, width: billWidth, height: 18)
        unitlbl.frame = CGRect(x: 8, y: namelbl.frame.origin.y + namelbl.frame.height, width: billWidth, height: 18)
        
        
        let newWidth = (backView.frame.width - 16) / 3
        totalFractionlbl.frame = CGRect(x: 8, y: unitlbl.frame.origin.y + unitlbl.frame.height, width: newWidth - 4, height: 18)
        totalincomelbl.frame = CGRect(x: 8, y: totalFractionlbl.frame.origin.y + totalFractionlbl.frame.height, width: newWidth - 8, height: 18)

        tdsamountlbl.frame = CGRect(x: totalFractionlbl.frame.origin.x + totalFractionlbl.frame.width + 2, y: totalFractionlbl.frame.origin.y, width: newWidth, height: 18)
        tdsamount.frame = CGRect(x: tdsamountlbl.frame.origin.x, y: totalincomelbl.frame.origin.y, width: newWidth, height: 18)
        
        
        netfractionlbl.frame = CGRect(x: tdsamountlbl.frame.origin.x + tdsamountlbl.frame.width + 2, y: totalFractionlbl.frame.origin.y, width: newWidth - 8, height: 18)
        netIncome.frame = CGRect(x: netfractionlbl.frame.origin.x, y: totalincomelbl.frame.origin.y, width: newWidth, height: 18)
              
    }
    
    var data: RevenueDetailModel?{
        didSet{
            guard let data = data else { return }
            billbl.text = data.billno
            namelbl.text = data.patientname
            unitlbl.text = String(format: "(%@)", data.unitamount)
            totalFractionlbl.text = "Rs." + data.totalfraction
            
            tdsamountlbl.text = "Rs." + data.tdsamount
            
            billdatelbl.text = data.billdate
            agegenderlbl.text = data.agegender
            testnamelbl.text = data.testname
            netfractionlbl.text = "Rs." + data.netfraction
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
