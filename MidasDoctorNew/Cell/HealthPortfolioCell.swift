//
//  HealthPortfolioCell.swift
//  HealthPartner
//
//  Created by ramesh prajapati on 1/20/21.
//

import UIKit

class HealthPortfolioCell: UICollectionViewCell {
    lazy var tagLabel: UILabel = {
        let tl = UILabel()
//        tl.translatesAutoresizingMaskIntoConstraints = false
        tl.font = UIFont.regular(ofSize: 15)
        tl.textColor = Theme.Color.lightGray
        
        return tl
    }()
    
    lazy var checkBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.contentMode = .scaleToFill
        
        return button
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(checkBtn)
        contentView.addSubview(tagLabel)

//        contentView.backgroundColor = Theme.Color.dark_Green_Color
//        checkBtn.layer.cornerRadius = 2
//        checkBtn.layer.borderWidth = 2
//        checkBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
//        checkBtn.frame = CGRect(x: 0, y: 10, width: 20, height: 20)
//        tagLabel.frame = CGRect(x: 25, y: 0, width: 55, height: 40)

        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
    }
    
}
