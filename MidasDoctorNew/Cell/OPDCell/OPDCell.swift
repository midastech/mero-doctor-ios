//
//  OPDCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit

class OPDCell: UITableViewCell {
    let backView: UIView = {
        let bv = BackViewProperties()
        return bv
    }()
    
    fileprivate lazy var statuslbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.darkGray
        return tl
    }()
    
    
    fileprivate lazy var calenderImg: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.clipsToBounds = true
        
        let image = Images.ic_calendar_empty.withRenderingMode(.alwaysTemplate)
        iv.image = image
        iv.tintColor = Theme.Color.dark_Green_Color
//        iv.image = Images.ic_calendar
        
        return iv
    }()
    
    fileprivate lazy var datelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.regular(ofSize: 13)
        tl.textColor = Theme.Color.darkGray
        tl.textAlignment = .right
        return tl
    }()
    
    fileprivate lazy var timelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = Theme.Color.black
        tl.textAlignment = .right
        return tl
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = Theme.Color.white
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        contentView.addSubview(backView)
        backView.addSubview(statuslbl)
        backView.addSubview(calenderImg)
        backView.addSubview(datelbl)
        backView.addSubview(timelbl)
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 80)
        calenderImg.frame = CGRect(x: backView.frame.width - 10 - 20, y: 10, width: 20, height: 20)
        datelbl.frame = CGRect(x: backView.frame.width - 10 - 80, y: calenderImg.frame.origin.y + calenderImg.frame.height + 5, width: 80, height: 18)
        timelbl.frame = CGRect(x: datelbl.frame.origin.x, y: datelbl.frame.origin.y + datelbl.frame.height, width: 80, height: 18)
        statuslbl.frame = CGRect(x: 10, y: datelbl.frame.origin.y, width: backView.frame.width - 10 - 5 - datelbl.frame.width - 10, height: 18)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var data: OPDAppointmentModel?{
        didSet{
            guard let data = data else { return }
            
            datelbl.text = data.appdate
            timelbl.text = data.starttime            
            let status = data.status
            if status == "" {
                statuslbl.text = "Not Taken"
            }else{
                statuslbl.text = status
            }
        }
    }
    
}
