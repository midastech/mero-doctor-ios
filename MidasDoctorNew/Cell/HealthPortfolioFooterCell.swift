//
//  HealthPortfolioFooterCell.swift
//  HealthPartner
//
//  Created by ramesh prajapati on 1/20/21.
//

import UIKit

class HealthPortfolioFooterCell: UICollectionViewCell {
    lazy var titlelbl: UILabel = {
        let tl = UILabel()
//        tl.translatesAutoresizingMaskIntoConstraints = false
        tl.font = UIFont.medium(ofSize: 15)
        tl.textColor = Theme.Color.black
        return tl
    }()
    
    lazy var infoTxtField: UITextField = {
        let tx = PrimaryInputField()
        /*
        tx.clipsToBounds = true
        tx.textColor = Theme.Color.darkGray
        tx.layer.borderWidth = 1
        tx.layer.borderColor = UIColor.darkGray.cgColor
        tx.layer.cornerRadius = 4
        
//        tx.layer.cornerRadius = 4
        tx.layer.masksToBounds = true
        tx.isUserInteractionEnabled = false
//        tx.translatesAutoresizingMaskIntoConstraints = false
 */
        return tx
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(titlelbl)
        contentView.addSubview(infoTxtField)
        
        titlelbl.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 20)
        infoTxtField.frame = CGRect(x: 10, y: titlelbl.frame.origin.y + titlelbl.frame.height, width: SCREEN.WIDTH - 20, height: 60)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
    }
    
}
