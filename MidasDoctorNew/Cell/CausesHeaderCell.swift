//
//  CausesHeaderCell.swift
//  HealthPartner
//
//  Created by ramesh prajapati on 1/20/21.
//

import UIKit

class CausesHeaderCell: UICollectionViewCell {
    fileprivate lazy var backView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.colorLightDarkGreen
        
        v.clipsToBounds = true
        v.layer.cornerRadius = 10
        v.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively

        
//        v.customRoundCorners(corners: [.topLeft, .topRight], radius: 8)
        
//        let path = UIBezierPath(roundedRect:v.bounds,
//                                byRoundingCorners:[.topRight, .topLeft],
//                                cornerRadii: CGSize(width: 8, height:  8))
//
//        let maskLayer = CAShapeLayer()
//
//        maskLayer.path = path.cgPath
//        v.layer.mask = maskLayer
        
        return v
    }()
    
    lazy var tagHeaderLabel: UILabel = {
        let tl = UILabel()
//        tl.translatesAutoresizingMaskIntoConstraints = false
//        tl.backgroundColor = Theme.Color.backgroundGray
        tl.textColor = Theme.Color.white
        tl.font = UIFont.semibold(ofSize: 15)
        tl.adjustsFontSizeToFitWidth = true
        return tl
    }()
    
    lazy var downBtn: UIButton = {
        let btn = UIButton()
//        btn.backgroundColor = Theme.Color.white
//        btn.setTitleColor(Theme.Color.Blue_Color, for: .normal)
//        btn.titleLabel?.font = UIFont.medium(ofSize: 13)
//        btn.setTitle("See All", for: .normal)
        let image = Images.ic_downfill.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.white
        
        
        return btn
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = Theme.Color.backgroundGray

        contentView.addSubview(backView)
        backView.addSubview(tagHeaderLabel)
        backView.addSubview(downBtn)
        
        backView.frame = CGRect(x: 8, y: 10, width: SCREEN.WIDTH - 16, height: 45)
        tagHeaderLabel.frame = CGRect(x: 5, y: 10, width: backView.frame.width - 5 - 5 - 25 - 10, height: 25)
        downBtn.frame = CGRect(x: backView.frame.width - 25 - 10, y: 8, width: 25, height: 25)

        
        
//        tagHeaderLabel.layer.cornerRadius = 12
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
//        userImage.image = nil
//        namelbl.textColor = .black
//        backgroundColor = .clear
//        backView.customRoundCorners(corners: [.topLeft, .topRight], radius: 8)
    }
    
}
