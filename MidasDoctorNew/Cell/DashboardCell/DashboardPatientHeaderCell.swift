//
//  DashboardPatientHeaderCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 29/03/2021.
//

import UIKit

class DashboardPatientHeaderCell: UITableViewCell {
    
    let availablelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 14)
        tl.textColor = .black
        tl.text = "My Patients"
        return tl
    }()
    
    let seeAllBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("See All", for: .normal)
        button.setTitleColor(Theme.Color.colorDarkBlue, for: .normal)
        button.titleLabel?.font = UIFont.medium(ofSize: 13)
        button.backgroundColor = .clear
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        contentView.addSubview(availablelbl)
        contentView.addSubview(seeAllBtn)
        contentView.backgroundColor = Theme.Color.backgroundGray
    }
    
    fileprivate func setupConstraint(){
        seeAllBtn.frame = CGRect(x: SCREEN.WIDTH - 60, y: 0, width: 50, height: contentView.frame.height)
        availablelbl.frame = CGRect(x: 10, y: 0, width: seeAllBtn.frame.origin.x, height: contentView.frame.height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
