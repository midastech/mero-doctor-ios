//
//  DashboardPatientEmptyCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 26/03/2021.
//

import UIKit

class DashboardPatientEmptyCell: UITableViewCell {
    
    let imgs: UIImageView = {
        let img = UIImageView()
        img.image = Images.ic_empty_patient
        return img
    }()
    
    let oopslbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 15)
        tl.textColor = Theme.Color.black
        tl.text = "Oops! Patient list is empty."
        tl.textAlignment = .center
        return tl
    }()
    
    let desclbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 13)
        tl.textColor = Theme.Color.lightGray
        tl.numberOfLines = 2
        tl.textAlignment = .center
        tl.text = "Looks like you do not have\nany patients in your list yet."
        return tl
    }()
        
    let refreshBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Refresh", for: .normal)
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.backgroundColor = Theme.Color.colorLightBlue
        btn.titleLabel?.font = UIFont.medium(ofSize: 15)
        return btn
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        contentView.backgroundColor = Theme.Color.backgroundGray
        contentView.addSubview(imgs)
        contentView.addSubview(oopslbl)
        contentView.addSubview(desclbl)
        contentView.addSubview(refreshBtn)
        
        refreshBtn.layer.cornerRadius = 4
    }
    
    fileprivate func setupConstraint(){
        imgs.frame = CGRect(x: SCREEN.WIDTH / 2 - 75, y: 10, width: 160, height: 90)
        oopslbl.frame = CGRect(x: 10, y: imgs.frame.origin.y + imgs.frame.height + 10, width: SCREEN.WIDTH - 20, height: 20)
        desclbl.frame = CGRect(x: 10, y: oopslbl.frame.origin.y + oopslbl.frame.height + 5, width: SCREEN.WIDTH - 20, height: 34)
        refreshBtn.frame = CGRect(x: SCREEN.WIDTH / 2 - 50, y: desclbl.frame.origin.y + desclbl.frame.height + 15, width: 100, height: 40)

    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
