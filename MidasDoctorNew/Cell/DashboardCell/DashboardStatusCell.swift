//
//  DashboardStatusCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 25/03/2021.
//

import UIKit

class DashboardStatusCell: UITableViewCell {
    let backView = UIView()
    
    let currentStatuslbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.regular(ofSize: 14)
        tl.textColor = Theme.Color.black
        tl.text = "Current Status"
        return tl
    }()
    
    lazy var statuslbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.medium(ofSize: 16)
        tl.textColor = Theme.Color.colorMidas
        return tl
    }()
    
    lazy var docNamelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.semibold(ofSize: 15)
        tl.textColor = Theme.Color.black
        tl.textAlignment = .right
        return tl
    }()
    
    lazy var hospitalNamelbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.semibold(ofSize: 14)
        tl.textColor = Theme.Color.colorGreen
        tl.textAlignment = .right
        return tl
    }()
    
    lazy var addresslbl: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.regular(ofSize: 14)
        tl.textColor = Theme.Color.black
        tl.textAlignment = .right
        return tl
    }()
    
    let changeBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Change", for: .normal)
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.backgroundColor = Theme.Color.Orange_Color
        btn.titleLabel?.font = UIFont.medium(ofSize: 15)
        return btn
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        contentView.backgroundColor = Theme.Color.backgroundGray
        contentView.addSubview(backView)
        backView.addSubview(currentStatuslbl)
        backView.addSubview(statuslbl)
        backView.addSubview(changeBtn)
        backView.addSubview(docNamelbl)
        backView.addSubview(hospitalNamelbl)
        backView.addSubview(addresslbl)
        
        backView.layer.cornerRadius = 8.0
        changeBtn.layer.cornerRadius = 17
        
        backView.backgroundColor = UIColor(patternImage: Images.ic_Dashboard_background_Image)
        backView.clipsToBounds = true
        backView.contentMode = .scaleAspectFit
        
//        backView.backgroundColor = Theme.Color.colorLightRed.withAlphaComponent(0.7)
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 100)
        currentStatuslbl.frame = CGRect(x: 10, y: 10, width: 100, height: 20)
        statuslbl.frame = CGRect(x: 10, y: currentStatuslbl.frame.origin.y + currentStatuslbl.frame.height, width: 120, height: 20)
        changeBtn.frame = CGRect(x: 10, y: statuslbl.frame.origin.y + statuslbl.frame.height + 5, width: 100, height: 34)
        
        docNamelbl.frame = CGRect(x: currentStatuslbl.frame.origin.x + currentStatuslbl.frame.width + 5, y: currentStatuslbl.frame.origin.y + 10, width: backView.frame.width - currentStatuslbl.frame.origin.x - currentStatuslbl.frame.width - 5 - 10, height: 20)
        
        hospitalNamelbl.frame = CGRect(x: changeBtn.frame.origin.x + changeBtn.frame.width - 15, y: statuslbl.frame.origin.y + 10, width: backView.frame.width - changeBtn.frame.origin.x - changeBtn.frame.width + 15 - 5, height: 20)
        
        addresslbl.frame = CGRect(x: changeBtn.frame.origin.x + changeBtn.frame.width + 5, y: changeBtn.frame.origin.y + 8, width: backView.frame.width - changeBtn.frame.origin.x - changeBtn.frame.width - 5 - 10, height: 20)
    }
    
    

    var data: listModel?{
        didSet{
            guard let data = data else { return }
//            profileImg.sd_setImage(with: URL(string: data.orgimageurl), placeholderImage: Images.ic_hospital)
           
            statuslbl.text = data.docstatus
//            hospitalNamelbl.text = data.orgname
        }
    }
    
//    var assocdata: [associatedhospitalModel]?{
//        didSet{
//            guard let data = assocdata else { return }
//            docNamelbl.text = data[0].docname
//            addresslbl.text = data[0].orna_address
//        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
