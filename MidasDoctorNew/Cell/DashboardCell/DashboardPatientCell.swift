//
//  DashboardPatientCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit

class DashboardPatientCell: UITableViewCell {
    
    fileprivate lazy var backView = BackViewProperties()
    lazy var profileImg: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        return img
    }()
    
    lazy var namelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 14)
        return lbl
    }()
    
    lazy var genderlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.darkGray
        lbl.font = UIFont.medium(ofSize: 13)
        return lbl
    }()
    
    lazy var addreshlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.lightGray
        lbl.font = UIFont.medium(ofSize: 13)
        return lbl
    }()
    
    
    lazy var orgNamelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.medium(ofSize: 13)
        return lbl
    }()
    
    lazy var statusBtn: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.titleLabel?.font = UIFont.medium(ofSize: 13)
        btn.layer.cornerRadius = 15
        return btn
    }()
    
    lazy var calenderImg: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        
        let image = Images.ic_calendar_empty.withRenderingMode(.alwaysTemplate)
        img.image = image
        img.tintColor = Theme.Color.dark_Green_Color
//        self.setImage(image, for: .normal)
//        self.tintColor = Theme.Color.black

        
        
//        img.image = Images.ic_calendar_empty
        return img
    }()
    
    lazy var datelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.lightGray
        lbl.font = UIFont.medium(ofSize: 13)
        lbl.textAlignment = .right
        return lbl
    }()
    
    lazy var timelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 14)
        lbl.textAlignment = .right
        return lbl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray
        
        contentView.addSubview(backView)
        backView.addSubview(profileImg)
        backView.addSubview(namelbl)
        backView.addSubview(genderlbl)
        backView.addSubview(addreshlbl)
        
        backView.addSubview(orgNamelbl)
        backView.addSubview(statusBtn)
        backView.addSubview(calenderImg)
        backView.addSubview(datelbl)
        backView.addSubview(timelbl)
    }
    
    fileprivate func setupConstraint(){
        backView.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 90)
        
        profileImg.frame = CGRect(x: 5, y: 10, width: 40, height: 40)
        calenderImg.frame = CGRect(x: backView.frame.width - 10 - 16, y: 10, width: 16, height: 16)
        datelbl.frame = CGRect(x: backView.frame.width - 85, y: calenderImg.frame.origin.y + calenderImg.frame.height + 5, width: 75, height: 20)
        timelbl.frame = CGRect(x: backView.frame.width - 90, y: datelbl.frame.origin.y + datelbl.frame.height, width: 80, height: 20)
        statusBtn.frame = CGRect(x: datelbl.frame.origin.x - 85, y: datelbl.frame.origin.y, width: 80, height: 30)
        
        namelbl.frame = CGRect(x: profileImg.frame.origin.x + profileImg.frame.width + 5, y: 10, width: backView.frame.width - profileImg.frame.origin.x - profileImg.frame.width - 5 - 10 - 20 - 5, height: 18)
        
        genderlbl.frame = CGRect(x: namelbl.frame.origin.x, y: namelbl.frame.origin.y + namelbl.frame.height + 5, width: statusBtn.frame.origin.x - profileImg.frame.origin.x - profileImg.frame.width - 5 - 5, height: 18)
        addreshlbl.frame = CGRect(x: namelbl.frame.origin.x, y: genderlbl.frame.origin.y + genderlbl.frame.height, width: backView.frame.width - profileImg.frame.origin.x - profileImg.frame.width - datelbl.frame.width - 10 - 5, height: 18)
        orgNamelbl.frame = CGRect(x: namelbl.frame.origin.x, y: addreshlbl.frame.origin.y + addreshlbl.frame.height, width: backView.frame.width - profileImg.frame.origin.x - profileImg.frame.width - 5 - 10, height: 18)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    var data: PatientListModel?{
        didSet{
            guard let data = data else { return }
            
            namelbl.text = data.patientname
            
            let trimmed = data.gender.trimmingCharacters(in: .whitespacesAndNewlines)

            genderlbl.text = String(format: "(%@/%@)", trimmed, data.age)// trimmed + "/" + data.age
            addreshlbl.text = data.districtname
            orgNamelbl.text = data.healthpartner
            datelbl.text = data.appdatenep
            timelbl.text = data.appo_apptime
            
            statusBtn.setTitle(data.patientstatus, for: .normal)
            if data.patientstatus == "NOT READY"{
                statusBtn.backgroundColor = Theme.Color.lightGray
            }else if data.patientstatus == "Waiting" {
                statusBtn.backgroundColor = Theme.Color.dark_Green_Color
            }else if data.patientstatus == "LIVE" {
                statusBtn.backgroundColor = Theme.Color.Dark_Red_Color
            }else if data.patientstatus == "END" {
                statusBtn.backgroundColor = Theme.Color.colorLightGreen
            }else{
                statusBtn.backgroundColor = Theme.Color.colorLightBlue
            }
            
            if data.gender == "male" || data.gender == "Male" || data.gender == "male  " || data.gender == "Male  "{
                let image = Images.ic_male.withRenderingMode(.alwaysTemplate)
                profileImg.image = image
                profileImg.tintColor = Theme.Color.lightGray
//                profileImg.sd_setImage(with: URL(string: ""), placeholderImage: Images.ic_user)
            }else{
                let image = Images.ic_female.withRenderingMode(.alwaysTemplate)
                profileImg.image = image
                profileImg.tintColor = Theme.Color.lightGray
            }
            
        }
    }
    
    
}
