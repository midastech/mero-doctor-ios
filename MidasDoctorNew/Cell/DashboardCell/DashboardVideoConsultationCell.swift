//
//  DashboardVideoConsultationCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 26/03/2021.
//

import UIKit

class DashboardVideoConsultationCell: UITableViewCell {
    /*
    let myVideolbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 14)
        lbl.text = "My Video Consultation Room"
        return lbl
    }()

    let videoConsultationView: UIView = {
        let bv = UIView()
        bv.contentMode = .scaleAspectFit
        bv.clipsToBounds = true
        bv.layer.cornerRadius = 8
        bv.isUserInteractionEnabled = true
        return bv
    }()
    
    
    let videoConsultationlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.medium(ofSize: 16)
        lbl.numberOfLines = 1
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.text = "Video Consultation Room"
        return lbl
    }()
    
    let videoConsultationImage: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = false
        iv.contentMode = .scaleToFill
//        iv.image = Images.ic_video
        
        let img = Images.ic_video.withRenderingMode(.alwaysTemplate)
        iv.image = img
        iv.tintColor = Theme.Color.white

        iv.alpha = 0.3
        return iv
    }()
    */
    let myAppointmentlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.medium(ofSize: 14)
        lbl.text = "My Appointments"
        return lbl
    }()
    
    let opdAppointmentView: UIView = {
        let bv = BackViewProperties()
        bv.contentMode = .scaleAspectFit
        bv.clipsToBounds = true
        bv.layer.cornerRadius = 8
//        bv.backgroundColor = Theme.Color.colorLightBlue
        return bv
    }()
    
    let videoAppointmentView: UIView = {
        let bv = BackViewProperties()
        bv.contentMode = .scaleAspectFit
        bv.clipsToBounds = true
        bv.layer.cornerRadius = 8
//        bv.backgroundColor = Theme.Color.colorLightGreen
        return bv
    }()
        
    let opdAppointmentlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.white
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let opdAppointmentImage: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = false
        iv.contentMode = .scaleToFill
        iv.image = Images.ic_calendar
        iv.alpha = 0.08
        return iv
    }()
    
    let videoAppointmentlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.white
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let videoAppointmentImage: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = false
        iv.contentMode = .scaleToFill
        iv.image = Images.ic_calendar
        iv.alpha = 0.08
        return iv
    }()
    
    
    let gradientLayer = CAGradientLayer()
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: self.layer)
//        gradientLayer.frame = videoConsultationView.bounds
//        videoConsultationView.applyGradient(colors: [Theme.Color.Light_Orange_Color, Theme.Color.Orange_Color], gradient: .horizontal)
        
        gradientLayer.frame = opdAppointmentView.bounds
        opdAppointmentView.applyGradient(colors: [Theme.Color.colorBlue, Theme.Color.colorLightBlue], gradient: .horizontal)

        gradientLayer.frame = videoAppointmentView.bounds
        videoAppointmentView.applyGradient(colors: [Theme.Color.colorLightGreen, Theme.Color.colorLightDarkGreen], gradient: .horizontal)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray

        /*
        contentView.addSubview(myVideolbl)
        contentView.addSubview(videoConsultationView)
        videoConsultationView.addSubview(videoConsultationlbl)
        videoConsultationView.addSubview(videoConsultationImage)
*/
        
        contentView.addSubview(myAppointmentlbl)
        contentView.addSubview(opdAppointmentView)
        opdAppointmentView.addSubview(opdAppointmentImage)
        opdAppointmentView.addSubview(opdAppointmentlbl)

       
        contentView.addSubview(videoAppointmentView)
        videoAppointmentView.addSubview(videoAppointmentImage)
        videoAppointmentView.addSubview(videoAppointmentlbl)
    }
    
    fileprivate func setupConstraint(){
        /*
        myVideolbl.frame = CGRect(x: 15, y: 5, width: SCREEN.WIDTH - 30, height: 20)
        videoConsultationView.frame = CGRect(x: 10, y: myVideolbl.frame.origin.y + myVideolbl.frame.height + 5, width: SCREEN.WIDTH - 20, height: 50)
        videoConsultationlbl.frame = CGRect(x: 0, y: 0, width: videoConsultationView.frame.width, height: videoConsultationView.frame.height)
        videoConsultationImage.frame = CGRect(x: videoConsultationView.frame.width - 10 - 40, y: 5, width: 40, height: 40)
        */

        myAppointmentlbl.frame = CGRect(x: 15, y: 10, width: SCREEN.WIDTH - 30, height: 20)
//        myAppointmentlbl.frame = CGRect(x: 15, y: videoConsultationView.frame.origin.y + videoConsultationView.frame.height + 10, width: SCREEN.WIDTH - 30, height: 20)

        opdAppointmentView.frame = CGRect(x: 10, y: myAppointmentlbl.frame.origin.y + myAppointmentlbl.frame.height + 5, width: SCREEN.WIDTH / 2 - 15, height: 70)
        opdAppointmentlbl.frame = CGRect(x: 8, y: 0, width: opdAppointmentView.frame.width - 16, height: opdAppointmentView.frame.height)
        opdAppointmentImage.frame = CGRect(x: opdAppointmentView.frame.width - 20 - 40, y: 15, width: 40, height: 40)
        
        videoAppointmentView.frame = CGRect(x: opdAppointmentView.frame.origin.x + opdAppointmentView.frame.width + 5, y: opdAppointmentView.frame.origin.y, width: SCREEN.WIDTH / 2 - 15, height: 70)
        videoAppointmentlbl.frame = CGRect(x: 8, y: 0, width: videoAppointmentView.frame.width - 16, height: videoAppointmentView.frame.height)
        videoAppointmentImage.frame = CGRect(x: videoAppointmentView.frame.width - 20 - 40, y: 15, width: 40, height: 40)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
