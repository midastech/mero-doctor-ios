//
//  DashboardRevenueCell.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 26/03/2021.
//

import UIKit

class DashboardRevenueCell: UITableViewCell {
    
    lazy var gradientColor: [UIColor] = [Theme.Color.colorLightGreen, Theme.Color.colorGreen]
    
    let myRevenuelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 14)
        lbl.text = "My Revenue"
        return lbl
    }()

    lazy var revenueView: UIView = {
        let bv = UIView()
        bv.contentMode = .scaleAspectFit
        bv.clipsToBounds = true
        bv.layer.cornerRadius = 8
        bv.isUserInteractionEnabled = true
        
        
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = bv.bounds
//        gradientLayer.colors = [Theme.Color.colorLightGreen.cgColor, Theme.Color.colorGreen.cgColor]
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//
//
//        let shapeMask = CAShapeLayer()
//        shapeMask.path = bv.accessibilityPath?.cgPath
//        gradientLayer.mask = shapeMask
//
//        bv.layer.addSublayer(gradientLayer)
        return bv
    }()
    
    
    let totallbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.medium(ofSize: 14)
        lbl.text = "Total Revenue"
        return lbl
    }()
    
    let totalAmountlbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.medium(ofSize: 16)
        return lbl
    }()
    
    let datelbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.regular(ofSize: 14)
        lbl.numberOfLines = 3
        lbl.textAlignment = .center
        return lbl
    }()
    
    
//    let gradientLayer = CAGradientLayer()
//    override func layoutSublayers(of layer: CALayer) {
//        super.layoutSublayers(of: self.layer)
//        gradientLayer.frame = revenueView.bounds
//        revenueView.applyGradient(colors: gradientColor, gradient: .horizontal)
//    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        setupView()
        setupConstraint()
    }

    
    func setupView() {
        contentView.backgroundColor = Theme.Color.backgroundGray
        
        contentView.addSubview(myRevenuelbl)
        contentView.addSubview(revenueView)
        revenueView.addSubview(totallbl)
        revenueView.addSubview(totalAmountlbl)
        revenueView.addSubview(datelbl)
        
        revenueView.backgroundColor = Theme.Color.dark_Green_Color
    }
    
    fileprivate func setupConstraint(){
        myRevenuelbl.frame = CGRect(x: 15, y: 5, width: SCREEN.WIDTH - 30, height: 20)
        
        revenueView.frame = CGRect(x: 10, y: myRevenuelbl.frame.origin.y + myRevenuelbl.frame.height + 5, width: SCREEN.WIDTH - 20, height: 80)
        
        totallbl.frame = CGRect(x: 10, y: 12, width: revenueView.frame.width / 2, height: 18)
        totalAmountlbl.frame = CGRect(x: 10, y: totallbl.frame.origin.y + totallbl.frame.height + 5, width: revenueView.frame.width / 2, height: 20)
        datelbl.frame = CGRect(x: revenueView.frame.width / 2 + 10, y: 10, width: revenueView.frame.width / 2, height: revenueView.frame.height - 20)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
