//
//  storyboard+main.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 19/07/2021.
//

import Foundation

extension UIStoryboard{
private struct Constants {
    static let homeStoryboard = "Main"
    static let examinationFormIdentifier = "ExaminationFormViewController"
    static let labTestOrderIdentifier = "LabTestOrderViewController"
    static let prescrioptionDetailsIdentifier = "PrescriptionDetailsViewController"
    static let addprescrioptionIdentifier = "AddPrescriptionViewController"

    
   
    
    
}
static var homeStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.homeStoryboard, bundle: nil)
}
func instantiateExaminationFormVC() -> ExaminationFormViewController {
    guard let viewController = UIStoryboard.homeStoryboard.instantiateViewController(withIdentifier: Constants.examinationFormIdentifier) as? ExaminationFormViewController else {
        fatalError("Couldn't instantiate ExaminationFormViewController")
    }
    return viewController
}
    func instantiateLabTestOrderVC() -> LabTestOrderViewController {
        guard let viewController = UIStoryboard.homeStoryboard.instantiateViewController(withIdentifier: Constants.labTestOrderIdentifier) as? LabTestOrderViewController else {
            fatalError("Couldn't instantiate LabTestOrderViewController")
        }
        return viewController
    }
    
    func instantiatePrescriptionDetailsVC() -> PrescriptionDetailsViewController {
        guard let viewController = UIStoryboard.homeStoryboard.instantiateViewController(withIdentifier: Constants.prescrioptionDetailsIdentifier) as? PrescriptionDetailsViewController else {
            fatalError("Couldn't instantiate PrescriptionDetailsViewController")
        }
        return viewController
    }
    func instantiateAddPrescriptionVC() -> AddPrescriptionViewController {
        guard let viewController = UIStoryboard.homeStoryboard.instantiateViewController(withIdentifier: Constants.addprescrioptionIdentifier) as? AddPrescriptionViewController else {
            fatalError("Couldn't instantiate AddPrescriptionViewController")
        }
        return viewController
    }
}
