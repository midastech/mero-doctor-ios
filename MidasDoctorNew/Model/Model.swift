//
//  Model.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 25/03/2021.
//
import Foundation
import SwiftyJSON

struct HospitalListModel {
    var gdepid                        :       String
    var islivemerodoctor         :       String
    var orgid                           :       String
    var orgimageurl                :       String
    var hospitaldomain           :       String
    var address                      :       String
    var orgname                     :       String
    
    init(json: JSON) {
        self.gdepid =   json["gdepid"].string ?? ""
        self.islivemerodoctor    =   json["islivemerodoctor"].string ?? ""
        self.orgid  =   json["orgid"].string ?? ""
        self.orgimageurl    =   json["orgimageurl"].string ?? ""
        self.hospitaldomain =   json["hospitaldomain"].string ?? ""
        self.address    =   json["address"].string ?? ""
        self.orgname    =   json["orgname"].string ?? ""
    }
}

struct statusModel: Codable {
    var associatedhospital:       [associatedhospitalModel]?
    var basic: [basicModel]?
    var list: [listModel]?
}

struct associatedhospitalModel: Codable  {
    var orna_address:       String
    var docid:       String
    var ehstime:       String
    var docname:       String
    var orgid:       String
    var orgimageurl:       String
    var feeehs:       String
    var orgfullname:       String
    var generaltime:       String
    var isgovernment:       String
    var bannerimage:       String
    var feegeneral:       String
    var orgcode:       String
    
    enum CodingKeys: String, CodingKey {
           case orna_address, docid, ehstime, docname, orgid, orgimageurl, feeehs, orgfullname, generaltime, isgovernment, bannerimage, feegeneral, orgcode
        }
}

struct basicModel: Codable  {
    var ndepname:       String
    var depname:       String
    var bsdate:          String
    var addate:         String
    var depid:              String
    var gdepid:         String
    
    
    enum CodingKeys: String, CodingKey {
        case ndepname, depname, bsdate, addate, depid, gdepid
        
        }
}

struct listModel: Codable  {
   var orgname:             String
   var leavefromdate:         String
   var docstatus:         String
   var depname:         String
   var ndepname:         String
   var reason:               String
   var delaytime:         String
   var docname:         String
   var orgid:                   String
   var gdocid:              String
   var gdepid:              String
   var bsdate:              String
   var doct_docid:         String
   var time:                 String
   var statusid:                String
   var depid:               String
   var leavetodate:         String
   var addate:              String
    
    
    enum CodingKeys: String, CodingKey {
        case orgname, leavefromdate, docstatus, depname, ndepname, reason, delaytime, docname, orgid, gdocid, gdepid, bsdate, doct_docid, time, statusid, depid, leavetodate, addate
        
        
        }
}

struct PatientListModel {    
    var districtname :       String
    var appo_apptime :       String
    var midasid :       String
    var appdatenep :       String
    var appdateng :       String
    var healthpartner :       String
    var title :       String
    var appo_callstatus :       String
    var age :       String
    var queueno :       String
    var appid :       String
    var address :       String
    var mobileno :       String
    var patientid :       String
    var gender :       String
    var patientname :       String
    var patientstatus :       String
    var agetype :       String
    var isurgent :       String
    /*
    init(json: JSON) {
        self.districtname = json["districtname"].string ?? ""
        self.appo_apptime = json["appo_apptime"].string ?? ""
        self.midasid = json["midasid"].string ?? ""
        self.appdatenep = json["appdatenep"].string ?? ""
        self.appdateng = json["appdateng"].string ?? ""
        self.healthpartner = json["healthpartner"].string ?? ""
        self.title = json["title"].string ?? ""
        self.appo_callstatus = json["appo_callstatus"].string ?? ""
        self.age = json["age"].string ?? ""
        self.queueno = json["queueno"].string ?? ""
        self.appid = json["appid"].string ?? ""
        self.address = json["address"].string ?? ""
        self.mobileno = json["mobileno"].string ?? ""
        self.patientid = json["patientid"].string ?? ""
        self.gender = json["gender"].string ?? ""
        self.patientname = json["patientname"].string ?? ""
        self.patientstatus = json["patientstatus"].string ?? ""
        self.agetype = json["agetype"].string ?? ""
        self.isurgent = json["isurgent"].string ?? ""
        
    }
 */
}



struct MyRevenueModel {
    var tdsamount                        :       String
    var totalfraction         :       String
    var netfraction                           :       String
    var qty                :       String
    
//    init(json: JSON) {
//        self.tdsamount =   json["tdsamount"].string ?? ""
//        self.totalfraction    =   json["totalfraction"].string ?? ""
//        self.netfraction  =   json["netfraction"].string ?? ""
//        self.qty    =   json["qty"].string ?? ""
//    }
}

struct SlotInfoModel {
    var OPD                        :       String
    var TELEMEDICINE         :       String
}



struct RevenueModel {
    var qty                        :       String
    var servicedescription         :       String
    var totalfraction                           :       String
    var netfraction                :       String
    var tdsamount           :       String
    
    init(json: JSON) {
        self.qty =   json["qty"].string ?? ""
        self.servicedescription    =   json["servicedescription"].string ?? ""
        self.totalfraction  =   json["totalfraction"].string ?? ""
        self.netfraction    =   json["netfraction"].string ?? ""
        self.tdsamount    =   json["tdsamount"].string ?? ""
    }
}



struct RevenueDetailModel {
    var qty:                String
    var billdate:           String
    var unitamount:       String
    var netfraction:       String
    var tdsamount:       String
    var agegender:       String
    var patientid:       String
    var billno:             String
    var testname:       String
    var patientname:       String
    var totalfraction:       String
    
    init(json: JSON) {
        self.qty        =   json["qty"].string ?? ""
        self.billdate       =   json["billdate"].string ?? ""
        self.unitamount     =   json["unitamount"].string ?? ""
        self.netfraction        =   json["netfraction"].string ?? ""
        self.tdsamount      =   json["tdsamount"].string ?? ""
        self.agegender      =   json["agegender"].string ?? ""
        self.patientid      =   json["patientid"].string ?? ""
        self.billno     =   json["billno"].string ?? ""
        self.testname       =   json["testname"].string ?? ""
        self.patientname        =   json["patientname"].string ?? ""
        self.totalfraction      =   json["totalfraction"].string ?? ""
    }
}

struct OPDAppointmentModel {
//    var qty:                String
    var addedby:                String?
    var queueno:                String?
    var midasid:                String?
    var starttime:                String?
    var status:                String?
    var appdate:                String?
    var healthpartnerinfo :      Array<String>?
    var patientinfo            :     [patientinfoModel]?
    
    
    enum CodingKeys: String, CodingKey {
        case addedby = "addedby"
        case queueno = "queueno"
        case midasid = "midasid"
        case starttime = "starttime"
        case status = "status"
        case appdate = "appdate"
        case healthpartnerinfo = "healthpartnerinfo"
//        case addedby = "addedby"
//        case queueno = "queueno"
        }
    
    /*
    init(json: JSON) {
        self.addedby = json["addedby"].string ?? ""
        self.queueno = json["queueno"].string ?? ""
        self.midasid = json["midasid"].string ?? ""
        self.starttime = json["starttime"].string ?? ""
        self.status = json["status"].string ?? ""
        self.appdate = json["appdate"].string ?? ""
        self.healthpartnerinfoArray =  [String(format: "%@", json["healthpartnerinfo"].arrayValue)]
        self.patientinfoArray =  [String(format: "%@", json["patientinfo"].arrayValue)]
        
    }*/
}

struct patientinfoModel: Codable {
    var patientname : String
    var address : String
    var dobad : String
    var age : String
    var gender : String
    
    enum CodingKeys: String, CodingKey {
           case patientname, address, dobad, age, gender
        }
}


struct PrescriptionModel {
    var tmat_appdate        :       String
    var tmat_midasid        :       String
    var tmat_category       :       String
    var tmat_file               :       String
    var tmat_remark         :       String
    var tmat_queueno     :       String
    var tmat_id                 :       String
    
    init(json: JSON) {
        self.tmat_appdate = json["tmat_appdate"].string ?? ""
        self.tmat_midasid = json["tmat_midasid"].string ?? ""
        self.tmat_category = json["tmat_category"].string ?? ""
        self.tmat_file = json["tmat_file"].string ?? ""
        self.tmat_remark = json["tmat_remark"].string ?? ""
        self.tmat_queueno = json["tmat_queueno"].string ?? ""
        self.tmat_id = json["tmat_id"].string ?? ""
        
    }
}
