//
//  PrimaryActionGreenButton.swift
//  MeroDoctorNew
//
//  Created by Ramesh Prajapati on 05/05/2021.
//

import UIKit

class PrimaryActionGreenButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
//    convenience init() {
//        <#statements#>
//    }
    
    
    func setupViews() {
        self.backgroundColor = Theme.Color.dark_Green_Color
        self.setTitleColor(Theme.Color.white, for: .normal)
        self.titleLabel?.font = UIFont.medium(ofSize: 16)// UIFont.systemFont(ofSize: 16, weight: .medium)
        self.layer.cornerRadius = 8

        self.layer.masksToBounds = false
        self.layer.shadowColor = Theme.Color.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 4
    }
    
    /*
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 4.0
    private var fillColor: UIColor = Theme.Color.SECONDARY_COLOR // the color applied to the shadowLayer, rather than the view's backgroundColor

    
    override func layoutSubviews() {
        super.layoutSubviews()

        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
          
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor

            shadowLayer.shadowColor = Theme.Color.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 1.0
            shadowLayer.shadowRadius = 6

            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    */
}
