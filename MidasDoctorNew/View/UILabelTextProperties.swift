//
//  File.swift
//  MeroDoctorNew
//
//  Created by ramesh prajapati on 2/9/21.
//

import UIKit

class UILabelTextProperties: UILabel {
    var textcolor: UIColor = .black
    var textfont: UIFont = UIFont.medium(ofSize: 15)
    var textalignment: NSTextAlignment = .left
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
        
    
    private func setupView() {
        //    self.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        //    self.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        self.textColor = textcolor
        self.font = textfont
        self.textAlignment = textalignment
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        self.sizeToFit()
    }
    
    
}
