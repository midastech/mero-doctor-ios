//
//  DropDownButtonProperty.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 04/04/2021.
//

import UIKit

class DropDownButtonProperty: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        self.clipsToBounds = true
        let img = Images.ic_downfill.withRenderingMode(.alwaysOriginal)
        self.setImage(img, for: .normal)
        self.tintColor = Theme.Color.darkGray
    }
}
