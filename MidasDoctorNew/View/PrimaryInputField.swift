//
//  PrimaryInputField.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 23/03/2021.
//

import UIKit

class PrimaryInputField: UITextField {
    var rectContainer: CGRect?
    let padding: UIEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    func setupViews() {
        
        self.layer.borderColor = Theme.Color.textFieldBorder.cgColor
        self.layer.borderWidth = 1
        
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        self.backgroundColor = UIColor.white
        self.font = UIFont.systemFont(ofSize: 16)
    }
}
