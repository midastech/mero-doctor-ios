//
//  SecondaryActionButton.swift
//  MeroDoctorNew
//
//  Created by ramesh prajapati on 2/10/21.
//

import UIKit

class SecondaryActionButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        self.backgroundColor = .white
        self.setTitleColor(.black, for: .normal)
        self.titleLabel?.font = UIFont.medium(ofSize: 15)
        self.layer.cornerRadius = 4

        self.layer.masksToBounds = false
        self.layer.shadowColor = Theme.Color.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0.8, height: 0.8)
        self.layer.shadowRadius = 2.5
    }
}
