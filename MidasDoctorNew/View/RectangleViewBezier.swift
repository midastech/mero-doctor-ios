//
//  RectangleViewBezier.swift
//  CoreGraphic
//
//  Created by ramesh prajapati on 7/1/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import UIKit

class RectangleViewBezier: UIView {

    var path: UIBezierPath!
    var fillColor: UIColor = .blue
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX, y: rect.maxY - 10))
        path.addQuadCurve(to: CGPoint(x: rect.midX, y: rect.maxY / 1.5), controlPoint: CGPoint(x: rect.midX / 2, y: (rect.midY / 2) + 100))
        path.addQuadCurve(to: CGPoint(x: rect.maxX, y: rect.minY), controlPoint: CGPoint(x: rect.maxX / 1.2, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        
        path.close()
        pathGradient(colors: [Theme.Color.Orange_Color, Theme.Color.Black_Orange_Color], gradient: .horizontal)
        path.fill()
    }
    
    func pathGradient(colors: [UIColor], gradient orientation: GradientOrientation){
//        let shape = CAShapeLayer()
//        shape.path = path.cgPath
//        shape.lineWidth = 2.0
//        shape.strokeColor = UIColor.black.cgColor
//        self.layer.addSublayer(shape)

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = path.bounds
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.startPoint = orientation.startPoint
        gradientLayer.endPoint = orientation.endPoint

        let shapeMask = CAShapeLayer()
        shapeMask.path = path.cgPath
        gradientLayer.mask = shapeMask

        self.layer.addSublayer(gradientLayer)
    }
}
