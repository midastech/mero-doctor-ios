//
//  StatusButtonProperties.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 01/04/2021.
//

import UIKit

class StatusButtonProperties: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        self.backgroundColor = Theme.Color.white
        self.setTitleColor(Theme.Color.black, for: .normal)
        self.titleLabel?.font = UIFont.medium(ofSize: 15)
        self.layer.cornerRadius = 20

        self.layer.masksToBounds = false
        self.layer.shadowColor = Theme.Color.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0.8, height: 0.8)
        self.layer.shadowRadius = 2.5
    }
}
