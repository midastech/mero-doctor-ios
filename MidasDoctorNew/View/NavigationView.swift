//
//  NavigationView.swift
//  MidaseClass
//
//  Created by ramesh prajapati on 11/10/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import UIKit

class NavigationView: UIView {
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //common func to init our view
    private func setupView() {
        
        backgroundColor = Theme.Color.backgroundGray
/*
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0.0, y: 54.0, width: SCREEN.WIDTH, height: 1.0)
        // Add a bottomBorder.
        topBorder.backgroundColor = Theme.Color.lightGray.cgColor
        self.layer.addSublayer(topBorder)
 */
    }
}
