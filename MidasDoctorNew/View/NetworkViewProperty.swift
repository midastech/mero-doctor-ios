//
//  NetworkViewProperty.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 03/04/2021.
//

import UIKit

protocol RefreshControllerDelegate: class {
    func refreshBtnPress(sender: UIButton)
}


class NetworkViewProperty: UIView {
    
    weak var delegate: RefreshControllerDelegate?

    //initWithFrame to init view from code
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupView()
//    }
//    
//    //initWithCode to init view from xib or storyboard
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    
    convenience init(frame: CGRect, buttonTitle: [String]) {
        self.init(frame: frame)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        
        self.backgroundColor = .clear

        self.addSubview(networkImage)
        self.addSubview(noInternetlbl)
        self.addSubview(pleaselbl)
        self.addSubview(refreshBtn)
        
//        configStackView()
        networkImage.frame = CGRect(x: self.frame.width / 2 - 50, y: 0, width: 100, height: 80)
        noInternetlbl.frame = CGRect(x: 0, y: networkImage.frame.origin.y + networkImage.frame.height, width: self.frame.width, height: 20)
//        networkImage.frame = CGRect(x: self.frame.width / 2 - 50, y: noInternetlbl.frame.origin.y - 100, width: 100, height: 100)
        pleaselbl.frame = CGRect(x: 0, y: noInternetlbl.frame.origin.y + noInternetlbl.frame.height + 5, width: self.frame.width, height: 38)
        refreshBtn.frame = CGRect(x: (self.frame.width / 2) - 50, y: pleaselbl.frame.origin.y + pleaselbl.frame.height + 10, width: 100, height: 40)
        refreshBtn.layer.cornerRadius = 8
    }
    
    private func configStackView(){
        let stack = UIStackView(arrangedSubviews: [networkImage, noInternetlbl, pleaselbl, refreshBtn])
        stack.axis = .vertical
        stack.alignment = .center
//        stack.distribution = .fillEqually
        stack.spacing = 10
        addSubview(stack)
        
        stack.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
//        stack.translatesAutoresizingMaskIntoConstraints = false
//        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
//        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
//        stack.layer.cornerRadius = 8
    }
    
    
    private lazy var networkImage: UIImageView = {
        let img = UIImageView()
        img.image = Images.ic_no_internet
        return img
    }()
    
    private let noInternetlbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "No Internet!"
        lbl.textAlignment = .center
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 17)
        return lbl
    }()
    private let pleaselbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Please check your internet\nconnection."
        lbl.numberOfLines = 2
        lbl.textAlignment = .center
        lbl.textColor = Theme.Color.darkGray
        lbl.font = UIFont.medium(ofSize: 14)
        return lbl
    }()
    private var refreshBtn: SecondaryActionButton = {
        let btn = SecondaryActionButton()
        btn.setTitle("Refresh", for: .normal)
        btn.backgroundColor = Theme.Color.colorBlue
        btn.setTitleColor(.black, for: .normal)
        btn.addTarget(self, action: #selector(NetworkViewProperty.buttonAction(sender:)), for: .touchUpInside)

        return btn
    }()
    
    @objc func buttonAction(sender: UIButton){
        
        delegate?.refreshBtnPress(sender: sender)

//        for (buttonIndex, btn) in buttons.enumerated() {
//            btn.setTitleColor(textColor, for: .normal)
//            btn.backgroundColor =  selectorTextColor
//            if btn == sender {
//                let selectorPosition = frame.width / CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
//                delegate?.changeToIndex(index: buttonIndex)
//                UIView.animate(withDuration: 0.3) {
//                    self.selectorView.frame.origin.x = selectorPosition
//                }
//                btn.setTitleColor(selectorTextColor, for: .normal)
//                btn.backgroundColor = selectorViewColor
//            }
//        }
    }
    
}


