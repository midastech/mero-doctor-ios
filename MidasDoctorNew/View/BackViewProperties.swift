//
//  BackViewProperties.swift
//  MiDasHMIS
//
//  Created by ramesh prajapati on 12/23/19.
//  Copyright © 2019 Midas. All rights reserved.
//

import UIKit

class BackViewProperties: UIView {
    //initWithFrame to init view from code
    
    let containerView = UIView()
    var cornerRadius: CGFloat = 4.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //common func to init our view
    private func setupView() {
        
        self.backgroundColor = Theme.Color.white
        layer.cornerRadius = cornerRadius
        
        // set the shadow of the view's layer
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 1.0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 2.0
        
        // set the cornerRadius of the containerView's layer
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.masksToBounds = true
        
        addSubview(containerView)
        
        // add constraints
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        // pin the containerView to the edges to the view
        containerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        
    }
}
