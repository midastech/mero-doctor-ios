//
//  CustomUITextView.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 15/05/2021.
//

import Foundation

class CustomUITextView: UITextView {
   
    lazy var placeholderLabel: UILabel = {
    let label = UILabel()
        label.textColor = UIColor(white: 0.5, alpha: 0.85)
        label.backgroundColor = .clear
    return label
      }()
    override init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
    setupNotificationObserver()
      }

      required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupNotificationObserver()
      }

      deinit {
        NotificationCenter.default.removeObserver(self)
      }

      private func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChangeHandler(notification:)),
                                               name: UITextView.textDidChangeNotification,
                                               object: self)
      }

      override func layoutSubviews() {
        super.layoutSubviews()

        if text.isEmpty {
          placeholderLabel.copyProperties(from: self)
          addSubview(placeholderLabel)
          bringSubviewToFront(placeholderLabel)
        } else {
          placeholderLabel.removeFromSuperview()
        }
      }

      @objc func textDidChangeHandler(notification: Notification) {
        setNeedsLayout()
      }
    }

    private extension UILabel {
      /// Copies common properties from UITextView. You can add more.
      func copyProperties(from textView: UITextView) {
        frame = textView.bounds.inset(by: textView.textContainerInset)
        lineBreakMode = textView.textContainer.lineBreakMode
        textAlignment = textView.textAlignment
        numberOfLines = textView.textContainer.maximumNumberOfLines
//        backgroundColor = .white
      }
    }
