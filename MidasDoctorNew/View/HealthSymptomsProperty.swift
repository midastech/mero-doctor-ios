//
//  HealthSymptomsProperty.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 04/04/2021.
//

import UIKit

class HealthSymptomsProperty: UIView, UIGestureRecognizerDelegate {
    
    weak var delegate: HealthSymptomsDelegate?
    private var titleText: String!
    private var btnTag: Int!

    convenience init(frame: CGRect, buttonTitle: String, btnTag: Int) {
        self.init(frame: frame)
        self.titleText = buttonTitle
        self.btnTag = btnTag
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        
        self.backgroundColor = .clear

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self

        self.addGestureRecognizer(tapGesture)

        
        
        self.addSubview(titlelbl)
        self.addSubview(checkBtn)
        
        titlelbl.text = titleText
        checkBtn.tag = btnTag
//        titlelbl.tag = btnTag
        self.tag = btnTag
        
        checkBtn.frame = CGRect(x: 5, y: 10, width: 20, height: 20)
        titlelbl.frame = CGRect(x: checkBtn.frame.origin.y + checkBtn.frame.width + 5, y: 0, width: self.frame.width - checkBtn.frame.origin.y - checkBtn.frame.width - 5 - 5, height: 40)
        checkBtn.layer.cornerRadius = 4
    }

    
    lazy var titlelbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = Theme.Color.darkGray
        lbl.font = UIFont.medium(ofSize: 13)
        lbl.isUserInteractionEnabled = true
        lbl.numberOfLines = 2
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(NetworkViewProperty.buttonAction(sender:)))
//        tapGesture.numberOfTapsRequired = 1
//        lbl.addGestureRecognizer(tapGesture)

        
        return lbl
    }()
    
    private var checkBtn: UIButton = {
        let btn = UIButton()

        let image = Images.TICK_UNSELECTED.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray

        btn.addTarget(self, action: #selector(NetworkViewProperty.buttonAction(sender:)), for: .touchUpInside)

        return btn
    }()
    
    @objc func buttonAction(sender: UIButton){
        
        if (checkBtn.image(for: .normal) == Images.TICK_SELECTED) {
            checkBtn.setImage(Images.TICK_UNSELECTED, for: .normal)
        }else{
            checkBtn.setImage(Images.TICK_SELECTED, for: .normal)
        }
        delegate?.symptomsBtnPress(index: sender.tag)
    }
    
    
    @objc func viewTap(sender : UITapGestureRecognizer) {
        
        
        if (checkBtn.image(for: .normal) == Images.TICK_SELECTED) {
            checkBtn.setImage(Images.TICK_UNSELECTED, for: .normal)
        }else{
            checkBtn.setImage(Images.TICK_SELECTED, for: .normal)
        }

        
        
        let tag = sender.view!.tag
        delegate?.symptomsBtnPress(index: tag)
    }
}

protocol HealthSymptomsDelegate: class {
    func symptomsBtnPress(index: Int)
}
