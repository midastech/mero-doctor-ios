//
//  EmptyPatientListProperty.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 03/04/2021.
//

import UIKit

class EmptyPatientListProperty: UIView {
    
    weak var delegate: RefreshPatientDelegate?
    private var titleString:String!
    private var subtitileString:String!

    convenience init(frame: CGRect, titleStr: String, subTitleStr: String) {
        self.init(frame: frame)
        titleString = titleStr
        subtitileString = subTitleStr
        
//        setupView()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        
        self.backgroundColor = .clear
        noInternetlbl.text = titleString
        pleaselbl.text = subtitileString
        
        self.addSubview(networkImage)
        self.addSubview(noInternetlbl)
        self.addSubview(pleaselbl)
        self.addSubview(refreshBtn)
        
        networkImage.frame = CGRect(x: self.frame.width / 2 - 75, y: 10, width: 150, height: 90)
        noInternetlbl.frame = CGRect(x: 10, y: networkImage.frame.origin.y + networkImage.frame.height + 10, width: self.frame.width - 20, height: 20)
        pleaselbl.frame = CGRect(x: 10, y: noInternetlbl.frame.origin.y + noInternetlbl.frame.height + 5, width: self.frame.width - 20, height: 34)
        refreshBtn.frame = CGRect(x: self.frame.width / 2 - 50, y: pleaselbl.frame.origin.y + pleaselbl.frame.height + 15, width: 100, height: 40)
        refreshBtn.layer.cornerRadius = 5
        
    }
    
    lazy var networkImage: UIImageView = {
        let img = UIImageView()
//        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = Images.ic_empty_patient
        return img
    }()

    lazy var noInternetlbl: UILabel = {
        let lbl = UILabel()
//        lbl.translatesAutoresizingMaskIntoConstraints = false
       // lbl.text = "Oops! Patient list is empty."
        lbl.textAlignment = .center
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 16)
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        return lbl
    }()
    lazy var pleaselbl: UILabel = {
        let lbl = UILabel()
//        lbl.translatesAutoresizingMaskIntoConstraints = false
        //lbl.text = "Looks like you do not have\nany patients in your list yet."
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        lbl.textAlignment = .center
        lbl.textColor = Theme.Color.lightGray
        lbl.font = UIFont.medium(ofSize: 13)
        return lbl
    }()
    lazy var refreshBtn: SecondaryActionButton = {
        let btn = SecondaryActionButton()
//        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Refresh", for: .normal)
        btn.backgroundColor = Theme.Color.colorLightBlue
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(NetworkViewProperty.buttonAction(sender:)), for: .touchUpInside)

        return btn
    }()
    
    @objc func buttonAction(sender: UIButton){
        delegate?.refreshBtnPress(sender: sender)
    }
    
}

protocol RefreshPatientDelegate: class {
    func refreshBtnPress(sender: UIButton)
}
