//
//  CustomSegmentedControl.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 29/03/2021.
//

import UIKit

class CustomSegmentedControl: UIView {

    private var buttonTitles:[String]!
    private var buttons: [UIButton] = []
    private var selectorView: UIView!
    
    var textColor: UIColor = .black
    var selectorViewColor: UIColor = Theme.Color.dark_Green_Color
    var selectorTextColor: UIColor = Theme.Color.white
    weak var delegate: CustomSegmentedControlDelegate?
    private var _seletedIndex: Int = 0
    public var selectedIndex: Int {
        return _seletedIndex
    }
    
    private func updateView(){
        createButton()
        configSelectorView()
        configStackView()
    }
    
    
    private func configStackView(){
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 8
        stack.backgroundColor = Theme.Color.backgroundGray
        self.clipsToBounds = true
        self.layer.cornerRadius = 8
    }

    private func configSelectorView(){
        let selectorWidth = frame.width / CGFloat(self.buttonTitles.count)
        
        selectorView = UIView(frame: CGRect(x: 0, y: self.frame.height, width: selectorWidth, height: 2))
        selectorView.backgroundColor = Theme.Color.backgroundGray
        addSubview(selectorView)
    }
    
    private func createButton(){
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach ({$0.removeFromSuperview()})
        
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.addTarget(self, action: #selector(CustomSegmentedControl.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            button.backgroundColor =  selectorTextColor
            button.clipsToBounds = true
            buttons.append(button)
            
        }
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        buttons[0].backgroundColor = selectorViewColor
        
//        buttons[0].roundedButton(round: [.topLeft, .bottomLeft])
//        buttons[2].roundedButton(round: [.topRight, .bottomRight])

    }
    
    @objc func buttonAction(sender: UIButton){
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            btn.backgroundColor =  selectorTextColor
            if btn == sender {
                let selectorPosition = frame.width / CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
                delegate?.changeToIndex(index: buttonIndex)
                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
                btn.backgroundColor = selectorViewColor
            }
        }
    }
    
    convenience init(frame: CGRect, buttonTitle: [String]) {
        self.init(frame: frame)
        self.buttonTitles = buttonTitle
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateView()
    }
    
    func setButtonTitles(buttonTitles: [String]){
        self.buttonTitles = buttonTitles
        updateView()
    }
    
    func setIndex(index: Int){
        buttons.forEach({ $0.setTitleColor(textColor, for: .normal)})
        let button = buttons[index]
        _seletedIndex = index
        
        button.titleLabel?.backgroundColor = selectorViewColor
        button.setTitleColor(selectorTextColor, for: .normal)
        let selectorPosition = frame.width / CGFloat(buttonTitles.count) * CGFloat(index)
        UIView.animate(withDuration: 0.2) {
            self.selectorView.frame.origin.x = selectorPosition
        }
        
    }
}

protocol CustomSegmentedControlDelegate: class {
    func changeToIndex(index: Int)
}
