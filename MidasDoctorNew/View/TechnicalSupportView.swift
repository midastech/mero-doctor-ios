//
//  TechnicalSupportView.swift
//  MidaseClass
//
//  Created by ramesh prajapati on 7/28/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import UIKit

protocol TechnicalSupportDelegate: class {
    func callBtnPress(callNumber: [String])
}

class TechnicalSupportView: UIView {
    weak var delegate: TechnicalSupportDelegate?
    
    @objc func callblPress(){
        delegate?.callBtnPress(callNumber: ["01-4002658", "9847393153", "9847396536"])
    }
        
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupNib()
  }
  

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  

  private func setupNib() {
    self.backgroundColor = .clear //Theme.Color.WHITE_DISABLE_COLOR
    
    self.addSubview(techlbl)
    
    techlbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    techlbl.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(callblPress))
    techlbl.addGestureRecognizer(tapGesture)

  }
    
    fileprivate let techlbl: UILabel = {
        
        let lbl = UILabel()
        lbl.text = "Technical Support (7 AM to 9 PM)\n01-4002658, 9847393153, 9847396536"
        lbl.textAlignment = .center
        lbl.textColor = Theme.Color.darkGray
        lbl.font = UIFont.medium(ofSize: 14)
        lbl.numberOfLines = 2
        lbl.lineBreakMode = .byWordWrapping
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.isUserInteractionEnabled = true
//        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callblPress)))

        return lbl
    }()
}

