//
//  CustomUILable.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 15/05/2021.
//

import Foundation

class CustomUILable: UILabel {
    
    private var textString: String!
    private var textcolor = Theme.Color.black
    private var textfont = UIFont.medium(ofSize: 15)

    private func setupView() {
        self.text = textString
        self.textColor = textcolor
        self.font = textfont
    }

    convenience init(titleStr: String, textcolor: UIColor, textfont: UIFont) {
        self.init(frame: .zero)
        self.textString = titleStr
        self.textcolor = textcolor
        self.textfont = textfont
        setupView()
    }
    
    override init(frame: CGRect) {
      super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
    
    
  }
