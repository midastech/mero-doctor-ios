//
//  PrimaryActionButton.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 05/04/2021.
//

import UIKit

class PrimaryActionButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        self.backgroundColor = Theme.Color.colorBlue
        self.setTitleColor(Theme.Color.white, for: .normal)
        self.titleLabel?.font = UIFont.medium(ofSize: 15)
        self.layer.cornerRadius = 6

        self.layer.masksToBounds = false
        self.layer.shadowColor = Theme.Color.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0.8, height: 0.8)
        self.layer.shadowRadius = 2.5
    }
}
