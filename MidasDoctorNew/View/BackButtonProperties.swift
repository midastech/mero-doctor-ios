//
//  BackButtonProperties.swift
//  MeroDoctorNew
//
//  Created by ramesh prajapati on 2/12/21.
//


import UIKit

class BackButtonProperties: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        let image = Images.Back_Icon.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.tintColor = Theme.Color.black
    }
}
