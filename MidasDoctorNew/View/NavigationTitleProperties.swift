//
//  NavigationTitleProperties.swift
//  MidaseClass
//
//  Created by ramesh prajapati on 11/11/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import UIKit

class NavigationTitleProperties: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    private func setupView() {
        //    self.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        //    self.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        self.textColor = .black
        self.font = UIFont.medium(ofSize: 16)
        self.textAlignment = .center
    }
    
    
}
