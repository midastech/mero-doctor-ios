//
//  AppDelegate.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 23/03/2021.
//

import UIKit
import SDWebImage
import MobileRTC
import IQKeyboardManagerSwift
import Firebase
import GoogleSignIn


@main
class AppDelegate: UIResponder, UIApplicationDelegate, MobileRTCAuthDelegate {
    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {        
        UIApplication.shared.statusBarView?.backgroundColor = Theme.Color.colorBlue
        IQKeyboardManager.shared.enable = true

       //aslkdjal;skdfj
       
     

//        GIDSignIn.sharedInstance.clientID = "1085451087324-pc77fmlpca4c5k5f80gigvq7gc3oj82u.apps.googleusercontent.com"
        self.initAppFlow()
        initZoom()
        
        return true
    }
    
    func initAppFlow(){
        if defaults.isUserLoggedIn()! {
            initLoggedInFlow()
        } else {
            initLoggedOutFlow()
        }
    }
    
    func initLoggedInFlow(){
        let vc = UINavigationController(rootViewController: DashboardVC())//OPDAppointmentVC
        window?.rootViewController = vc
    }
    
    func initLoggedOutFlow(){
        //        let mainVC = UIStoryboard(name: "Main", bundle: nil) .instantiateViewController(withIdentifier: "LoginVC")
        //        self.window?.rootViewController = mainVC
        
        let vc = UINavigationController(rootViewController: LoginVC())
        self.window?.rootViewController = vc
    }
    
    func initZoom(){
        let mainSDK = MobileRTCSDKInitContext()
        mainSDK.domain = "zoom.us"
        
        mainSDK.enableLog = true
        let sdkInitSuc = MobileRTC.shared().initialize(mainSDK)
        if sdkInitSuc {
            let authService = MobileRTC.shared().getAuthService()
            if let authService = authService {
                print(MobileRTC.shared().mobileRTCVersion)
                authService.delegate        = self
                authService.clientKey       = "51xD002m517Li5sUi03KTBEQdfBLgPlA2pmB" //videoConsultationStruct.apikey //sdkKey
                authService.clientSecret    = "9Jyl3rVWXbvtRKEGEwKms43qXXmOzbgOBgEg" //videoConsultationStruct.apisecret //sdkSecret
                authService.sdkAuth()
            }
            
        }
        //        MobileRTC.shared().initialize(mainSDK)
        //        let authService = MobileRTC.shared().getAuthService()
        //        print(MobileRTC.shared().mobileRTCVersion)
        //        authService?.delegate        = self
        //        authService?.clientKey       = "Your Client Key" //sdkKey
        //        authService?.clientSecret    = "Your Client Secret Key" //sdkSecret
        //        authService?.sdkAuth()
        
    }
    
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        //        print(returnValue)
        //        if returnValue != MobileRTCAuthError.success{ //MobileRTCAuthError_Success{
        //           let msg = "SDK authentication failed, error code: \(returnValue)"
        //            print("msg \(msg)")
        //         }
        
        switch returnValue {
        case MobileRTCAuthError.success:
            print("SDK successfully initialized.")
        case MobileRTCAuthError.keyOrSecretEmpty:// MobileRTCAuthError_KeyOrSecretEmpty:
            print("SDK key/secret was not provided. Replace sdkKey and sdkSecret at the top of this file with your SDK key/secret.")
        case MobileRTCAuthError.keyOrSecretWrong:// MobileRTCAuthError_KeyOrSecretWrong:
            print("SDK key/secret is not valid.")
        case MobileRTCAuthError.unknown:// MobileRTCAuthError_Unknown:
            print("SDK key/secret is not valid.")
        default:
            print("SDK Authorization failed with MobileRTCAuthError: \(returnValue)")
        }
        
    }
}

