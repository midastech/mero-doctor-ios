//
//  LoginVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 23/03/2021.
//

import UIKit

class LoginVC: BaseViewController {
    
    private var isPasswordShown: Bool = false
    
    
    
    fileprivate lazy var scrollView = SPKeyBoardAvoiding()
    let orgImg: UIImageView = {
        let img = UIImageView()
        img.image = Images.meroDoctor_icon
        return img
    }()
    
    fileprivate lazy var servicelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "24/7 Online Health Services"
        lbl.font = UIFont.medium(ofSize: 13)
        lbl.textColor = Theme.Color.Dark_Red_Color
        lbl.textAlignment = .center
        return lbl
    }()
    
    let loginlbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Log In as Doctor"
        lbl.font = UIFont.bold(ofSize: 19)
        lbl.textColor = Theme.Color.black
        return lbl
    }()
    let pleaselbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Please enter mobile number and password to log in."
        lbl.font = UIFont.medium(ofSize: 13)
        lbl.textColor = Theme.Color.darkGray
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let backView = BackViewProperties()
    
    let usernameBtn: UIButton = {
        let btn = UIButton()
        
        let image = Images.single_user.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.colorGray
        return btn
    }()
    
    fileprivate lazy var usernameField: UITextField = {
        let txt = UITextField()
        txt.attributedPlaceholder = NSAttributedString(string: "User name",
                                                       attributes: [NSAttributedString.Key.foregroundColor: Theme.Color.lightGray])
        
//        txt.text = "9843569096"
        txt.borderStyle = .none
        txt.textColor = Theme.Color.black
        txt.tag = 1
        txt.delegate = self
        txt.keyboardType = .phonePad
        txt.returnKeyType = .next
        return txt
    }()
    
    let lineView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.colorLightWhite
        return v
    }()
    
    
    let passwordBtn: UIButton = {
        let btn = UIButton()
        let image = Images.ic_lock.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.colorGray
        return btn
    }()
    
    let passwordField: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Password"
        
        txt.attributedPlaceholder = NSAttributedString(string: "Password",
                                                       attributes: [NSAttributedString.Key.foregroundColor: Theme.Color.lightGray])
        
        
        txt.borderStyle = .none
        txt.textColor = Theme.Color.black
        txt.tag = 2
        txt.returnKeyType = .next
        return txt
    }()
    
    fileprivate lazy var togglePasswordButton: UIButton = {
        let btn = UIButton()
        return btn
    }()
    /*
    fileprivate lazy var faceIDBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Tap to login with face ID or fringer print. ", for: .normal)
        btn.setTitleColor(Theme.Color.lightGray, for: .normal)
        btn.addTarget(self, action: #selector(onLoginButtonTap), for: .touchUpInside)
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 8
        btn.titleLabel?.font = UIFont.semibold(ofSize: 15)
        
        let image = Images.ic_next.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.white
        
        btn.semanticContentAttribute = .forceRightToLeft //UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        return btn
    }()
    */
    fileprivate lazy var loginBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("LOGIN ", for: .normal)
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.addTarget(self, action: #selector(onLoginButtonTap), for: .touchUpInside)
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 8
        btn.titleLabel?.font = UIFont.semibold(ofSize: 15)
        
        let image = Images.ic_next.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.white
        
        btn.semanticContentAttribute = .forceRightToLeft //UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        return btn
    }()
    
    let technicalSupportView = TechnicalSupportView.init()

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarView?.backgroundColor = Theme.Color.dark_Green_Color
        setupView()
        setupConstraint()
    }
    
    fileprivate func setupView(){
        view.backgroundColor = Theme.Color.colorLightWhite
        
        view.addSubview(scrollView)
        scrollView.addSubview(orgImg)
        scrollView.addSubview(servicelbl)
        scrollView.addSubview(loginlbl)
        scrollView.addSubview(pleaselbl)
        
        scrollView.addSubview(backView)
        backView.addSubview(usernameBtn)
        backView.addSubview(usernameField)
        backView.addSubview(lineView)
        
        backView.addSubview(passwordBtn)
        backView.addSubview(passwordField)
        
        backView.addSubview(togglePasswordButton)
        scrollView.addSubview(loginBtn)
        
        
        
        isPasswordShown = false
        setupPasswordField()
        togglePasswordButton.addTarget(self, action: #selector(togglePasswordVisibility), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onViewTap))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func setupConstraint(){
        scrollView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - SCREEN.statusBarHeight)
        orgImg.frame = CGRect(x: scrollView.frame.width / 2 - 80, y: 80, width: 150, height: 50)
        
        servicelbl.frame = CGRect(x: 25, y: orgImg.frame.origin.y + orgImg.frame.height , width: scrollView.frame.size.width - 50, height: 20)

        
        
        loginlbl.frame = CGRect(x: 15, y: servicelbl.frame.origin.y + servicelbl.frame.height + 30, width: scrollView.frame.width - 30, height: 21)
        pleaselbl.frame = CGRect(x: 15, y: loginlbl.frame.origin.y + loginlbl.frame.height + 5, width: scrollView.frame.width - 30, height: 36)
        
        backView.frame = CGRect(x: 15, y: pleaselbl.frame.origin.y + pleaselbl.frame.height + 20, width: scrollView.frame.width - 30, height: 90)
        usernameBtn.frame = CGRect(x: 5, y: 15, width: 20, height: 20)
        usernameField.frame = CGRect(x: usernameBtn.frame.origin.x + usernameBtn.frame.width + 5, y: usernameBtn.frame.origin.y - 5, width: backView.frame.width - usernameBtn.frame.origin.x - usernameBtn.frame.width - 5 - 10, height: 30)
        
        lineView.frame = CGRect(x: 5, y: usernameBtn.frame.origin.y + usernameBtn.frame.height + 10, width: backView.frame.width - 10, height: 1)
        
        passwordBtn.frame = CGRect(x: 5, y: lineView.frame.origin.y + lineView.frame.height + 10, width: 20, height: 20)
        togglePasswordButton.frame = CGRect(x: backView.frame.width - 30, y: passwordBtn.frame.origin.y, width: 20, height: 20)
        passwordField.frame = CGRect(x: passwordBtn.frame.origin.x + passwordBtn.frame.width + 5, y: passwordBtn.frame.origin.y - 5, width: backView.frame.width - passwordBtn.frame.origin.x - passwordBtn.frame.width - 5 - togglePasswordButton.frame.width - 10, height: 30)
        
        loginBtn.frame = CGRect(x: scrollView.frame.width - 15 - 120, y: backView.frame.origin.y + backView.frame.height + 30, width: 120, height: 44)

        
        loginBtn.applyGradient(colors: [Theme.Color.Light_Orange_Color, Theme.Color.Orange_Color], gradient: .horizontal)
        
        let av = RectangleViewBezier(frame: CGRect(x: 0, y: view.frame.height - 130, width: view.frame.width, height: 130))
        av.backgroundColor = .clear

        view.addSubview(av)
        
        view.addSubview(technicalSupportView)
        technicalSupportView.frame = CGRect(x: 0, y: view.frame.height - 60, width: SCREEN.WIDTH, height: 60)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func setupPasswordField() {
        if isPasswordShown {
            passwordField.isSecureTextEntry = false
            let image = Images.pwdhide.withRenderingMode(.alwaysTemplate)
            togglePasswordButton.setImage(image, for: .normal)
            togglePasswordButton.tintColor = Theme.Color.black
        } else{
            passwordField.isSecureTextEntry = true
            let image = Images.pwdShow.withRenderingMode(.alwaysTemplate)
            togglePasswordButton.setImage(image, for: .normal)
            togglePasswordButton.tintColor = Theme.Color.darkGray
        }
    }
    
    @objc func togglePasswordVisibility() {
        self.isPasswordShown = !self.isPasswordShown
        setupPasswordField()
    }
    
    @objc func onViewTap() {
        scrollView.setContentOffset(CGPoint.zero, animated: true)
        self.view.endEditing(true)
    }
    
    // MARK:- Login Button Actions
    
    @objc func onLoginButtonTap() {
        onViewTap()
        checkValidation()
    }
    
    func checkValidation(){
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()

        if usernameField.text?.isBlank == true {
            //Empty
            self.toastMessage(message: String.MOBILE_NUMBER_CANNOT_BE_EMPTY, toastType: .message)
            usernameField.becomeFirstResponder()
        }
        else if usernameField.text!.count < 10{
            self.toastMessage(message: String.INVALID_MOBILE_NUMBER, toastType: .message)
        }
        else if passwordField.text?.isBlank == true{
            self.toastMessage(message: String.PASSWORD_CANNOT_BE_EMPTY, toastType: .message)
            passwordField.becomeFirstResponder()
        }else{
            checkNetworkConnection(username: usernameField.text ?? "", password: passwordField.text ?? "")
        }
    }
    
    func  checkNetworkConnection(username: String, password: String){
        //9851031128  midas1234
        //9843569096  123456  test doctor
        
        if Reachability.isConnectedToNetwork(){
            self.activityIndicatorBegin()
            self.apiCall(username: username, password: password)
        }else{
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    //Test Doctor
    //9843569096  123456
    func apiCall(username: String, password: String){
        ApiManager.sendRequest(toApi: Api.Endpoint.login(username: usernameField.text ?? "", password: passwordField.text ?? "")) { (status, data) in
            self.activityIndicatorEnd()
            let status = data["type"].string
            
            if status == "success" {
                self.toastMessage(message: "Login success..", toastType: .success)
                let respoonseData = data["response"]
                
                let itemA = DataManager(json: [
                    "gdocid" : respoonseData["gdocid"].string ?? "",
                    "fullname": respoonseData["fullname"].string ?? "",
                    "mobile_number"             : username,
                    "pwd"             : password
                ])
                
                
                let vc = HospitalListVC()
                vc.loginData = [itemA]
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                self.toastMessage(message: data["message"].string, toastType: .failure)
            }
            
            self.passwordField.text = ""
        } onError: { (error) in
            self.activityIndicatorEnd()
        }

    }
    
}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 2 {
            checkValidation()
        }
        return false
    }
    //    shouldChangeCharactersInRange
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == usernameField{
            let newTextLength: Int = usernameField.text!.count - range.length + string.count
            if newTextLength > 10 {
                // don't allow change
                return false
            }
            return true
        }
        return true
    }
}
