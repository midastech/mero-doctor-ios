//
//  HospitalListVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 25/03/2021.
//

import UIKit

class HospitalListVC: BaseViewController {
            
    var loginData = [DataManager]()

    fileprivate var myArray: [HospitalListModel] = []
    var hospitalListCell = HospitalListCell()
    
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
   
    //MARK: - UITableView
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        tv.register(HospitalListCell.self, forCellReuseIdentifier: "HospitalListCell")
        return tv
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
        
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()
        
        checkNetwork()
    }
    
    func setupViews() {
        UIApplication.shared.statusBarView?.backgroundColor = Theme.Color.dark_Green_Color
        titlelbl.text = "Select Hospital"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        view.addSubview(tableView)
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refresher
        }else{
            tableView.addSubview(refresher)
        }
    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)

        tableView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height + 5, width: view.frame.width, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height - 5)

    }
    
    @objc
    func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetwork()
                self.refresher.endRefreshing()
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
//            self.refresher.endRefreshing()
//            self.activityIndicatorEnd()
        }
    }
    
    func checkNetwork(){
        if Reachability.isConnectedToNetwork(){
            APICall()
        }else{
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - APICall
extension HospitalListVC {
    fileprivate func APICall(){
        self.activityIndicatorBegin()
//        let users = DataManager.getUserDetail()[0]
//        print(users.gdocid ?? "")
        
        ApiManager.sendRequest(toApi: Api.Endpoint.getdochospital(gdocid: loginData[0].gdocid ?? "")) { (statusCode, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                if let responseData = data["response"].array{
                    for data in responseData{
                        let item = HospitalListModel(json: data)
                        self.myArray.append(item)
                        self.tableView.reloadData()
                    }
                }
            } else {
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message:message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
}


// MARK: - UITableView Delegate
extension HospitalListVC :UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        hospitalListCell = tableView.dequeueReusableCell(withIdentifier: "HospitalListCell", for: indexPath) as! HospitalListCell
        hospitalListCell.selectionStyle = .none
        
        hospitalListCell.data = myArray[indexPath.row]
        return hospitalListCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        
        let itemA = DataManager(json: [
            "gdocid" : loginData[0].gdocid ?? "",
            "fullname": loginData[0].fullname ?? "",
            "mobile_number"             : loginData[0].mobile_number ?? "",
            "pwd"             : loginData[0].pwd ?? "",
            
            "gdepid" : myArray[indexPath.row].gdepid,
            "islivemerodoctor" : myArray[indexPath.row].islivemerodoctor,
            "orgid" : myArray[indexPath.row].orgid,
            "orgimageurl" : myArray[indexPath.row].orgimageurl,
            "hospitaldomain" : myArray[indexPath.row].hospitaldomain,
            "address" : myArray[indexPath.row].address,
            "orgname" : myArray[indexPath.row].orgname
        ])
        
        let itemsArray = [itemA]
        print("itemsArray",itemsArray)
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: itemsArray)
         UserDefaults.standard.set(encodedData, forKey: "UserDetail")
        
//        let userDetail = DataManager.getUserDetail()[0]
        
        defaults.setUserLogin(value: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.initLoggedInFlow()
        
        
        
//        let vc = HospitalOPDVC()
//        vc.isFrom = isFrom
//        let dic = myArray[indexPath.row]
//        vc.hospitalData = [dic]
//
//        let img = UIImageView()
//        img.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
//        img.sd_setImage(with: URL(string: dic.bannerimage), placeholderImage: nil)
//        vc.bannerImg = img.image ?? Images.meroDoctor_icon
//        navigationController?.pushViewController(vc, animated: true)
    }
}
