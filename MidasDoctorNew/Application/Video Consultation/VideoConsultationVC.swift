//
//  VideoConsultationVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 31/03/2021.
//

import UIKit
import MobileRTC
protocol videoConsultationDelegate: class {
    func meetingEnded()
}
struct videoConsultationStruct {
    static var appsecret = String()
    static var meetingroom = String()
    static var meetingpassword = String()
    static var appkey = String()
}


class VideoConsultationVC: BaseViewController {
    weak var delegate: videoConsultationDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        checkNetwork()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       
    }
    
    deinit {
        MobileRTC.shared().getMeetingService()?.delegate = nil
    }
    
    fileprivate func checkNetwork(){
        if Reachability.isConnectedToNetwork(){
            updateCallApi()
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }

    fileprivate func  updateCallApi(){
        showHud(withTitle: "calling..", and: "")
        ApiManager.sendRequest(toApi: Api.Endpoint.getzoomcredentials) { (status, data) in
            self.hideHud()
            if data["type"].string == "success"{
                videoConsultationStruct.appsecret = data["response"]["appsecret"].string!
                videoConsultationStruct.meetingroom = data["response"]["meetingroom"].string!
                videoConsultationStruct.meetingpassword = data["response"]["meetingpassword"].string!
                videoConsultationStruct.appkey = data["response"]["appkey"].string!
                self.loadZoom()
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        } onError: { (error) in
            print("\(error.localizedDescription)")
            self.hideHud()
            self.navigationController?.popViewController(animated: true)
        }

    }
    
    fileprivate func loadZoom(){
        if let meetingService = MobileRTC.shared().getMeetingService() {
            meetingService.delegate = self
            
            let joinMeetingParameters = MobileRTCMeetingJoinParam()
            joinMeetingParameters.meetingNumber = videoConsultationStruct.meetingroom
            joinMeetingParameters.password = videoConsultationStruct.meetingpassword
            joinMeetingParameters.userName = users.fullname
            
            meetingService.joinMeeting(with: joinMeetingParameters)
        }
    }
}


//MARK:- MobileRTCMeetingServiceDelegate
extension VideoConsultationVC: MobileRTCMeetingServiceDelegate{
    func onMeetingStateChange(_ state: MobileRTCMeetingState) {
       print("state : \(state)")
    }
    
    func onMeetingError(_ error: MobileRTCMeetError, message: String?) {
        print("error : \(error)")
        print("message : \(String(describing: message))")
        if message != "success" {
            popupAlert(title: "Something went wrong. Please try again later...", message: "", actionTitles: ["Ok"], perferredType: .alert, actionStyle: [.default], action: [{
                ok in
                print("ok")
                self.movebacktoView()
            }
            ])
        }
    }
    
    func onJoinMeetingInfo(_ info: MobileRTCJoinMeetingInfo, completion: @escaping (String, String, Bool) -> Void) {
        print("info : \(info)")
    }
    
    func onMeetingEndedReason(_ reason: MobileRTCMeetingEndReason) {
//        let manager = DataManager.getUserDetail()[0]
        popupAlert(title: users.fullname ?? "", message: "Do you want to call again?", actionTitles: ["Yes", "No" ], perferredType: .alert, actionStyle: [.default, .cancel], action: [{
            yes in
            print("yes")
            self.checkNetwork()
        },{
            no in
            self.delegate?.meetingEnded()
            print("no")
            self.movebacktoView()
        }
        
        ])
        
    }
    
   
        
    func movebacktoView(){
       
        navigationController?.popViewController(animated: true)
    }
    
}
