//
//  RevenueDetailVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 30/03/2021.
//

import UIKit

class RevenueDetailVC: BaseViewController {

    lazy var revenueData = [RevenueModel]()
    lazy var myArray = [RevenueDetailModel]()
    
    lazy var fromDate = String()
    lazy var toDate = String()

    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()

    //MARK: - UITableView
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        tv.register(RevenueDetailCell.self, forCellReuseIdentifier: "RevenueDetailCell")

        return tv
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()
        checkNetwork()
    }

    func setupViews() {
        titlelbl.text = "Revenue Details"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        
        view.addSubview(tableView)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refresher
        }else{
            tableView.addSubview(refresher)
        }
        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
        tableView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: view.frame.width, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)

    }
    
    @objc func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetwork()
                self.refresher.endRefreshing()
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()        }
    }
    
    
    fileprivate func checkNetwork(){
        if Reachability.isConnectedToNetwork() {
            revenueAPICall()
        }else{
            toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    
    

    
    fileprivate func revenueAPICall(){
        self.activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.getrevenuebycategory(fromdate: fromDate, todate: toDate, orgid: users.orgid ?? "", gdocid: users.gdocid ?? "", category: revenueData[0].servicedescription)) { (statusCode, data) in
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                if let responseData = data["response"].array{
                    if responseData.count > 0 {
                        for data in responseData{
                            let item = RevenueDetailModel(json: data)
                            self.myArray.append(item)
                        }
                        self.tableView.reloadData()
                    }else{
                        
                    }
                    
                }
            } else {
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message:message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension RevenueDetailVC: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RevenueDetailCell", for: indexPath) as! RevenueDetailCell
        cell.selectionStyle = .none

        let data = myArray[indexPath.row]
//        cell.titlelbl.text = String(format: "%@, (%@)", data.servicedescription, data.qty)
//        cell.amountlbl.text = "NRs. " + data.netfraction
        
        cell.data = data
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
   
}
