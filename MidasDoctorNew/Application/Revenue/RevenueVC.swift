//
//  RevenueVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 30/03/2021.
//

import UIKit

class RevenueVC: BaseViewController, RefreshPatientDelegate {
    func refreshBtnPress(sender: UIButton) {
        checkNetwork()
    }
    
    var totalFractionAmount: Float = 0.00
    lazy var myRevenue = [MyRevenueModel]()
    lazy var myArray = [RevenueModel]()

    lazy var fromDate = String()
    lazy var toDate = String()

    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    let filterBtn: UIButton = {
        let btn = UIButton()
        let image = Images.ic_filter.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        return btn
    }()

    //MARK: - UITableView
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        tv.register(DashboardRevenueCell.self, forCellReuseIdentifier: "DashboardRevenueCell")
        tv.register(RevenueHeaderCell.self, forCellReuseIdentifier: "RevenueHeaderCell")
        tv.register(RevenueCell.self, forCellReuseIdentifier: "RevenueCell")

        return tv
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    lazy var errorView: EmptyPatientListProperty = {
        let vi = EmptyPatientListProperty(frame: CGRect(x: (SCREEN.WIDTH / 2) - 110, y: (SCREEN.HEIGHT / 2) - 120, width: 220, height: 203), titleStr: "No Revenue Found",  subTitleStr: "You don't have any revenue details in this given date.")
        vi.delegate = self
        vi.backgroundColor = .clear
        return vi
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()
        checkNetwork()
    }
    
    func setupViews() {
        titlelbl.text = "My Revenue"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        navView.addSubview(filterBtn)
        
        view.addSubview(tableView)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refresher
        }else{
            tableView.addSubview(refresher)
        }
        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        filterBtn.addTarget(self, action:#selector(filterBtnPress), for: .touchUpInside)

    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
        filterBtn.frame = CGRect(x: navView.frame.width - 5 - 45, y: 0, width: 45, height: 55)
        tableView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: view.frame.width, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)

    }
    
    @objc func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetwork()
                self.refresher.endRefreshing()
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()        }
    }
    
    
    fileprivate func checkNetwork(){
        if Reachability.isConnectedToNetwork() {
//            let engDate : String = Date().todayDate(format: String.DATE_FORMATE)
//            let todayNepaliDate = getNepaliDate(engDate: engDate)
//            let getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
            
            getRevenueAPICall()
        }else{
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            self.showError()
            toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    func getRevenueAPICall(){
        self.activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.getrevenue(fromdate: fromDate, todate: toDate, orgid: users.orgid ?? "", gdocid: users.gdocid ?? "", iscategory: "Y")) { (status, data) in
            self.hideError()
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            if data["type"].string == "success"{
                if let responseData = data["response"].array {
                    if responseData.count > 0 {
                        for data in responseData{
                            let item = RevenueModel(json: data)
                            self.myArray.append(item)
                        }
                        for amountData in self.myArray {
                            let totalfraction: String = amountData.totalfraction
                            let totalfractionReplaced = totalfraction.replacingOccurrences(of: ",", with: "")

                            
                            
    //                        let numberFormatter = NumberFormatter()
    //                        let totalfraction = numberFormatter.number(from: amountData.totalfraction)
    //                        let totalfractionFloatValue = totalfraction?.floatValue

                            
                            
                            let totalfractionVal: Float = (totalfractionReplaced as NSString).floatValue //Float(amountData.totalfraction)!
                            
                            self.totalFractionAmount += totalfractionVal
                        }
                        
                        self.tableView.reloadData()
                    }else{
                        self.showError()
                    }
                    
                }
            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message:message, toastType: .message)
            }
        } onError: { (error) in
            self.showError()
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }

    }
    

    /*
    fileprivate func revenueAPICall(){
//        let users = DataManager.getUserDetail()[0]

        let engDate : String = Date().todayDate(format: String.DATE_FORMATE)
        let todayNepaliDate = getNepaliDate(engDate: engDate)
        print("todayNepaliDate \(todayNepaliDate)")
        
        let getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
        
        ApiManager.sendRequest(toApi: Api.Endpoint.getrevenuebycategory(fromdate: getnepaliFirstDate, todate: todayNepaliDate, orgid: users.orgid ?? "", gdocid: users.gdocid ?? "", category: "")) { (statusCode, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                if let responseData = data["response"].array{
                    for data in responseData{
//                        let item = HospitalListModel(json: data)
//                        self.myArray.append(item)
//                        self.tableView.reloadData()
                    }
                }
            } else {
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message:message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }

    }*/
    @objc func filterBtnPress(){
        
    }
    
    func showError(){
        view.addSubview(errorView)
    }
    
    func hideError(){
        errorView.removeFromSuperview()
    }
    
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension RevenueVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardRevenueCell", for: indexPath) as! DashboardRevenueCell
            cell.selectionStyle = .none
            cell.revenueView.frame = CGRect(x: 10, y: 10, width: cell.frame.width - 20, height: 80)
            cell.gradientColor = [Theme.Color.colorLightBlue, Theme.Color.colorBlue]
            if myRevenue.count > 0 {
                cell.totalAmountlbl.text = "Rs. " + myRevenue[0].netfraction
                cell.datelbl.text = fromDate + "\n" + "To" + "\n" + toDate

            }
            
//            let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(revenueViewTap(sender:)))
//            tapGesture4.numberOfTapsRequired = 1
//            cell.revenueView.addGestureRecognizer(tapGesture4)
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "RevenueCell", for: indexPath) as! RevenueCell
        cell.selectionStyle = .none

        let data = myArray[indexPath.row]
        cell.titlelbl.text = String(format: "%@, (%@)", data.servicedescription, data.qty)
        cell.amountlbl.text = "NRs. " + data.netfraction
        
        
        
        
        let totalfraction: String = data.totalfraction
        let totalfractionReplaced = totalfraction.replacingOccurrences(of: ",", with: "")
        let totalfractionVal: Float = (totalfractionReplaced as NSString).floatValue
        
        let progressPercent = totalfractionVal * 100 / totalFractionAmount
        cell.percentlbl.text = (String(format: "%.2f%@", progressPercent, "%"))//String(progressPercent)
        cell.ProgressBar(progressPercent: progressPercent)

        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       /*
         if section == 1 {
            let backView: UIView = {
                let v = UIView()
                //                v.translatesAutoresizingMaskIntoConstraints = false
                v.backgroundColor = .clear
                return v
            }()
            
            let categorylbl: UILabel = {
                let tl = UILabel()
                tl.font = UIFont.medium(ofSize: 14)
                tl.textColor = Theme.Color.darkGray
                tl.text = "Category"
                return tl
            }()
            
            let incomelbl: UILabel = {
                let tl = UILabel()
                tl.font = UIFont.medium(ofSize: 14)
                tl.textColor = Theme.Color.darkGray
                tl.text = "Net Income"
                return tl
            }()
            
            backView.addSubview(categorylbl)
            backView.addSubview(incomelbl)

            backView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 35)
            incomelbl.frame = CGRect(x: backView.frame.width - 100, y: 0, width: 90, height: SCREEN.WIDTH)
            categorylbl.frame = CGRect(x: 10, y: 0, width: incomelbl.frame.origin.x, height: SCREEN.WIDTH)

            return backView
        }
        return nil
        */
         if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RevenueHeaderCell") as! RevenueHeaderCell
            return cell
        }
        let backView: UIView = {
            let v = UIView()
            v.backgroundColor = .clear
            return v
        }()
        
        return backView
 
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 25
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            let vc = RevenueDetailVC()
            vc.toDate = toDate
            vc.fromDate = fromDate
            vc.revenueData = [myArray[indexPath.row]]
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

