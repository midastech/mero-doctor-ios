//
//  ExaminationFormViewController.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 19/07/2021.
//

import UIKit
import CropViewController

class ExaminationFormViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bacBtn: UIButton!
    var dataSource = [String]()
    var examinationDataSource: ExaminationDataResponseModel?
    lazy var patientArray: [PatientListModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        loadData()
        getComplataintsApiCall()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func uisetup() {
        let image = Images.Back_Icon.withRenderingMode(.alwaysTemplate)
        bacBtn.setImage(image, for: .normal)
        bacBtn.tintColor = Theme.Color.black
        tableView.register(UINib(nibName: "ExaminationFormTableViewCell", bundle: nil), forCellReuseIdentifier: "ExaminationFormTableViewCell")
        tableView.register(UINib(nibName: "ExaminationSectionTableViewCell", bundle: nil), forCellReuseIdentifier: "ExaminationSectionTableViewCell")
        //tableView.register(UINib(nibName: "NewSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "NewSectionHeader")
       // self.tableView.register(NewSectionHeader.self,
          //  forHeaderFooterViewReuseIdentifier: "NewSectionHeader")
       // tableView.register(UINib(nibName: "NewSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "NewSectionHeader")


        tableView.dataSource = self
        tableView.delegate = self
    }
    func loadData() {
        dataSource = ["Complaints", "Observations", "Diagnosis","Doctor Note(Patient Advice)"]
        tableView.reloadData()
    }
   func getComplataintsApiCall() {
    ExaminationDataResponse.requestToExaminationData(toApi: Api.Endpoint.getExaminationFullData(appid: patientArray[0].appid)) { [weak self] response in
        guard let strongSelf = self else { return }
        if response?.type == "success" {
        strongSelf.examinationDataSource = response
            strongSelf.tableView.reloadData()
        }
    
    }
    }
    
//    func getComplataintsApiCall() {
//
//            ApiManager.sendRequest(toApi: Api.Endpoint.getchiefcomplaints(appid: patientArray[0].appid)) { (status, data) in
//               // self.activityIndicatorEnd()
//                if data["type"].string == "success"{
//                    print(data)
//                }else{
//    //                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
//    //                self.toastMessage(message: message, toastType: .message)
//                }
//            } onError: { (error) in
//               // self.activityIndicatorEnd()
//                self.toastMessage(message: error.localizedDescription, toastType: .message)
//            }
//
//
//    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ExaminationFormViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
        return dataSource.count
        } else if section == 1 {
            return 0
        } else if section == 2 {
            return 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExaminationFormTableViewCell", for: indexPath) as! ExaminationFormTableViewCell
            cell.titleLabel.text = dataSource[indexPath.row]
            cell.saveBtn.alpha = 0.5
            cell.delegate = self
        cell.saveBtn.tag = indexPath.row
        cell.descTxtView.tag = indexPath.row
            cell.delegate = self
            if indexPath.row == 0 {
                print(examinationDataSource?.response?.chief?[0].hITACOMPLAINT)
                if examinationDataSource?.response?.chief?.count ?? 0 > 0 {
                    cell.saveBtn.tag = indexPath.row
                    cell.descTxtView.text = examinationDataSource?.response?.chief?[0].hITACOMPLAINT
                    cell.complaintID = examinationDataSource?.response?.chief?[0].hITACOMPLAINID
                } else {
                   
                }
               
            } else if indexPath.row == 1 {
                print(examinationDataSource?.response?.ex?[0].eXFOEXID)
                if examinationDataSource?.response?.ex?.count ?? 0 > 0 {
                    cell.descTxtView.text = examinationDataSource?.response?.ex?[0].eXFODESCRIPTION
                    cell.observationId = examinationDataSource?.response?.ex?[0].eXFOEXID
                } else {
                   
                }
                
            } else if indexPath.row == 2 {
                print(examinationDataSource?.response?.diagnosis?.count)
                if examinationDataSource?.response?.diagnosis?.count ?? 0 > 0 {
                    cell.descTxtView.text = examinationDataSource?.response?.diagnosis?[0].iCLODIAGNOSIS
                    cell.diagnosisId = examinationDataSource?.response?.diagnosis?[0].iCLOFINALDIAGONOSISID
                    
                } else {
                   
                }
              
            } else if indexPath.row == 3 {
                if examinationDataSource?.response?.doctornote?.hEDEREMARKS == nil {
                    
                } else {
                cell.descTxtView.text = examinationDataSource?.response?.doctornote?.hEDEREMARKS
                    cell.doctorNoteId = examinationDataSource?.response?.doctornote?.hEDEDETAILID
            }
            }
        return cell
        } else if indexPath.section == 1 {
            return UITableViewCell()
        } else if indexPath.section == 2 {
            return UITableViewCell()
        } else {
            return UITableViewCell()
        }
    }

}
extension ExaminationFormViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExaminationSectionTableViewCell") as! ExaminationSectionTableViewCell
        cell.section = section
        cell.delegate = self
        cell.sectionAddBtn.imageView?.contentMode = .center
        if section == 0 {
            cell.sectionAddBtn.isHidden = true
            cell.sectionTitle.text = "Clinical Notes"

            return cell
        } else if section == 1 {
            cell.sectionAddBtn.isHidden = false
            cell.sectionTitle.text = "Lab Orders"
            return cell
        } else if section == 2 {
            cell.sectionTitle.text = "Prescription"
            cell.sectionAddBtn.isHidden = false
            return cell
        } else {
            return UITableViewCell()
        }
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NewSectionHeader") as! NewSectionHeader
//        return headerView
//
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK:- CELL DELEGATE
extension ExaminationFormViewController: examinationFormCellDelegate {
    func saveBtnAction(index: Int?, text: String?, complaintId: String?, observationId: String?, diagnosisId: String?, doctornoteId: String?) {
        print()
        switch index {
        case 0:
            print(patientArray.first?.appid,complaintId,text)
           
            ExaminationDataResponse.requestToUpdateComplaints(toApi: Api.Endpoint.updateChieftComplatints(appid: (patientArray[0].appid), complaintId: complaintId ?? "", complaintText: text!)) { [weak self]response in
                guard let strongSelf = self else { return }
                if response?.type == "success" {
                    self?.toastMessage(message: response?.message, toastType: .message)
//                    let indexPath = IndexPath(row: index!, section: 0)
//                    strongSelf.tableView.reloadRows(at: [indexPath], with: .none)
                   // strongSelf.getComplataintsApiCall()
                } else {
                    self?.toastMessage(message: response?.message, toastType: .message)
                }
            }
        case 1:
            print(patientArray.first?.appid,observationId,text)
            ExaminationDataResponse.requestToUpdateObservations(toApi: Api.Endpoint.updateObservations(appid: patientArray[0].appid , observationId: observationId ?? "", observationText: text ?? "")) { [weak self]response in
                guard let strongSelf = self else { return }
                if response?.type == "success" {
                    self?.toastMessage(message: response?.message, toastType: .message)
                    //strongSelf.getComplataintsApiCall()
//                    let indexPath = IndexPath(row: index!, section: 0)
//                    strongSelf.tableView.reloadRows(at: [indexPath], with: .none)
                    
                } else {
                    self?.toastMessage(message: response?.message, toastType: .message)
                }
            }
        case 2:
            print(patientArray.first?.appid,diagnosisId,text)
            ExaminationDataResponse.requestToUpdateDiagnosis(toApi: Api.Endpoint.updateDiagnosis(appid: patientArray.first?.appid ?? "", diagnosisId: diagnosisId ?? "", diagnosisText: text ?? "")) { [weak self]response in
                guard let strongSelf = self else { return }
                if response?.type == "success" {
                    self?.toastMessage(message: response?.message, toastType: .message)
                    strongSelf.getComplataintsApiCall()
//                    let indexPath = IndexPath(row: index!, section: 0)
//                    strongSelf.tableView.reloadRows(at: [indexPath], with: .none)
                } else {
                    self?.toastMessage(message: response?.message, toastType: .message)
                }
            }
        case 3:
            print(patientArray.first?.appid,doctornoteId,text)
            ExaminationDataResponse.requestToUpdateDoctorNote(toApi: Api.Endpoint.updateDoctorNote(appid: patientArray.first?.appid ?? "", doctornoteId: doctornoteId ?? "", doctornoteText: text ?? "")) { [weak self]response in
                guard let strongSelf = self else { return }
                if response?.type == "success" {
                    self?.toastMessage(message: response?.message, toastType: .message)
                    strongSelf.getComplataintsApiCall()
//                    let indexPath = IndexPath(row: index!, section: 0)
//                    strongSelf.tableView.reloadRows(at: [indexPath], with: .none)
                } else {
                    self?.toastMessage(message: response?.message, toastType: .message)
                }
            }
        default:
            break
        }
    }
    
    func saveBtnAction(index: Int?, text: String?) {
//        switch index {
//        case 0:
//            ExaminationDataResponse.requestToUpdateComplaints(toApi: Api.Endpoint.updateChieftComplatints(appid: patientArray.first?.appid, complaintId: "", complaintText: "")) { [weak self]response in
//                guard let strongSelf = self else { return }
//            }
//        case 1:
//            ExaminationDataResponse.requestToUpdateObservations(toApi: Api.Endpoint.updateObservations(appid: "", observationId: "", observationText: "")) { [weak self]response in
//                guard let strongSelf = self else { return }
//            }
//        case 2:
//            ExaminationDataResponse.requestToUpdateDiagnosis(toApi: Api.Endpoint.updateDiagnosis(appid: "", diagnosisId: "", diagnosisText: "")) { [weak self]response in
//                guard let strongSelf = self else { return }
//            }
//        case 3:
//            ExaminationDataResponse.requestToUpdateDoctorNote(toApi: Api.Endpoint.updateDoctorNote(appid: "", doctornoteId: "", doctornoteText: "")) { [weak self]response in
//                guard let strongSelf = self else { return }
//            }
//        default:
//            break
//        }
    }
    
    func saveBtnAction(index: Int?) {
        
    }
    
    
}
//MARK:- SECTION DELEGATE
extension ExaminationFormViewController: sectionDelegate {
    func sectionDidTap(section: Int) {
        switch section {
        case 1:
            ExaminationDataResponse.requestToUpdateComplaints(toApi: Api.Endpoint.updateChieftComplatints(appid: "74775", complaintId: "1002", complaintText: "Ehello")) { [weak self]response in
                guard let strongSelf = self else { return }
                if response?.type == "success" {
                    self?.toastMessage(message: response?.message, toastType: .message)
//                    let indexPath = IndexPath(row: index!, section: 0)
//                    strongSelf.tableView.reloadRows(at: [indexPath], with: .none)
                   // strongSelf.getComplataintsApiCall()
                } else {
                    self?.toastMessage(message: response?.message, toastType: .message)
                }
            }
//            let vc = UIStoryboard.homeStoryboard.instantiateLabTestOrderVC()
//            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            print("")
//            let vc = UIStoryboard.homeStoryboard.instantiateAddPrescriptionVC()
//            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        
    }
    
    
}
