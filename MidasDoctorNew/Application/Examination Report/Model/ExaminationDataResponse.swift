//
//  ExaminationDataResponse.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 21/07/2021.
//

import Foundation
import ObjectMapper

class ExaminationDataResponse: DefaultResponse {
   
override func mapping(map: Map) {
  super.mapping(map: map)

}
// MARK: - EXAMINATION DATA REQUEST
    class func requestToExaminationData(toApi api:Api.Endpoint,showHud: Bool = false ,completionHandler:@escaping ((ExaminationDataResponseModel?) -> Void)) {
        
        APIManager(toApi: api).handleResponse(showProgressHud: showHud,completionHandler: { (response: ExaminationDataResponseModel) in
              print(response)
                  completionHandler(response)
              }, failureBlock: {
                completionHandler(nil)
              })
      }
    // MARK: - UPDATE COMPLAINT
        class func requestToUpdateComplaints(toApi api:Api.Endpoint,showHud: Bool = false ,completionHandler:@escaping ((DefaultResponse?) -> Void)) {
            
            APIManager(toApi: api).handleResponse(showProgressHud: showHud,completionHandler: { (response: DefaultResponse) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
    // MARK: - UPDATE OBSERVATION
        class func requestToUpdateObservations(toApi api:Api.Endpoint,showHud: Bool = false ,completionHandler:@escaping ((DefaultResponse?) -> Void)) {
            
            APIManager(toApi: api).handleResponse(showProgressHud: showHud,completionHandler: { (response: DefaultResponse) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
    // MARK: - UPDATE DIAGNOSIS
        class func requestToUpdateDiagnosis(toApi api:Api.Endpoint,showHud: Bool = false ,completionHandler:@escaping ((DefaultResponse?) -> Void)) {
            
            APIManager(toApi: api).handleResponse(showProgressHud: showHud,completionHandler: { (response: DefaultResponse) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
    // MARK: - UPDATE DOCTORNOTE
        class func requestToUpdateDoctorNote(toApi api:Api.Endpoint,showHud: Bool = false ,completionHandler:@escaping ((DefaultResponse?) -> Void)) {
            APIManager(toApi: api).handleResponse(showProgressHud: showHud,completionHandler: { (response: DefaultResponse) in
                  print(response)
                      completionHandler(response)
                  }, failureBlock: {
                    completionHandler(nil)
                  })
          }
    
}
 class ExaminationDataResponseModel: DefaultResponse {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kExaminationDataResponseModelResponseKey: String = "response"
    internal let kExaminationDataResponseModelMessageKey: String = "message"
    internal let kExaminationDataResponseModelTypeKey: String = "type"


    // MARK: Properties
    public var response: ExaminationResponse?
//    public var message: String?
//    public var type: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
   

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public override func mapping(map: Map) {
        response <- map[kExaminationDataResponseModelResponseKey]
        message <- map[kExaminationDataResponseModelMessageKey]
        type <- map[kExaminationDataResponseModelTypeKey]

    }
}
public class ExaminationResponse: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kResponseExKey: String = "ex"
    internal let kResponseChiefKey: String = "chief"
    internal let kResponseDiagnosisKey: String = "diagnosis"
    internal let kResponseDoctornoteKey: String = "doctornote"
    internal let kResponseMedKey: String = "med"
    internal let kResponseLabKey: String = "lab"


    // MARK: Properties
    public var ex: [ExData]?
    public var chief: [Chief]?
    public var diagnosis: [ExDiagnosis]?
    public var doctornote: DoctorNote?
    public var med: [String]?
    public var lab: Bool = false



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
   

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        ex <- map[kResponseExKey]
        chief <- map[kResponseChiefKey]
        diagnosis <- map[kResponseDiagnosisKey]
        doctornote <- map[kResponseDoctornoteKey]
        med <- map[kResponseMedKey]
        lab <- map[kResponseLabKey]

    }
}
public class Chief: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kChiefDOCTMODIFYDATEKey: String = "DOCT_MODIFYDATE"
    internal let kChiefDOCTISDOCTORKey: String = "DOCT_ISDOCTOR"
    internal let kChiefDEPTDEPCODEKey: String = "DEPT_DEPCODE"
    internal let kChiefDOCTPANNOKey: String = "DOCT_PANNO"
    internal let kChiefDOCTDESIGNATIONIDKey: String = "DOCT_DESIGNATIONID"
    internal let kChiefDOCTISNONEHOSDOCSTAFFKey: String = "DOCT_ISNONEHOSDOCSTAFF"
    internal let kChiefDOCTISSURGEONKey: String = "DOCT_ISSURGEON"
    internal let kChiefDOCTISONCALLDOCTORKey: String = "DOCT_ISONCALLDOCTOR"
    internal let kChiefHITAENTEREDBYKey: String = "HITA_ENTEREDBY"
    internal let kChiefDOCTISINHOUSESTAFFDOCTORKey: String = "DOCT_ISINHOUSESTAFFDOCTOR"
    internal let kChiefDEPTPARENTDEPIDKey: String = "DEPT_PARENTDEPID"
    internal let kChiefDEPTISPARENTDEPKey: String = "DEPT_ISPARENTDEP"
    internal let kChiefDEPTROOMNOKey: String = "DEPT_ROOMNO"
    internal let kChiefHITAPATIENTIDKey: String = "HITA_PATIENTID"
    internal let kChiefHITAFROMKey: String = "HITA_FROM"
    internal let kChiefDOCTMACIDKey: String = "DOCT_MACID"
    internal let kChiefDOCTISANAESTHETISTKey: String = "DOCT_ISANAESTHETIST"
    internal let kChiefDOCTGDOCIDKey: String = "DOCT_GDOCID"
    internal let kChiefDOCTPAYMENTBYKey: String = "DOCT_PAYMENTBY"
    internal let kChiefDOCTISREFERRALKey: String = "DOCT_ISREFERRAL"
    internal let kChiefDOCTACCOUNTHEADIDOPDKey: String = "DOCT_ACCOUNTHEADIDOPD"
    internal let kChiefDOCTZIPCODEKey: String = "DOCT_ZIPCODE"
    internal let kChiefDOCTFUNDRATEKey: String = "DOCT_FUNDRATE"
    internal let kChiefDOCTMODIFYBYKey: String = "DOCT_MODIFYBY"
    internal let kChiefDOCTMODIFYTIMEKey: String = "DOCT_MODIFYTIME"
    internal let kChiefDOCTDOCIDKey: String = "DOCT_DOCID"
    internal let kChiefHITADOCIDKey: String = "HITA_DOCID"
    internal let kChiefDOCTMOBILENOKey: String = "DOCT_MOBILENO"
    internal let kChiefDEPTGDEPNAMEKey: String = "DEPT_GDEPNAME"
    internal let kChiefDOCTREFDOCIDKey: String = "DOCT_REFDOCID"
    internal let kChiefHITADATAPOSTDATEKey: String = "HITA_DATAPOSTDATE"
    internal let kChiefDEPTGDEPIDKey: String = "DEPT_GDEPID"
    internal let kChiefDOCTISUNITKey: String = "DOCT_ISUNIT"
    internal let kChiefDEPTISFEMININEDEPTKey: String = "DEPT_ISFEMININEDEPT"
    internal let kChiefHITACOMPLAINDATEKey: String = "HITA_COMPLAINDATE"
    internal let kChiefDOCTDATAPOSTTIMEKey: String = "DOCT_DATAPOSTTIME"
    internal let kChiefDOCTNMCNOKey: String = "DOCT_NMCNO"
    internal let kChiefDEPTISEMERGENCYDEPTKey: String = "DEPT_ISEMERGENCYDEPT"
    internal let kChiefDEPTSUBPARENTDEPIDKey: String = "DEPT_SUBPARENTDEPID"
    internal let kChiefDOCTISACTIVEKey: String = "DOCT_ISACTIVE"
    internal let kChiefDOCTISERSTAFFKey: String = "DOCT_ISERSTAFF"
    internal let kChiefDEPTDEPNAMEKey: String = "DEPT_DEPNAME"
    internal let kChiefDOCTDISPLAYNAMEKey: String = "DOCT_DISPLAYNAME"
    internal let kChiefDEPTDATAPOSTBYKey: String = "DEPT_DATAPOSTBY"
    internal let kChiefDOCTDOCPAYTYPEKey: String = "DOCT_DOCPAYTYPE"
    internal let kChiefDEPTACCHEADIDKey: String = "DEPT_ACCHEADID"
    internal let kChiefDOCTSPECIALIZATIONKey: String = "DOCT_SPECIALIZATION"
    internal let kChiefDOCTDOCORDERKey: String = "DOCT_DOCORDER"
    internal let kChiefDOCTDOCCODEKey: String = "DOCT_DOCCODE"
    internal let kChiefDEPTISACTIVEKey: String = "DEPT_ISACTIVE"
    internal let kChiefDOCTSTATUSKey: String = "DOCT_STATUS"
    internal let kChiefDEPTDEPIDKey: String = "DEPT_DEPID"
    internal let kChiefDOCTQUALIFICATIONKey: String = "DOCT_QUALIFICATION"
    internal let kChiefDEPTDEPTYPEKey: String = "DEPT_DEPTYPE"
    internal let kChiefDEPTEXTENDEDREPORTKey: String = "DEPT_EXTENDED_REPORT"
    internal let kChiefDOCTDEPIDKey: String = "DOCT_DEPID"
    internal let kChiefHITAINPATIENTIDKey: String = "HITA_INPATIENTID"
    internal let kChiefDEPTTESTNAMECODEPREFIXKey: String = "DEPT_TESTNAMECODEPREFIX"
    internal let kChiefDOCTPISDOCIDKey: String = "DOCT_PISDOCID"
    internal let kChiefDOCTDATAPOSTBYKey: String = "DOCT_DATAPOSTBY"
    internal let kChiefDOCTIMAGEKey: String = "DOCT_IMAGE"
    internal let kChiefDOCTTDSPERKey: String = "DOCT_TDSPER"
    internal let kChiefDOCTISENDODOCTORKey: String = "DOCT_ISENDODOCTOR"
    internal let kChiefDOCTDESIGKey: String = "DOCT_DESIG"
    internal let kChiefHITAVISITIDKey: String = "HITA_VISITID"
    internal let kChiefHITACOMPLAINTKey: String = "HITA_COMPLAINT"
    internal let kChiefDOCTISLABDOCTORKey: String = "DOCT_ISLABDOCTOR"
    internal let kChiefHITACOMPLAINTIMEKey: String = "HITA_COMPLAINTIME"
    internal let kChiefDOCTORGIDKey: String = "DOCT_ORGID"
    internal let kChiefDEPTDATAPOSTDATEKey: String = "DEPT_DATAPOSTDATE"
    internal let kChiefDOCTDEPTYPEKey: String = "DOCT_DEPTYPE"
    internal let kChiefDOCTACCOUNTHEADIDIPDKey: String = "DOCT_ACCOUNTHEADIDIPD"
    internal let kChiefDEPTISREFERRALDEPTKey: String = "DEPT_ISREFERRALDEPT"
    internal let kChiefDEPTISREFERALCOMPKey: String = "DEPT_ISREFERALCOMP"
    internal let kChiefDOCTISOTASSISTANTKey: String = "DOCT_ISOTASSISTANT"
    internal let kChiefDOCTDATAPOSTDATEKey: String = "DOCT_DATAPOSTDATE"
    internal let kChiefDOCTPOSITIONIDKey: String = "DOCT_POSITIONID"
    internal let kChiefDOCTISQUEUENOGENINREGKey: String = "DOCT_ISQUEUENOGENINREG"
    internal let kChiefDEPTORGIDKey: String = "DEPT_ORGID"
    internal let kChiefDEPTISPATHOLOGICALDEPKey: String = "DEPT_ISPATHOLOGICALDEP"
    internal let kChiefDOCTDOCTYPEKey: String = "DOCT_DOCTYPE"
    internal let kChiefDEPTDEPTYPEBYSERVICEKey: String = "DEPT_DEPTYPEBYSERVICE"
    internal let kChiefDEPTDATAPOSTTIMEKey: String = "DEPT_DATAPOSTTIME"
    internal let kChiefHITADATAPOSTDATEADKey: String = "HITA_DATAPOSTDATEAD"
    internal let kChiefDEPTISMEMBERSHIPDEPTKey: String = "DEPT_ISMEMBERSHIPDEPT"
    internal let kChiefDOCTISSMSKey: String = "DOCT_ISSMS"
    internal let kChiefDEPTDEPORDERKey: String = "DEPT_DEPORDER"
    internal let kChiefDOCTGDOCNAMEKey: String = "DOCT_GDOCNAME"
    internal let kChiefDOCTDOCSHAREGROUPKey: String = "DOCT_DOCSHAREGROUP"
    internal let kChiefDOCTGDEPIDKey: String = "DOCT_GDEPID"
    internal let kChiefDOCTISHODKey: String = "DOCT_ISHOD"
    internal let kChiefDOCTYPEKey: String = "DOCTYPE"
    internal let kChiefHITADEPIDKey: String = "HITA_DEPID"
    internal let kChiefDOCTROOMNOKey: String = "DOCT_ROOMNO"
    internal let kChiefDOCTEMAILKey: String = "DOCT_EMAIL"
    internal let kChiefDOCTDEPCODEKey: String = "DOCT_DEPCODE"
    internal let kChiefDOCTISERDOCTORKey: String = "DOCT_ISERDOCTOR"
    internal let kChiefHITADATAPOSTTIMEKey: String = "HITA_DATAPOSTTIME"
    internal let kChiefDOCTPHONENOKey: String = "DOCT_PHONENO"
    internal let kChiefHITACOMPLAINIDKey: String = "HITA_COMPLAINID"
    internal let kChiefDOCTISBLKDOCTORKey: String = "DOCT_ISBLKDOCTOR"
    internal let kChiefDEPTREPORTTITLEKey: String = "DEPT_REPORTTITLE"
    internal let kChiefDOCTDOCNAMEKey: String = "DOCT_DOCNAME"


    // MARK: Properties
    public var dOCTMODIFYDATE: String?
    public var dOCTISDOCTOR: String?
    public var dEPTDEPCODE: String?
    public var dOCTPANNO: String?
    public var dOCTDESIGNATIONID: String?
    public var dOCTISNONEHOSDOCSTAFF: String?
    public var dOCTISSURGEON: String?
    public var dOCTISONCALLDOCTOR: String?
    public var hITAENTEREDBY: String?
    public var dOCTISINHOUSESTAFFDOCTOR: String?
    public var dEPTPARENTDEPID: String?
    public var dEPTISPARENTDEP: String?
    public var dEPTROOMNO: String?
    public var hITAPATIENTID: String?
    public var hITAFROM: String?
    public var dOCTMACID: String?
    public var dOCTISANAESTHETIST: String?
    public var dOCTGDOCID: String?
    public var dOCTPAYMENTBY: String?
    public var dOCTISREFERRAL: String?
    public var dOCTACCOUNTHEADIDOPD: String?
    public var dOCTZIPCODE: String?
    public var dOCTFUNDRATE: String?
    public var dOCTMODIFYBY: String?
    public var dOCTMODIFYTIME: String?
    public var dOCTDOCID: String?
    public var hITADOCID: String?
    public var dOCTMOBILENO: String?
    public var dEPTGDEPNAME: String?
    public var dOCTREFDOCID: String?
    public var hITADATAPOSTDATE: String?
    public var dEPTGDEPID: String?
    public var dOCTISUNIT: String?
    public var dEPTISFEMININEDEPT: String?
    public var hITACOMPLAINDATE: String?
    public var dOCTDATAPOSTTIME: String?
    public var dOCTNMCNO: String?
    public var dEPTISEMERGENCYDEPT: String?
    public var dEPTSUBPARENTDEPID: String?
    public var dOCTISACTIVE: String?
    public var dOCTISERSTAFF: String?
    public var dEPTDEPNAME: String?
    public var dOCTDISPLAYNAME: String?
    public var dEPTDATAPOSTBY: String?
    public var dOCTDOCPAYTYPE: String?
    public var dEPTACCHEADID: String?
    public var dOCTSPECIALIZATION: String?
    public var dOCTDOCORDER: String?
    public var dOCTDOCCODE: String?
    public var dEPTISACTIVE: String?
    public var dOCTSTATUS: String?
    public var dEPTDEPID: String?
    public var dOCTQUALIFICATION: String?
    public var dEPTDEPTYPE: String?
    public var dEPTEXTENDEDREPORT: String?
    public var dOCTDEPID: String?
    public var hITAINPATIENTID: String?
    public var dEPTTESTNAMECODEPREFIX: String?
    public var dOCTPISDOCID: String?
    public var dOCTDATAPOSTBY: String?
    public var dOCTIMAGE: String?
    public var dOCTTDSPER: String?
    public var dOCTISENDODOCTOR: String?
    public var dOCTDESIG: String?
    public var hITAVISITID: String?
    public var hITACOMPLAINT: String?
    public var dOCTISLABDOCTOR: String?
    public var hITACOMPLAINTIME: String?
    public var dOCTORGID: String?
    public var dEPTDATAPOSTDATE: String?
    public var dOCTDEPTYPE: String?
    public var dOCTACCOUNTHEADIDIPD: String?
    public var dEPTISREFERRALDEPT: String?
    public var dEPTISREFERALCOMP: String?
    public var dOCTISOTASSISTANT: String?
    public var dOCTDATAPOSTDATE: String?
    public var dOCTPOSITIONID: String?
    public var dOCTISQUEUENOGENINREG: String?
    public var dEPTORGID: String?
    public var dEPTISPATHOLOGICALDEP: String?
    public var dOCTDOCTYPE: String?
    public var dEPTDEPTYPEBYSERVICE: String?
    public var dEPTDATAPOSTTIME: String?
    public var hITADATAPOSTDATEAD: String?
    public var dEPTISMEMBERSHIPDEPT: String?
    public var dOCTISSMS: String?
    public var dEPTDEPORDER: String?
    public var dOCTGDOCNAME: String?
    public var dOCTDOCSHAREGROUP: String?
    public var dOCTGDEPID: String?
    public var dOCTISHOD: String?
    public var dOCTYPE: String?
    public var hITADEPID: String?
    public var dOCTROOMNO: String?
    public var dOCTEMAIL: String?
    public var dOCTDEPCODE: String?
    public var dOCTISERDOCTOR: String?
    public var hITADATAPOSTTIME: String?
    public var dOCTPHONENO: String?
    public var hITACOMPLAINID: String?
    public var dOCTISBLKDOCTOR: String?
    public var dEPTREPORTTITLE: String?
    public var dOCTDOCNAME: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
   

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        dOCTMODIFYDATE <- map[kChiefDOCTMODIFYDATEKey]
        dOCTISDOCTOR <- map[kChiefDOCTISDOCTORKey]
        dEPTDEPCODE <- map[kChiefDEPTDEPCODEKey]
        dOCTPANNO <- map[kChiefDOCTPANNOKey]
        dOCTDESIGNATIONID <- map[kChiefDOCTDESIGNATIONIDKey]
        dOCTISNONEHOSDOCSTAFF <- map[kChiefDOCTISNONEHOSDOCSTAFFKey]
        dOCTISSURGEON <- map[kChiefDOCTISSURGEONKey]
        dOCTISONCALLDOCTOR <- map[kChiefDOCTISONCALLDOCTORKey]
        hITAENTEREDBY <- map[kChiefHITAENTEREDBYKey]
        dOCTISINHOUSESTAFFDOCTOR <- map[kChiefDOCTISINHOUSESTAFFDOCTORKey]
        dEPTPARENTDEPID <- map[kChiefDEPTPARENTDEPIDKey]
        dEPTISPARENTDEP <- map[kChiefDEPTISPARENTDEPKey]
        dEPTROOMNO <- map[kChiefDEPTROOMNOKey]
        hITAPATIENTID <- map[kChiefHITAPATIENTIDKey]
        hITAFROM <- map[kChiefHITAFROMKey]
        dOCTMACID <- map[kChiefDOCTMACIDKey]
        dOCTISANAESTHETIST <- map[kChiefDOCTISANAESTHETISTKey]
        dOCTGDOCID <- map[kChiefDOCTGDOCIDKey]
        dOCTPAYMENTBY <- map[kChiefDOCTPAYMENTBYKey]
        dOCTISREFERRAL <- map[kChiefDOCTISREFERRALKey]
        dOCTACCOUNTHEADIDOPD <- map[kChiefDOCTACCOUNTHEADIDOPDKey]
        dOCTZIPCODE <- map[kChiefDOCTZIPCODEKey]
        dOCTFUNDRATE <- map[kChiefDOCTFUNDRATEKey]
        dOCTMODIFYBY <- map[kChiefDOCTMODIFYBYKey]
        dOCTMODIFYTIME <- map[kChiefDOCTMODIFYTIMEKey]
        dOCTDOCID <- map[kChiefDOCTDOCIDKey]
        hITADOCID <- map[kChiefHITADOCIDKey]
        dOCTMOBILENO <- map[kChiefDOCTMOBILENOKey]
        dEPTGDEPNAME <- map[kChiefDEPTGDEPNAMEKey]
        dOCTREFDOCID <- map[kChiefDOCTREFDOCIDKey]
        hITADATAPOSTDATE <- map[kChiefHITADATAPOSTDATEKey]
        dEPTGDEPID <- map[kChiefDEPTGDEPIDKey]
        dOCTISUNIT <- map[kChiefDOCTISUNITKey]
        dEPTISFEMININEDEPT <- map[kChiefDEPTISFEMININEDEPTKey]
        hITACOMPLAINDATE <- map[kChiefHITACOMPLAINDATEKey]
        dOCTDATAPOSTTIME <- map[kChiefDOCTDATAPOSTTIMEKey]
        dOCTNMCNO <- map[kChiefDOCTNMCNOKey]
        dEPTISEMERGENCYDEPT <- map[kChiefDEPTISEMERGENCYDEPTKey]
        dEPTSUBPARENTDEPID <- map[kChiefDEPTSUBPARENTDEPIDKey]
        dOCTISACTIVE <- map[kChiefDOCTISACTIVEKey]
        dOCTISERSTAFF <- map[kChiefDOCTISERSTAFFKey]
        dEPTDEPNAME <- map[kChiefDEPTDEPNAMEKey]
        dOCTDISPLAYNAME <- map[kChiefDOCTDISPLAYNAMEKey]
        dEPTDATAPOSTBY <- map[kChiefDEPTDATAPOSTBYKey]
        dOCTDOCPAYTYPE <- map[kChiefDOCTDOCPAYTYPEKey]
        dEPTACCHEADID <- map[kChiefDEPTACCHEADIDKey]
        dOCTSPECIALIZATION <- map[kChiefDOCTSPECIALIZATIONKey]
        dOCTDOCORDER <- map[kChiefDOCTDOCORDERKey]
        dOCTDOCCODE <- map[kChiefDOCTDOCCODEKey]
        dEPTISACTIVE <- map[kChiefDEPTISACTIVEKey]
        dOCTSTATUS <- map[kChiefDOCTSTATUSKey]
        dEPTDEPID <- map[kChiefDEPTDEPIDKey]
        dOCTQUALIFICATION <- map[kChiefDOCTQUALIFICATIONKey]
        dEPTDEPTYPE <- map[kChiefDEPTDEPTYPEKey]
        dEPTEXTENDEDREPORT <- map[kChiefDEPTEXTENDEDREPORTKey]
        dOCTDEPID <- map[kChiefDOCTDEPIDKey]
        hITAINPATIENTID <- map[kChiefHITAINPATIENTIDKey]
        dEPTTESTNAMECODEPREFIX <- map[kChiefDEPTTESTNAMECODEPREFIXKey]
        dOCTPISDOCID <- map[kChiefDOCTPISDOCIDKey]
        dOCTDATAPOSTBY <- map[kChiefDOCTDATAPOSTBYKey]
        dOCTIMAGE <- map[kChiefDOCTIMAGEKey]
        dOCTTDSPER <- map[kChiefDOCTTDSPERKey]
        dOCTISENDODOCTOR <- map[kChiefDOCTISENDODOCTORKey]
        dOCTDESIG <- map[kChiefDOCTDESIGKey]
        hITAVISITID <- map[kChiefHITAVISITIDKey]
        hITACOMPLAINT <- map[kChiefHITACOMPLAINTKey]
        dOCTISLABDOCTOR <- map[kChiefDOCTISLABDOCTORKey]
        hITACOMPLAINTIME <- map[kChiefHITACOMPLAINTIMEKey]
        dOCTORGID <- map[kChiefDOCTORGIDKey]
        dEPTDATAPOSTDATE <- map[kChiefDEPTDATAPOSTDATEKey]
        dOCTDEPTYPE <- map[kChiefDOCTDEPTYPEKey]
        dOCTACCOUNTHEADIDIPD <- map[kChiefDOCTACCOUNTHEADIDIPDKey]
        dEPTISREFERRALDEPT <- map[kChiefDEPTISREFERRALDEPTKey]
        dEPTISREFERALCOMP <- map[kChiefDEPTISREFERALCOMPKey]
        dOCTISOTASSISTANT <- map[kChiefDOCTISOTASSISTANTKey]
        dOCTDATAPOSTDATE <- map[kChiefDOCTDATAPOSTDATEKey]
        dOCTPOSITIONID <- map[kChiefDOCTPOSITIONIDKey]
        dOCTISQUEUENOGENINREG <- map[kChiefDOCTISQUEUENOGENINREGKey]
        dEPTORGID <- map[kChiefDEPTORGIDKey]
        dEPTISPATHOLOGICALDEP <- map[kChiefDEPTISPATHOLOGICALDEPKey]
        dOCTDOCTYPE <- map[kChiefDOCTDOCTYPEKey]
        dEPTDEPTYPEBYSERVICE <- map[kChiefDEPTDEPTYPEBYSERVICEKey]
        dEPTDATAPOSTTIME <- map[kChiefDEPTDATAPOSTTIMEKey]
        hITADATAPOSTDATEAD <- map[kChiefHITADATAPOSTDATEADKey]
        dEPTISMEMBERSHIPDEPT <- map[kChiefDEPTISMEMBERSHIPDEPTKey]
        dOCTISSMS <- map[kChiefDOCTISSMSKey]
        dEPTDEPORDER <- map[kChiefDEPTDEPORDERKey]
        dOCTGDOCNAME <- map[kChiefDOCTGDOCNAMEKey]
        dOCTDOCSHAREGROUP <- map[kChiefDOCTDOCSHAREGROUPKey]
        dOCTGDEPID <- map[kChiefDOCTGDEPIDKey]
        dOCTISHOD <- map[kChiefDOCTISHODKey]
        dOCTYPE <- map[kChiefDOCTYPEKey]
        hITADEPID <- map[kChiefHITADEPIDKey]
        dOCTROOMNO <- map[kChiefDOCTROOMNOKey]
        dOCTEMAIL <- map[kChiefDOCTEMAILKey]
        dOCTDEPCODE <- map[kChiefDOCTDEPCODEKey]
        dOCTISERDOCTOR <- map[kChiefDOCTISERDOCTORKey]
        hITADATAPOSTTIME <- map[kChiefHITADATAPOSTTIMEKey]
        dOCTPHONENO <- map[kChiefDOCTPHONENOKey]
        hITACOMPLAINID <- map[kChiefHITACOMPLAINIDKey]
        dOCTISBLKDOCTOR <- map[kChiefDOCTISBLKDOCTORKey]
        dEPTREPORTTITLE <- map[kChiefDEPTREPORTTITLEKey]
        dOCTDOCNAME <- map[kChiefDOCTDOCNAMEKey]
    }
    
}
public class ExData: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kExDataDOCTISDOCTORKey: String = "DOCT_ISDOCTOR"
    internal let kExDataDOCTMODIFYDATEKey: String = "DOCT_MODIFYDATE"
    internal let kExDataEXFODOCIDKey: String = "EXFO_DOCID"
    internal let kExDataDOCTPANNOKey: String = "DOCT_PANNO"
    internal let kExDataDEPTDEPCODEKey: String = "DEPT_DEPCODE"
    internal let kExDataEXFOVISITIDKey: String = "EXFO_VISITID"
    internal let kExDataDOCTDESIGNATIONIDKey: String = "DOCT_DESIGNATIONID"
    internal let kExDataDOCTISNONEHOSDOCSTAFFKey: String = "DOCT_ISNONEHOSDOCSTAFF"
    internal let kExDataDOCTISSURGEONKey: String = "DOCT_ISSURGEON"
    internal let kExDataDOCTISONCALLDOCTORKey: String = "DOCT_ISONCALLDOCTOR"
    internal let kExDataDEPTISPARENTDEPKey: String = "DEPT_ISPARENTDEP"
    internal let kExDataDOCTISINHOUSESTAFFDOCTORKey: String = "DOCT_ISINHOUSESTAFFDOCTOR"
    internal let kExDataDEPTPARENTDEPIDKey: String = "DEPT_PARENTDEPID"
    internal let kExDataDOCTMACIDKey: String = "DOCT_MACID"
    internal let kExDataDEPTROOMNOKey: String = "DEPT_ROOMNO"
    internal let kExDataDOCTISANAESTHETISTKey: String = "DOCT_ISANAESTHETIST"
    internal let kExDataDOCTGDOCIDKey: String = "DOCT_GDOCID"
    internal let kExDataDOCTPAYMENTBYKey: String = "DOCT_PAYMENTBY"
    internal let kExDataDOCTISREFERRALKey: String = "DOCT_ISREFERRAL"
    internal let kExDataDOCTACCOUNTHEADIDOPDKey: String = "DOCT_ACCOUNTHEADIDOPD"
    internal let kExDataDOCTZIPCODEKey: String = "DOCT_ZIPCODE"
    internal let kExDataEXFODATAPOSTDATEADKey: String = "EXFO_DATAPOSTDATEAD"
    internal let kExDataDOCTFUNDRATEKey: String = "DOCT_FUNDRATE"
    internal let kExDataDOCTMODIFYBYKey: String = "DOCT_MODIFYBY"
    internal let kExDataDOCTMODIFYTIMEKey: String = "DOCT_MODIFYTIME"
    internal let kExDataEXFODATEKey: String = "EXFO_DATE"
    internal let kExDataDOCTDOCIDKey: String = "DOCT_DOCID"
    internal let kExDataDOCTMOBILENOKey: String = "DOCT_MOBILENO"
    internal let kExDataDEPTGDEPNAMEKey: String = "DEPT_GDEPNAME"
    internal let kExDataEXFOTIMEKey: String = "EXFO_TIME"
    internal let kExDataDOCTREFDOCIDKey: String = "DOCT_REFDOCID"
    internal let kExDataDEPTGDEPIDKey: String = "DEPT_GDEPID"
    internal let kExDataDOCTISUNITKey: String = "DOCT_ISUNIT"
    internal let kExDataDEPTISFEMININEDEPTKey: String = "DEPT_ISFEMININEDEPT"
    internal let kExDataDOCTDATAPOSTTIMEKey: String = "DOCT_DATAPOSTTIME"
    internal let kExDataEXFOINPATIENTIDKey: String = "EXFO_INPATIENTID"
    internal let kExDataDOCTNMCNOKey: String = "DOCT_NMCNO"
    internal let kExDataDEPTISEMERGENCYDEPTKey: String = "DEPT_ISEMERGENCYDEPT"
    internal let kExDataDEPTSUBPARENTDEPIDKey: String = "DEPT_SUBPARENTDEPID"
    internal let kExDataDOCTISACTIVEKey: String = "DOCT_ISACTIVE"
    internal let kExDataDOCTISERSTAFFKey: String = "DOCT_ISERSTAFF"
    internal let kExDataDEPTDEPNAMEKey: String = "DEPT_DEPNAME"
    internal let kExDataDOCTDISPLAYNAMEKey: String = "DOCT_DISPLAYNAME"
    internal let kExDataDEPTDATAPOSTBYKey: String = "DEPT_DATAPOSTBY"
    internal let kExDataDOCTDOCPAYTYPEKey: String = "DOCT_DOCPAYTYPE"
    internal let kExDataDEPTACCHEADIDKey: String = "DEPT_ACCHEADID"
    internal let kExDataEXFOPATIENTIDKey: String = "EXFO_PATIENTID"
    internal let kExDataDOCTSPECIALIZATIONKey: String = "DOCT_SPECIALIZATION"
    internal let kExDataDOCTDOCORDERKey: String = "DOCT_DOCORDER"
    internal let kExDataDOCTDOCCODEKey: String = "DOCT_DOCCODE"
    internal let kExDataDEPTISACTIVEKey: String = "DEPT_ISACTIVE"
    internal let kExDataDOCTSTATUSKey: String = "DOCT_STATUS"
    internal let kExDataDEPTDEPIDKey: String = "DEPT_DEPID"
    internal let kExDataDOCTQUALIFICATIONKey: String = "DOCT_QUALIFICATION"
    internal let kExDataDEPTDEPTYPEKey: String = "DEPT_DEPTYPE"
    internal let kExDataDEPTEXTENDEDREPORTKey: String = "DEPT_EXTENDED_REPORT"
    internal let kExDataDOCTDEPIDKey: String = "DOCT_DEPID"
    internal let kExDataDEPTTESTNAMECODEPREFIXKey: String = "DEPT_TESTNAMECODEPREFIX"
    internal let kExDataDOCTPISDOCIDKey: String = "DOCT_PISDOCID"
    internal let kExDataDOCTDATAPOSTBYKey: String = "DOCT_DATAPOSTBY"
    internal let kExDataDOCTIMAGEKey: String = "DOCT_IMAGE"
    internal let kExDataDOCTTDSPERKey: String = "DOCT_TDSPER"
    internal let kExDataDOCTISENDODOCTORKey: String = "DOCT_ISENDODOCTOR"
    internal let kExDataDOCTDESIGKey: String = "DOCT_DESIG"
    internal let kExDataDOCTISLABDOCTORKey: String = "DOCT_ISLABDOCTOR"
    internal let kExDataDEPTDATAPOSTDATEKey: String = "DEPT_DATAPOSTDATE"
    internal let kExDataDOCTORGIDKey: String = "DOCT_ORGID"
    internal let kExDataDOCTDEPTYPEKey: String = "DOCT_DEPTYPE"
    internal let kExDataDOCTACCOUNTHEADIDIPDKey: String = "DOCT_ACCOUNTHEADIDIPD"
    internal let kExDataDEPTISREFERRALDEPTKey: String = "DEPT_ISREFERRALDEPT"
    internal let kExDataDEPTISREFERALCOMPKey: String = "DEPT_ISREFERALCOMP"
    internal let kExDataDOCTISOTASSISTANTKey: String = "DOCT_ISOTASSISTANT"
    internal let kExDataDOCTDATAPOSTDATEKey: String = "DOCT_DATAPOSTDATE"
    internal let kExDataDOCTPOSITIONIDKey: String = "DOCT_POSITIONID"
    internal let kExDataDOCTISQUEUENOGENINREGKey: String = "DOCT_ISQUEUENOGENINREG"
    internal let kExDataEXFODATAPOSTDATEKey: String = "EXFO_DATAPOSTDATE"
    internal let kExDataDEPTORGIDKey: String = "DEPT_ORGID"
    internal let kExDataDEPTISPATHOLOGICALDEPKey: String = "DEPT_ISPATHOLOGICALDEP"
    internal let kExDataEXFOIMAGEKey: String = "EXFO_IMAGE"
    internal let kExDataDOCTDOCTYPEKey: String = "DOCT_DOCTYPE"
    internal let kExDataDEPTDEPTYPEBYSERVICEKey: String = "DEPT_DEPTYPEBYSERVICE"
    internal let kExDataDEPTDATAPOSTTIMEKey: String = "DEPT_DATAPOSTTIME"
    internal let kExDataDEPTISMEMBERSHIPDEPTKey: String = "DEPT_ISMEMBERSHIPDEPT"
    internal let kExDataDOCTISSMSKey: String = "DOCT_ISSMS"
    internal let kExDataDEPTDEPORDERKey: String = "DEPT_DEPORDER"
    internal let kExDataDOCTGDOCNAMEKey: String = "DOCT_GDOCNAME"
    internal let kExDataDOCTDOCSHAREGROUPKey: String = "DOCT_DOCSHAREGROUP"
    internal let kExDataDOCTGDEPIDKey: String = "DOCT_GDEPID"
    internal let kExDataEXFOEXIDKey: String = "EXFO_EXID"
    internal let kExDataDOCTISHODKey: String = "DOCT_ISHOD"
    internal let kExDataDOCTYPEKey: String = "DOCTYPE"
    internal let kExDataDOCTROOMNOKey: String = "DOCT_ROOMNO"
    internal let kExDataEXFODEPIDKey: String = "EXFO_DEPID"
    internal let kExDataEXFOENTEREDBYKey: String = "EXFO_ENTEREDBY"
    internal let kExDataEXFODESCRIPTIONKey: String = "EXFO_DESCRIPTION"
    internal let kExDataEXFODATAPOSTTIMEKey: String = "EXFO_DATAPOSTTIME"
    internal let kExDataDOCTEMAILKey: String = "DOCT_EMAIL"
    internal let kExDataEXFOFROMKey: String = "EXFO_FROM"
    internal let kExDataDOCTDEPCODEKey: String = "DOCT_DEPCODE"
    internal let kExDataDOCTISERDOCTORKey: String = "DOCT_ISERDOCTOR"
    internal let kExDataDOCTPHONENOKey: String = "DOCT_PHONENO"
    internal let kExDataEXFOIMEKey: String = "EXFO_IME"
    internal let kExDataDOCTISBLKDOCTORKey: String = "DOCT_ISBLKDOCTOR"
    internal let kExDataDEPTREPORTTITLEKey: String = "DEPT_REPORTTITLE"
    internal let kExDataDOCTDOCNAMEKey: String = "DOCT_DOCNAME"


    // MARK: Properties
    public var dOCTISDOCTOR: String?
    public var dOCTMODIFYDATE: String?
    public var eXFODOCID: String?
    public var dOCTPANNO: String?
    public var dEPTDEPCODE: String?
    public var eXFOVISITID: String?
    public var dOCTDESIGNATIONID: String?
    public var dOCTISNONEHOSDOCSTAFF: String?
    public var dOCTISSURGEON: String?
    public var dOCTISONCALLDOCTOR: String?
    public var dEPTISPARENTDEP: String?
    public var dOCTISINHOUSESTAFFDOCTOR: String?
    public var dEPTPARENTDEPID: String?
    public var dOCTMACID: String?
    public var dEPTROOMNO: String?
    public var dOCTISANAESTHETIST: String?
    public var dOCTGDOCID: String?
    public var dOCTPAYMENTBY: String?
    public var dOCTISREFERRAL: String?
    public var dOCTACCOUNTHEADIDOPD: String?
    public var dOCTZIPCODE: String?
    public var eXFODATAPOSTDATEAD: String?
    public var dOCTFUNDRATE: String?
    public var dOCTMODIFYBY: String?
    public var dOCTMODIFYTIME: String?
    public var eXFODATE: String?
    public var dOCTDOCID: String?
    public var dOCTMOBILENO: String?
    public var dEPTGDEPNAME: String?
    public var eXFOTIME: String?
    public var dOCTREFDOCID: String?
    public var dEPTGDEPID: String?
    public var dOCTISUNIT: String?
    public var dEPTISFEMININEDEPT: String?
    public var dOCTDATAPOSTTIME: String?
    public var eXFOINPATIENTID: String?
    public var dOCTNMCNO: String?
    public var dEPTISEMERGENCYDEPT: String?
    public var dEPTSUBPARENTDEPID: String?
    public var dOCTISACTIVE: String?
    public var dOCTISERSTAFF: String?
    public var dEPTDEPNAME: String?
    public var dOCTDISPLAYNAME: String?
    public var dEPTDATAPOSTBY: String?
    public var dOCTDOCPAYTYPE: String?
    public var dEPTACCHEADID: String?
    public var eXFOPATIENTID: String?
    public var dOCTSPECIALIZATION: String?
    public var dOCTDOCORDER: String?
    public var dOCTDOCCODE: String?
    public var dEPTISACTIVE: String?
    public var dOCTSTATUS: String?
    public var dEPTDEPID: String?
    public var dOCTQUALIFICATION: String?
    public var dEPTDEPTYPE: String?
    public var dEPTEXTENDEDREPORT: String?
    public var dOCTDEPID: String?
    public var dEPTTESTNAMECODEPREFIX: String?
    public var dOCTPISDOCID: String?
    public var dOCTDATAPOSTBY: String?
    public var dOCTIMAGE: String?
    public var dOCTTDSPER: String?
    public var dOCTISENDODOCTOR: String?
    public var dOCTDESIG: String?
    public var dOCTISLABDOCTOR: String?
    public var dEPTDATAPOSTDATE: String?
    public var dOCTORGID: String?
    public var dOCTDEPTYPE: String?
    public var dOCTACCOUNTHEADIDIPD: String?
    public var dEPTISREFERRALDEPT: String?
    public var dEPTISREFERALCOMP: String?
    public var dOCTISOTASSISTANT: String?
    public var dOCTDATAPOSTDATE: String?
    public var dOCTPOSITIONID: String?
    public var dOCTISQUEUENOGENINREG: String?
    public var eXFODATAPOSTDATE: String?
    public var dEPTORGID: String?
    public var dEPTISPATHOLOGICALDEP: String?
    public var eXFOIMAGE: String?
    public var dOCTDOCTYPE: String?
    public var dEPTDEPTYPEBYSERVICE: String?
    public var dEPTDATAPOSTTIME: String?
    public var dEPTISMEMBERSHIPDEPT: String?
    public var dOCTISSMS: String?
    public var dEPTDEPORDER: String?
    public var dOCTGDOCNAME: String?
    public var dOCTDOCSHAREGROUP: String?
    public var dOCTGDEPID: String?
    public var eXFOEXID: String?
    public var dOCTISHOD: String?
    public var dOCTYPE: String?
    public var dOCTROOMNO: String?
    public var eXFODEPID: String?
    public var eXFOENTEREDBY: String?
    public var eXFODESCRIPTION: String?
    public var eXFODATAPOSTTIME: String?
    public var dOCTEMAIL: String?
    public var eXFOFROM: String?
    public var dOCTDEPCODE: String?
    public var dOCTISERDOCTOR: String?
    public var dOCTPHONENO: String?
    public var eXFOIME: String?
    public var dOCTISBLKDOCTOR: String?
    public var dEPTREPORTTITLE: String?
    public var dOCTDOCNAME: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
   

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        dOCTISDOCTOR <- map[kExDataDOCTISDOCTORKey]
        dOCTMODIFYDATE <- map[kExDataDOCTMODIFYDATEKey]
        eXFODOCID <- map[kExDataEXFODOCIDKey]
        dOCTPANNO <- map[kExDataDOCTPANNOKey]
        dEPTDEPCODE <- map[kExDataDEPTDEPCODEKey]
        eXFOVISITID <- map[kExDataEXFOVISITIDKey]
        dOCTDESIGNATIONID <- map[kExDataDOCTDESIGNATIONIDKey]
        dOCTISNONEHOSDOCSTAFF <- map[kExDataDOCTISNONEHOSDOCSTAFFKey]
        dOCTISSURGEON <- map[kExDataDOCTISSURGEONKey]
        dOCTISONCALLDOCTOR <- map[kExDataDOCTISONCALLDOCTORKey]
        dEPTISPARENTDEP <- map[kExDataDEPTISPARENTDEPKey]
        dOCTISINHOUSESTAFFDOCTOR <- map[kExDataDOCTISINHOUSESTAFFDOCTORKey]
        dEPTPARENTDEPID <- map[kExDataDEPTPARENTDEPIDKey]
        dOCTMACID <- map[kExDataDOCTMACIDKey]
        dEPTROOMNO <- map[kExDataDEPTROOMNOKey]
        dOCTISANAESTHETIST <- map[kExDataDOCTISANAESTHETISTKey]
        dOCTGDOCID <- map[kExDataDOCTGDOCIDKey]
        dOCTPAYMENTBY <- map[kExDataDOCTPAYMENTBYKey]
        dOCTISREFERRAL <- map[kExDataDOCTISREFERRALKey]
        dOCTACCOUNTHEADIDOPD <- map[kExDataDOCTACCOUNTHEADIDOPDKey]
        dOCTZIPCODE <- map[kExDataDOCTZIPCODEKey]
        eXFODATAPOSTDATEAD <- map[kExDataEXFODATAPOSTDATEADKey]
        dOCTFUNDRATE <- map[kExDataDOCTFUNDRATEKey]
        dOCTMODIFYBY <- map[kExDataDOCTMODIFYBYKey]
        dOCTMODIFYTIME <- map[kExDataDOCTMODIFYTIMEKey]
        eXFODATE <- map[kExDataEXFODATEKey]
        dOCTDOCID <- map[kExDataDOCTDOCIDKey]
        dOCTMOBILENO <- map[kExDataDOCTMOBILENOKey]
        dEPTGDEPNAME <- map[kExDataDEPTGDEPNAMEKey]
        eXFOTIME <- map[kExDataEXFOTIMEKey]
        dOCTREFDOCID <- map[kExDataDOCTREFDOCIDKey]
        dEPTGDEPID <- map[kExDataDEPTGDEPIDKey]
        dOCTISUNIT <- map[kExDataDOCTISUNITKey]
        dEPTISFEMININEDEPT <- map[kExDataDEPTISFEMININEDEPTKey]
        dOCTDATAPOSTTIME <- map[kExDataDOCTDATAPOSTTIMEKey]
        eXFOINPATIENTID <- map[kExDataEXFOINPATIENTIDKey]
        dOCTNMCNO <- map[kExDataDOCTNMCNOKey]
        dEPTISEMERGENCYDEPT <- map[kExDataDEPTISEMERGENCYDEPTKey]
        dEPTSUBPARENTDEPID <- map[kExDataDEPTSUBPARENTDEPIDKey]
        dOCTISACTIVE <- map[kExDataDOCTISACTIVEKey]
        dOCTISERSTAFF <- map[kExDataDOCTISERSTAFFKey]
        dEPTDEPNAME <- map[kExDataDEPTDEPNAMEKey]
        dOCTDISPLAYNAME <- map[kExDataDOCTDISPLAYNAMEKey]
        dEPTDATAPOSTBY <- map[kExDataDEPTDATAPOSTBYKey]
        dOCTDOCPAYTYPE <- map[kExDataDOCTDOCPAYTYPEKey]
        dEPTACCHEADID <- map[kExDataDEPTACCHEADIDKey]
        eXFOPATIENTID <- map[kExDataEXFOPATIENTIDKey]
        dOCTSPECIALIZATION <- map[kExDataDOCTSPECIALIZATIONKey]
        dOCTDOCORDER <- map[kExDataDOCTDOCORDERKey]
        dOCTDOCCODE <- map[kExDataDOCTDOCCODEKey]
        dEPTISACTIVE <- map[kExDataDEPTISACTIVEKey]
        dOCTSTATUS <- map[kExDataDOCTSTATUSKey]
        dEPTDEPID <- map[kExDataDEPTDEPIDKey]
        dOCTQUALIFICATION <- map[kExDataDOCTQUALIFICATIONKey]
        dEPTDEPTYPE <- map[kExDataDEPTDEPTYPEKey]
        dEPTEXTENDEDREPORT <- map[kExDataDEPTEXTENDEDREPORTKey]
        dOCTDEPID <- map[kExDataDOCTDEPIDKey]
        dEPTTESTNAMECODEPREFIX <- map[kExDataDEPTTESTNAMECODEPREFIXKey]
        dOCTPISDOCID <- map[kExDataDOCTPISDOCIDKey]
        dOCTDATAPOSTBY <- map[kExDataDOCTDATAPOSTBYKey]
        dOCTIMAGE <- map[kExDataDOCTIMAGEKey]
        dOCTTDSPER <- map[kExDataDOCTTDSPERKey]
        dOCTISENDODOCTOR <- map[kExDataDOCTISENDODOCTORKey]
        dOCTDESIG <- map[kExDataDOCTDESIGKey]
        dOCTISLABDOCTOR <- map[kExDataDOCTISLABDOCTORKey]
        dEPTDATAPOSTDATE <- map[kExDataDEPTDATAPOSTDATEKey]
        dOCTORGID <- map[kExDataDOCTORGIDKey]
        dOCTDEPTYPE <- map[kExDataDOCTDEPTYPEKey]
        dOCTACCOUNTHEADIDIPD <- map[kExDataDOCTACCOUNTHEADIDIPDKey]
        dEPTISREFERRALDEPT <- map[kExDataDEPTISREFERRALDEPTKey]
        dEPTISREFERALCOMP <- map[kExDataDEPTISREFERALCOMPKey]
        dOCTISOTASSISTANT <- map[kExDataDOCTISOTASSISTANTKey]
        dOCTDATAPOSTDATE <- map[kExDataDOCTDATAPOSTDATEKey]
        dOCTPOSITIONID <- map[kExDataDOCTPOSITIONIDKey]
        dOCTISQUEUENOGENINREG <- map[kExDataDOCTISQUEUENOGENINREGKey]
        eXFODATAPOSTDATE <- map[kExDataEXFODATAPOSTDATEKey]
        dEPTORGID <- map[kExDataDEPTORGIDKey]
        dEPTISPATHOLOGICALDEP <- map[kExDataDEPTISPATHOLOGICALDEPKey]
        eXFOIMAGE <- map[kExDataEXFOIMAGEKey]
        dOCTDOCTYPE <- map[kExDataDOCTDOCTYPEKey]
        dEPTDEPTYPEBYSERVICE <- map[kExDataDEPTDEPTYPEBYSERVICEKey]
        dEPTDATAPOSTTIME <- map[kExDataDEPTDATAPOSTTIMEKey]
        dEPTISMEMBERSHIPDEPT <- map[kExDataDEPTISMEMBERSHIPDEPTKey]
        dOCTISSMS <- map[kExDataDOCTISSMSKey]
        dEPTDEPORDER <- map[kExDataDEPTDEPORDERKey]
        dOCTGDOCNAME <- map[kExDataDOCTGDOCNAMEKey]
        dOCTDOCSHAREGROUP <- map[kExDataDOCTDOCSHAREGROUPKey]
        dOCTGDEPID <- map[kExDataDOCTGDEPIDKey]
        eXFOEXID <- map[kExDataEXFOEXIDKey]
        dOCTISHOD <- map[kExDataDOCTISHODKey]
        dOCTYPE <- map[kExDataDOCTYPEKey]
        dOCTROOMNO <- map[kExDataDOCTROOMNOKey]
        eXFODEPID <- map[kExDataEXFODEPIDKey]
        eXFOENTEREDBY <- map[kExDataEXFOENTEREDBYKey]
        eXFODESCRIPTION <- map[kExDataEXFODESCRIPTIONKey]
        eXFODATAPOSTTIME <- map[kExDataEXFODATAPOSTTIMEKey]
        dOCTEMAIL <- map[kExDataDOCTEMAILKey]
        eXFOFROM <- map[kExDataEXFOFROMKey]
        dOCTDEPCODE <- map[kExDataDOCTDEPCODEKey]
        dOCTISERDOCTOR <- map[kExDataDOCTISERDOCTORKey]
        dOCTPHONENO <- map[kExDataDOCTPHONENOKey]
        eXFOIME <- map[kExDataEXFOIMEKey]
        dOCTISBLKDOCTOR <- map[kExDataDOCTISBLKDOCTORKey]
        dEPTREPORTTITLE <- map[kExDataDEPTREPORTTITLEKey]
        dOCTDOCNAME <- map[kExDataDOCTDOCNAMEKey]

    }
}
public class ExDiagnosis: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kExDiagnosisDOCTISDOCTORKey: String = "DOCT_ISDOCTOR"
    internal let kExDiagnosisDOCTMODIFYDATEKey: String = "DOCT_MODIFYDATE"
    internal let kExDiagnosisICLODATAPOSTBYKey: String = "ICLO_DATAPOSTBY"
    internal let kExDiagnosisDEPTDEPCODEKey: String = "DEPT_DEPCODE"
    internal let kExDiagnosisDOCTPANNOKey: String = "DOCT_PANNO"
    internal let kExDiagnosisDOCTDESIGNATIONIDKey: String = "DOCT_DESIGNATIONID"
    internal let kExDiagnosisICLOVISITIDKey: String = "ICLO_VISITID"
    internal let kExDiagnosisDOCTISNONEHOSDOCSTAFFKey: String = "DOCT_ISNONEHOSDOCSTAFF"
    internal let kExDiagnosisDOCTISONCALLDOCTORKey: String = "DOCT_ISONCALLDOCTOR"
    internal let kExDiagnosisDEPTISPARENTDEPKey: String = "DEPT_ISPARENTDEP"
    internal let kExDiagnosisDOCTMACIDKey: String = "DOCT_MACID"
    internal let kExDiagnosisDOCTISINHOUSESTAFFDOCTORKey: String = "DOCT_ISINHOUSESTAFFDOCTOR"
    internal let kExDiagnosisDOCTISSURGEONKey: String = "DOCT_ISSURGEON"
    internal let kExDiagnosisDEPTPARENTDEPIDKey: String = "DEPT_PARENTDEPID"
    internal let kExDiagnosisDEPTROOMNOKey: String = "DEPT_ROOMNO"
    internal let kExDiagnosisDOCTISANAESTHETISTKey: String = "DOCT_ISANAESTHETIST"
    internal let kExDiagnosisICLOINACTIVEBYKey: String = "ICLO_INACTIVEBY"
    internal let kExDiagnosisDOCTGDOCIDKey: String = "DOCT_GDOCID"
    internal let kExDiagnosisICLOSTATUSKey: String = "ICLO_STATUS"
    internal let kExDiagnosisICLODIACODEKey: String = "ICLO_DIACODE"
    internal let kExDiagnosisICLOMODIFYMACIDKey: String = "ICLO_MODIFYMACID"
    internal let kExDiagnosisICLOPATIENTTYPEKey: String = "ICLO_PATIENTTYPE"
    internal let kExDiagnosisDOCTISREFERRALKey: String = "DOCT_ISREFERRAL"
    internal let kExDiagnosisDOCTPAYMENTBYKey: String = "DOCT_PAYMENTBY"
    internal let kExDiagnosisDOCTACCOUNTHEADIDOPDKey: String = "DOCT_ACCOUNTHEADIDOPD"
    internal let kExDiagnosisICLOISICDCODEKey: String = "ICLO_ISICDCODE"
    internal let kExDiagnosisDOCTZIPCODEKey: String = "DOCT_ZIPCODE"
    internal let kExDiagnosisDOCTFUNDRATEKey: String = "DOCT_FUNDRATE"
    internal let kExDiagnosisDOCTMODIFYBYKey: String = "DOCT_MODIFYBY"
    internal let kExDiagnosisDOCTMODIFYTIMEKey: String = "DOCT_MODIFYTIME"
    internal let kExDiagnosisDOCTDOCIDKey: String = "DOCT_DOCID"
    internal let kExDiagnosisICLOINACTIVEBTIMEKey: String = "ICLO_INACTIVEBTIME"
    internal let kExDiagnosisICLODEPIDKey: String = "ICLO_DEPID"
    internal let kExDiagnosisDEPTGDEPNAMEKey: String = "DEPT_GDEPNAME"
    internal let kExDiagnosisDOCTMOBILENOKey: String = "DOCT_MOBILENO"
    internal let kExDiagnosisICLOCATEGORYNAMEKey: String = "ICLO_CATEGORYNAME"
    internal let kExDiagnosisDOCTREFDOCIDKey: String = "DOCT_REFDOCID"
    internal let kExDiagnosisDEPTGDEPIDKey: String = "DEPT_GDEPID"
    internal let kExDiagnosisDOCTISUNITKey: String = "DOCT_ISUNIT"
    internal let kExDiagnosisICLOMODIFIEDDATEKey: String = "ICLO_MODIFIEDDATE"
    internal let kExDiagnosisICLOINPATIENTIDKey: String = "ICLO_INPATIENTID"
    internal let kExDiagnosisDEPTISFEMININEDEPTKey: String = "DEPT_ISFEMININEDEPT"
    internal let kExDiagnosisICLOBLOCKNAMEKey: String = "ICLO_BLOCKNAME"
    internal let kExDiagnosisDOCTDATAPOSTTIMEKey: String = "DOCT_DATAPOSTTIME"
    internal let kExDiagnosisDOCTNMCNOKey: String = "DOCT_NMCNO"
    internal let kExDiagnosisDEPTISEMERGENCYDEPTKey: String = "DEPT_ISEMERGENCYDEPT"
    internal let kExDiagnosisDEPTSUBPARENTDEPIDKey: String = "DEPT_SUBPARENTDEPID"
    internal let kExDiagnosisDOCTISACTIVEKey: String = "DOCT_ISACTIVE"
    internal let kExDiagnosisICLOPATIENTIDKey: String = "ICLO_PATIENTID"
    internal let kExDiagnosisDOCTISERSTAFFKey: String = "DOCT_ISERSTAFF"
    internal let kExDiagnosisDEPTDEPNAMEKey: String = "DEPT_DEPNAME"
    internal let kExDiagnosisDOCTDISPLAYNAMEKey: String = "DOCT_DISPLAYNAME"
    internal let kExDiagnosisDEPTDATAPOSTBYKey: String = "DEPT_DATAPOSTBY"
    internal let kExDiagnosisDOCTDOCPAYTYPEKey: String = "DOCT_DOCPAYTYPE"
    internal let kExDiagnosisICLOCHAPTERNAMEKey: String = "ICLO_CHAPTERNAME"
    internal let kExDiagnosisICLODIAGNOSISTYPEKey: String = "ICLO_DIAGNOSISTYPE"
    internal let kExDiagnosisDEPTACCHEADIDKey: String = "DEPT_ACCHEADID"
    internal let kExDiagnosisICLOMODIFIEDBYKey: String = "ICLO_MODIFIEDBY"
    internal let kExDiagnosisICLOFINALDIAGONOSISIDKey: String = "ICLO_FINALDIAGONOSISID"
    internal let kExDiagnosisDOCTSPECIALIZATIONKey: String = "DOCT_SPECIALIZATION"
    internal let kExDiagnosisDOCTDOCORDERKey: String = "DOCT_DOCORDER"
    internal let kExDiagnosisDEPTDEPIDKey: String = "DEPT_DEPID"
    internal let kExDiagnosisDEPTISACTIVEKey: String = "DEPT_ISACTIVE"
    internal let kExDiagnosisDOCTDOCCODEKey: String = "DOCT_DOCCODE"
    internal let kExDiagnosisDOCTQUALIFICATIONKey: String = "DOCT_QUALIFICATION"
    internal let kExDiagnosisDOCTSTATUSKey: String = "DOCT_STATUS"
    internal let kExDiagnosisICLODATAPOSTTIMEKey: String = "ICLO_DATAPOSTTIME"
    internal let kExDiagnosisICLODEPARTMENTKey: String = "ICLO_DEPARTMENT"
    internal let kExDiagnosisDEPTDEPTYPEKey: String = "DEPT_DEPTYPE"
    internal let kExDiagnosisDEPTEXTENDEDREPORTKey: String = "DEPT_EXTENDED_REPORT"
    internal let kExDiagnosisDOCTDEPIDKey: String = "DOCT_DEPID"
    internal let kExDiagnosisICLODIAGNOSISIDKey: String = "ICLO_DIAGNOSISID"
    internal let kExDiagnosisDEPTTESTNAMECODEPREFIXKey: String = "DEPT_TESTNAMECODEPREFIX"
    internal let kExDiagnosisDOCTPISDOCIDKey: String = "DOCT_PISDOCID"
    internal let kExDiagnosisICLOLOCALDIAGONOSISIDKey: String = "ICLO_LOCALDIAGONOSISID"
    internal let kExDiagnosisDOCTDATAPOSTBYKey: String = "DOCT_DATAPOSTBY"
    internal let kExDiagnosisICLODOCTORKey: String = "ICLO_DOCTOR"
    internal let kExDiagnosisDOCTTDSPERKey: String = "DOCT_TDSPER"
    internal let kExDiagnosisDOCTIMAGEKey: String = "DOCT_IMAGE"
    internal let kExDiagnosisDOCTISENDODOCTORKey: String = "DOCT_ISENDODOCTOR"
    internal let kExDiagnosisDOCTDESIGKey: String = "DOCT_DESIG"
    internal let kExDiagnosisDOCTISLABDOCTORKey: String = "DOCT_ISLABDOCTOR"
    internal let kExDiagnosisDEPTDATAPOSTDATEKey: String = "DEPT_DATAPOSTDATE"
    internal let kExDiagnosisDOCTORGIDKey: String = "DOCT_ORGID"
    internal let kExDiagnosisICLOISACTIVEKey: String = "ICLO_ISACTIVE"
    internal let kExDiagnosisDOCTACCOUNTHEADIDIPDKey: String = "DOCT_ACCOUNTHEADIDIPD"
    internal let kExDiagnosisDOCTDEPTYPEKey: String = "DOCT_DEPTYPE"
    internal let kExDiagnosisDEPTISREFERRALDEPTKey: String = "DEPT_ISREFERRALDEPT"
    internal let kExDiagnosisICLOINACTIVEBDATEKey: String = "ICLO_INACTIVEBDATE"
    internal let kExDiagnosisDOCTISOTASSISTANTKey: String = "DOCT_ISOTASSISTANT"
    internal let kExDiagnosisICLOMODIFIEDTIMEKey: String = "ICLO_MODIFIEDTIME"
    internal let kExDiagnosisDEPTISREFERALCOMPKey: String = "DEPT_ISREFERALCOMP"
    internal let kExDiagnosisDOCTDATAPOSTDATEKey: String = "DOCT_DATAPOSTDATE"
    internal let kExDiagnosisICLOLOCALDIAGONOSISNAMEKey: String = "ICLO_LOCALDIAGONOSISNAME"
    internal let kExDiagnosisICLOMACIDKey: String = "ICLO_MACID"
    internal let kExDiagnosisDOCTPOSITIONIDKey: String = "DOCT_POSITIONID"
    internal let kExDiagnosisICLODEPNAMEKey: String = "ICLO_DEPNAME"
    internal let kExDiagnosisDOCTISQUEUENOGENINREGKey: String = "DOCT_ISQUEUENOGENINREG"
    internal let kExDiagnosisICLOFROMKey: String = "ICLO_FROM"
    internal let kExDiagnosisICLODIAGNOSISKey: String = "ICLO_DIAGNOSIS"
    internal let kExDiagnosisDEPTISPATHOLOGICALDEPKey: String = "DEPT_ISPATHOLOGICALDEP"
    internal let kExDiagnosisDEPTORGIDKey: String = "DEPT_ORGID"
    internal let kExDiagnosisDOCTDOCTYPEKey: String = "DOCT_DOCTYPE"
    internal let kExDiagnosisICLODATAPOSTDATEKey: String = "ICLO_DATAPOSTDATE"
    internal let kExDiagnosisDEPTDEPTYPEBYSERVICEKey: String = "DEPT_DEPTYPEBYSERVICE"
    internal let kExDiagnosisDEPTDATAPOSTTIMEKey: String = "DEPT_DATAPOSTTIME"
    internal let kExDiagnosisDOCTISSMSKey: String = "DOCT_ISSMS"
    internal let kExDiagnosisDEPTISMEMBERSHIPDEPTKey: String = "DEPT_ISMEMBERSHIPDEPT"
    internal let kExDiagnosisDEPTDEPORDERKey: String = "DEPT_DEPORDER"
    internal let kExDiagnosisDOCTGDOCNAMEKey: String = "DOCT_GDOCNAME"
    internal let kExDiagnosisDOCTDOCSHAREGROUPKey: String = "DOCT_DOCSHAREGROUP"
    internal let kExDiagnosisDOCTGDEPIDKey: String = "DOCT_GDEPID"
    internal let kExDiagnosisDOCTISHODKey: String = "DOCT_ISHOD"
    internal let kExDiagnosisDOCTYPEKey: String = "DOCTYPE"
    internal let kExDiagnosisDOCTROOMNOKey: String = "DOCT_ROOMNO"
    internal let kExDiagnosisDOCTEMAILKey: String = "DOCT_EMAIL"
    internal let kExDiagnosisICLOOPERATIONPLANIDKey: String = "ICLO_OPERATIONPLANID"
    internal let kExDiagnosisDOCTDEPCODEKey: String = "DOCT_DEPCODE"
    internal let kExDiagnosisDOCTISERDOCTORKey: String = "DOCT_ISERDOCTOR"
    internal let kExDiagnosisDOCTPHONENOKey: String = "DOCT_PHONENO"
    internal let kExDiagnosisDOCTISBLKDOCTORKey: String = "DOCT_ISBLKDOCTOR"
    internal let kExDiagnosisDEPTREPORTTITLEKey: String = "DEPT_REPORTTITLE"
    internal let kExDiagnosisDOCTDOCNAMEKey: String = "DOCT_DOCNAME"


    // MARK: Properties
    public var dOCTISDOCTOR: String?
    public var dOCTMODIFYDATE: String?
    public var iCLODATAPOSTBY: String?
    public var dEPTDEPCODE: String?
    public var dOCTPANNO: String?
    public var dOCTDESIGNATIONID: String?
    public var iCLOVISITID: String?
    public var dOCTISNONEHOSDOCSTAFF: String?
    public var dOCTISONCALLDOCTOR: String?
    public var dEPTISPARENTDEP: String?
    public var dOCTMACID: String?
    public var dOCTISINHOUSESTAFFDOCTOR: String?
    public var dOCTISSURGEON: String?
    public var dEPTPARENTDEPID: String?
    public var dEPTROOMNO: String?
    public var dOCTISANAESTHETIST: String?
    public var iCLOINACTIVEBY: String?
    public var dOCTGDOCID: String?
    public var iCLOSTATUS: String?
    public var iCLODIACODE: String?
    public var iCLOMODIFYMACID: String?
    public var iCLOPATIENTTYPE: String?
    public var dOCTISREFERRAL: String?
    public var dOCTPAYMENTBY: String?
    public var dOCTACCOUNTHEADIDOPD: String?
    public var iCLOISICDCODE: String?
    public var dOCTZIPCODE: String?
    public var dOCTFUNDRATE: String?
    public var dOCTMODIFYBY: String?
    public var dOCTMODIFYTIME: String?
    public var dOCTDOCID: String?
    public var iCLOINACTIVEBTIME: String?
    public var iCLODEPID: String?
    public var dEPTGDEPNAME: String?
    public var dOCTMOBILENO: String?
    public var iCLOCATEGORYNAME: String?
    public var dOCTREFDOCID: String?
    public var dEPTGDEPID: String?
    public var dOCTISUNIT: String?
    public var iCLOMODIFIEDDATE: String?
    public var iCLOINPATIENTID: String?
    public var dEPTISFEMININEDEPT: String?
    public var iCLOBLOCKNAME: String?
    public var dOCTDATAPOSTTIME: String?
    public var dOCTNMCNO: String?
    public var dEPTISEMERGENCYDEPT: String?
    public var dEPTSUBPARENTDEPID: String?
    public var dOCTISACTIVE: String?
    public var iCLOPATIENTID: String?
    public var dOCTISERSTAFF: String?
    public var dEPTDEPNAME: String?
    public var dOCTDISPLAYNAME: String?
    public var dEPTDATAPOSTBY: String?
    public var dOCTDOCPAYTYPE: String?
    public var iCLOCHAPTERNAME: String?
    public var iCLODIAGNOSISTYPE: String?
    public var dEPTACCHEADID: String?
    public var iCLOMODIFIEDBY: String?
    public var iCLOFINALDIAGONOSISID: String?
    public var dOCTSPECIALIZATION: String?
    public var dOCTDOCORDER: String?
    public var dEPTDEPID: String?
    public var dEPTISACTIVE: String?
    public var dOCTDOCCODE: String?
    public var dOCTQUALIFICATION: String?
    public var dOCTSTATUS: String?
    public var iCLODATAPOSTTIME: String?
    public var iCLODEPARTMENT: String?
    public var dEPTDEPTYPE: String?
    public var dEPTEXTENDEDREPORT: String?
    public var dOCTDEPID: String?
    public var iCLODIAGNOSISID: String?
    public var dEPTTESTNAMECODEPREFIX: String?
    public var dOCTPISDOCID: String?
    public var iCLOLOCALDIAGONOSISID: String?
    public var dOCTDATAPOSTBY: String?
    public var iCLODOCTOR: String?
    public var dOCTTDSPER: String?
    public var dOCTIMAGE: String?
    public var dOCTISENDODOCTOR: String?
    public var dOCTDESIG: String?
    public var dOCTISLABDOCTOR: String?
    public var dEPTDATAPOSTDATE: String?
    public var dOCTORGID: String?
    public var iCLOISACTIVE: String?
    public var dOCTACCOUNTHEADIDIPD: String?
    public var dOCTDEPTYPE: String?
    public var dEPTISREFERRALDEPT: String?
    public var iCLOINACTIVEBDATE: String?
    public var dOCTISOTASSISTANT: String?
    public var iCLOMODIFIEDTIME: String?
    public var dEPTISREFERALCOMP: String?
    public var dOCTDATAPOSTDATE: String?
    public var iCLOLOCALDIAGONOSISNAME: String?
    public var iCLOMACID: String?
    public var dOCTPOSITIONID: String?
    public var iCLODEPNAME: String?
    public var dOCTISQUEUENOGENINREG: String?
    public var iCLOFROM: String?
    public var iCLODIAGNOSIS: String?
    public var dEPTISPATHOLOGICALDEP: String?
    public var dEPTORGID: String?
    public var dOCTDOCTYPE: String?
    public var iCLODATAPOSTDATE: String?
    public var dEPTDEPTYPEBYSERVICE: String?
    public var dEPTDATAPOSTTIME: String?
    public var dOCTISSMS: String?
    public var dEPTISMEMBERSHIPDEPT: String?
    public var dEPTDEPORDER: String?
    public var dOCTGDOCNAME: String?
    public var dOCTDOCSHAREGROUP: String?
    public var dOCTGDEPID: String?
    public var dOCTISHOD: String?
    public var dOCTYPE: String?
    public var dOCTROOMNO: String?
    public var dOCTEMAIL: String?
    public var iCLOOPERATIONPLANID: String?
    public var dOCTDEPCODE: String?
    public var dOCTISERDOCTOR: String?
    public var dOCTPHONENO: String?
    public var dOCTISBLKDOCTOR: String?
    public var dEPTREPORTTITLE: String?
    public var dOCTDOCNAME: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        dOCTISDOCTOR <- map[kExDiagnosisDOCTISDOCTORKey]
        dOCTMODIFYDATE <- map[kExDiagnosisDOCTMODIFYDATEKey]
        iCLODATAPOSTBY <- map[kExDiagnosisICLODATAPOSTBYKey]
        dEPTDEPCODE <- map[kExDiagnosisDEPTDEPCODEKey]
        dOCTPANNO <- map[kExDiagnosisDOCTPANNOKey]
        dOCTDESIGNATIONID <- map[kExDiagnosisDOCTDESIGNATIONIDKey]
        iCLOVISITID <- map[kExDiagnosisICLOVISITIDKey]
        dOCTISNONEHOSDOCSTAFF <- map[kExDiagnosisDOCTISNONEHOSDOCSTAFFKey]
        dOCTISONCALLDOCTOR <- map[kExDiagnosisDOCTISONCALLDOCTORKey]
        dEPTISPARENTDEP <- map[kExDiagnosisDEPTISPARENTDEPKey]
        dOCTMACID <- map[kExDiagnosisDOCTMACIDKey]
        dOCTISINHOUSESTAFFDOCTOR <- map[kExDiagnosisDOCTISINHOUSESTAFFDOCTORKey]
        dOCTISSURGEON <- map[kExDiagnosisDOCTISSURGEONKey]
        dEPTPARENTDEPID <- map[kExDiagnosisDEPTPARENTDEPIDKey]
        dEPTROOMNO <- map[kExDiagnosisDEPTROOMNOKey]
        dOCTISANAESTHETIST <- map[kExDiagnosisDOCTISANAESTHETISTKey]
        iCLOINACTIVEBY <- map[kExDiagnosisICLOINACTIVEBYKey]
        dOCTGDOCID <- map[kExDiagnosisDOCTGDOCIDKey]
        iCLOSTATUS <- map[kExDiagnosisICLOSTATUSKey]
        iCLODIACODE <- map[kExDiagnosisICLODIACODEKey]
        iCLOMODIFYMACID <- map[kExDiagnosisICLOMODIFYMACIDKey]
        iCLOPATIENTTYPE <- map[kExDiagnosisICLOPATIENTTYPEKey]
        dOCTISREFERRAL <- map[kExDiagnosisDOCTISREFERRALKey]
        dOCTPAYMENTBY <- map[kExDiagnosisDOCTPAYMENTBYKey]
        dOCTACCOUNTHEADIDOPD <- map[kExDiagnosisDOCTACCOUNTHEADIDOPDKey]
        iCLOISICDCODE <- map[kExDiagnosisICLOISICDCODEKey]
        dOCTZIPCODE <- map[kExDiagnosisDOCTZIPCODEKey]
        dOCTFUNDRATE <- map[kExDiagnosisDOCTFUNDRATEKey]
        dOCTMODIFYBY <- map[kExDiagnosisDOCTMODIFYBYKey]
        dOCTMODIFYTIME <- map[kExDiagnosisDOCTMODIFYTIMEKey]
        dOCTDOCID <- map[kExDiagnosisDOCTDOCIDKey]
        iCLOINACTIVEBTIME <- map[kExDiagnosisICLOINACTIVEBTIMEKey]
        iCLODEPID <- map[kExDiagnosisICLODEPIDKey]
        dEPTGDEPNAME <- map[kExDiagnosisDEPTGDEPNAMEKey]
        dOCTMOBILENO <- map[kExDiagnosisDOCTMOBILENOKey]
        iCLOCATEGORYNAME <- map[kExDiagnosisICLOCATEGORYNAMEKey]
        dOCTREFDOCID <- map[kExDiagnosisDOCTREFDOCIDKey]
        dEPTGDEPID <- map[kExDiagnosisDEPTGDEPIDKey]
        dOCTISUNIT <- map[kExDiagnosisDOCTISUNITKey]
        iCLOMODIFIEDDATE <- map[kExDiagnosisICLOMODIFIEDDATEKey]
        iCLOINPATIENTID <- map[kExDiagnosisICLOINPATIENTIDKey]
        dEPTISFEMININEDEPT <- map[kExDiagnosisDEPTISFEMININEDEPTKey]
        iCLOBLOCKNAME <- map[kExDiagnosisICLOBLOCKNAMEKey]
        dOCTDATAPOSTTIME <- map[kExDiagnosisDOCTDATAPOSTTIMEKey]
        dOCTNMCNO <- map[kExDiagnosisDOCTNMCNOKey]
        dEPTISEMERGENCYDEPT <- map[kExDiagnosisDEPTISEMERGENCYDEPTKey]
        dEPTSUBPARENTDEPID <- map[kExDiagnosisDEPTSUBPARENTDEPIDKey]
        dOCTISACTIVE <- map[kExDiagnosisDOCTISACTIVEKey]
        iCLOPATIENTID <- map[kExDiagnosisICLOPATIENTIDKey]
        dOCTISERSTAFF <- map[kExDiagnosisDOCTISERSTAFFKey]
        dEPTDEPNAME <- map[kExDiagnosisDEPTDEPNAMEKey]
        dOCTDISPLAYNAME <- map[kExDiagnosisDOCTDISPLAYNAMEKey]
        dEPTDATAPOSTBY <- map[kExDiagnosisDEPTDATAPOSTBYKey]
        dOCTDOCPAYTYPE <- map[kExDiagnosisDOCTDOCPAYTYPEKey]
        iCLOCHAPTERNAME <- map[kExDiagnosisICLOCHAPTERNAMEKey]
        iCLODIAGNOSISTYPE <- map[kExDiagnosisICLODIAGNOSISTYPEKey]
        dEPTACCHEADID <- map[kExDiagnosisDEPTACCHEADIDKey]
        iCLOMODIFIEDBY <- map[kExDiagnosisICLOMODIFIEDBYKey]
        iCLOFINALDIAGONOSISID <- map[kExDiagnosisICLOFINALDIAGONOSISIDKey]
        dOCTSPECIALIZATION <- map[kExDiagnosisDOCTSPECIALIZATIONKey]
        dOCTDOCORDER <- map[kExDiagnosisDOCTDOCORDERKey]
        dEPTDEPID <- map[kExDiagnosisDEPTDEPIDKey]
        dEPTISACTIVE <- map[kExDiagnosisDEPTISACTIVEKey]
        dOCTDOCCODE <- map[kExDiagnosisDOCTDOCCODEKey]
        dOCTQUALIFICATION <- map[kExDiagnosisDOCTQUALIFICATIONKey]
        dOCTSTATUS <- map[kExDiagnosisDOCTSTATUSKey]
        iCLODATAPOSTTIME <- map[kExDiagnosisICLODATAPOSTTIMEKey]
        iCLODEPARTMENT <- map[kExDiagnosisICLODEPARTMENTKey]
        dEPTDEPTYPE <- map[kExDiagnosisDEPTDEPTYPEKey]
        dEPTEXTENDEDREPORT <- map[kExDiagnosisDEPTEXTENDEDREPORTKey]
        dOCTDEPID <- map[kExDiagnosisDOCTDEPIDKey]
        iCLODIAGNOSISID <- map[kExDiagnosisICLODIAGNOSISIDKey]
        dEPTTESTNAMECODEPREFIX <- map[kExDiagnosisDEPTTESTNAMECODEPREFIXKey]
        dOCTPISDOCID <- map[kExDiagnosisDOCTPISDOCIDKey]
        iCLOLOCALDIAGONOSISID <- map[kExDiagnosisICLOLOCALDIAGONOSISIDKey]
        dOCTDATAPOSTBY <- map[kExDiagnosisDOCTDATAPOSTBYKey]
        iCLODOCTOR <- map[kExDiagnosisICLODOCTORKey]
        dOCTTDSPER <- map[kExDiagnosisDOCTTDSPERKey]
        dOCTIMAGE <- map[kExDiagnosisDOCTIMAGEKey]
        dOCTISENDODOCTOR <- map[kExDiagnosisDOCTISENDODOCTORKey]
        dOCTDESIG <- map[kExDiagnosisDOCTDESIGKey]
        dOCTISLABDOCTOR <- map[kExDiagnosisDOCTISLABDOCTORKey]
        dEPTDATAPOSTDATE <- map[kExDiagnosisDEPTDATAPOSTDATEKey]
        dOCTORGID <- map[kExDiagnosisDOCTORGIDKey]
        iCLOISACTIVE <- map[kExDiagnosisICLOISACTIVEKey]
        dOCTACCOUNTHEADIDIPD <- map[kExDiagnosisDOCTACCOUNTHEADIDIPDKey]
        dOCTDEPTYPE <- map[kExDiagnosisDOCTDEPTYPEKey]
        dEPTISREFERRALDEPT <- map[kExDiagnosisDEPTISREFERRALDEPTKey]
        iCLOINACTIVEBDATE <- map[kExDiagnosisICLOINACTIVEBDATEKey]
        dOCTISOTASSISTANT <- map[kExDiagnosisDOCTISOTASSISTANTKey]
        iCLOMODIFIEDTIME <- map[kExDiagnosisICLOMODIFIEDTIMEKey]
        dEPTISREFERALCOMP <- map[kExDiagnosisDEPTISREFERALCOMPKey]
        dOCTDATAPOSTDATE <- map[kExDiagnosisDOCTDATAPOSTDATEKey]
        iCLOLOCALDIAGONOSISNAME <- map[kExDiagnosisICLOLOCALDIAGONOSISNAMEKey]
        iCLOMACID <- map[kExDiagnosisICLOMACIDKey]
        dOCTPOSITIONID <- map[kExDiagnosisDOCTPOSITIONIDKey]
        iCLODEPNAME <- map[kExDiagnosisICLODEPNAMEKey]
        dOCTISQUEUENOGENINREG <- map[kExDiagnosisDOCTISQUEUENOGENINREGKey]
        iCLOFROM <- map[kExDiagnosisICLOFROMKey]
        iCLODIAGNOSIS <- map[kExDiagnosisICLODIAGNOSISKey]
        dEPTISPATHOLOGICALDEP <- map[kExDiagnosisDEPTISPATHOLOGICALDEPKey]
        dEPTORGID <- map[kExDiagnosisDEPTORGIDKey]
        dOCTDOCTYPE <- map[kExDiagnosisDOCTDOCTYPEKey]
        iCLODATAPOSTDATE <- map[kExDiagnosisICLODATAPOSTDATEKey]
        dEPTDEPTYPEBYSERVICE <- map[kExDiagnosisDEPTDEPTYPEBYSERVICEKey]
        dEPTDATAPOSTTIME <- map[kExDiagnosisDEPTDATAPOSTTIMEKey]
        dOCTISSMS <- map[kExDiagnosisDOCTISSMSKey]
        dEPTISMEMBERSHIPDEPT <- map[kExDiagnosisDEPTISMEMBERSHIPDEPTKey]
        dEPTDEPORDER <- map[kExDiagnosisDEPTDEPORDERKey]
        dOCTGDOCNAME <- map[kExDiagnosisDOCTGDOCNAMEKey]
        dOCTDOCSHAREGROUP <- map[kExDiagnosisDOCTDOCSHAREGROUPKey]
        dOCTGDEPID <- map[kExDiagnosisDOCTGDEPIDKey]
        dOCTISHOD <- map[kExDiagnosisDOCTISHODKey]
        dOCTYPE <- map[kExDiagnosisDOCTYPEKey]
        dOCTROOMNO <- map[kExDiagnosisDOCTROOMNOKey]
        dOCTEMAIL <- map[kExDiagnosisDOCTEMAILKey]
        iCLOOPERATIONPLANID <- map[kExDiagnosisICLOOPERATIONPLANIDKey]
        dOCTDEPCODE <- map[kExDiagnosisDOCTDEPCODEKey]
        dOCTISERDOCTOR <- map[kExDiagnosisDOCTISERDOCTORKey]
        dOCTPHONENO <- map[kExDiagnosisDOCTPHONENOKey]
        dOCTISBLKDOCTOR <- map[kExDiagnosisDOCTISBLKDOCTORKey]
        dEPTREPORTTITLE <- map[kExDiagnosisDEPTREPORTTITLEKey]
        dOCTDOCNAME <- map[kExDiagnosisDOCTDOCNAMEKey]

    }
}
public class DoctorNote: Mappable {
    public required init?(map: Map) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kDoctorNoteHEDEENQUIRYNOKey: String = "HEDE_ENQUIRYNO"
    internal let kDoctorNoteHEDEHISTORYKey: String = "HEDE_HISTORY"
    internal let kDoctorNoteHEDEDATAPOSTDATEADKey: String = "HEDE_DATAPOSTDATEAD"
    internal let kDoctorNoteHEDEPATIENTIDKey: String = "HEDE_PATIENTID"
    internal let kDoctorNoteHEDEVISITIDKey: String = "HEDE_VISITID"
    internal let kDoctorNoteHEDEREMARKSKey: String = "HEDE_REMARKS"
    internal let kDoctorNoteHEDEDATAPOSTDATEKey: String = "HEDE_DATAPOSTDATE"
    internal let kDoctorNoteHEDEDATAPOSTTIMEKey: String = "HEDE_DATAPOSTTIME"
    internal let kDoctorNoteHEDEDEPIDKey: String = "HEDE_DEPID"
    internal let kDoctorNoteHEDEDETAILIDKey: String = "HEDE_DETAILID"
    internal let kDoctorNoteHEDEPREPAREBYKey: String = "HEDE_PREPAREBY"


    // MARK: Properties
    public var hEDEENQUIRYNO: String?
    public var hEDEHISTORY: String?
    public var hEDEDATAPOSTDATEAD: String?
    public var hEDEPATIENTID: String?
    public var hEDEVISITID: String?
    public var hEDEREMARKS: String?
    public var hEDEDATAPOSTDATE: String?
    public var hEDEDATAPOSTTIME: String?
    public var hEDEDEPID: String?
    public var hEDEDETAILID: String?
    public var hEDEPREPAREBY: String?



    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
        hEDEENQUIRYNO <- map[kDoctorNoteHEDEENQUIRYNOKey]
        hEDEHISTORY <- map[kDoctorNoteHEDEHISTORYKey]
        hEDEDATAPOSTDATEAD <- map[kDoctorNoteHEDEDATAPOSTDATEADKey]
        hEDEPATIENTID <- map[kDoctorNoteHEDEPATIENTIDKey]
        hEDEVISITID <- map[kDoctorNoteHEDEVISITIDKey]
        hEDEREMARKS <- map[kDoctorNoteHEDEREMARKSKey]
        hEDEDATAPOSTDATE <- map[kDoctorNoteHEDEDATAPOSTDATEKey]
        hEDEDATAPOSTTIME <- map[kDoctorNoteHEDEDATAPOSTTIMEKey]
        hEDEDEPID <- map[kDoctorNoteHEDEDEPIDKey]
        hEDEDETAILID <- map[kDoctorNoteHEDEDETAILIDKey]
        hEDEPREPAREBY <- map[kDoctorNoteHEDEPREPAREBYKey]

    }
}
