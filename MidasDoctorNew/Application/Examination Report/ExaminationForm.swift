//
//  ExaminationForm.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 16/05/2021.
//

import UIKit

class ExaminationForm: BaseViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    
    lazy var myArray = [UIImage]()
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    let scrollView: UIScrollView = {
        let sv = TPKeyboardAvoidingScrollView()
        sv.contentMode = .scaleToFill
        sv.clipsToBounds = true
        sv.bounces = false
        sv.backgroundColor = Theme.Color.backgroundGray
        sv.showsVerticalScrollIndicator = false
        sv.showsHorizontalScrollIndicator = false
        return sv
    }()
    
    let clinicNotelbl = CustomUILable(titleStr: "Clinical Notes", textcolor: .black, textfont: UIFont.medium(ofSize: 15))
    let complaintslbl = CustomUILable(titleStr: "Complaints", textcolor: .black, textfont: UIFont.regular(ofSize: 14))
    let complaintTxt = CustomUITextView()
    let observertionlbl = CustomUILable(titleStr: "Observations", textcolor: .black, textfont: UIFont.regular(ofSize: 14))
    let observertionTxt = CustomUITextView()
    let diagnosislbl = CustomUILable(titleStr: "Diagnosis", textcolor: .black, textfont: UIFont.regular(ofSize: 14))
    let diagnosisTxt = CustomUITextView()

    let lablbl = CustomUILable(titleStr: "Lab Orders", textcolor: .black, textfont: UIFont.medium(ofSize: 15))
    let labBtn: UIButton = {
        let btn = UIButton()
        let image = Images.Add_ImageXX.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        return btn
    }()
    
    let doctorNamelbl = CustomUILable(titleStr: "", textcolor: .black, textfont: UIFont.regular(ofSize: 14))
    let instructionlbl = CustomUILable(titleStr: "INSTRUCTION", textcolor: .black, textfont: UIFont.regular(ofSize: 14))

    let prescriptionlbl = CustomUILable(titleStr: "Prescriptions", textcolor: .black, textfont: UIFont.medium(ofSize: 15))
    let prescriptionBtn: UIButton = {
        let btn = UIButton()
        let image = Images.Add_ImageXX.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        return btn
    }()
    let infolbl = CustomUILable(titleStr: "Take adequate rest and drink plenty of clean water.", textcolor: .black, textfont: UIFont.regular(ofSize: 14))

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()
        loadData()
    }
    
    func setupViews(){
        titlelbl.text = "Examination Form"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        view.addSubview(scrollView)
        scrollView.addSubview(clinicNotelbl)
        scrollView.addSubview(complaintslbl)
        scrollView.addSubview(complaintTxt)
        scrollView.addSubview(observertionlbl)
        scrollView.addSubview(observertionTxt)
        scrollView.addSubview(diagnosislbl)
        scrollView.addSubview(diagnosisTxt)
        scrollView.addSubview(lablbl)
        scrollView.addSubview(labBtn)
        
        scrollView.addSubview(doctorNamelbl)
        scrollView.addSubview(instructionlbl)
        scrollView.addSubview(prescriptionlbl)
        scrollView.addSubview(prescriptionBtn)
        scrollView.addSubview(infolbl)

        
        complaintTxt.font = UIFont.regular(ofSize: 15)
        observertionTxt.font = UIFont.regular(ofSize: 15)
        diagnosisTxt.font = UIFont.regular(ofSize: 15)

        complaintTxt.placeholderLabel.text = "Complaints"
        observertionTxt.placeholderLabel.text = "Observations"
        diagnosisTxt.placeholderLabel.text = "Diagnosis"
        
    }
    
    func setupConstarint(){
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 5 - 45 - 5, height: 55)

        let scrollViewwidth: CGFloat = SCREEN.WIDTH
        
        scrollView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: scrollViewwidth, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)
        
        
        clinicNotelbl.frame = CGRect(x: 10, y: 10, width: scrollViewwidth - 20, height: 20)
        complaintslbl.frame = CGRect(x: 10, y: clinicNotelbl.frame.origin.y + clinicNotelbl.frame.height + 10, width: scrollViewwidth - 20, height: 20)
        complaintTxt.frame = CGRect(x: 8, y: complaintslbl.frame.origin.y + complaintslbl.frame.height, width: scrollViewwidth - 16, height: 50)
        observertionlbl.frame = CGRect(x: 10, y: complaintTxt.frame.origin.y + complaintTxt.frame.height + 10, width: scrollViewwidth - 20, height: 20)
        observertionTxt.frame = CGRect(x: 8, y: observertionlbl.frame.origin.y + observertionlbl.frame.height, width: scrollViewwidth - 16, height: 50)
        diagnosislbl.frame = CGRect(x: 10, y: observertionTxt.frame.origin.y + observertionTxt.frame.height + 10, width: scrollViewwidth - 20, height: 20)
        diagnosisTxt.frame = CGRect(x: 8, y: diagnosislbl.frame.origin.y + diagnosislbl.frame.height, width: scrollViewwidth - 16, height: 50)
        lablbl.frame = CGRect(x: 10, y: diagnosisTxt.frame.origin.y + diagnosisTxt.frame.height + 15, width: scrollViewwidth - 20 - 10 - 25, height: 20)
        labBtn.frame = CGRect(x: scrollViewwidth - 10 - 30, y: lablbl.frame.origin.y - 5, width: 30, height: 30)
        doctorNamelbl.frame = CGRect(x: 15, y: labBtn.frame.origin.y + labBtn.frame.height + 10, width: scrollViewwidth / 2 - 5, height: 20)
        instructionlbl.frame = CGRect(x: doctorNamelbl.frame.origin.x + doctorNamelbl.frame.width + 5, y: doctorNamelbl.frame.origin.y, width: scrollViewwidth / 2 - 15, height: 20)

        prescriptionlbl.frame = CGRect(x: 10, y: doctorNamelbl.frame.origin.y + doctorNamelbl.frame.height + 15, width: scrollViewwidth - 20 - 10 - 25, height: 20)
        prescriptionBtn.frame = CGRect(x: scrollViewwidth - 10 - 30, y: prescriptionlbl.frame.origin.y - 5, width: 30, height: 30)
        infolbl.frame = CGRect(x: 10, y: prescriptionlbl.frame.origin.y + prescriptionlbl.frame.height + 15, width: scrollViewwidth - 20, height: 20)
    }
    
    func loadData(){
        doctorNamelbl.text = "TEST" //users.fullname
    }
    
}
