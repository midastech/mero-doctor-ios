//
//  ExaminationSectionTableViewCell.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 19/07/2021.
//

import UIKit
protocol sectionDelegate: class {
    func sectionDidTap(section: Int)
}
class ExaminationSectionTableViewCell: UITableViewCell {
    weak var delegate: sectionDelegate?
    @IBOutlet weak var sectionAddBtn: UIButton!
    @IBOutlet weak var sectionTitle: UILabel!
    var section: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.numberOfTapsRequired = 1
        self.contentView.addGestureRecognizer(tap)
    
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        delegate?.sectionDidTap(section: section ?? 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func addBtnAction(_ sender: Any) {
    }
    
}
