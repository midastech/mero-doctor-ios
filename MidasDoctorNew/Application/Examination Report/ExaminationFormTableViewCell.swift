//
//  ExaminationFormTableViewCell.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 19/07/2021.
//

import UIKit
protocol examinationFormCellDelegate: class {
    func saveBtnAction(index: Int?, text: String?, complaintId: String?, observationId: String?,diagnosisId: String?, doctornoteId: String?)
}
class ExaminationFormTableViewCell: UITableViewCell {
    weak var delegate: examinationFormCellDelegate?
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    var complaintID: String?
    var apiCalled: Bool = false
    var observationId: String?
    var diagnosisId: String?
    var doctorNoteId: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }
    func uisetup() {
        backView.layer.cornerRadius = 16
        backView.clipsToBounds = true
        descTxtView.delegate = self
        saveBtn.isEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func saveBtnAction(_ sender: UIButton) {
       
        print(saveBtn.isEnabled)
        if saveBtn.isEnabled {
            apiCalled = true
            saveBtn.alpha = 0.5
            saveBtn.isEnabled = false
            delegate?.saveBtnAction(index: saveBtn.tag, text: descTxtView.text, complaintId: complaintID, observationId: observationId,diagnosisId: diagnosisId,doctornoteId: doctorNoteId)
           
        }
      

    }
    
}
extension ExaminationFormTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        print(descTxtView.tag)
        print(saveBtn.tag)
        saveBtn.alpha = 1
        saveBtn.isEnabled = true

    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if apiCalled == true {
            saveBtn.alpha = 0.5
            
        } else {
        if descTxtView.text.isEmpty {
            saveBtn.alpha = 0.5
            saveBtn.isEnabled = false
        } else {
            saveBtn.alpha = 1
            saveBtn.isEnabled = true
        }
        }
    }
}
