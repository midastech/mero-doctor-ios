//
//  VideoAppointmentVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 05/04/2021.
//

import UIKit

class VideoAppointmentVC: BaseViewController, RefreshControllerDelegate {
    func refreshBtnPress(sender: UIButton) {
        print("Refresh")
        checkNetwork(fromdate: getnepaliFirstDate, todate: todayNepaliDate, type: type)
    }
    

    lazy var myArray = [OPDAppointmentModel]()
    
    var engDate : String = ""
    var todayNepaliDate : String = ""
    var getnepaliFirstDate : String = ""
    var type : String = ""
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    fileprivate lazy var segment: UISegmentedControl = {
        let items = ["OPD", "TELE"]
        let segment = UISegmentedControl(items: items)
        
        segment.layer.cornerRadius = 8
        segment.backgroundColor = Theme.Color.white
        segment.tintColor = Theme.Color.colorDarkBlue

        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)

        let selectedfont = UIFont.medium(ofSize: 14)
        let normalfont = UIFont.medium(ofSize: 15)
        
        segment.setTitleTextAttributes([NSAttributedString.Key.font: selectedfont], for: .selected)
        segment.setTitleTextAttributes([NSAttributedString.Key.font: normalfont], for: .normal)

//        segment.layer.shadowColor = UIColor.darkGray.cgColor
//        segment.layer.shadowOffset = CGSize(width: -0.5, height: 1.0)
//        segment.layer.shadowOpacity = 0.4
//        segment.layer.shadowRadius = 2.0
        
        segment.addTarget(self, action: #selector(changeValue), for: .valueChanged)
        return segment
    }()
    
    
    let dateView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.colorLightWhite
        return v
    }()
    
    fileprivate lazy var previousBtn: UIButton = {
        let btn = UIButton()
        let image = Images.ic_previous.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        btn.addTarget(self, action: #selector(previousBtnPress), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var nextBtn: UIButton = {
        let btn = UIButton()
        let image = Images.ic_next.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        btn.addTarget(self, action: #selector(nextBtnPress), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var datelbl: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = Theme.Color.white
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.semibold(ofSize: 13)
        lbl.textAlignment = .center
        lbl.dropShadow()
//        lbl.clipsToBounds = true
//        lbl.layer.cornerRadius = 8
        
//        lbl.layer.shadowColor = UIColor.darkGray.cgColor
//        lbl.layer.shadowOffset = CGSize(width: -0.5, height: 1.0)
//        lbl.layer.shadowOpacity = 0.4
//        lbl.layer.shadowRadius = 2.0
        return lbl
    }()
    
    fileprivate lazy var countlbl: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = Theme.Color.white
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.semibold(ofSize: 13)
        lbl.textAlignment = .center
        lbl.dropShadow()
//        lbl.clipsToBounds = true
//        lbl.layer.cornerRadius = 8
        return lbl
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    //MARK:- TableView
    fileprivate lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.register(OPDCell.self, forCellReuseIdentifier: "OPDCell")
        
        if #available(iOS 10.0, *) {
            tv.refreshControl = refresher
        }else{
            tv.addSubview(refresher)
        }
        return tv
    }()
    
    lazy var errorView: NetworkViewProperty = {
        let vi = NetworkViewProperty(frame: CGRect(x: (SCREEN.WIDTH / 2) - 110, y: (SCREEN.HEIGHT / 2) - 110, width: 220, height: 203))
        vi.backgroundColor = .clear
        return vi
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupConstarint()
        segment.selectedSegmentIndex = 0
        
        checkNetwork(fromdate: getnepaliFirstDate, todate: todayNepaliDate, type: type)
    }

    func setupViews() {
        titlelbl.text = "My Appointments"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        navView.addSubview(segment)
        view.addSubview(dateView)
        dateView.addSubview(previousBtn)
        dateView.addSubview(nextBtn)
        dateView.addSubview(datelbl)
        dateView.addSubview(countlbl)

        view.addSubview(tableView)

        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        
        engDate = Date().todayDate(format: String.DATE_FORMATE)
        todayNepaliDate = getNepaliDate(engDate: engDate)
        getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
        type = "OPD"
        
        let todayDay : String = Date().todayDate(format: String.DATE_FORMATE_DAY)
        
        datelbl.text =  todayNepaliDate + " " + convertEnglishDay(day: todayDay)
        countlbl.text = "0/0"
        
        
//        let engDate : String = Date().todayDate(format: String.DATE_FORMATE)
//        let todayNepaliDate = getNepaliDate(engDate: engDate)
//        let getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
        
        
    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        segment.frame = CGRect(x: navView.frame.width - 120, y: 8, width: 110, height: 39)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 5 - 120 - 5, height: 55)
        
        dateView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height + 5, width: SCREEN.WIDTH, height: 45)
        previousBtn.frame = CGRect(x: 10, y: 0, width: 35, height: 45)
        nextBtn.frame = CGRect(x: dateView.frame.width - 10 - 35, y: 0, width: 35, height: 45)
        
        datelbl.frame = CGRect(x: dateView.frame.width / 2 - 85 - 30, y: 5, width: 170, height: 35)
        countlbl.frame = CGRect(x: datelbl.frame.origin.x + datelbl.frame.width + 5, y: 5, width: 55, height: 35)
        
        tableView.frame = CGRect(x: 0, y: dateView.frame.origin.y + dateView.frame.height + 5, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - dateView.frame.origin.y - dateView.frame.height - 5)
        
    }
    
    @objc func changeValue(sender: UISegmentedControl) {
//        print("selectedSegmentIndex \(sender.selectedSegmentIndex)")
        switch sender.selectedSegmentIndex {
        case 0:
            print("\(sender.selectedSegmentIndex)")
            
            type = "OPD"
            checkNetwork(fromdate: getnepaliFirstDate, todate: todayNepaliDate, type: type )
            break
        case 1:
            type = "TELE"
            checkNetwork(fromdate: getnepaliFirstDate, todate: todayNepaliDate, type: type )
            print("\(sender.selectedSegmentIndex)")
            break
            
        default:
            break
        }
    }
    
    @objc func previousBtnPress(){
        
    }
    @objc func nextBtnPress(){
        
    }
    
    
    @objc func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetwork(fromdate: self.getnepaliFirstDate, todate: self.todayNepaliDate, type: self.type)
                self.refresher.endRefreshing()
            }
        }else{
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    func checkNetwork(fromdate: String, todate: String, type: String){
        if Reachability.isConnectedToNetwork(){
            getScheduleAPICall(fromdate: fromdate, todate: todate, type: type)
        }else{
            self.showError()
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showError(){
        
//        let vi = NetworkViewProperty(frame: CGRect(x: (SCREEN.WIDTH / 2) - 110, y: (SCREEN.HEIGHT / 2) - 110, width: 220, height: 220)) //CustomSegmentedControl(frame: CGRect(x: SCREEN.WIDTH / 2 - 125, y: navView.frame.origin.y + navView.frame.height + 10, width: 250, height: 45), buttonTitle: ["Upcoming", "Completed", "Cancelled"])
//        vi.delegate = self
//        vi.backgroundColor = .clear
//        view.addSubview(vi)

//        errorView.frame = CGRect(x: (SCREEN.WIDTH / 2) - 102, y: (SCREEN.HEIGHT / 2) - 150, width: 205, height: 205)
        errorView.delegate = self
        view.addSubview(errorView)
    }
    
    func hideError(){
        errorView.removeFromSuperview()
    }
}

//MARK:- API Call
extension VideoAppointmentVC{
    func getScheduleAPICall(fromdate: String, todate: String, type: String){
        self.activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.getSchedule(orgid: users.orgid ?? "", gdepid: users.gdepid ?? "", gdocid: users.gdocid ?? "", fromdate: fromdate, todate: todate, type: type)) { (status, data) in
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            if data["type"].string == "success"{
                if let responseArray = data["response"].array {
                    self.myArray.removeAll()
//                    for responseDic in responseArray{
//                        let item = OPDAppointmentModel(json: responseDic)
//                        self.myArray.append(item)
//                    }
                    
                    for responseDic in responseArray {
                        var vdc: [patientinfoModel] = []
                        if let vdcarray = responseDic["patientinfo"].dictionary{
                            let item = patientinfoModel.init(patientname: vdcarray["patientname"]!.stringValue,
                                                             address: vdcarray["address"]!.stringValue,
                                                                 dobad: vdcarray["dobad"]!.stringValue,
                                                                 age: vdcarray["age"]!.stringValue,
                                                                 gender: vdcarray["gender"]!.stringValue)
                                vdc.append(contentsOf: [item])
                        }
                        let item = OPDAppointmentModel(addedby: responseDic["addedby"].stringValue,
                                                       queueno: responseDic["queueno"].stringValue,
                                                       midasid: responseDic["midasid"].stringValue,
                                                       starttime: responseDic["starttime"].stringValue,
                                                       status: responseDic["status"].stringValue,
                                                       appdate: responseDic["appdate"].stringValue,
                                                       healthpartnerinfo: [String(format: "%@", responseDic["healthpartnerinfo"].arrayValue)],
                                                       patientinfo: vdc)
                        self.myArray.append(item)
                    }
                    
                    print("myArray \(self.myArray)")
                    
                    self.tableView.reloadData()
                }
               
            }else{
                let message = data["message"].string
                self.toastMessage(message: message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
}

extension VideoAppointmentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OPDCell", for: indexPath) as! OPDCell
        cell.selectionStyle = .none
        cell.data = myArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
