//
//  OPDAppointmentVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 31/03/2021.
//

import UIKit

class OPDAppointmentVC: BaseViewController, RefreshControllerDelegate {
    func refreshBtnPress(sender: UIButton) {
        print("Refresh")
        hideError()
        checkNetwork()
    }
    

    lazy var myArray = [OPDAppointmentModel]()
    var type : String = ""
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    fileprivate lazy var segment: UISegmentedControl = {
        let items = ["OPD", "TELE"]
        let segment = UISegmentedControl(items: items)
        
        segment.layer.cornerRadius = 8
        segment.backgroundColor = Theme.Color.white
        segment.tintColor = Theme.Color.colorDarkBlue

        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)

        let selectedfont = UIFont.medium(ofSize: 14)
        let normalfont = UIFont.medium(ofSize: 15)
        
        segment.setTitleTextAttributes([NSAttributedString.Key.font: selectedfont], for: .selected)
        segment.setTitleTextAttributes([NSAttributedString.Key.font: normalfont], for: .normal)

//        segment.layer.shadowColor = UIColor.darkGray.cgColor
//        segment.layer.shadowOffset = CGSize(width: -0.5, height: 1.0)
//        segment.layer.shadowOpacity = 0.4
//        segment.layer.shadowRadius = 2.0
        
        segment.addTarget(self, action: #selector(changeValue), for: .valueChanged)
        return segment
    }()
    
    
    let dateView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.colorLightWhite
        return v
    }()
    
    fileprivate lazy var previousBtn: UIButton = {
        let btn = UIButton()
        let image = Images.ic_previous.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        btn.addTarget(self, action: #selector(previousBtnPress), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var nextBtn: UIButton = {
        let btn = UIButton()
        let image = Images.ic_next.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.darkGray
        btn.addTarget(self, action: #selector(nextBtnPress), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var datelbl: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = Theme.Color.white
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.semibold(ofSize: 13)
        lbl.textAlignment = .center
        lbl.layer.cornerRadius = 4
        lbl.layer.borderWidth = 1
        lbl.layer.borderColor = Theme.Color.colorLightWhite.cgColor
//        lbl.dropShadow()
        lbl.clipsToBounds = true
//        lbl.layer.cornerRadius = 8
        
//        lbl.layer.shadowColor = UIColor.darkGray.cgColor
//        lbl.layer.shadowOffset = CGSize(width: -0.5, height: 1.0)
//        lbl.layer.shadowOpacity = 0.4
//        lbl.layer.shadowRadius = 2.0
        return lbl
    }()
    
    fileprivate lazy var countlbl: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = Theme.Color.white
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.semibold(ofSize: 13)
        lbl.textAlignment = .center
        lbl.layer.cornerRadius = 4
        lbl.layer.borderWidth = 1
        lbl.layer.borderColor = Theme.Color.colorLightWhite.cgColor
        
        lbl.clipsToBounds = true
//        lbl.layer.cornerRadius = 8
        return lbl
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    //MARK:- TableView
    fileprivate lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.backgroundColor = Theme.Color.backgroundGray
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.register(OPDCell.self, forCellReuseIdentifier: "OPDCell")
        tv.register(DashboardPatientCell.self, forCellReuseIdentifier: "DashboardPatientCell")

        if #available(iOS 10.0, *) {
            tv.refreshControl = refresher
        }else{
            tv.addSubview(refresher)
        }
        return tv
    }()
    
    lazy var errorView: NetworkViewProperty = {
        let vi = NetworkViewProperty(frame: CGRect(x: (SCREEN.WIDTH / 2) - 110, y: (SCREEN.HEIGHT / 2) - 110, width: 220, height: 203))
        vi.backgroundColor = .clear
        return vi
    }()
    
    
    var patientCount: [patientinfoModel] = []
    lazy var selectedDate = Date()
    var selectedDateString = String(), selectedNepaliDateString = String()
    lazy var patientArray = [PatientListModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupConstarint()
        
        segment.selectedSegmentIndex = type == "OPD" ? 0 : 1
        
        
        checkNetwork()
    }

    func setupViews() {
        titlelbl.text = "My Appointments"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        navView.addSubview(segment)
        view.addSubview(dateView)
        dateView.addSubview(previousBtn)
        dateView.addSubview(nextBtn)
        dateView.addSubview(datelbl)
        dateView.addSubview(countlbl)

        view.addSubview(tableView)

        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        
        countlbl.text = "0/0"
        
//        engDate = Utility.todayDate()
        selectedDate = Utility.stringToDate(Utility.todayDate())!
        loadDate(date: selectedDate)
        
    }
    
    func loadDate(date: Date){
        selectedDateString = Utility.dateToString(date: date)
        selectedNepaliDateString = getNepaliDate(engDate: selectedDateString)
        datelbl.text =  selectedNepaliDateString
    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        segment.frame = CGRect(x: navView.frame.width - 120, y: 8, width: 110, height: 39)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 5 - 120 - 5, height: 55)
        
        dateView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height + 5, width: SCREEN.WIDTH, height: 45)
        previousBtn.frame = CGRect(x: 10, y: 0, width: 35, height: 45)
        nextBtn.frame = CGRect(x: dateView.frame.width - 10 - 35, y: 0, width: 35, height: 45)
        
        datelbl.frame = CGRect(x: dateView.frame.width / 2 - 65 - 30, y: 8, width: 130, height: 30)
        countlbl.frame = CGRect(x: datelbl.frame.origin.x + datelbl.frame.width + 5, y: 8, width: 55, height: 30)
        
        tableView.frame = CGRect(x: 0, y: dateView.frame.origin.y + dateView.frame.height + 5, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - dateView.frame.origin.y - dateView.frame.height - 5)
        
    }
    
    @objc func changeValue(sender: UISegmentedControl) {
//        print("selectedSegmentIndex \(sender.selectedSegmentIndex)")
        switch sender.selectedSegmentIndex {
        case 0:
            print("\(sender.selectedSegmentIndex)")
            
            type = "OPD"
            checkNetwork()
            break
        case 1:
            type = "TELE"
            checkNetwork()
            print("\(sender.selectedSegmentIndex)")
            break
            
        default:
            break
        }
    }
    
    @objc func previousBtnPress(){
        loadNewData(count: -1)
    }
    @objc func nextBtnPress(){
        loadNewData(count: 1)
    }
    
    fileprivate func loadNewData(count: Int){
        var dayComponent = DateComponents()
        dayComponent.day = count // For removing one day (yesterday): -1
        let theCalendar = Calendar.current
        selectedDate = theCalendar.date(byAdding: dayComponent, to: selectedDate as Date)!
        print("selectedDate : \(String(describing: selectedDate))")
        
        loadDate(date: selectedDate)
        checkNetwork()
    }
    
    @objc func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetwork()
                self.refresher.endRefreshing()
            }
        }else{
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    func checkNetwork(){
        if Reachability.isConnectedToNetwork(){
            getScheduleAPICall()
        }else{
            self.showError()
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showError(){
        
//        let vi = NetworkViewProperty(frame: CGRect(x: (SCREEN.WIDTH / 2) - 110, y: (SCREEN.HEIGHT / 2) - 110, width: 220, height: 220)) //CustomSegmentedControl(frame: CGRect(x: SCREEN.WIDTH / 2 - 125, y: navView.frame.origin.y + navView.frame.height + 10, width: 250, height: 45), buttonTitle: ["Upcoming", "Completed", "Cancelled"])
//        vi.delegate = self
//        vi.backgroundColor = .clear
//        view.addSubview(vi)

//        errorView.frame = CGRect(x: (SCREEN.WIDTH / 2) - 102, y: (SCREEN.HEIGHT / 2) - 150, width: 205, height: 205)
        errorView.delegate = self
        view.addSubview(errorView)
    }
    
    func hideError(){
        errorView.removeFromSuperview()
    }
}

//MARK:- API Call
extension OPDAppointmentVC{
    func getScheduleAPICall(){
        self.activityIndicatorBegin()
        
//        let selectedNepaliDate = getNepaliDate(engDate: engDate)
        
        ApiManager.sendRequest(toApi: Api.Endpoint.getSchedule(orgid: users.orgid ?? "", gdepid: users.gdepid ?? "", gdocid: users.gdocid ?? "", fromdate: selectedNepaliDateString, todate: selectedNepaliDateString, type: type)) { (status, data) in
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            if data["type"].string == "success"{
                if let responseArray = data["response"].array {
                    self.myArray.removeAll()
                    self.patientCount.removeAll()
//                    for responseDic in responseArray{
//                        let item = OPDAppointmentModel(json: responseDic)
//                        self.myArray.append(item)
//                    }
                    
                    for responseDic in responseArray {
                        var vdc: [patientinfoModel] = []
                        if let vdcarray = responseDic["patientinfo"].dictionary{
                            let item = patientinfoModel.init(patientname: vdcarray["patientname"]?.stringValue ?? "",
                                                             address: vdcarray["address"]?.stringValue ?? "",
                                                             dobad: vdcarray["dobad"]?.stringValue ?? "",
                                                             age: vdcarray["age"]?.stringValue ?? "",
                                                             gender: vdcarray["gender"]?.stringValue ?? "")
                                vdc.append(contentsOf: [item])
                                                        
                            if item.patientname != ""{
                                self.patientCount.append(item)
                            }
                        }
                        
                        
                        let item = OPDAppointmentModel(addedby: responseDic["addedby"].stringValue,
                                                       queueno: responseDic["queueno"].stringValue,
                                                       midasid: responseDic["midasid"].stringValue,
                                                       starttime: responseDic["starttime"].stringValue,
                                                       status: responseDic["status"].stringValue,
                                                       appdate: responseDic["appdate"].stringValue,
                                                       healthpartnerinfo: [String(format: "%@", responseDic["healthpartnerinfo"].arrayValue)],
                                                       patientinfo: vdc)
                        self.myArray.append(item)
                    }
                    print("myArray count \(self.myArray.count)")
                    self.countlbl.text = String(format: "%@/%@", String(self.patientCount.count), String(self.myArray.count))
                    
                    if self.patientCount.count > 0 {
                        self.telemedicinepatientlistAPICall()
                    }

                    
                    self.tableView.reloadData()
                }
               
            }else{
                let message = data["message"].string
                self.toastMessage(message: message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.refresher.endRefreshing()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    
    //MARK:- telemedicinepatientlistAPICall
    func telemedicinepatientlistAPICall(){
        let users = DataManager.getUserDetail()[0]
        ApiManager.sendRequest(toApi: Api.Endpoint.telemedicinepatientlist(docid: users.gdocid ?? "", orgid: users.orgid ?? "", type: "", fromdate: selectedNepaliDateString, todate: selectedNepaliDateString)) { (status, data) in
            if data["type"].string == "success"{
                if let responseArray = data["response"].array {
                    self.patientArray.removeAll()
                    for responseDic in responseArray{
//                        let item = PatientListModel(json: responseDic)
//                        self.patientArray.append(item)
                        let item = PatientListModel(districtname: responseDic["districtname"].string ?? "",
                                                    appo_apptime: responseDic["appo_apptime"].string ?? "",
                                                    midasid: responseDic["midasid"].string ?? "",
                                                    appdatenep: responseDic["appdatenep"].string ?? "",
                                                    appdateng: responseDic["appdateng"].string ?? "",
                                                    healthpartner: responseDic["healthpartner"].string ?? "",
                                                    title: responseDic["title"].string ?? "",
                                                    appo_callstatus: responseDic["appo_callstatus"].string ?? "",
                                                    age: responseDic["age"].string ?? "",
                                                    queueno: responseDic["queueno"].string ?? "",
                                                    appid: responseDic["appid"].string ?? "",
                                                    address: responseDic["address"].string ?? "",
                                                    mobileno: responseDic["mobileno"].string ?? "",
                                                    patientid: responseDic["patientid"].string ?? "",
                                                    gender: responseDic["gender"].string ?? "",
                                                    patientname: responseDic["patientname"].string ?? "",
                                                    patientstatus: responseDic["patientstatus"].string ?? "",
                                                    agetype: responseDic["agetype"].string ?? "",
                                                    isurgent:responseDic["isurgent"].string ?? ""
                        )
                        self.patientArray.append(item)
                    }
                }
            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message: message, toastType: .message)
            }
        } onError: { (error) in
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
}

extension OPDAppointmentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let patientInfo = myArray[indexPath.row].patientinfo?[0]
        if patientInfo?.patientname == "" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OPDCell", for: indexPath) as! OPDCell
            cell.selectionStyle = .none
            cell.data = myArray[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardPatientCell", for: indexPath) as! DashboardPatientCell
            cell.selectionStyle = .none
            
            cell.namelbl.text = patientInfo?.patientname
            
            let genderTrimmed = patientInfo?.gender.trimmingCharacters(in: .whitespacesAndNewlines)

            cell.genderlbl.text = String(format: "(%@/%@)", genderTrimmed! as String, patientInfo!.dobad as String)
                        
            cell.addreshlbl.text = patientInfo?.address
            cell.datelbl.text = myArray[indexPath.row].appdate
            cell.timelbl.text = myArray[indexPath.row].starttime
            
            if genderTrimmed == "male" || genderTrimmed == "Male"{
                let image = Images.ic_male.withRenderingMode(.alwaysTemplate)
                cell.profileImg.image = image
                cell.profileImg.tintColor = Theme.Color.lightGray
            }else{
                let image = Images.ic_female.withRenderingMode(.alwaysTemplate)
                cell.profileImg.image = image
                cell.profileImg.tintColor = Theme.Color.lightGray
            }
            
            return cell
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let patientInfo = myArray[indexPath.row].patientinfo?[0]
        if patientInfo?.patientname == "" {
            return 90
        }else{
            return 101
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let patientInfo = myArray[indexPath.row].patientinfo?[0]
        if patientInfo?.patientname == "" {
            toastMessage(message: "No appointment taken.", toastType: .message)
        }else{
//            let dic = getPatientListModel(data: myArray[indexPath.row])
//            print("dic \(dic)")
            
            let midasID = myArray[indexPath.row].midasid
            let queueno = myArray[indexPath.row].queueno
            
            
            for patientDic in patientArray {
                if patientDic.midasid == midasID && patientDic.queueno == queueno {
                    let vc = PatientProfileVC()
                    vc.patientArray = [patientDic]
                    navigationController?.pushViewController(vc, animated: true)
                    
                    return
                }
            }
            
            
        }
    }
    
    
    /*
    func getPatientListModel(data: OPDAppointmentModel) -> PatientListModel{
//        var patientArray: [PatientListModel] = []
//        print("address = \(patientArray[0].address)")
        
//        patientArray[0].districtname = ""
//        patientArray[0].appo_apptime = data.starttime ?? ""
//        patientArray[0].midasid = data.midasid ?? ""
//        patientArray[0].appdatenep = ""
//        patientArray[0].appdateng = data.appdate ?? ""
//        patientArray[0].healthpartner = data.addedby ?? ""
//        patientArray[0].title = ""
//        patientArray[0].appo_callstatus = ""
//        patientArray[0].age = data.patientinfo?[0].age ?? ""
//        patientArray[0].queueno = data.queueno ?? ""
//        patientArray[0].appid = ""
//        patientArray[0].address = data.patientinfo?[0].address ?? ""
//        patientArray[0].mobileno = ""
//        patientArray[0].patientid = ""
//        patientArray[0].gender = data.patientinfo?[0].gender ?? ""
//        patientArray[0].patientname = data.patientinfo?[0].patientname ?? ""
//        patientArray[0].patientstatus = data.status ?? ""
//        patientArray[0].agetype = ""
//        patientArray[0].isurgent = ""

        
        let item = PatientListModel(districtname: "",
                                    appo_apptime: data.starttime ?? "",
                                    midasid: data.midasid ?? "",
                                    appdatenep: "",
                                    appdateng: data.appdate ?? "",
                                    healthpartner: data.addedby ?? "",
                                    title: "",
                                    appo_callstatus: "",
                                    age: data.patientinfo?[0].age ?? "",
                                    queueno: data.queueno ?? "",
                                    appid: "",
                                    address: data.patientinfo?[0].address ?? "",
                                    mobileno: "",
                                    patientid: "",
                                    gender: data.patientinfo?[0].gender ?? "",
                                    patientname: data.patientinfo?[0].patientname ?? "",
                                    patientstatus: data.status ?? "",
                                    agetype: "",
                                    isurgent: ""
        )
        return item
        
    }
 */
}
