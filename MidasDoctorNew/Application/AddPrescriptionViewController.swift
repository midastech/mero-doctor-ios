//
//  AddPrescriptionViewController.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 21/07/2021.
//

import UIKit

class AddPrescriptionViewController: UIViewController {
    @IBOutlet weak var backBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        let vc = UIStoryboard.homeStoryboard.instantiatePrescriptionDetailsVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    func uisetup() {
        let image = Images.Back_Icon.withRenderingMode(.alwaysTemplate)
        backBtn.setImage(image, for: .normal)
        backBtn.tintColor = Theme.Color.black
    }
    

    @IBAction func backBtnActin(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
