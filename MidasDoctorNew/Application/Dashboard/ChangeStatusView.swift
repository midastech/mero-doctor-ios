//
//  ChangeStatusView.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 31/03/2021.
//

import UIKit

class ChangeStatusView: BottomPopupViewController {
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var reloadStatus: ((_ delaytime: String, _ frmdate: String, _ todate: String, _ reason: String, _ status: String)->())?
//    (delaytime, frmdate, todate, reason, status)
    fileprivate lazy var changelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Change Status"
        lbl.textColor = Theme.Color.white
        lbl.font = UIFont.medium(ofSize: 17)
        return lbl
    }()
    
    lazy var isOnTheWayRadioBtn: Bool = true
    lazy var status: String = ""
    
    
    fileprivate lazy var inBtn = StatusButtonProperties()
    fileprivate lazy var outBtn = StatusButtonProperties()
    fileprivate lazy var onTimeBtn = StatusButtonProperties()
    fileprivate lazy var delayBtn = StatusButtonProperties()
    fileprivate lazy var breakBtn = StatusButtonProperties()
    fileprivate lazy var emergencyBtn = StatusButtonProperties()
    fileprivate lazy var roundBtn = StatusButtonProperties()
    fileprivate lazy var otBtn = StatusButtonProperties()
    fileprivate lazy var meetingBtn = StatusButtonProperties()
    fileprivate lazy var leaveBtn = StatusButtonProperties()
    fileprivate lazy var lineView = UIView()
    
    //MARK:- Time Popup
    fileprivate lazy var timeBackView = BackViewProperties()
    fileprivate lazy var pleaselbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Please select reason for Delay"
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    fileprivate lazy var onTheWayRadioBtn = UIButton()
    fileprivate lazy var onTheWayRadiolbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "On the way"
        lbl.textColor = Theme.Color.darkGray
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    fileprivate lazy var personalRadioBtn = UIButton()
    fileprivate lazy var personallbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Personal"
        lbl.textColor = Theme.Color.darkGray
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    fileprivate lazy var approximatelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Approximate time for delay"
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.semibold(ofSize: 15)
        return lbl
    }()
    
    fileprivate lazy var tenBtn = StatusButtonProperties()
    fileprivate lazy var twentyBtn = StatusButtonProperties()
    fileprivate lazy var thirtyBtn = StatusButtonProperties()
    fileprivate lazy var fortyFiveBtn = StatusButtonProperties()
    fileprivate lazy var oneHrBtn = StatusButtonProperties()
    fileprivate lazy var oneNfiveHrBtn = StatusButtonProperties()
    fileprivate lazy var twoHrBtn = StatusButtonProperties()
    fileprivate lazy var twoNfiveHrBtn = StatusButtonProperties()
    fileprivate lazy var threeHrBtn = StatusButtonProperties()
    fileprivate lazy var fourHrBtn = StatusButtonProperties()

    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        setupView()
        setupFrame()
        
        
        
        timeBackView.isHidden = true
    }
    
    fileprivate func setupView(){
        view.backgroundColor = Theme.Color.colorLightWhite
        view.applyGradient(colors: [Theme.Color.colorLightBlue, Theme.Color.colorDarkBlue], gradient: .vertical)
        
        view.addSubview(changelbl)
        view.addSubview(inBtn)
        view.addSubview(outBtn)
        view.addSubview(onTimeBtn)
        
        view.addSubview(lineView)

        view.addSubview(delayBtn)
        view.addSubview(breakBtn)
        view.addSubview(emergencyBtn)
        view.addSubview(roundBtn)
        view.addSubview(otBtn)
        view.addSubview(meetingBtn)
        view.addSubview(leaveBtn)
        
        
        inBtn.setTitle("IN", for: .normal)
        outBtn.setTitle("OUT", for: .normal)
        onTimeBtn.setTitle("ON TIME", for: .normal)
        delayBtn.setTitle("DELAY", for: .normal)
        breakBtn.setTitle("BREAK", for: .normal)
        emergencyBtn.setTitle("EMERGENCY", for: .normal)
        roundBtn.setTitle("ROUND", for: .normal)
        otBtn.setTitle("OT", for: .normal)
        meetingBtn.setTitle("MEETING", for: .normal)
        leaveBtn.setTitle("LEAVE", for: .normal)
        lineView.backgroundColor = Theme.Color.colorLightWhite
        
        
        inBtn.tag = 1
        outBtn.tag = 2
        onTimeBtn.tag = 3
        delayBtn.tag = 4
        breakBtn.tag = 5
        emergencyBtn.tag = 6
        roundBtn.tag = 7
        otBtn.tag = 8
        meetingBtn.tag = 9
        leaveBtn.tag = 10
            
        inBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        outBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        onTimeBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        delayBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        breakBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        emergencyBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        roundBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        otBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        meetingBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
        leaveBtn.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
    }
    
    fileprivate func setupFrame(){
        changelbl.frame = CGRect(x: 10, y: 12, width: SCREEN.WIDTH, height: 20)
        let inBtnWidth: CGFloat = (SCREEN.WIDTH - 20) / 3
        inBtn.frame = CGRect(x: 10, y: changelbl.frame.origin.y + changelbl.frame.height + 12, width: inBtnWidth - 3, height: 40)
        outBtn.frame = CGRect(x: inBtn.frame.origin.x + inBtn.frame.width + 5, y: inBtn.frame.origin.y, width: inBtnWidth - 3, height: 40)
        onTimeBtn.frame = CGRect(x: outBtn.frame.origin.x + outBtn.frame.width + 5, y: inBtn.frame.origin.y, width: inBtnWidth - 3, height: 40)
        
        lineView.frame = CGRect(x: 10, y: inBtn.frame.origin.y + inBtn.frame.height + 15, width: SCREEN.WIDTH - 20, height: 1)
        
        delayBtn.frame = CGRect(x: 10, y: lineView.frame.origin.y + lineView.frame.height + 25, width: inBtnWidth - 3, height: 40)
        breakBtn.frame = CGRect(x: delayBtn.frame.origin.x + delayBtn.frame.width + 5, y: delayBtn.frame.origin.y, width: inBtnWidth - 3, height: 40)
        emergencyBtn.frame = CGRect(x: breakBtn.frame.origin.x + breakBtn.frame.width + 5, y: delayBtn.frame.origin.y, width: inBtnWidth - 3, height: 40)

        let roundBtnWidth: CGFloat = (SCREEN.WIDTH - 20) / 4

        roundBtn.frame = CGRect(x: 10, y: delayBtn.frame.origin.y + delayBtn.frame.height + 12, width: roundBtnWidth - 2, height: 40)
        otBtn.frame = CGRect(x: roundBtn.frame.origin.x + roundBtn.frame.width + 5, y: roundBtn.frame.origin.y, width: roundBtnWidth - 2, height: 40)
        meetingBtn.frame = CGRect(x: otBtn.frame.origin.x + otBtn.frame.width + 5, y: roundBtn.frame.origin.y, width: roundBtnWidth - 2, height: 40)
        leaveBtn.frame = CGRect(x: meetingBtn.frame.origin.x + meetingBtn.frame.width + 5, y: roundBtn.frame.origin.y, width: roundBtnWidth - 2, height: 40)
    }
    
    //MARK:- TimeView Frame
    func setupTimeView(isDelay: Bool){
        //MARK:- time view
        view.addSubview(timeBackView)
        timeBackView.addSubview(pleaselbl)
        timeBackView.addSubview(onTheWayRadioBtn)
        timeBackView.addSubview(onTheWayRadiolbl)
        timeBackView.addSubview(personalRadioBtn)
        timeBackView.addSubview(personallbl)
        timeBackView.addSubview(approximatelbl)
        timeBackView.addSubview(tenBtn)
        timeBackView.addSubview(twentyBtn)
        timeBackView.addSubview(thirtyBtn)
        timeBackView.addSubview(fortyFiveBtn)
        timeBackView.addSubview(oneHrBtn)
        timeBackView.addSubview(oneNfiveHrBtn)
        timeBackView.addSubview(twoHrBtn)
        timeBackView.addSubview(twoNfiveHrBtn)
        timeBackView.addSubview(threeHrBtn)
        timeBackView.addSubview(fourHrBtn)
        
        tenBtn.setTitle("10 min", for: .normal)
        twentyBtn.setTitle("20 min", for: .normal)
        thirtyBtn.setTitle("30 min", for: .normal)
        fortyFiveBtn.setTitle("45 min", for: .normal)
        oneHrBtn.setTitle("1 hour", for: .normal)
        oneNfiveHrBtn.setTitle("1.5 hour", for: .normal)
        twoHrBtn.setTitle("2 hour", for: .normal)
        twoNfiveHrBtn.setTitle("2.5 hour", for: .normal)
        threeHrBtn.setTitle("3 hour", for: .normal)
        fourHrBtn.setTitle("4 hour", for: .normal)
        
        tenBtn.tag = 21
        twentyBtn.tag = 22
        thirtyBtn.tag = 23
        fortyFiveBtn.tag = 24
        oneHrBtn.tag = 25
        oneNfiveHrBtn.tag = 26
        twoHrBtn.tag = 27
        twoNfiveHrBtn.tag = 28
        threeHrBtn.tag = 29
        fourHrBtn.tag = 30
        
        
        
        let timeBackViewWidth: CGFloat = SCREEN.WIDTH
        timeBackView.frame = CGRect(x: 0, y: 0, width: timeBackViewWidth, height: self.height!)
        
        if isDelay == true {
            pleaselbl.frame = CGRect(x: 8, y: 10, width: timeBackViewWidth - 16, height: 20)
            onTheWayRadioBtn.frame = CGRect(x: 10, y: pleaselbl.frame.origin.y + pleaselbl.frame.height + 5, width: 20, height: 20)
            onTheWayRadiolbl.frame = CGRect(x: onTheWayRadioBtn.frame.origin.x + onTheWayRadioBtn.frame.width + 5, y: onTheWayRadioBtn.frame.origin.y, width: 120, height: 20)
            personalRadioBtn.frame = CGRect(x: onTheWayRadiolbl.frame.origin.x + onTheWayRadiolbl.frame.width + 10, y: onTheWayRadioBtn.frame.origin.y, width: 20, height: 20)

            personallbl.frame = CGRect(x: personalRadioBtn.frame.origin.x + personalRadioBtn.frame.width + 10, y: onTheWayRadioBtn.frame.origin.y, width: 120, height: 20)

            approximatelbl.frame = CGRect(x: 8, y: onTheWayRadioBtn.frame.origin.y + onTheWayRadioBtn.frame.height + 20, width: timeBackViewWidth - 16, height: 20)
        }else{
            approximatelbl.frame = CGRect(x: 8, y: 15, width: timeBackViewWidth - 16, height: 20)

        }

        let hrBtnWidth: CGFloat = (timeBackView.frame.width - 20) / 3
        tenBtn.frame = CGRect(x: 10, y: approximatelbl.frame.origin.y + approximatelbl.frame.height + 8, width: hrBtnWidth - 3, height: 40)
        twentyBtn.frame = CGRect(x: tenBtn.frame.origin.x + tenBtn.frame.width + 5, y: tenBtn.frame.origin.y, width: hrBtnWidth - 3, height: 40)
        thirtyBtn.frame = CGRect(x: twentyBtn.frame.origin.x + twentyBtn.frame.width + 5, y: tenBtn.frame.origin.y, width: hrBtnWidth - 3, height: 40)

        fortyFiveBtn.frame = CGRect(x: 10, y: tenBtn.frame.origin.y + tenBtn.frame.height + 8, width: hrBtnWidth - 3, height: 40)
        oneHrBtn.frame = CGRect(x: twentyBtn.frame.origin.x, y: fortyFiveBtn.frame.origin.y, width: hrBtnWidth - 3, height: 40)
        oneNfiveHrBtn.frame = CGRect(x: thirtyBtn.frame.origin.x, y: fortyFiveBtn.frame.origin.y, width: hrBtnWidth - 3, height: 40)

        twoHrBtn.frame = CGRect(x: 10, y: fortyFiveBtn.frame.origin.y + fortyFiveBtn.frame.height + 8, width: hrBtnWidth - 3, height: 40)
        twoNfiveHrBtn.frame = CGRect(x: twentyBtn.frame.origin.x, y: twoHrBtn.frame.origin.y, width: hrBtnWidth - 3, height: 40)
        threeHrBtn.frame = CGRect(x: thirtyBtn.frame.origin.x, y: twoHrBtn.frame.origin.y, width: hrBtnWidth - 3, height: 40)
        
        fourHrBtn.frame = CGRect(x: 10, y: twoHrBtn.frame.origin.y + twoHrBtn.frame.height + 8, width: hrBtnWidth - 3, height: 40)
        
        
        
        let image = Images.ic_radio.withRenderingMode(.alwaysTemplate)
        onTheWayRadioBtn.setImage(image, for: .normal)
        personalRadioBtn.setImage(image, for: .normal)
        onTheWayRadioBtn.tintColor = Theme.Color.colorBlue
        personalRadioBtn.tintColor = Theme.Color.lightGray
        
        onTheWayRadioBtn.addTarget(self, action: #selector(radioPress), for: .touchUpInside)
        personalRadioBtn.addTarget(self, action: #selector(radioPress), for: .touchUpInside)

        
        tenBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        twentyBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        thirtyBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        fortyFiveBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        oneHrBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        oneNfiveHrBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        twoHrBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        twoNfiveHrBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        threeHrBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
        fourHrBtn.addTarget(self, action: #selector(timeBtnTapped(sender:)), for: .touchUpInside)
    }
    
    
    @objc private func radioPress(){
        isOnTheWayRadioBtn = !isOnTheWayRadioBtn
        if isOnTheWayRadioBtn == true {
            onTheWayRadioBtn.tintColor = Theme.Color.colorBlue
            personalRadioBtn.tintColor = Theme.Color.lightGray
        }else{
            onTheWayRadioBtn.tintColor = Theme.Color.lightGray
            personalRadioBtn.tintColor = Theme.Color.colorBlue
        }
    }
    
    @objc fileprivate func timeBtnTapped(sender: UIButton) {
        let tag = sender.tag
        var timeString: String = ""
        if tag == 21 {
            timeString = "10 min"
        }else if tag == 22 {
            timeString = "20 min"
        }else if tag == 23 {
            timeString = "30 min"
        }else if tag == 24 {
            timeString = "45 min"
        }else if tag == 25 {
            timeString = "1 hour"
        }else if tag == 26 {
            timeString = "1.5 hour"
        }else if tag == 27 {
            timeString = "2 hour"
        }else if tag == 28 {
            timeString = "2.5 hour"
        }else if tag == 29 {
            timeString = "3 hour"
        }else if tag == 30 {
            timeString = "4 hour"
        }
        
//        (delaytime, frmdate, todate, reason, status)
        let reason = isOnTheWayRadioBtn == true ? "On the way" : "Personal"
        if status == "DELAY" {
            self.reloadStatus!(timeString, "", "", reason, status)
        }else if status == "LEAVE" {
            
        }else{
            self.reloadStatus!(timeString, "", "", "", status)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func btnTapped(sender: UIButton) {
        let tag = sender.tag        
        
        if tag == 1 {
            status = "IN"
        }else if tag == 2 {
            status = "OUT"
        }else if tag == 3 {
            status = "ON TIME"
        }else if tag == 4 {
            status = "DELAY"
            setupTimeView(isDelay: true)
        }else if tag == 5 {
            status = "BREAK"
            setupTimeView(isDelay: false)
        }else if tag == 6 {
            status = "EMERGENCY"
            setupTimeView(isDelay: false)
        }else if tag == 7 {
            status = "ROUND"
            setupTimeView(isDelay: false)
        }else if tag == 8 {
            status = "OT"
            setupTimeView(isDelay: false)
        }else if tag == 9 {
            status = "MEETING"
            setupTimeView(isDelay: false)
        }else if tag == 10 {
            status = "LEAVE"
        }
        
        
        if Reachability.isConnectedToNetwork(){
            
//            (delaytime, frmdate, todate, reason, status)
            if tag == 1 || tag == 2 || tag == 3 {
                self.reloadStatus!("", "", "", "", status)
                self.dismiss(animated: true, completion: nil)
            }else {
                timeBackView.isHidden = false
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    //MARK:- BottomPopupViewController Delegate
    // Bottom popup attribute variables
    // You can override the desired variable to change appearance
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }
}

extension ChangeStatusView{
   
}
