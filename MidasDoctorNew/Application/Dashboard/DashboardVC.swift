//
//  DashboardVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 23/03/2021.
//

import UIKit

let users = DataManager.getUserDetail()[0]


class DashboardVC: BaseViewController, DashboardPresentation {
    
    var todayNepaliDate = String()
    var getnepaliFirstDate = String()
//    let userDetail = DataManager.getUserDetail()[0]

    
    //MARK: - eClass Navigation Bar
    
    //MARK: - Navigation Bar
    fileprivate let navView = NavigationView()
    
    
    fileprivate let notificationBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .clear
        
//        let image = Images.ic_notification.withRenderingMode(.alwaysTemplate)
        btn.setImage(Images.ic_notification, for: .normal)
        btn.contentMode = .scaleAspectFit
//        btn.tintColor = Theme.Color.Orange_Color
        btn.addTarget(self, action:#selector(notificationBtnPress), for: .touchUpInside)
        btn.clipsToBounds = true
        return btn
    }()
    
    fileprivate lazy var notificationlbl: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = Theme.Color.Dark_Red_Color
        lbl.textColor = Theme.Color.white
        lbl.font = UIFont.regular(ofSize: 12)
        lbl.textAlignment = .center
        lbl.clipsToBounds = true
        lbl.text = "0"
        return lbl
    }()
    
    fileprivate let profileBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = Theme.Color.white
        let image = Images.ic_user.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.lightGray
        btn.addTarget(self, action:#selector(profileBtnPress), for: .touchUpInside)
        btn.clipsToBounds = true
        return btn
    }()
    
    fileprivate let navImage: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = false
        iv.contentMode = .scaleToFill
        iv.image = Images.meroDoctor_icon
        return iv
    }()
    
    
    let refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.showsHorizontalScrollIndicator = false

        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        
        tv.register(DashboardStatusCell.self, forCellReuseIdentifier: "DashboardStatusCell")
        tv.register(DashboardPatientEmptyCell.self, forCellReuseIdentifier: "DashboardPatientEmptyCell")
        tv.register(DashboardVideoConsultationCell.self, forCellReuseIdentifier: "DashboardVideoConsultationCell")
        tv.register(DashboardRevenueCell.self, forCellReuseIdentifier: "DashboardRevenueCell")
        tv.register(DashboardPatientHeaderCell.self, forCellReuseIdentifier: "DashboardPatientHeaderCell")
        tv.register(DashboardPatientCell.self, forCellReuseIdentifier: "DashboardPatientCell")

        tv.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
//        tv.register(DashboardTakeAppointmentCell.self, forCellReuseIdentifier: "DashboardTakeAppointmentCell")
//        tv.register(DashboardSpecialityCell.self, forCellReuseIdentifier: "DashboardSpecialityCell")
//        tv.register(DashboardAvailableMedicalSpecialitesCell.self, forCellReuseIdentifier: "DashboardAvailableMedicalSpecialitesCell")

        
        if #available(iOS 10.0, *) {
            tv.refreshControl = refresher
        }else{
            tv.addSubview(refresher)
        }
        
        
        return tv
    }()
    
    lazy var listArray: [listModel] = []
    lazy var assocArray: [associatedhospitalModel] = []
    lazy var basicArray: [basicModel] = []
    lazy var patientArray: [PatientListModel] = []
    lazy var filteredPatientArray: [PatientListModel] = []
    lazy var myRevenue = [MyRevenueModel]()
    lazy var slotData = [SlotInfoModel]()


    private var presenter: DashboardPresenter!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = Theme.Color.dark_Green_Color 
        presenter = DashboardPresenter(controller: self)
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.checkNetworkConnection()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        
    }
    
    //MARK:- setupViews
    func setupViews() {
        //WHITE_DISABLE_COLOR

        view.addSubview(navView)
        navView.addSubview(navImage)
        navView.addSubview(profileBtn)
        navView.addSubview(notificationBtn)
        navView.addSubview(notificationlbl)
        view.addSubview(tableView)
    }
    
    //MARK:- setupConstraint
    func setupConstraint(){
        
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        navImage.frame = CGRect(x: 15, y: 12, width: 90, height: 30)
        profileBtn.frame = CGRect(x: navView.frame.width - 40, y: 12.5, width: 30, height: 30)

        notificationBtn.frame = CGRect(x: profileBtn.frame.origin.x  - 50, y: 12.5, width: 30, height: 30)
        notificationlbl.frame = CGRect(x: notificationBtn.frame.origin.x + notificationBtn.frame.width - 13, y: notificationBtn.frame.origin.y - 3, width: 20, height: 20)
        
        notificationlbl.layer.cornerRadius = 10
        profileBtn.layer.cornerRadius = 15
        
        tableView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)
    }
    
    @objc func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetworkConnection()
                self.refresher.endRefreshing()
            }
        }else{
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    //MARK:- checkNetworkConnection
    fileprivate func checkNetworkConnection(){
        if Reachability.isConnectedToNetwork(){
            self.activityIndicatorBegin()
            
            let engDate : String = Utility.todayDate() //Date().todayDate(format: String.DATE_FORMATE)
            todayNepaliDate = getNepaliDate(engDate: engDate)            
            getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
            filteredPatientArray.removeAll()
            DispatchQueue.main.async {
                self.presenter.doctorStatusApiCall()
            }
            DispatchQueue.main.async {
                if users.islivemerodoctor == "Y" {
                    //self.presenter.telemedicinepatientlistAPICall(fromdate: self.getnepaliFirstDate, todate: self.todayNepaliDate)
                    self.presenter.telemedicinepatientlistAPICall(fromdate: self.todayNepaliDate, todate: self.todayNepaliDate)
                }else{
                    self.presenter.localPatientlistAPICall(fromdate: self.todayNepaliDate, todate: self.todayNepaliDate)
                }
            }
           
            DispatchQueue.main.async {
                self.presenter.getRevenueAPICall(fromdate: self.getnepaliFirstDate, todate: self.todayNepaliDate)
            }
            
            DispatchQueue.main.async {
                self.presenter.getSlotInfoAPICall()
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    //MARK:- Get Data
    func displayDoctorStatus(listData: [listModel], assocData: [associatedhospitalModel], basicData: [basicModel]){
        self.activityIndicatorEnd()
        
        listArray = listData
        assocArray = assocData
        basicArray = basicData      

        tableView.reloadData()
    }
    
    
    func displayPatientList(data: Array<PatientListModel>) {
        self.activityIndicatorEnd()
        patientArray = data
//        for item in data {
//            print(item.patientstatus)
//            if item.patientstatus == "NOT READY" || item.patientstatus == "Waiting" || item.patientstatus == "LIVE" || item.patientstatus == "CALLED" {
//                filteredPatientArray.append(item)
//            }
//        }
        filteredPatientArray = data
        print(filteredPatientArray)
        tableView.reloadData()
    }
    
    func myRevenue(data: MyRevenueModel){
        self.activityIndicatorEnd()
        myRevenue = [data]
        tableView.reloadData()
    }

    func slotData(data: SlotInfoModel){
        self.activityIndicatorEnd()
        slotData = [data]
        tableView.reloadData()
    }
    
    func displayError(error: ApplicationError) {
        self.refresher.endRefreshing()
        self.activityIndicatorEnd()
        self.toastMessage(message: error.localizedDescription, toastType: .message)
    }
    
    
    @objc fileprivate func profileBtnPress(){
        if !defaults.isUserLoggedIn()! {
            let vc = LoginVC()
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = ProfileVC()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc fileprivate func notificationBtnPress(){
//        let vc = NotificationVC()
//        navigationController?.pushViewController(vc, animated: true)
    }
    
}


//MARK:- UITableViewDelegate
extension DashboardVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1{
            if patientArray.isEmpty {
                return 1
            }
            return filteredPatientArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 120
        }else if indexPath.section == 1 {
            if patientArray.isEmpty {
                return 245
            }
            return 100
        }else if indexPath.section == 2 {
            return 115 //220
        }else {
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let dashboardBannerCell = tableView.dequeueReusableCell(withIdentifier: "DashboardStatusCell", for: indexPath) as! DashboardStatusCell
            dashboardBannerCell.selectionStyle = .none
            // Set cell's delegate
            dashboardBannerCell.docNamelbl.text = users.fullname
            dashboardBannerCell.hospitalNamelbl.text = users.orgname
            dashboardBannerCell.addresslbl.text = users.address
            dashboardBannerCell.statuslbl.text = "Will be on"

            if listArray.count > 0{
                dashboardBannerCell.data = listArray[indexPath.row]
            }
            
//            if assocArray.count > 0{
//                dashboardBannerCell.assocdata = [assocArray[indexPath.row]]
//            }
            
            dashboardBannerCell.changeBtn.addTarget(self, action: #selector(changeStatusBtnPress), for: .touchUpInside)
            return dashboardBannerCell
        }else if indexPath.section == 1 {
            if patientArray.count < 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardPatientEmptyCell", for: indexPath) as! DashboardPatientEmptyCell
                cell.selectionStyle = .none
                cell.refreshBtn.addTarget(self, action: #selector(refreshBtnPress), for: .touchUpInside)
                return cell
            }
            let doctorDashboardCell = tableView.dequeueReusableCell(withIdentifier: "DashboardPatientCell", for: indexPath) as! DashboardPatientCell
            doctorDashboardCell.selectionStyle = .none
            let data = filteredPatientArray[indexPath.row]
            doctorDashboardCell.data = data
            return doctorDashboardCell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardVideoConsultationCell", for: indexPath) as! DashboardVideoConsultationCell
            cell.selectionStyle = .none
            
//            let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(videoConsultViewTap(sender:)))
//            tapGesture1.numberOfTapsRequired = 1
//            cell.videoConsultationView.addGestureRecognizer(tapGesture1)

            let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(opdAppointmentViewTap(sender:)))
            tapGesture2.numberOfTapsRequired = 1
            cell.opdAppointmentView.addGestureRecognizer(tapGesture2)
            
            let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(videoAppointmentViewTap(sender:)))
            tapGesture3.numberOfTapsRequired = 1
            cell.videoAppointmentView.addGestureRecognizer(tapGesture3)
            
            let opdtxt = NSMutableAttributedString(string:"OPD Appointment\n", attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 15)])
            
            var opdString = "0/0    "
            var teleString = "0/0    "
            if slotData.count > 0{
                opdString =  slotData[0].OPD + "     "
                teleString =  slotData[0].TELEMEDICINE + "     "

            }
            opdtxt.append(NSAttributedString(string: opdString,
                                          attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 15)]))
            opdtxt.append(NSAttributedString(string: todayNepaliDate,
                                          attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 14)]))
            cell.opdAppointmentlbl.attributedText = opdtxt
            
            let videoTxt = NSMutableAttributedString(string:"Video Appointment\n", attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 14)])
            videoTxt.append(NSAttributedString(string: teleString,
                                          attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 16)]))
            videoTxt.append(NSAttributedString(string: todayNepaliDate, attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 14)]))
            cell.videoAppointmentlbl.attributedText = videoTxt
            
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardRevenueCell", for: indexPath) as! DashboardRevenueCell
        cell.selectionStyle = .none
        
        if myRevenue.count > 0 {
            cell.totalAmountlbl.text = "Rs. " + myRevenue[0].netfraction
            cell.datelbl.text = getnepaliFirstDate + "\n" + "To" + "\n" + todayNepaliDate

        }
        
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(revenueViewTap(sender:)))
        tapGesture4.numberOfTapsRequired = 1
        cell.revenueView.addGestureRecognizer(tapGesture4)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if patientArray.count > 0 {
                let vc = PatientProfileVC()
                vc.patientArray = [self.filteredPatientArray[indexPath.row]]
                navigationController?.pushViewController(vc, animated: true)
            }
           
        }
        
        if indexPath.row == 0 {
//            let vc = HospitalProfileVC()
//            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 { return 35 }
        else { return 0 }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardPatientHeaderCell") as! DashboardPatientHeaderCell
            cell.seeAllBtn.addTarget(self, action:#selector(patientAllBtnPress(sender:)), for: .touchUpInside)
            return cell
        }
        let backView: UIView = {
            let v = UIView()
            v.backgroundColor = .clear
            return v
        }()
        
        return backView
    }
    
    
    //MARK:- patient All Btn Press Tap
    @objc func patientAllBtnPress(sender: UIButton!){
        print("View all Hospital ..")
        let vc = PatientLandingVC()
//        vc.toDate = todayNepaliDate
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- changeStatusBtnPress
    @objc func changeStatusBtnPress(){
        print("changeStatusBtnPress")
        
        let popupVC = ChangeStatusView()
        popupVC.height = 290
        popupVC.topCornerRadius = 10
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.shouldDismissInteractivelty = true
        popupVC.popupDelegate = self
        
        popupVC.reloadStatus = {[weak self] (delaytime, frmdate, todate, reason, status) in
                self?.changeStatusApiCall(delaytime: delaytime, frmdate: frmdate, todate: todate, reason: reason, statusString: status)
        }
        present(popupVC, animated: true, completion: nil)
//        navigationController?.pushViewController(popupVC, animated: true)
//        present(popupVC, animated: true, completion: nil)
    }
    
    //MARK:- refreshBtnPress
    @objc func refreshBtnPress(){
        print("refreshBtnPress")
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetworkConnection()
                self.refresher.endRefreshing()
            }
        }else{
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    /*
    //MARK:- videoConsultViewTap
    @objc func videoConsultViewTap(sender : UITapGestureRecognizer) {
        print("videoConsultViewTap")
        let vc = VideoConsultationVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    */
    //MARK:- opdAppointmentViewTap
    @objc func opdAppointmentViewTap(sender : UITapGestureRecognizer) {
        print("opdAppointmentViewTap")
        let vc = OPDAppointmentVC()
        vc.type = "OPD"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- videoAppointmentViewTap
    @objc func videoAppointmentViewTap(sender : UITapGestureRecognizer) {
        print("videoAppointmentViewTap")
        let vc = OPDAppointmentVC()
        vc.type = "TELE"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- revenueViewTap
    @objc func revenueViewTap(sender : UITapGestureRecognizer) {
        print("revenueViewTap")
        let vc = RevenueVC()
        vc.fromDate = self.getnepaliFirstDate
        vc.toDate = self.todayNepaliDate
        vc.myRevenue = self.myRevenue
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension DashboardVC: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}

extension DashboardVC{
    fileprivate func changeStatusApiCall(delaytime: String, frmdate: String, todate: String, reason: String, statusString: String){
        self.activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.changeDocStatus(orgid: users.orgid ?? "", gdocid: users.gdocid ?? "", delaytime: delaytime, frmdate: frmdate, todate: todate, reason: reason, status: statusString)) { (status, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                self.toastMessage(message:  data["message"].string, toastType: .success)
                let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DashboardStatusCell
                cell.statuslbl.text = statusString
//                DashboardStatusCell
//                self.
            }else{
                self.toastMessage(message:  data["message"].string, toastType: .failure)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }

    }
}
