//
//  DashboardPresenter.swift
//  MeroDoctorNew
//
//  Created by ramesh prajapati on 2/3/21.
//

import UIKit

protocol DashboardPresentation: BasePresentation {
    
    func displayError(error: ApplicationError)
//    func displayDepartmentData(arr: Array<DepartmentModel>)
//    func displayDoctorData(arr: Array<DoctorModel>)
//    func displayHospitalData(arr: Array<HospitalModel>)
//    func displayAppointmentData(arr: Array<UpcomingAppointmentModel>)
    func displayDoctorStatus(listData: Array<listModel>, assocData: Array<associatedhospitalModel>, basicData: Array<basicModel>)
    
    func displayPatientList(data: Array<PatientListModel>)
    func myRevenue(data: MyRevenueModel)
    func slotData(data: SlotInfoModel)
}

class DashboardPresenter {
    weak var viewDelegate: DashboardPresentation!
    lazy var statusArray = [statusModel]()
    
    lazy var patientArray = [PatientListModel]()
//    lazy var myrevenue = MyRevenueModel()
    
//    lazy var docArray = [DoctorModel]()
//    lazy var hospitalArray = [HospitalModel]()
//    lazy var appointmetArray = [UpcomingAppointmentModel]()

    init(controller: DashboardPresentation) {
        self.viewDelegate = controller
        self.viewDelegate.setupViews()
        self.viewDelegate.setupConstraint()
    }
    
    func doctorStatusApiCall(){
        let users = DataManager.getUserDetail()[0]
        ApiManager.sendRequest(toApi: Api.Endpoint.getDocStatus(docid: users.gdocid ?? "", orgid: users.orgid ?? ""), onSuccess: {(statusCode, data) in
            if data["type"].string == "success"{
                if let responseData = data["response"].dictionary{
                    
                    
                    var associatedhospital: [associatedhospitalModel] = []
                    var basicDic: [basicModel] = []
                    var listArray: [listModel] = []
                    
                    if let associatedhospitalArray = responseData["associatedhospital"]?.array{
                        for hospitalData in associatedhospitalArray {
                            let item = associatedhospitalModel.init(
                                orna_address: hospitalData["orna_address"].stringValue,
                                docid: hospitalData["docid"].stringValue,
                                ehstime: hospitalData["ehstime"].stringValue,
                                docname: hospitalData["docname"].stringValue,
                                orgid: hospitalData["orgid"].stringValue,
                                orgimageurl: hospitalData["orgimageurl"].stringValue,
                                feeehs: hospitalData["feeehs"].stringValue,
                                orgfullname: hospitalData["orgfullname"].stringValue,
                                generaltime: hospitalData["generaltime"].stringValue,
                                isgovernment: hospitalData["isgovernment"].stringValue,
                                bannerimage: hospitalData["bannerimage"].stringValue,
                                feegeneral: hospitalData["feegeneral"].stringValue,
                                orgcode: hospitalData["orgcode"].stringValue)
                            
                            associatedhospital.append(contentsOf: [item])
                        }
                        
                    }
                    
                    if let basicData = responseData["basic"]?.dictionary{
                        let item1 = basicModel.init(ndepname: basicData["ndepname"]?.string ?? "", depname: basicData["depname"]?.string ?? "", bsdate: basicData["bsdate"]?.string ?? "", addate: basicData["addate"]?.string ?? "", depid: basicData["depid"]?.string ?? "", gdepid: basicData["gdepid"]?.string ?? "")
                        
                        basicDic.append(item1)
                    }
                    
                    
                    if let listArr = responseData["list"]?.array{
                        for listArrayData in listArr {
                            let item2 = listModel.init(orgname:  listArrayData["orgname"].stringValue,
                                                       leavefromdate:  listArrayData["leavefromdate"].stringValue,
                                                       docstatus:  listArrayData["docstatus"].stringValue,
                                                       depname:  listArrayData["depname"].stringValue,
                                                       ndepname:  listArrayData["ndepname"].stringValue,
                                                       reason:  listArrayData["reason"].stringValue,
                                                       delaytime:  listArrayData["delaytime"].stringValue,
                                                       docname:  listArrayData["docname"].stringValue,
                                                       orgid:  listArrayData["orgid"].stringValue,
                                                       gdocid:  listArrayData["gdocid"].stringValue,
                                                       gdepid:  listArrayData["gdepid"].stringValue,
                                                       bsdate:  listArrayData["bsdate"].stringValue,
                                                       doct_docid:  listArrayData["doct_docid"].stringValue,
                                                       time:  listArrayData["time"].stringValue,
                                                       statusid:  listArrayData["statusid"].stringValue,
                                                       depid:  listArrayData["depid"].stringValue,
                                                       leavetodate:  listArrayData["leavetodate"].stringValue,
                                                       addate:  listArrayData["addate"].stringValue)
                            
                            listArray.append(item2)
                        }
                    }
                    self.viewDelegate.displayDoctorStatus(listData: listArray, assocData: associatedhospital, basicData: basicDic)

                }
            } else {
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.viewDelegate.displayError(error: ApiError.invalidResponse(message: message))
            }

        }) { (error) in
            self.viewDelegate.displayError(error: ApiError.invalidData)
        }
        
    }
    
    //MARK:- telemedicinepatientlistAPICall
    func telemedicinepatientlistAPICall(fromdate: String, todate: String){
        let users = DataManager.getUserDetail()[0]
        ApiManager.sendRequest(toApi: Api.Endpoint.telemedicinepatientlist(docid: users.gdocid ?? "", orgid: users.orgid ?? "", type: "Upcoming", fromdate: fromdate, todate: todate)) { (status, data) in
            if data["type"].string == "success"{
                if let responseArray = data["response"].array {
                    self.patientArray.removeAll()
                    for responseDic in responseArray{
                        print(responseDic)
//                        let item = PatientListModel(json: responseDic)
//                        self.patientArray.append(item)
                        let item = PatientListModel(districtname: responseDic["districtname"].string ?? "",
                                                    appo_apptime: responseDic["appo_apptime"].string ?? "",
                                                    midasid: responseDic["midasid"].string ?? "",
                                                    appdatenep: responseDic["appdatenep"].string ?? "",
                                                    appdateng: responseDic["appdateng"].string ?? "",
                                                    healthpartner: responseDic["healthpartner"].string ?? "",
                                                    title: responseDic["title"].string ?? "",
                                                    appo_callstatus: responseDic["appo_callstatus"].string ?? "",
                                                    age: responseDic["age"].string ?? "",
                                                    queueno: responseDic["queueno"].string ?? "",
                                                    appid: responseDic["appid"].string ?? "",
                                                    address: responseDic["address"].string ?? "",
                                                    mobileno: responseDic["mobileno"].string ?? "",
                                                    patientid: responseDic["patientid"].string ?? "",
                                                    gender: responseDic["gender"].string ?? "",
                                                    patientname: responseDic["patientname"].string ?? "",
                                                    patientstatus: responseDic["patientstatus"].string ?? "",
                                                    agetype: responseDic["agetype"].string ?? "",
                                                    isurgent:responseDic["isurgent"].string ?? ""
                        )
                        self.patientArray.append(item)
                        
                    }
                    self.viewDelegate.displayPatientList(data: self.patientArray)
                }
                

            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.viewDelegate.displayError(error: ApiError.invalidResponse(message: message))
            }
        } onError: { (error) in
            self.viewDelegate.displayError(error: ApiError.invalidData)
        }

    }

    //MARK:- telemedicinepatientlistAPICall
    func localPatientlistAPICall(fromdate: String, todate: String){
//        let users = DataManager.getUserDetail()[0]
        ApiManager.sendRequest(toApi: Api.Endpoint.patientlist_local(docid: users.gdocid ?? "", fromdate: fromdate, todate: todate)) { (status, data) in
            if data["type"].string == "success"{
                if let responseArray = data["response"].array {
//                    self.patientArray.removeAll()
//                    for responseDic in responseArray{
//                        let item = PatientListModel(json: responseDic)
//                        self.patientArray.append(item)
//                    }
                }
//                self.viewDelegate.displayPatientList(data: self.patientArray)

            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.viewDelegate.displayError(error: ApiError.invalidResponse(message: message))
            }
        } onError: { (error) in
            self.viewDelegate.displayError(error: error.localizedDescription as! ApplicationError)
        }

    }
    
    
    func getRevenueAPICall(fromdate: String, todate: String){
        let users = DataManager.getUserDetail()[0]
        ApiManager.sendRequest(toApi: Api.Endpoint.getrevenue(fromdate: fromdate, todate: todate, orgid: users.orgid ?? "", gdocid: users.gdocid ?? "", iscategory: "")) { (status, data) in
            if data["type"].string == "success"{
                if let responseDic = data["response"].dictionary {
                    let myrevenue = MyRevenueModel(tdsamount: responseDic["tdsamount"]?.string ?? "",
                                                   totalfraction: responseDic["totalfraction"]?.string ?? "",
                                                   netfraction: responseDic["netfraction"]?.string ?? "",
                                                   qty: responseDic["qty"]?.string ?? "")
                    
//                    self.patientArray.removeAll()
//                    for responseDic in responseArray{
//                        let item = PatientListModel(json: responseDic)
//                        self.patientArray.append(item)
//                    }
                    self.viewDelegate.myRevenue(data: myrevenue)
                }
//                self.viewDelegate.displayPatientList(data: self.patientArray)
                
            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.viewDelegate.displayError(error: ApiError.invalidResponse(message: message))
            }
        } onError: { (error) in
            self.viewDelegate.displayError(error: ApiError.invalidData)
        }

    }
//    (fromdate: fromdate, todate: todate, orgid: users.orgid ?? "", gdocid: users.gdocid ?? "", iscategory: "")
    func getSlotInfoAPICall(){
//        let users = DataManager.getUserDetail()[0]
        ApiManager.sendRequest(toApi: Api.Endpoint.getslotinfo(orgid: users.orgid ?? "", gdocid: users.gdocid ?? "")) { (status, data) in
            if data["type"].string == "success"{
                
                
                
                if let responseDic = data["response"].dictionary {
                    let slot = SlotInfoModel(OPD: responseDic["OPD"]?.string ?? "", TELEMEDICINE: responseDic["TELEMEDICINE"]?.string ?? "")
                    self.viewDelegate.slotData(data: slot)
                }
            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.viewDelegate.displayError(error: ApiError.invalidResponse(message: message))
            }
        } onError: { (error) in
            self.viewDelegate.displayError(error: ApiError.invalidData)
        }

    }
    
   
    
    
    
    deinit{
        print("OS reclaiming memory - No Retain Cycle/Leak!")
    }
    
}
