//
//  PrescriptionDetailsViewController.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 21/07/2021.
//

import UIKit

class PrescriptionDetailsViewController: UIViewController {

    @IBOutlet weak var backbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
    }
    func uisetup() {
        let image = Images.Back_Icon.withRenderingMode(.alwaysTemplate)
        backbtn.setImage(image, for: .normal)
        backbtn.tintColor = Theme.Color.black
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

   

}
