//
//  ProfileVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 30/03/2021.
//

import UIKit

class ProfileVC: UIViewController {
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()

    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.contentMode = .scaleToFill
        sv.clipsToBounds = true
        sv.bounces = false
        sv.backgroundColor = Theme.Color.backgroundGray
        sv.showsVerticalScrollIndicator = false
        sv.showsHorizontalScrollIndicator = false
        return sv
    }()
    
    fileprivate lazy var profileImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.clipsToBounds = true
        return iv
    }()
 
    fileprivate lazy var doctorNamelbl: UILabel = {
       let v = UILabelTextProperties()
        v.textAlignment = .center
//        v.text = "Doctor Name"
        return v
    }()
    
    fileprivate lazy var doctorSpecialitylbl: UILabel = {
       let v = UILabel()
        v.textColor = Theme.Color.darkGray
        v.textAlignment = .center
        v.numberOfLines = 2
        v.font = UIFont.medium(ofSize: 13)
//        v.text = "Doctor Speciality"
        return v
    }()
    
    
    fileprivate lazy var backView: UIView = {
       let v = BackViewProperties()
        
        return v
    }()
    
    fileprivate lazy var experience: UILabel = {
       let v = UILabelTextProperties()
        v.text = "Experience"
        return v
    }()
    
    fileprivate lazy var experiencelbl: UILabel = {
       let txt = UILabel()
//        v.text = "Experience Text"
        txt.textColor = .darkGray
        txt.font = UIFont.regular(ofSize: 13)
        return txt
    }()
    
    let experienceView: UIView = {
       let v = UIView()
        v.backgroundColor = Theme.Color.colorBlue
        return v
    }()
    
    fileprivate lazy var qualification: UILabel = {
       let v = UILabelTextProperties()
        v.text = "Qualification"
        return v
    }()
    
    fileprivate lazy var qualificationlbl: UILabel = {
       let txt = UILabel()
//        txt.text = "Qualification Text"
        txt.font = UIFont.regular(ofSize: 13)
        txt.textColor = .darkGray
        return txt
    }()
    
    let qualificationView: UIView = {
       let v = UIView()
        v.backgroundColor = Theme.Color.colorBlue
        return v
    }()
    
    fileprivate lazy var hospitalName: UILabel = {
       let v = UILabelTextProperties()
        v.text = "Associated Hospital"
        return v
    }()
    
    fileprivate lazy var hospitalNamelbl: UILabel = {
       let txt = UILabel()
//        v.text = "Hospital Name Text"
        txt.textColor = .darkGray
        txt.font = UIFont.regular(ofSize: 13)
        return txt
    }()
        
    fileprivate lazy var passwordBtn: UIButton = {
       let v = UIButton()
        v.setTitle("Change Password", for: .normal)
        v.setTitleColor(Theme.Color.white, for: .normal)
        v.backgroundColor = Theme.Color.dark_Green_Color
        v.titleLabel?.font = UIFont.medium(ofSize: 15)
        v.addTarget(self, action: #selector(passwordBtnPress), for: .touchUpInside)
        v.layer.cornerRadius = 8
        return v
    }()
    fileprivate lazy var logoutBtn: UIButton = {
       let v = UIButton()
        v.setTitle("Logout", for: .normal)
        v.setTitleColor(Theme.Color.white, for: .normal)
        v.backgroundColor = Theme.Color.colorBlue
        v.titleLabel?.font = UIFont.medium(ofSize: 15)
        v.addTarget(self, action: #selector(logoutBtnPress), for: .touchUpInside)
        v.layer.cornerRadius = 8
        return v
    }()
   
    lazy var userImage = UIImage()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupFrame()
        loadData()
    }
    
    //MARK:- setupView
    fileprivate func setupView(){
        view.backgroundColor = Theme.Color.backgroundGray
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)

        view.addSubview(scrollView)
        scrollView.addSubview(profileImage)
        scrollView.addSubview(doctorNamelbl)
        scrollView.addSubview(doctorSpecialitylbl)

        scrollView.addSubview(backView)
        backView.addSubview(experience)
        backView.addSubview(experiencelbl)
        backView.addSubview(experienceView)
        
        backView.addSubview(qualification)
        backView.addSubview(qualificationlbl)
        backView.addSubview(qualificationView)
        
        backView.addSubview(hospitalName)
        backView.addSubview(hospitalNamelbl)
//        backView.addSubview(hospitalNameView)
        
        view.addSubview(passwordBtn)
        view.addSubview(logoutBtn)
        
        titlelbl.text = "My Profile"
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)

        
    }
    
    //MARK:- setupFrame
    fileprivate func setupFrame(){
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: 50, y: 0, width: SCREEN.WIDTH - 50 - 50 , height: 55)
        
        logoutBtn.frame = CGRect(x: 10, y: SCREEN.HEIGHT - 60, width: SCREEN.WIDTH - 20, height: 50)
        passwordBtn.frame = CGRect(x: 10, y: logoutBtn.frame.origin.y - 60, width: SCREEN.WIDTH - 20, height: 50)

        scrollView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height - 60 - 60)
        
        let scrollWidth = scrollView.frame.width
        profileImage.frame = CGRect(x: scrollWidth / 2 - 40, y: 10, width: 80, height: 80)
        doctorNamelbl.frame = CGRect(x: 20, y: profileImage.frame.origin.y + profileImage.frame.height + 5, width: scrollWidth - 40, height: 21)
        doctorSpecialitylbl.frame = CGRect(x: 20, y: doctorNamelbl.frame.origin.y + doctorNamelbl.frame.height, width: scrollWidth - 40, height: 36)
        backView.frame = CGRect(x: 10, y: doctorSpecialitylbl.frame.origin.y + doctorSpecialitylbl.frame.height + 5, width: scrollWidth - 20, height: 180)

        let backViewWidth = backView.frame.width
        experience.frame = CGRect(x: 5, y: 10, width: backViewWidth - 10, height: 18)
        experiencelbl.frame = CGRect(x: 10, y: experience.frame.origin.y + experience.frame.height + 5, width: backViewWidth - 20, height: 18)
        experienceView.frame = CGRect(x: 5, y: experiencelbl.frame.origin.y + experiencelbl.frame.height + 5, width: backViewWidth - 10, height: 1)

        qualification.frame = CGRect(x: 5, y: experienceView.frame.origin.y + experienceView.frame.height + 10, width: backViewWidth - 10, height: 18)
        qualificationlbl.frame = CGRect(x: 10, y: qualification.frame.origin.y + qualification.frame.height + 5, width: backViewWidth - 20, height: 18)
        qualificationView.frame = CGRect(x: 5, y: qualificationlbl.frame.origin.y + qualificationlbl.frame.height + 5, width: backViewWidth - 10, height: 1)
    
        
        hospitalName.frame = CGRect(x: 5, y: qualificationView.frame.origin.y + qualificationView.frame.height + 10, width: backViewWidth - 10, height: 18)
        hospitalNamelbl.frame = CGRect(x: 10, y: hospitalName.frame.origin.y + hospitalName.frame.height + 5, width: backViewWidth - 20, height: 18)
//        hospitalNameView.frame = CGRect(x: 5, y: hospitalNamelbl.frame.origin.y + hospitalNamelbl.frame.height + 5, width: backViewWidth - 10, height: 1)

        
        scrollView.contentSize = CGSize(width: SCREEN.WIDTH, height: backView.frame.origin.y + backView.frame.height + 20)
                
        profileImage.layer.cornerRadius = 40
        profileImage.layer.borderWidth = 3
        profileImage.layer.borderColor = UIColor.white.cgColor
       
    }

    
    fileprivate func loadData(){
        let userDetail = DataManager.getUserDetail()[0]        
        doctorNamelbl.text = userDetail.fullname == "" ? "N/A" : userDetail.fullname
        doctorSpecialitylbl.text = "N/A" //userDetail.orgname == "" ? "N/A" : userDetail.orgname
        
        experiencelbl.text = "N/A"
        qualificationlbl.text = "N/A"
        hospitalNamelbl.text = userDetail.orgname == "" ? "N/A" : userDetail.orgname
        
        
        if userImage.size.width != 0 {
            profileImage.image = userImage
        }else{
            profileImage.sd_setImage(with: URL(string: userDetail.orgimageurl!), placeholderImage: Images.ic_user)
        }
        
    }
    
    //MARK:- Password Btn Press
    @objc func passwordBtnPress(){
        let vc = ChangePasswordVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- Logout Btn Press
    @objc func logoutBtnPress(){
        defaults.setUserLogin(value: false)
//                clearAndRemoveUserProfileDetails()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.initLoggedOutFlow()
    }
    
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
}
