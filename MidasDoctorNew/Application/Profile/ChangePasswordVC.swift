//
//  ChangePasswordVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 21/05/2021.
//

import UIKit

class ChangePasswordVC: BaseViewController {
    
    private var isPasswordShown1: Bool = false
    private var isPasswordShown2: Bool = false
    private var isPasswordShown3: Bool = false
    
    //MARK: - Navigation Bar
    fileprivate let navView = NavigationView()
    fileprivate let backBtn = BackButtonProperties()
    fileprivate let titlelbl = NavigationTitleProperties()
    
    fileprivate let scrollView: UIScrollView = {
        let sv = TPKeyboardAvoidingScrollView()
        sv.contentMode = .scaleToFill
        sv.clipsToBounds = true
        sv.bounces = false
        sv.backgroundColor = Theme.Color.white
        sv.showsVerticalScrollIndicator = false
        sv.showsHorizontalScrollIndicator = false
        return sv
    }()
    
    fileprivate let pwdImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.clipsToBounds = true
        iv.image = Images.ic_change_password
        return iv
    }()
    
    fileprivate let changelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Change Your Password?"
        lbl.font = UIFont.bold(ofSize: 25)
        lbl.textColor = Theme.Color.black
        lbl.textAlignment = .center
        lbl.numberOfLines = 2
        return lbl
    }()
    
    fileprivate lazy var tipslbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let backView = BackViewProperties()
    
    fileprivate lazy var currentPwdText: UITextField = {
        let txt = UITextField()
        txt.placeholder = String(format: "%@", "Current Password")
        txt.textColor = Theme.Color.darkGray
        txt.returnKeyType = .next
        txt.delegate = self
        
        
        txt.backgroundColor = UIColor.white
        txt.font = UIFont.systemFont(ofSize: 16)
        txt.borderStyle = .none
        txt.background = Images.ic_textfield_background
        txt.tag = 1
        return txt
    }()
    
    fileprivate lazy var newPwdText: UITextField = {
        let txt = UITextField()
        txt.placeholder = String(format: "%@", "New Password")
        txt.textColor = Theme.Color.darkGray
        txt.returnKeyType = .next
        txt.delegate = self
        
        txt.backgroundColor = UIColor.white
        txt.font = UIFont.systemFont(ofSize: 16)
        txt.borderStyle = .none
        txt.background = Images.ic_textfield_background
        txt.tag = 2
        return txt
    }()
    
    fileprivate lazy var confirmPwdText: UITextField = {
        let txt = UITextField()
        txt.placeholder = String(format: "%@", "Confirm Password")
        txt.textColor = Theme.Color.darkGray
        txt.returnKeyType = .continue
        txt.delegate = self
        
        txt.backgroundColor = UIColor.white
        txt.font = UIFont.systemFont(ofSize: 16)
        txt.borderStyle = .none
        txt.background = Images.ic_textfield_background
        txt.tag = 3
        return txt
    }()
    
    fileprivate lazy var togglePasswordButton1 = UIButton()
    fileprivate lazy var togglePasswordButton2 = UIButton()
    fileprivate lazy var togglePasswordButton3 = UIButton()

    fileprivate lazy var changePwdBtn: UIButton = {
        let btn = PrimaryActionButton()
        btn.backgroundColor = Theme.Color.dark_Green_Color
        btn.setTitle("Change Password", for: .normal)
        btn.addTarget(self, action: #selector(changePwdBtnPress), for: .touchUpInside)
        btn.layer.cornerRadius = 20
        return btn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraint()
        
        setupPasswordField1()
        setupPasswordField2()
        setupPasswordField3()
        
    }
    
    fileprivate func setupView(){
        titlelbl.text = "Change Password"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        view.addSubview(scrollView)
        
        scrollView.addSubview(pwdImage)
        scrollView.addSubview(changelbl)
        scrollView.addSubview(tipslbl)
        scrollView.addSubview(backView)
        
        backView.addSubview(currentPwdText)
        backView.addSubview(newPwdText)
        backView.addSubview(confirmPwdText)
        backView.addSubview(togglePasswordButton1)
        backView.addSubview(togglePasswordButton2)
        backView.addSubview(togglePasswordButton3)
        
        scrollView.addSubview(changePwdBtn)
        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        togglePasswordButton1.addTarget(self, action: #selector(togglePassword1Visibility), for: .touchUpInside)
        togglePasswordButton2.addTarget(self, action: #selector(togglePassword2Visibility), for: .touchUpInside)
        togglePasswordButton3.addTarget(self, action: #selector(togglePassword3Visibility), for: .touchUpInside)

        
        
        
        let tipsText = NSMutableAttributedString(string:"Tips : ", attributes: [.foregroundColor: Theme.Color.Dark_Red_Color, .font:UIFont.semibold(ofSize: 14)])
        
        tipsText.append(NSAttributedString(string: "Try changing your password once in a while to keep your security stronger.",
                                      attributes: [.foregroundColor: Theme.Color.darkGray, .font:UIFont.medium(ofSize: 14)]))
        tipslbl.attributedText = tipsText

    }

    fileprivate func setupConstraint(){
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)

        scrollView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)
        
        pwdImage.frame = CGRect(x: scrollView.frame.midX - 60, y: 20, width: 120, height: 120)
        changelbl.frame = CGRect(x: 50, y: pwdImage.frame.origin.y + pwdImage.frame.height + 5, width: scrollView.frame.width - 100, height: 50)
        tipslbl.frame = CGRect(x: 15, y: changelbl.frame.origin.y + changelbl.frame.height + 10, width: scrollView.frame.width - 30, height: 50)

        
        backView.frame = CGRect(x: 15, y: tipslbl.frame.origin.y + tipslbl.frame.height + 10, width: scrollView.frame.width - 30, height: 140)
        let backViewWidth: CGFloat = backView.frame.width
        
        currentPwdText.frame = CGRect(x: 8, y: 10, width: backViewWidth - 16, height: 35)
        newPwdText.frame = CGRect(x: 8, y: currentPwdText.frame.origin.y + currentPwdText.frame.height + 5, width: backViewWidth - 16, height: 35)
        confirmPwdText.frame = CGRect(x: 8, y: newPwdText.frame.origin.y + newPwdText.frame.height + 5, width: backViewWidth - 16, height: 35)

        
        changePwdBtn.frame = CGRect(x: scrollView.frame.width - 160, y: backView.frame.origin.y + backView.frame.height + 10, width: 150, height: 40)
        
        togglePasswordButton1.frame = CGRect(x: currentPwdText.frame.origin.x + currentPwdText.frame.size.width - 45, y: currentPwdText.frame.origin.y, width: 40, height: currentPwdText.frame.size.height)
        
        togglePasswordButton2.frame = CGRect(x: togglePasswordButton1.frame.origin.x, y: newPwdText.frame.origin.y, width: 40, height: togglePasswordButton1.frame.height)
        togglePasswordButton3.frame = CGRect(x: togglePasswordButton1.frame.origin.x, y: confirmPwdText.frame.origin.y, width: 40, height: togglePasswordButton1.frame.height)

    }
    
    func setupPasswordField1() {
        if isPasswordShown1 {
            currentPwdText.isSecureTextEntry = false
            let image = Images.pwdhide.withRenderingMode(.alwaysTemplate)
            togglePasswordButton1.setImage(image, for: .normal)
            togglePasswordButton1.tintColor = Theme.Color.black
        } else{
            currentPwdText.isSecureTextEntry = true
            let image = Images.pwdShow.withRenderingMode(.alwaysTemplate)
            togglePasswordButton1.setImage(image, for: .normal)
            togglePasswordButton1.tintColor = Theme.Color.darkGray
        }
    }
    
    func setupPasswordField2(){
        if isPasswordShown2 {
            newPwdText.isSecureTextEntry = false
            let image = Images.pwdhide.withRenderingMode(.alwaysTemplate)
            togglePasswordButton2.setImage(image, for: .normal)
            togglePasswordButton2.tintColor = Theme.Color.black
        } else{
            newPwdText.isSecureTextEntry = true
            let image = Images.pwdShow.withRenderingMode(.alwaysTemplate)
            togglePasswordButton2.setImage(image, for: .normal)
            togglePasswordButton2.tintColor = Theme.Color.darkGray
        }
    }
    
    func setupPasswordField3() {
        if isPasswordShown3 {
            confirmPwdText.isSecureTextEntry = false
            let image = Images.pwdhide.withRenderingMode(.alwaysTemplate)
            togglePasswordButton3.setImage(image, for: .normal)
            togglePasswordButton3.tintColor = Theme.Color.black
        } else{
            confirmPwdText.isSecureTextEntry = true
            let image = Images.pwdShow.withRenderingMode(.alwaysTemplate)
            togglePasswordButton3.setImage(image, for: .normal)
            togglePasswordButton3.tintColor = Theme.Color.darkGray
        }
    }
    
    
    @objc func togglePassword1Visibility() {
        self.isPasswordShown1 = !self.isPasswordShown1
        setupPasswordField1()
    }
    
    @objc func togglePassword2Visibility() {
        self.isPasswordShown2 = !self.isPasswordShown2
        setupPasswordField2()
    }
    
    @objc func togglePassword3Visibility() {
        self.isPasswordShown3 = !self.isPasswordShown3
        setupPasswordField3()
    }
    
    @objc private func changePwdBtnPress(){
        if currentPwdText.text?.count == 0{
            self.toastMessage(message: String.CURRENT_PASSWORD_EMPTY, toastType: .message)
            currentPwdText.becomeFirstResponder()
        }else if newPwdText.text?.count == 0 {
            self.toastMessage(message: String.NEW_PASSWORD_EMPTY, toastType: .message)
            newPwdText.becomeFirstResponder()
        }else if confirmPwdText.text?.count == 0 {
            self.toastMessage(message: String.CONFIRM_PASSWORD_EMPTY, toastType: .message)
            confirmPwdText.becomeFirstResponder()
        }else if currentPwdText.text!.count < 6 {
            self.toastMessage(message: String.CURRENT_PASSWORD_NOT_LESS, toastType: .message)
            currentPwdText.becomeFirstResponder()
        }else if newPwdText.text!.count < 6 {
            self.toastMessage(message: String.NEW_PASSWORD_NOT_LESS, toastType: .message)
            newPwdText.becomeFirstResponder()
        }else if confirmPwdText.text!.count < 6 {
            self.toastMessage(message: String.CONFIRM_PASSWORD_NOT_LESS, toastType: .message)
            confirmPwdText.becomeFirstResponder()
        }else{
            checkNetwork()
        }
        
    }
    
    @objc func backBtnPress() {
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate func checkNetwork(){
        if Reachability.isConnectedToNetwork() {
            changePwdApiCall()
        }else{
            self.hideHud()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    fileprivate func changePwdApiCall(){
        self.showHud(withTitle: "", and: "Password changing...")
        ApiManager.sendRequest(toApi: Api.Endpoint.changePassword(oldpassword: self.currentPwdText.text ?? "", newpassword: newPwdText.text ?? "", username: users.mobile_number ?? "")) { status, data in
            self.hideHud()
            let status = data["type"].string
            if status == "success" {
                self.toastMessage(message: data["message"].string, toastType: .success)
                
                self.currentPwdText.text = ""
                self.newPwdText.text = ""
                self.confirmPwdText.text = ""
            }else{
                self.toastMessage(message: data["message"].string, toastType: .message)
            }
        } onError: { error in
            self.hideHud()
            self.toastMessage(message: error.localizedDescription, toastType: .failure)
        }
    }
}


extension ChangePasswordVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            newPwdText.becomeFirstResponder()
        }else if textField.tag == 2{
            confirmPwdText.becomeFirstResponder()
        }else{
            newPwdText.resignFirstResponder()
            changePwdBtnPress()
        }
        return false
    }
}

