//
//  LabTestOrderViewController.swift
//  MidasDoctorNew
//
//  Created by Mousham Pradhan on 21/07/2021.
//

import UIKit

class LabTestOrderViewController: UIViewController {

    @IBOutlet weak var testQuantityLabel: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
    }
    func uisetup() {
        let image = Images.Back_Icon.withRenderingMode(.alwaysTemplate)
        backBtn.setImage(image, for: .normal)
        backBtn.tintColor = Theme.Color.black
    }
    
    @IBAction func addBtnAction(_ sender: Any) {
    }
    @IBAction func saveBtn(_ sender: Any) {
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
   
}
