//
//  PatientCancelVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 29/03/2021.
//

import UIKit

class PatientCancelVC: BaseViewController, RefreshPatientDelegate {
    func refreshBtnPress(sender: UIButton) {
        checkNetwork()
    }
    

    lazy var myArray = [PatientListModel]()
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    //MARK: - UITableView
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        tv.register(DashboardPatientCell.self, forCellReuseIdentifier: "DashboardPatientCell")
        
        if #available(iOS 10.0, *) {
            tv.refreshControl = refresher
        }else{
            tv.addSubview(refresher)
        }
        return tv
    }()
    
    lazy var errorView: EmptyPatientListProperty = {
        let vi = EmptyPatientListProperty(frame: CGRect(x: (SCREEN.WIDTH / 2) - 110, y: (SCREEN.HEIGHT / 2) - 120, width: 220, height: 203))
        vi.delegate = self
        vi.backgroundColor = .clear
        return vi
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()
        checkNetwork()
    }
    
    
    
    func setupViews() {
        view.addSubview(tableView)
    }
    
    func setupConstarint() {
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: SCREEN.HEIGHT - SCREEN.statusBarHeight - 55 - 10 - 45 - 1)
    }
    
    @objc func requestData() {
        if Reachability.isConnectedToNetwork(){
            let deadline = DispatchTime.now() + .milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                self.checkNetwork()
                self.refresher.endRefreshing()
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
            self.refresher.endRefreshing()
            self.activityIndicatorEnd()
        }
    }
    
    func checkNetwork(){
        if Reachability.isConnectedToNetwork(){
            APICall()
        }else{
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - APICall
extension PatientCancelVC {
    fileprivate func APICall(){
        self.hideError()
        self.activityIndicatorBegin()
        
        let engDate : String = Date().todayDate(format: String.DATE_FORMATE)
        let todayNepaliDate = getNepaliDate(engDate: engDate)
        
        let getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
        
        ApiManager.sendRequest(toApi: Api.Endpoint.telemedicinepatientlist(docid: users.gdocid ?? "", orgid: users.orgid ?? "", type: "cancelled", fromdate: todayNepaliDate, todate: todayNepaliDate)) { (statusCode, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                if let responseData = data["response"].array{
                    for responseDic in responseData{
//                        let item = PatientListModel(json: data)
//                        self.myArray.append(item)
                        
                        let item = PatientListModel(districtname: responseDic["districtname"].string ?? "",
                                                    appo_apptime: responseDic["appo_apptime"].string ?? "",
                                                    midasid: responseDic["midasid"].string ?? "",
                                                    appdatenep: responseDic["appdatenep"].string ?? "",
                                                    appdateng: responseDic["appdateng"].string ?? "",
                                                    healthpartner: responseDic["healthpartner"].string ?? "",
                                                    title: responseDic["title"].string ?? "",
                                                    appo_callstatus: responseDic["appo_callstatus"].string ?? "",
                                                    age: responseDic["age"].string ?? "",
                                                    queueno: responseDic["queueno"].string ?? "",
                                                    appid: responseDic["appid"].string ?? "",
                                                    address: responseDic["address"].string ?? "",
                                                    mobileno: responseDic["mobileno"].string ?? "",
                                                    patientid: responseDic["patientid"].string ?? "",
                                                    gender: responseDic["gender"].string ?? "",
                                                    patientname: responseDic["patientname"].string ?? "",
                                                    patientstatus: responseDic["patientstatus"].string ?? "",
                                                    agetype: responseDic["agetype"].string ?? "",
                                                    isurgent:responseDic["isurgent"].string ?? ""
                        )
                        self.myArray.append(item)
                        
                        self.tableView.reloadData()
                    }
                }
            } else {
                self.showError()
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message:message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.showError()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    
    func showError(){
        view.addSubview(errorView)
    }
    
    func hideError(){
        errorView.removeFromSuperview()
    }
    
}


// MARK: - UITableView Delegate
extension PatientCancelVC :UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let hospitalListCell = tableView.dequeueReusableCell(withIdentifier: "DashboardPatientCell", for: indexPath) as! DashboardPatientCell
        hospitalListCell.selectionStyle = .none
        
//        hospitalListCell.data = myArray[indexPath.row]
        
        let data = myArray[indexPath.row]
        
        hospitalListCell.data = data
        
        
        return hospitalListCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PatientProfileVC()
        vc.patientArray = [self.myArray[indexPath.row]]
        navigationController?.pushViewController(vc, animated: true)
    }
}

