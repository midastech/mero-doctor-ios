//
//  PatientProfileVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit
struct videoConsultValidationNum {
    static let notStarted = 0
    static let readyForCall = 1
    static let nurseCalled = 2
    static let roomEntered = 3
    static let doctorEntered = 4
    static let doctorExit = 5
    
}

struct MenuData {
    var title : String
    var image : UIImage
}

struct NoteModel {
    var opdid : String
    var personalnote : String
}


class PatientProfileVC: BaseViewController, DashboardPresentation {
    func displayError(error: ApplicationError) {
        
    }
    
    func displayDoctorStatus(listData: Array<listModel>, assocData: Array<associatedhospitalModel>, basicData: Array<basicModel>) {
        
    }
    
    func displayPatientList(data: Array<PatientListModel>) {
        print(patientArray)
        for item in data {
            print(item)
            if item.patientid == patientArray[0].patientid {
                filteredPatientArray.append(item)
            }
            
        }
        print(filteredPatientArray)
        videoConsultValidation()
        
    }
    
    func myRevenue(data: MyRevenueModel) {
        
    }
    
    func slotData(data: SlotInfoModel) {
        
    }
    
    func setupConstraint() {
        
    }
    
    lazy var isFrom = ""
    private var presenter: DashboardPresenter!
    var todayNepaliDate = String()
    var getnepaliFirstDate = String()
    lazy var patientArray: [PatientListModel] = []
    lazy var filteredPatientArray: [PatientListModel] = []
    var outPatientSuccess: Bool?
    var enableOutPaeitent: Bool? = false
//    let rowArray = ["Symptoms & Documents", "Personal Notes", "Examination Report", "Prescription"]
    fileprivate var rowArray = [MenuData]()
    fileprivate var noteData = [NoteModel]()

    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    //MARK: - UITableView
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        
        tv.register(PatientInfoCell.self, forCellReuseIdentifier: "PatientInfoCell")
        tv.register(HealthPartnerInfoCell.self, forCellReuseIdentifier: "HealthPartnerInfoCell")
        tv.register(PatientVideoCell.self, forCellReuseIdentifier: "PatientVideoCell")
        tv.register(PatientProfileCell.self, forCellReuseIdentifier: "PatientProfileCell")

        
        if #available(iOS 10.0, *) {
            tv.refreshControl = refresher
        }else{
            tv.addSubview(refresher)
        }
        
        return tv
    }()
    
    
    fileprivate let noteSuperView: UIView = {
        let v = UIView()
        v.backgroundColor = Theme.Color.backgroundGray
        return v
    }()
    
    fileprivate let noteView: BackViewProperties = {
        let v = BackViewProperties()
        v.backgroundColor = Theme.Color.backgroundGray
        return v
    }()
    
    fileprivate let notelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Personal Note"
        lbl.textColor = Theme.Color.colorMidas
        lbl.font = UIFont.semibold(ofSize: 17)
        return lbl
    }()
    fileprivate lazy var noteTxt: UITextView = {
        let txt = UITextView()
        txt.backgroundColor = Theme.Color.white
        txt.textColor = Theme.Color.darkGray
        txt.font = UIFont.regular(ofSize: 15)
        txt.layer.borderWidth = 1.5
        txt.layer.borderColor = Theme.Color.lightGray.cgColor
        txt.delegate = self
        return txt
    }()
    
    fileprivate let submitBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Submit", for: .normal)
        btn.setTitleColor(Theme.Color.white, for: .normal)
        btn.titleLabel?.font = UIFont.medium(ofSize: 16)
        btn.backgroundColor = Theme.Color.colorBlue
        return btn
    }()
    
    fileprivate let cancelBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Cancel", for: .normal)
        btn.setTitleColor(Theme.Color.darkGray, for: .normal)
        btn.titleLabel?.font = UIFont.medium(ofSize: 16)
        btn.backgroundColor = Theme.Color.lightGray
        return btn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = DashboardPresenter(controller: self)
        setupViews()
        setupConstarint()
    }
    override func viewWillAppear(_ animated: Bool) {
        getpersonalnoteAPICall()
    }
    
    internal func setupViews() {
        titlelbl.text = "Patient"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        view.addSubview(tableView)

        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        rowArray = [
            MenuData(title: "Symptoms & Documents", image: Images.ic_document),
            MenuData(title: "Personal Notes", image: Images.ic_menu),
            MenuData(title: "Examination Form", image: Images.ic_upload),
            MenuData(title: "Examination Report", image: Images.ic_upload),
            MenuData(title: "Prescription", image: Images.ic_upload)
            ]
        
    }
    
    fileprivate func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
        tableView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: view.frame.width, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)

    }
    func videoConsultValidation() {
        print(filteredPatientArray[0].appo_callstatus)
        print(videoConsultValidationNum.notStarted)
        self.activityIndicatorEnd()
        if  (filteredPatientArray[0].appo_callstatus) == String(videoConsultValidationNum.notStarted)  {
            let alert = UIAlertController(title: "Not Ready", message: "The patient is not ready for video consultation.", preferredStyle: UIAlertController.Style.alert)

                   // add the actions (buttons)
        
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
               
                alert.dismiss(animated: true, completion: nil)
               
                self.hideHud()
            }))
                   

                   // show the alert
                   self.present(alert, animated: true, completion: nil)
        } else {
            let vc = VideoConsultationVC()
            vc.delegate = self
                        navigationController?.pushViewController(vc, animated: true)
        }
       
    }

    
    @objc func requestData() {
//        if Reachability.isConnectedToNetwork(){
//            let deadline = DispatchTime.now() + .milliseconds(700)
//            DispatchQueue.main.asyncAfter(deadline: deadline) {
//                self.checkNetwork()
//                self.refresher.endRefreshing()
//            }
//        }else{
//            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
//        self.refresher.endRefreshing()
//        self.activityIndicatorEnd()
//        }
    }
    
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- APICall
extension PatientProfileVC{
    //MARK:- checkNetworkConnection
    fileprivate func checkNetworkConnection(){
        if Reachability.isConnectedToNetwork(){
            self.activityIndicatorBegin()
            
            let engDate : String = Utility.todayDate() //Date().todayDate(format: String.DATE_FORMATE)
            todayNepaliDate = getNepaliDate(engDate: engDate)
            getnepaliFirstDate = getNepaliFirstDateFromTodayDate(todayNepaliDate: todayNepaliDate)
            
            DispatchQueue.main.async {
                self.presenter.doctorStatusApiCall()
            }
            DispatchQueue.main.async {
                if users.islivemerodoctor == "Y" {
                    //self.presenter.telemedicinepatientlistAPICall(fromdate: self.getnepaliFirstDate, todate: self.todayNepaliDate)
                    self.presenter.telemedicinepatientlistAPICall(fromdate: self.todayNepaliDate, todate: self.todayNepaliDate)
                }else{
                    self.presenter.localPatientlistAPICall(fromdate: self.todayNepaliDate, todate: self.todayNepaliDate)
                }
            }
           
            DispatchQueue.main.async {
                self.presenter.getRevenueAPICall(fromdate: self.getnepaliFirstDate, todate: self.todayNepaliDate)
            }
            
            DispatchQueue.main.async {
                self.presenter.getSlotInfoAPICall()
            }
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    func getpersonalnoteAPICall(){
        let patientData = patientArray[0]
        
        ApiManager.sendRequest(toApi: Api.Endpoint.getpersonalnote(patientid: patientData.patientid, appid: patientData.appid, midasid: patientData.midasid)) { (status, data) in
            
            if data["type"] == "success"{
                if let responseData = data["response"].dictionary{
                    self.noteData = [NoteModel(opdid: responseData["opdid"]?.string ?? "", personalnote: responseData["personalnote"]?.string ?? "")]
                }
                
            }
        } onError: { (error) in
            
        }

    }
    
    func noteSubmitApiCall(){
        let noteDic = noteData[0]
        self.showHud(withTitle: "", and: "")
        ApiManager.sendRequest(toApi: Api.Endpoint.updatepersonalnote(opdid: noteDic.opdid, personalnote: self.noteTxt.text)) { (status, data) in
            self.hideHud()
            
            
            if data["type"] == "success"{
                self.toastMessage(message: data["message"].string, toastType: .success)
                self.getpersonalnoteAPICall()
                self.noteSuperView.removeFromSuperview()
            }
        } onError: { (error) in
            self.hideHud()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    @objc func outBtnPressed() {
        let alert = UIAlertController(title: "Out Patient", message: "Please confirm that the appointment is completed and you want to out the patient.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
           
            alert.dismiss(animated: true, completion: nil)
           
           
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
           
            alert.dismiss(animated: true) {
                self.checkNetworkConnections()
            }
           
            
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)
//
        
    }
    fileprivate func checkNetworkConnections(){
        if Reachability.isConnectedToNetwork(){
            self.activityIndicatorBegin()
            
           
            DispatchQueue.main.async {
                self.outPateintApi()
            }
           
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    //MARK:- OUT PATIENT API CALL
    func outPateintApi() {
        print(patientArray[0].patientid)
        ApiManager.sendRequest(toApi: Api.Endpoint.outPatientApi(appid: patientArray[0].appid, midasid: patientArray[0].patientid, status: String(5))) { (status, data) in
                   self.hideHud()
       
       print(status,data)
            self.activityIndicatorEnd()
            self.hideHud()
                   if data["type"] == "success"{
                       self.toastMessage(message: data["message"].string, toastType: .success)
                   
                    self.tableView.reloadData()
                   }
               } onError: { (error) in
                   self.hideHud()
                self.activityIndicatorEnd()
                   self.toastMessage(message: error.localizedDescription, toastType: .message)
               }
    }
}

// MARK: - UITableView Delegate
extension PatientProfileVC :UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFrom == "completed" {
            return 1 + rowArray.count
        }
        return 2 + rowArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*
         let cell = tableView.dequeueReusableCell(withIdentifier: "HealthPartnerInfoCell", for: indexPath) as! HealthPartnerInfoCell
         cell.selectionStyle = .none
         cell.orglbl.text = "ABC Poly clinic"
         cell.addresslbl.text = "Patan"
         cell.namelbl.text = "Manoj"
         cell.profileImg.image = Images.placeholder_clinic
         return cell
         */
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientInfoCell", for: indexPath) as! PatientInfoCell
            cell.selectionStyle = .none
            let data = patientArray[indexPath.row]
            cell.namelbl.text = data.patientname
            cell.genderlbl.text = "Gender : " + data.gender
            cell.agelbl.text = "Age : " + data.age
            
            cell.tokenlbl.text = "Token No. : " + data.appid
            cell.appDate.text = "Appointment Date : "
            cell.appDatelbl.text = data.appdatenep
            cell.consultationTime.text = "Consultation Time : "
            cell.consultationTimelbl.text = data.appo_apptime
            if data.gender == "male" || data.gender == "Male" || data.gender == "male  " || data.gender == "Male  "{
                let image = Images.ic_male.withRenderingMode(.alwaysTemplate)
                cell.profileImg.image = image
                cell.tintColor = Theme.Color.lightGray

            }else{
                let image = Images.ic_female.withRenderingMode(.alwaysTemplate)
                cell.profileImg.image = image
                cell.tintColor = Theme.Color.lightGray

            }
            

                
                
//            cell.profileImg.sd_setImage(with: URL(string: ""), placeholderImage: Images.single_user)
            return cell
        }else if indexPath.row == 6{
            if outPatientSuccess == true {
                return UITableViewCell()
            } else {
                
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientVideoCell", for: indexPath) as! PatientVideoCell
               
                if enableOutPaeitent == true {
                    cell.selectionStyle = .none
                    cell.outPatientBtn.isHidden = false
                    cell.outPatientBtn.addTarget(self, action: #selector(outBtnPressed), for: .touchUpInside)
                } else {
                    cell.selectionStyle = .none
                    cell.outPatientBtn.isHidden = true
                    cell.outPatientBtn.addTarget(self, action: #selector(outBtnPressed), for: .touchUpInside)
                }
//                } else {
//                    return UITableViewCell()
//                }
           

            return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientProfileCell", for: indexPath) as! PatientProfileCell
            cell.selectionStyle = .none
            cell.infolbl.text = rowArray[indexPath.row - 1].title
            cell.profileImg.image = Images.placeholder_clinic
            
//            cell.profileImg.sd_setImage(with: URL(string: rowArray[indexPath.row + 2].image), placeholderImage: Images.placeholder_clinic)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 180
        }else if indexPath.row == 6{
            return 80
        } else{
            return 60
        }
        
        /*
         else if indexPath.row == 1 {
             return 120
         }
         */
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 1 {
//            let vc = PartnerProfileVC()
//            navigationController?.pushViewController(vc, animated: true)
//        } else
        if indexPath.row == 1 {
            let vc = HealthPortfolio()
            vc.patientArray = self.patientArray
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 2 {
            setupNoteView()
        }else if indexPath.row == 3{
            //Examination Form
//            let vc = ExaminationForm()
//            navigationController?.pushViewController(vc, animated: true)
            let vc = UIStoryboard.homeStoryboard.instantiateExaminationFormVC()
            vc.patientArray = patientArray
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4{
            //Examination Report
            let vc = ExaminationReportVC()
            vc.patientArray = self.patientArray
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 5{
            // Prescription
            let vc = PrescriptionVC()
            vc.patientArray = self.patientArray
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 6{
            //Video
            checkNetworkConnection()
//            print(todayNepaliDate, todayNepaliDate)
//            self.presenter.telemedicinepatientlistAPICall(fromdate: self.todayNepaliDate, todate: self.todayNepaliDate)
//            let vc = VideoConsultationVC()
//            navigationController?.pushViewController(vc, animated: true)

            
            
        }
        
    }
}

extension PatientProfileVC {
    func setupNoteView(){
        view.addSubview(noteSuperView)

        noteSuperView.addSubview(noteView)
        noteView.addSubview(notelbl)
        noteView.addSubview(noteTxt)
        noteView.addSubview(submitBtn)
        noteView.addSubview(cancelBtn)
        
        noteSuperView.frame = CGRect(x: 0, y: 0, width: SCREEN.WIDTH, height: SCREEN.HEIGHT)
        
        noteView.frame = CGRect(x: 8, y: noteSuperView.frame.midY - 90, width: noteSuperView.frame.width - 16, height: 180)
        notelbl.frame = CGRect(x: 10, y: 10, width: noteView.frame.width - 20, height: 20)
        noteTxt.frame = CGRect(x: 8, y: notelbl.frame.origin.y + notelbl.frame.height + 5, width: noteView.frame.width - 16, height: 80)
        submitBtn.frame = CGRect(x: noteView.frame.width - 10 - 80, y: noteTxt.frame.origin.y + noteTxt.frame.height + 10, width: 80, height: 40)
        cancelBtn.frame = CGRect(x: submitBtn.frame.origin.x - 10 - 80, y: submitBtn.frame.origin.y, width: 80, height: 40)
        
        submitBtn.layer.cornerRadius = 20
        cancelBtn.layer.cornerRadius = 20

        submitBtn.addTarget(self, action: #selector(noteSubmitBtnPress), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(noteCancelBtnPress), for: .touchUpInside)
        
        noteSuperView.backgroundColor = Theme.Color.darkGray.withAlphaComponent(0.3)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onViewTap))
        noteSuperView.addGestureRecognizer(tapGesture)

        
        
        if noteData.count > 0{
            noteTxt.text = noteData[0].personalnote
        }
        
    }
    
    @objc func noteSubmitBtnPress(){
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            noteSubmitApiCall()
        }else{
            toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    @objc func noteCancelBtnPress(){
        noteSuperView.removeFromSuperview()
    }
    
    @objc func onViewTap() {
        self.view.endEditing(true)
    }
    
}

extension PatientProfileVC : UITextViewDelegate{
    func textViewShouldReturn(textView: UITextView!) -> Bool {
            self.view.endEditing(true)
            return true;
        }
}
extension PatientProfileVC: videoConsultationDelegate {
    func meetingEnded() {
        enableOutPaeitent = true
        tableView.reloadData()
    }
}
