//
//  PartnerProfileVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 02/04/2021.
//

import UIKit

class PartnerProfileVC: BaseViewController {

    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    //MARK: - UITableView
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.showsVerticalScrollIndicator = false
        tv.tableFooterView = UIView()
        tv.delegate = self
        tv.dataSource = self
        tv.bounces = true
        tv.separatorStyle = .none
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = Theme.Color.backgroundGray
        
        tv.register(PatientInfoCell.self, forCellReuseIdentifier: "PatientInfoCell")
        tv.register(HealthPartnerInfoCell.self, forCellReuseIdentifier: "HealthPartnerInfoCell")
        tv.register(PatientVideoCell.self, forCellReuseIdentifier: "PatientVideoCell")
        tv.register(PatientProfileCell.self, forCellReuseIdentifier: "PatientProfileCell")

        
        if #available(iOS 10.0, *) {
            tv.refreshControl = refresher
        }else{
            tv.addSubview(refresher)
        }
        
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupConstarint()
    }

    func setupViews() {
        titlelbl.text = "Partner Profile"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        view.addSubview(tableView)
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
        tableView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: view.frame.width, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)

    }

    @objc func requestData() {
//        if Reachability.isConnectedToNetwork(){
//            let deadline = DispatchTime.now() + .milliseconds(700)
//            DispatchQueue.main.asyncAfter(deadline: deadline) {
//                self.checkNetwork()
//                self.refresher.endRefreshing()
//            }
//        }else{
//            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
//        self.refresher.endRefreshing()
//        self.activityIndicatorEnd()
//        }
    }
    
    
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }

}

// MARK: - UITableView Delegate
extension PartnerProfileVC :UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HealthPartnerInfoCell", for: indexPath) as! HealthPartnerInfoCell
            cell.selectionStyle = .none
            cell.orglbl.text = "ABC Poly clinic"
            cell.addresslbl.text = "Patan"
            cell.namelbl.text = "Manoj"
            cell.profileImg.image = Images.placeholder_clinic
            return cell
//    }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//       if indexPath.row == 1 {
            return 120
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

