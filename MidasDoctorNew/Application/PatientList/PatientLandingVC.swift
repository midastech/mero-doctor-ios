//
//  PatientLandingVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 29/03/2021.
//

import UIKit
import YYCalendar

var fromDate = "", toDate = ""

class PatientLandingVC: BaseViewController, UITextFieldDelegate, CustomSegmentedControlDelegate {
    lazy var selectedSegment = 0
//    lazy var toDate = String()
    
    private var arrayViewController = [UIViewController]()
    
    fileprivate var isFilterOpen = false
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    let filterBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(Images.ic_filter, for: .normal)
        btn.addTarget(self, action: #selector(filterBtnPress), for: .touchUpInside)
        return btn
    }()

    fileprivate lazy var scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.contentMode = .scaleAspectFit
        sv.clipsToBounds = true
        sv.backgroundColor = Theme.Color.backgroundGray
        return sv
    }()
    
    fileprivate lazy var dateView: UIView = {
        let sv = UIView()
        sv.backgroundColor = Theme.Color.lightGray
        return sv
    }()

    
    fileprivate lazy var fromlbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "From"
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    fileprivate lazy var tolbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "To"
        lbl.textColor = Theme.Color.black
        lbl.font = UIFont.medium(ofSize: 15)
        return lbl
    }()
    
    
    fileprivate lazy var fromTxt = PrimaryInputField()
    fileprivate lazy var toTxt = PrimaryInputField()
    
    fileprivate let fromBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(Images.ic_calendar_empty, for: .normal)
        btn.addTarget(self, action:#selector(fromDateBtnPress), for: .touchUpInside)

        return btn
    }()
    
    fileprivate let toBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(Images.ic_calendar_empty, for: .normal)
        btn.addTarget(self, action:#selector(toDateBtnPress), for: .touchUpInside)

        return btn
    }()
    
    
    fileprivate lazy var submitBtn : UIButton = {
       let btn = PrimaryActionButton()
        btn.setTitle("Submit", for: .normal)
        btn.backgroundColor = Theme.Color.Dark_Red_Color
        btn.addTarget(self, action: #selector(submitBtnPress), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()
        
        self.addChildView(childViewController: arrayViewController[0])
        
        
    }
    
    func setupViews() {
        titlelbl.text = "Patient List"
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        navView.addSubview(filterBtn)

        view.addSubview(scrollView)
        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
//        arrayViewController = [PatientUpcomingVC(), PatientCompletedVC(), PatientCancelVC()]
        arrayViewController = [PatientUpcomingVC(), PatientCompletedVC()]

        fromDate = Utility.todayDate()
        toDate = Utility.todayDate()
    }
    
    func setupConstarint() {
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
        filterBtn.frame = CGRect(x: navView.frame.width - 45, y: 0, width: 45, height: 55)
        
        let segment = CustomSegmentedControl(frame: CGRect(x: SCREEN.WIDTH / 2 - 125, y: navView.frame.origin.y + navView.frame.height + 10, width: 250, height: 45), buttonTitle: ["Upcoming", "Completed"])
        segment.delegate = self
        segment.backgroundColor = Theme.Color.backgroundGray
        segment.layer.cornerRadius = 8
        view.addSubview(segment)
        
        
        scrollView.frame = CGRect(x: 0, y: segment.frame.origin.y + segment.frame.height + 1, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - segment.frame.origin.y - segment.frame.height - 1)
        dateView.frame = CGRect(x: 0, y: SCREEN.HEIGHT - 250, width: SCREEN.WIDTH, height: 250)
    }
    
    func changeToIndex(index: Int) {
        isFilterOpen = false
        dateView.isHidden = true
        
        fromDate = Utility.todayDate()
        toDate = Utility.todayDate()
        selectedSegment = index
        self.addChildView(childViewController: arrayViewController[index])

    }
    
    
    func setupDateView(){
//        view.bringSubviewToFront(dateView)
//        view.superview?.bringSubviewToFront(dateView)
        dateView.isHidden = true
        
        
        view.addSubview(dateView)
        
//        view.superview?.bringSubviewToFront(dateView)
        dateView.addSubview(fromlbl)
        dateView.addSubview(tolbl)
        
        dateView.addSubview(fromTxt)
        fromTxt.addSubview(fromBtn)

        dateView.addSubview(toTxt)
        toTxt.addSubview(toBtn)

        dateView.addSubview(submitBtn)
        
        fromTxt.isUserInteractionEnabled = true
        toTxt.isUserInteractionEnabled = true
        
        dateView.applyGradient(colors: [Theme.Color.backgroundGray, Theme.Color.colorLightBlue], gradient: .vertical)

    }
    
    func setupDateViewConstraint(){
        fromlbl.frame = CGRect(x: 25, y: 50, width: SCREEN.WIDTH / 2 - 30, height: 20)
        tolbl.frame = CGRect(x: fromlbl.frame.origin.x + fromlbl.frame.width + 10, y: 50, width: SCREEN.WIDTH / 2 - 30, height: 20)
        
        fromTxt.frame = CGRect(x: 20, y: fromlbl.frame.origin.y + fromlbl.frame.height, width: fromlbl.frame.width, height: 35)
        toTxt.frame = CGRect(x: tolbl.frame.origin.x, y: fromlbl.frame.origin.y + fromlbl.frame.height, width: fromlbl.frame.width, height: 35)
        
        fromBtn.frame = CGRect(x: fromTxt.frame.width - 25, y: 7, width: 20, height: 20)
        toBtn.frame = CGRect(x: toTxt.frame.width - 25, y: 7, width: 20, height: 20)
        submitBtn.frame = CGRect(x: 20, y: fromTxt.frame.origin.y + fromTxt.frame.height + 20, width: SCREEN.WIDTH - 40, height: 40)
    }
    
    
    func addChildView(childViewController: UIViewController){
        addChild(childViewController)
        scrollView.addSubview(childViewController.view)
        view.addSubview(scrollView)
        
        childViewController.view.frame = scrollView.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childViewController.didMove(toParent: self)
        
//        childViewController.
        
        
        
    }
    
    @objc func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func fromDateBtnPress(){
        let weekArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        
        let calendar = YYCalendar(limitedCalendarLangType: .custom(weekArray), date: Utility.todayDate(), minDate: "", maxDate: Utility.todayDate(), format: String.DATE_FORMATE) { (date) in
            self.fromTxt.text = date
        }
        
        calendar.dayButtonStyle = DayButtonStyle.roundishSquare// DayButtonStyle (.roundishSquare, .square, .circle)
        calendar.sundayColor = .black
        calendar.headerLabelFont = UIFont.medium(ofSize: 17)
        calendar.weekLabelFont = UIFont.medium(ofSize: 14)
        calendar.dayLabelFont = UIFont.medium(ofSize: 14)
        calendar.componentType = .limited
        calendar.show()
    }
    
    @objc fileprivate func toDateBtnPress(){
        let weekArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        
        let calendar = YYCalendar(limitedCalendarLangType: .custom(weekArray), date: Utility.todayDate(), minDate: fromTxt.text, maxDate: Utility.todayDate(), format: String.DATE_FORMATE) { (date) in
            self.toTxt.text = date
        }
        calendar.dayButtonStyle = DayButtonStyle.roundishSquare// DayButtonStyle (.roundishSquare, .square, .circle)
        calendar.sundayColor = .black
        calendar.headerLabelFont = UIFont.medium(ofSize: 17)
        calendar.weekLabelFont = UIFont.medium(ofSize: 14)
        calendar.dayLabelFont = UIFont.medium(ofSize: 14)
        calendar.componentType = .limited
        calendar.show()
    }
    

    @objc func submitBtnPress(){
        isFilterOpen = false
        dateView.isHidden = true
        dateView.removeFromSuperview()
        
        fromDate = fromTxt.text ?? Utility.todayDate()
        toDate = toTxt.text ?? Utility.todayDate()
        
        arrayViewController = [PatientUpcomingVC(), PatientCompletedVC()]

        if selectedSegment == 0 {
            self.addChildView(childViewController: arrayViewController[0])

//            PatientUpcomingVC()setupViews()
//            PatientUpcomingVC()setupConstarint()
//            print("from \(fromTxt.text)")
//            print("toTxt \(toTxt.text)")
//            PatientUpcomingVC().checkNetwork(fromDate: fromTxt.text ?? Utility.todayDate(), toDate: toTxt.text ?? Utility.todayDate())
        }else if selectedSegment == 1{
            self.addChildView(childViewController: arrayViewController[1])

//            PatientCompletedVC().checkNetwork(fromDate: fromTxt.text ?? Utility.todayDate(), toDate: toTxt.text ?? Utility.todayDate())
        }else{
            
        }
 
    }
}

extension PatientLandingVC {
    @objc func filterBtnPress(){
        print("filterBtnPress")
        
        if isFilterOpen == true {
            isFilterOpen = false
            dateView.isHidden = true
            dateView.removeFromSuperview()
        }else{
            setupDateView()
            setupDateViewConstraint()
            
            fromTxt.text = toDate
            toTxt.text = toDate
            fromTxt.delegate = self
            toTxt.delegate = self
            
            isFilterOpen = true
            dateView.isHidden = false
        }
        
    }
}

extension PatientLandingVC{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
        
        if textField == fromTxt {
            //            showYearTypeAlert()
            fromDateBtnPress()
            
            
            return false
        }else if textField == toTxt {
            //            showYearTypeAlert()
            
            toDateBtnPress()
            
            return false
        }
        
        self.view.endEditing(false)
        return true
    }
}
