//
//  PrescriptionVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 15/05/2021.
//

import UIKit
import Alamofire
import SwiftyJSON
import CropViewController

class PrescriptionVC: BaseViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    lazy var patientArray: [PatientListModel] = []
    
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    fileprivate let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(GalleryCell.self, forCellWithReuseIdentifier: "GalleryCell")
        cv.backgroundColor = Theme.Color.backgroundGray
        
        return cv
    }()

    fileprivate lazy var addBtn: UIButton = {
        let btn = UIButton()
        let image = Images.Add_Image.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = Theme.Color.white
        btn.addTarget(self, action:#selector(addBtnPress), for: .touchUpInside)
        btn.backgroundColor = Theme.Color.dark_Green_Color
        return btn
    }()
    
    
    fileprivate lazy var uploadBtn: UIButton = {
        let btn = PrimaryActionGreenButton()
        btn.addTarget(self, action:#selector(uploadBtnPress), for: .touchUpInside)
        btn.backgroundColor = Theme.Color.dark_Green_Color
        btn.setTitle("UPLOAD", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    lazy var imageArray = [PrescriptionModel]()
    lazy var myArray = [UIImage]()
    lazy var newMyArray = [UIImage]()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstarint()

        collectionView.delegate = self
        collectionView.dataSource = self
        checkNetwork()
    }
    func setupViews(){
        titlelbl.text = "Prescription"
        backBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        view.addSubview(collectionView)
        view.addSubview(addBtn)
        view.addSubview(uploadBtn)
    }
    @objc func buttonAction(sender: UIButton!) {
        navigationController?.popViewController(animated: true)
       }
    func setupConstarint(){
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 5 - 45 - 5, height: 55)
        collectionView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)
        addBtn.frame = CGRect(x: SCREEN.WIDTH - 70, y: SCREEN.HEIGHT - 70, width: 50, height: 50)
        uploadBtn.frame = CGRect(x: 15, y: addBtn.frame.origin.y, width: SCREEN.WIDTH - 15 - 70 - 15, height: 40)
        
        addBtn.layer.cornerRadius = 25

    }
    
    func checkNetwork(){
        if Reachability.isConnectedToNetwork() {
            prescriptionApiCall()
        }else{
            toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    @objc func addBtnPress() {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Take a new Photo", style: .default, handler: { (handler) in
            
            self.openCamera()
        }))
        
        controller.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: { (handler) in
            self.openGallery()
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        present(controller, animated: true, completion: nil)
    }
    
    @objc func uploadBtnPress() {
      
        if Reachability.isConnectedToNetwork() {
            prescriptionUploadApiCall()
        }else{
            toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    private func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
          //  imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
           // imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as! UIImage
//        myArray.append(image)
//        newMyArray.append(image)
      //  collectionView.reloadData()
        dismiss(animated:true, completion: nil)
        presentCropViewController(image: image)
    }
    func presentCropViewController(image: UIImage) {
           let image: UIImage = image

           let cropViewController = CropViewController(image: image)
           cropViewController.delegate = self
           present(cropViewController, animated: true, completion: nil)
       }

}


extension PrescriptionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell

//        cell.backgroundColor = .lightGray
//        cell.data = data[indexPath.row]
        
        cell.profileImage.image = myArray[indexPath.row]
        
//        sd_setImage(with: URL(string: imageArray[indexPath.row].tmat_file), placeholderImage: Images.ic_picture)
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnPress(sender:)), for: .touchUpInside)
            
        return cell
    }
    
    @objc func deleteBtnPress(sender: UIButton){
        let controller = UIAlertController.init(title: "Confirmation", message: "Are you sure, you want to delete this report?", preferredStyle: .alert)
        controller.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (alert) in
            let indexPath = Utility.getCollectionViewCellIndexForInfobtnFromSubview(view: sender, inCollection: self.collectionView)
            print("section \(String(describing: indexPath?.section)) \nrow \(String(describing: indexPath?.row))")

            print("imageArray.count \(self.imageArray.count)")
            print("index Path \(String(describing: indexPath!.row + 1))")
            
            
            
            
            if Int(indexPath!.row + 1) > self.imageArray.count{
                
                print("newMyArray.count \(self.newMyArray.count)")
                print("index Path \(String(describing: indexPath!.row))")
                print("imageArray.count \(self.imageArray.count - 1)")
                
                self.newMyArray.remove(at: indexPath!.row - self.imageArray.count)
                self.myArray.remove(at: indexPath!.row)
                self.collectionView.reloadData()
            }else{
                self.deleteAPICall(fieldID: self.imageArray[indexPath!.row].tmat_id, indexPath: indexPath!)
            }
            
        }))
        controller.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var Width: CGFloat = collectionView.frame.width/4 - 1
        if UIDevice.current.orientation.isLandscape {
            Width = collectionView.frame.width/6 - 1
        }
        return CGSize(width: Width, height: Width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let vc = ImageVC()
//        vc.selectedIndex = indexPath.row
//        vc.imageArr = data
//        pushView(viewController: vc)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API CALL
extension PrescriptionVC{
    fileprivate func prescriptionApiCall(){
        activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.getPrescriptionData(userid: patientArray[0].midasid, appid: patientArray[0].appid, orgid: users.orgid ?? "", category: "doc_pres")) { status, data in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                if let responseData = data["response"].array{
                    
                    self.imageArray.removeAll()
                    for dic in responseData {
                        
                        let url = URL(string:dic["tmat_file"].string!)
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.myArray.append(image)
                        }
                        
                        
//                        let img = UIImage(named: URL(string: dic["tmat_file"].string!))
                        
                        
                        let item = PrescriptionModel(json: dic)
                        self.imageArray.append(item)
                    }
                    self.collectionView.reloadData()
                }
            }
        } onError: { error in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    
    fileprivate func prescriptionUploadApiCall(){
        self.showHud(withTitle: "", and: "")
        
        var attachedDetail = [String]()
        
        var ImagesData = [Data]()
        
        for img in newMyArray {
            let data  = img.jpegData(compressionQuality: 0.7)!
            ImagesData.append(data)
            attachedDetail.append("doc_pres")
        }
        print("ImagesData \(ImagesData)")
        
        let headers: HTTPHeaders = [
            "apikey": "0ee4198537b966818a4fbc1e81d7494d",
            "gcm": "",
            "macid": Utility.iosVersion(),
            "ipaddress": Utility.getIPAddress() ?? "",
            "device": Utility.deviceModelName(),
            "imei": "",
            "androidid": Utility.getDeviceUniqueIdentifier(),
            "apiversion": "v1",
            "mobileno": users.mobile_number ?? "",
            "userid": users.gdocid ?? "",
            "midasid": users.gdocid ?? "",
            "orgid": "0",
            "Machinetype": "Android",
            
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        let parameters: [String: Any] = [
            "appid"       :  patientArray[0].appid,
            "userid"            : patientArray[0].midasid,
            "hosid"             :  users.orgid ?? "-1",
            "attached_detail": attachedDetail
        ] //Optional for extra parameter
//        patientArray[0].midasid
        AF.upload(
            multipartFormData: { multipartFormData in
                for imageData in ImagesData {
                    multipartFormData.append(imageData, withName: "ATTFILE[]", fileName: "photos123.jpeg", mimeType: "image/jpeg")
                }
                
                print("param \(parameters)")
                
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            },
            to: "https://api.mero.doctor/api/v1/appointments/saveAttachment", method: .post , headers: headers)
            .response { response in
                
                self.hideHud()
                self.newMyArray.removeAll()
                print("response \(response)")
                
                print(response)
                
                if let err = response.error{
                    print("error : \(err)")
                    return
                }
                print("Succesfully uploaded")
                self.toastMessage(message: "Succesfully uploaded", toastType: .success)
                let json = response.data
                if (json != nil)
                {
                    let jsonObject = JSON(json!)
                    print("jsonObject  \(jsonObject)")
                }
                
            }
    }
    
    func deleteAPICall(fieldID: String, indexPath: IndexPath){
        ApiManager.sendRequest(toApi: Api.Endpoint.delattachment(id: fieldID, hosid: users.orgid ?? "-1")) { status, data in
            
            self.myArray.remove(at: indexPath.row)
            
            self.collectionView.reloadData()
            
            
        } onError: { error in
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }

    }
}
extension PrescriptionVC: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        myArray.append(image)
        newMyArray.append(image)
        collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
}

