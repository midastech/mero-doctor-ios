//
//  ImageVC.swift
//  Gallery
//
//  Created by ramesh prajapati on 7/6/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import UIKit
class ImageVC: BaseViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {

    lazy var selectedImageView = UIImage()
    lazy var selectedImageString = String()

    fileprivate let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.contentMode = .scaleAspectFit
        sv.showsVerticalScrollIndicator = false
        sv.showsHorizontalScrollIndicator = false
        sv.backgroundColor = Theme.Color.lightGray
        sv.minimumZoomScale = 1
        sv.maximumZoomScale = 6
        return sv
    }()
    
    fileprivate let img: UIImageView = {
       let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    
    fileprivate let closeBtn: UIButton = {
        let button = UIButton(type: .custom)
        let image = Images.ic_close.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = Theme.Color.white
//        button.backgroundColor = .red
        button.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        return button
    }()
    
    @objc func closeBtnTapped(){
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        setupGesture()
        setupView()
        setupconstraint()
        loadImage()
    }
    
    func setupGesture(){
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSingleTapOnScrollView(recognizer:)))
        singleTapGesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(singleTapGesture)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapOnScrollView(recognizer:)))
        doubleTapGesture.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTapGesture)
        
        singleTapGesture.require(toFail: doubleTapGesture)
      
    }

    
    
    
    func setupView(){
        view.addSubview(scrollView)
        scrollView.addSubview(img)
        view.addSubview(closeBtn)
    }
    
    func setupconstraint(){
        
//        scrollView.frame = view.bounds
//        img.frame = scrollView.bounds
        
        scrollView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - SCREEN.statusBarHeight)
        img.frame = scrollView.bounds

        closeBtn.frame = CGRect(x: 20, y: SCREEN.statusBarHeight + 10, width: 25, height: 25)
    }
    
    func loadImage(){
        img.sd_setImage(with: URL(string: selectedImageString), placeholderImage: selectedImageView)
    }
    
    @objc func handleSingleTapOnScrollView(recognizer: UITapGestureRecognizer){
        if closeBtn.isHidden {
            closeBtn.isHidden = false
        }else{
            closeBtn.isHidden = true
        }
    }
    
    @objc func handleDoubleTapOnScrollView(recognizer: UITapGestureRecognizer){
        if scrollView.zoomScale == 1 {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
            closeBtn.isHidden = true
        } else {
            scrollView.setZoomScale(1, animated: true)
            closeBtn.isHidden = false
        }
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = img.frame.size.height / scale
        zoomRect.size.width  = img.frame.size.width  / scale
        let newCenter = img.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return img
    }
 
    @objc func handlePinch(recognizer: UIPinchGestureRecognizer){
        print(recognizer)
        recognizer.view?.transform = (recognizer.view?.transform.scaledBy(x: recognizer.scale, y: recognizer.scale))!
        recognizer.scale = 1
        img.contentMode = .scaleAspectFit
    }
    
   
}
