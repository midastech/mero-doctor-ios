//
//  HealthPortfolioUploadVC.swift
//  MidasDoctorNew
//
//  Created by Ramesh Prajapati on 05/04/2021.
//

import UIKit
import Photos
import AVFoundation
import Alamofire
import SwiftyJSON

class HealthPortfolioUploadVC: BaseViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate, ELCImagePickerControllerDelegate {
   
    
    lazy var patientArray: [PatientListModel] = []
    
    lazy var SelectedAsstes = [PHAsset]()
    
    lazy var selectedImages = [UIImage]()
    lazy var pathologyImages = [UIImage]()
    lazy var xRayImages = [UIImage]()
    lazy var prescriptionImages = [UIImage]()

    lazy var attachedDetail = [String]()
    lazy var pathologyStringArr = [String]()
    lazy var xRayStringArr = [String]()
    lazy var prescriptionStringArr = [String]()

    fileprivate lazy var chooseBtnTag = Int()
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    
    //MARK: - Navigation Bar
    let noteView: UIView = {
        let v = BackViewProperties()
        v.backgroundColor = Theme.Color.colorLightRed
        return v
    }()
    
    let notelbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.medium(ofSize: 17)
        lbl.numberOfLines = 2
        let notetxt = NSMutableAttributedString(string:"Note: ", attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 15)])
        notetxt.append(NSAttributedString(string: "Allowed file type: png, jpg, jpeg, pdf | Max. file size: 2MB",
                                      attributes: [.foregroundColor: Theme.Color.white, .font:UIFont.medium(ofSize: 14)]))
        lbl.attributedText = notetxt
        return lbl
    }()
    
    fileprivate let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(GalleryCell.self, forCellWithReuseIdentifier: "GalleryCell")
        cv.backgroundColor = Theme.Color.backgroundGray
        return cv
    }()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupConstraint()
        
        checkNetwork()
//        headerTitles = ["Pathology Report (Photo)","X-ray, ECG, Echo, MRI, CT Reports (Photo:)","Prescription / Medication (Photo)"]
        
    }
    

    private func setupView(){
        view.backgroundColor = .white
        titlelbl.text = String(format: "%@", "Health Portfolio")
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
        
        view.addSubview(noteView)
        noteView.addSubview(notelbl)
        
        view.addSubview(collectionView)
        
        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(HistoryCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HistoryCell")
        collectionView.register(ChiefComplaintsCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ChiefComplaintsCell")
        collectionView.register(HealthPortfolioUploadCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HealthPortfolioUploadCell")

//        collectionView.reloadData()

    }
    
    private func setupConstraint(){
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
        
        noteView.frame = CGRect(x: 10, y: navView.frame.origin.y + navView.frame.height + 10, width: SCREEN.WIDTH - 20, height: 50)
        notelbl.frame = CGRect(x: 10, y: 5, width: noteView.frame.width - 20, height: 40)
        
        collectionView.frame = CGRect(x: 0, y: noteView.frame.origin.y + noteView.frame.height + 10, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - noteView.frame.origin.y - noteView.frame.height - 10)
    }

    fileprivate func checkNetwork(){
        if Reachability.isConnectedToNetwork() {
            getHistoryApiCall()
            getChiefComplaintsApiCall()
        }else{
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    
        
    @objc private func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:- UICollectionViewDelegate
extension HealthPortfolioUploadVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 || section == 1{
            return CGSize(width: SCREEN.WIDTH, height: 195)
        } else if section == 2{
            return  CGSize(width: SCREEN.WIDTH, height: 315)
        }else{
            return CGSize(width: SCREEN.WIDTH, height: 0)
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 || section == 1 || section == 2{
            return 0
        } else if section == 3{
            return pathologyImages.count
        }else if section == 4{
            return xRayImages.count
        }else{
            return prescriptionImages.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var reusableView : UICollectionReusableView? = nil
        if kind == UICollectionView.elementKindSectionHeader {
            
            if indexPath.section == 0 {
                let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ChiefComplaintsCell", for: indexPath) as! ChiefComplaintsCell
                
                reusableView = cell
                cell.saveBtn.tag = indexPath.section
                cell.editBtn.tag = indexPath.section
                cell.saveBtn.addTarget(self, action: #selector(saveBtnTap(sender:)), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(editBtnTap(sender:)), for: .touchUpInside)
                
            }else if indexPath.section == 1{
                let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
                
                reusableView = cell
                cell.saveBtn.tag = indexPath.section
                cell.editBtn.tag = indexPath.row
                cell.saveBtn.addTarget(self, action: #selector(saveBtnTap(sender:)), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(editBtnTap(sender:)), for: .touchUpInside)
                
            }else if indexPath.section == 2{
                let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HealthPortfolioUploadCell", for: indexPath) as! HealthPortfolioUploadCell
                
                reusableView = cell

                cell.saveBtn.tag = indexPath.section
                cell.editBtn.tag = indexPath.section
                cell.saveBtn.addTarget(self, action: #selector(saveBtnTap(sender:)), for: .touchUpInside)
                cell.editBtn.addTarget(self, action: #selector(editBtnTap(sender:)), for: .touchUpInside)
                
                cell.chooseBtn1.tag = 1
                cell.chooseBtn2.tag = 2
                cell.chooseBtn3.tag = 3
                
                cell.chooseBtn1.addTarget(self, action: #selector(chooseBtnTap(sender:)), for: .touchUpInside)
                cell.chooseBtn2.addTarget(self, action: #selector(chooseBtnTap(sender:)), for: .touchUpInside)
                cell.chooseBtn3.addTarget(self, action: #selector(chooseBtnTap(sender:)), for: .touchUpInside)
            }
        }
        return reusableView!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        //        cell.profileImage.image = myArray[indexPath.row]
        cell.deleteBtn.tag = indexPath.section
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnPress(sender:)), for: .touchUpInside)
        if indexPath.section == 3 {
            cell.profileImage.image = pathologyImages[indexPath.row]
        }else if indexPath.section == 4 {
            cell.profileImage.image = xRayImages[indexPath.row]
        }else{
            cell.profileImage.image = prescriptionImages[indexPath.row]
        }
        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Width: CGFloat = collectionView.frame.width/4 - 1
        return CGSize(width: Width, height: Width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
}


//MARK:- Button Action
extension HealthPortfolioUploadVC{
    @objc private func editBtnTap(sender: UIButton){
        print("edit index path \(sender.tag)");
    }
    
    @objc private func saveBtnTap(sender: UIButton){
        let tag = sender.tag
        
        print("save index path \(sender.tag)");
        
        let indexPath = Utility.getCollectionViewCellIndexForInfobtnFromSubview(view: sender, inCollection: collectionView)

        
        if tag == 0{
            //MARK:- Chief Complain
        }else if tag == 1 {
            //MARK:- Save History API Call
            let cell = collectionView.cellForItem(at: indexPath!) as! HistoryCell
            ApiManager.sendRequest(toApi: Api.Endpoint.savehistory(appid: patientArray[0].appid, hede_history: cell.complaintTextField.text ?? "", hede_detailid: "")) { (status, data) in
                
            } onError: { (error) in
                
            }
        }else{
            selectedImages.removeAll()
            attachedDetail.removeAll()
            
            if pathologyImages.count > 0 {
                selectedImages.append(contentsOf: pathologyImages)
                attachedDetail.append(contentsOf: pathologyStringArr)
            }
            if xRayImages.count > 0 {
                selectedImages.append(contentsOf: xRayImages)
                attachedDetail.append(contentsOf: xRayStringArr)
            }
            if prescriptionImages.count > 0 {
                selectedImages.append(contentsOf: prescriptionImages)
                attachedDetail.append(contentsOf: prescriptionStringArr)
            }
            
            UploadApiCall(imageOrVideo: self.selectedImages)
        }
    }
    
    @objc private func chooseBtnTap(sender: UIButton){
        print("index path \(sender.tag)");
        chooseBtnTag = sender.tag
        
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        controller.addAction(UIAlertAction(title: "Take a new Photo", style: .default, handler: { (handler) in
            
            self.openCamera()
        }))
        
        controller.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: { (handler) in
            self.openGallery()
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        present(controller, animated: true, completion: nil)
    }
    
    private func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func openGallery(){
//        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .photoLibrary;
//            imagePicker.allowsEditing = true
//            self.present(imagePicker, animated: true, completion: nil)
//        }
                
//        let elcPicker = ELCImagePickerController(imagePicker: ())
        let elcPicker = ELCImagePickerController(imagePicker: ())
        
        elcPicker?.maximumImagesCount = 10 //Set the maximum number of images to select, defaults to 4
        elcPicker?.returnsOriginalImage = false //Only return the fullScreenImage, not the fullResolutionImage
        elcPicker?.returnsImage = true //Return UIimage if YES. If NO, only return asset location information
        elcPicker?.onOrder = true //For multiple image selection, display and return selected order of images
        elcPicker?.imagePickerDelegate = self
        
//        elcPicker?.delegate = self
                self.present(elcPicker!, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        guard let image = info[.editedImage] as? UIImage else {
            return
        }
        if chooseBtnTag == 1 {
            pathologyImages.append(image)
            pathologyStringArr.append(String.PATHOLOGY)
        }else if chooseBtnTag == 2 {
            xRayImages.append(image)
            xRayStringArr.append(String.ECG)
        }else {
            prescriptionImages.append(image)
            prescriptionStringArr.append(String.PRESCRIPTION)
        }
        print("image : \(image)")

        self.collectionView.reloadData()
//        imgView.image = image
        dismiss(animated:true, completion: nil)
        
    }
        
    
    
    func elcImagePickerController(_ picker: ELCImagePickerController!, didFinishPickingMediaWithInfo info: [Any]!) {
        
        print("info \(String(describing: info))")
        if (info.count == 0) {
            return
        }
        self.dismiss(animated: true, completion: nil)
        if (info.count == 0) {
            return
        }
        if chooseBtnTag == 1 && pathologyImages.count > 0 {
            pathologyImages.removeAll()
            pathologyStringArr.removeAll()
        } else if chooseBtnTag == 2 && xRayImages.count > 0 {
            xRayImages.removeAll()
            xRayStringArr.removeAll()
        } else if chooseBtnTag == 3 && prescriptionImages.count > 0 {
            prescriptionImages.removeAll()
            prescriptionStringArr.removeAll()
        }
        
        var selectAssets = [PHAsset]()
        for infoDic in info {
            selectAssets.append(infoDic as! PHAsset)
        }
        print("selectAssets \(selectAssets)")
        
        getAssetThumbnail(assets: selectAssets)
        self.dismiss(animated: true, completion: nil)
    }
    
    func elcImagePickerControllerDidCancel(_ picker: ELCImagePickerController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAssetThumbnail(assets: [PHAsset]) -> Void {
        print("assets count \(assets.count)")
        if assets.count != 0 {
            for i in 0..<assets.count {
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                var thumbnail = UIImage()
                option.isSynchronous = true
                manager.requestImage(for: assets[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                    thumbnail = result!
                })
                let data = thumbnail.jpegData(compressionQuality: 0.7)
                let newImage = UIImage(data: data!)
                
                if chooseBtnTag == 1{
                    pathologyImages.append(newImage! as UIImage)
                    pathologyStringArr.append(String.PATHOLOGY)
                }else if chooseBtnTag == 2{
                    xRayImages.append(newImage! as UIImage)
                    xRayStringArr.append(String.ECG)
                }else{
                    prescriptionImages.append(newImage! as UIImage)
                    prescriptionStringArr.append(String.PRESCRIPTION)
                }
                
            }
            self.collectionView.reloadData()
            var lastSection = Int()

            if chooseBtnTag == 1{
                lastSection = collectionView.numberOfSections - 3
            }else if chooseBtnTag == 2{
                lastSection = collectionView.numberOfSections - 2
            }else{
                lastSection = collectionView.numberOfSections - 1
            }

            let item = self.collectionView(self.collectionView, numberOfItemsInSection: lastSection)
            let lastItemIndex = IndexPath(item: item - 1, section: lastSection)
            self.collectionView.scrollToItem(at: lastItemIndex, at: .top, animated: true)
            

            
            
//            self.collectionView.indexPathForItem(at: <#T##CGPoint#>)
        }
        
        
//        print("pathologyImages \(pathologyImages)")
    }
    
    
   
    //MARK:- Delete Btn Press
    @objc func deleteBtnPress(sender: UIButton){
        let indexPath = Utility.getCollectionViewCellIndexForInfobtnFromSubview(view: sender, inCollection: collectionView)
        print("section \(String(describing: indexPath?.section)) \nrow \(String(describing: indexPath?.row))")
//        let cell = collectionView.cellForItem(at: indexPath!) as! GalleryCell
//        dataArray[indexPath!.section][indexPath!.row] = footerCell.infoTxtField.text ?? ""

       
        let section = indexPath?.section
        if  section == 3{
            pathologyImages.remove(at: indexPath!.row)
            pathologyStringArr.remove(at: indexPath!.row)
        }else if  section == 4{
            xRayImages.remove(at: indexPath!.row)
            xRayStringArr.remove(at: indexPath!.row)
        }else{
            prescriptionImages.remove(at: indexPath!.row)
            prescriptionStringArr.remove(at: indexPath!.row)
        }
        collectionView.reloadData()
    }

}
//MARK:- ApiCall
extension HealthPortfolioUploadVC{
    //MARK:- getHistoryApiCall

    func getHistoryApiCall(){
        self.activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.gethistory(appid: patientArray[0].appid)) { (status, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                
            }else{
//                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
//                self.toastMessage(message: message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    
    func getChiefComplaintsApiCall(){
        ApiManager.sendRequest(toApi: Api.Endpoint.getchiefcomplaints(appid: patientArray[0].appid)) { (status, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                
            }else{
//                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
//                self.toastMessage(message: message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    
    //MARK:- UploadApiCall

    func UploadApiCall(imageOrVideo : [UIImage]){
        print("attachedDetail \(attachedDetail)")
        print("imageOrVideo \(imageOrVideo)")

        var ImagesData = [Data]()
        
        for img in imageOrVideo {
            let data  = img.jpegData(compressionQuality: 0.7)!
            ImagesData.append(data)
        }
        
        let headers: HTTPHeaders = [
            "apikey": "0ee4198537b966818a4fbc1e81d7494d",
            "gcm": "",
            "macid": Utility.iosVersion(),
            "ipaddress": Utility.getIPAddress() ?? "",
            "device": Utility.deviceModelName(),
            "imei": "",
            "androidid": Utility.getDeviceUniqueIdentifier(),
            "apiversion": "v1",
            "mobileno": users.mobile_number ?? "",
            "userid": users.gdocid ?? "",
            "midasid": users.gdocid ?? "",
            "orgid": users.orgid ?? "0",
            "Machinetype": "Android",
            
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        let parameters: [String: Any] = [
            "queueno"       : patientArray[0].queueno,
            "userid"            : patientArray[0].patientid,
            "hosid"             : users.orgid ?? "",
            "appdatead"     : patientArray[0].appdateng,
            "attached_detail": attachedDetail
        ] //Optional for extra parameter
        
        AF.upload(
            multipartFormData: { multipartFormData in
                for imageData in ImagesData {
                    multipartFormData.append(imageData, withName: "ATTFILE[]", fileName: "photos123.jpeg", mimeType: "image/jpeg")
                }
                
                print("param \(parameters)")
                
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            },
            to: "https://api.mero.doctor/api/v1/appointments/saveAttachment", method: .post , headers: headers)
            .response { response in
//                print("response \(response)")
                
                print(response)
                
                if let err = response.error{
                    print("error : \(err)")
                    //                                onError?(err)
                    return
                }
//                print("Succesfully uploaded")
                self.toastMessage(message: "Succesfully uploaded", toastType: .success)
                
               
                
                self.selectedImages.removeAll()
                self.pathologyImages.removeAll()
                self.xRayImages.removeAll()
                self.prescriptionImages.removeAll()
                self.attachedDetail.removeAll()
                self.pathologyStringArr.removeAll()
                self.xRayStringArr.removeAll()
                self.prescriptionStringArr.removeAll()
                
                
                let json = response.data
                
                if (json != nil)
                {
                    let jsonObject = JSON(json!)
                    print("jsonObject  \(jsonObject)")
                }
            }
    }
}
