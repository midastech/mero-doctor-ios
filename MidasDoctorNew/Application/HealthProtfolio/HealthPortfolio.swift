//
//  HealthPortfolioVC.swift
//  HealthPartner
//
//  Created by ramesh prajapati on 1/20/21.
//

import UIKit

class HealthPortfolio: BaseViewController, UITextFieldDelegate {

    lazy var patientArray: [PatientListModel] = []

    
    fileprivate lazy var nameArray = [[String]]()
    fileprivate lazy var dataArray = [[String]]()
    
    //MARK: - Navigation Bar
    let navView = NavigationView()
    let backBtn = BackButtonProperties()
    let titlelbl = NavigationTitleProperties()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewLeftAlignedLayout()
        layout.itemSize = UICollectionViewFlowLayout.automaticSize //UICollectionViewFlowLayout.automaticSize;
        layout.estimatedItemSize = CGSize(width: 140, height: 40);
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = Theme.Color.backgroundGray
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
//        cv.bounces = true
        cv.delegate = self
        cv.dataSource = self
        cv.collectionViewLayout = layout
        cv.keyboardDismissMode = .interactive
        return cv
    }()
    
    /*
    let nextBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Next", for: .normal)
        btn.backgroundColor = Theme.Color.colorBlue
        btn.titleLabel?.textColor = .white
        btn.titleLabel?.font = UIFont.regular(ofSize: 17)
        btn.layer.cornerRadius = 4
        btn.addTarget(self, action:#selector(nextBtnPress), for: .touchUpInside)
        return btn
    }()
    */
//    fileprivate var headerTitles = [String]()
    fileprivate var titles = [[String]]()

    var healthPortfolioCell = HealthPortfolioCell()
    var footerCell = HealthPortfolioFooterCell()
    var historyCell = HealthPortfolioHistoryCell()
    var galleryCell = GalleryCell()
    
   let headerTitles = ["Common Symptoms (सामान्य लक्षणहरू)","Medical Issues (मेडिकल मुद्दाहरु)", "Chief Complaints", "History" ,"Allergies (एलर्जी)", "Medical Records"]

    let headerTitleArray = ["Pathology Report (Photo)", "X-ray, ECG, Echo, MRI, CT Reports (Photo)", "Prescription / Medication (Photo)"]

    lazy var pathologyImages = [String]()
    lazy var ecgImages = [String]()
    lazy var prescriptionImages = [String]()

    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupConstraint()
        loadSymp()
        
        checkNetwork()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: Selector(("keyboardWillShow:")), name:UIResponder.keyboardWillShowNotification, object: nil);
//        NotificationCenter.default.addObserver(self, selector: Selector(("keyboardWillHide:")), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
    
//    @objc func keyboardWillShow(sender: NSNotification) {
//         self.view.frame.origin.y = -150 // Move view 150 points upward
//    }
//
//    @objc func keyboardWillHide(sender: NSNotification) {
//         self.view.frame.origin.y = 0 // Move view to original position
//    }
    
    
    private func checkNetwork(){
        if Reachability.isConnectedToNetwork() {
            getHealthPortfolio()
            getHistoryApiCall()
            getChiefComplaintsApiCall()
            getTelemedicineattachmentApiCall()
        }else{
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    
    
    
    private func setupView(){
        titlelbl.text = String(format: "%@", "Symptoms & Reports")
        view.addSubview(navView)
        navView.addSubview(backBtn)
        navView.addSubview(titlelbl)
                
        view.addSubview(collectionView)
//        view.addSubview(nextBtn)

        backBtn.addTarget(self, action:#selector(backBtnPress), for: .touchUpInside)

        
//        collectionView.register(CausesHeaderCell.self, forCellWithReuseIdentifier: "CausesHeaderCell")
//        collectionView.register(HealthPortfolioFooterCell.self, forCellWithReuseIdentifier: "HealthPortfolioFooterCell")
        

        collectionView.register(HealthPortfolioCell.self, forCellWithReuseIdentifier: "HealthPortfolioCell")
        collectionView.register(HealthPortfolioHistoryCell.self, forCellWithReuseIdentifier: "HealthPortfolioHistoryCell")

        collectionView.register(HealthPortfolioFooterCell.self, forCellWithReuseIdentifier: "HealthPortfolioFooterCell")
        collectionView.register(CausesHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CausesHeaderCell")

        collectionView.register(HealthPortfolioUploadHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HealthPortfolioUploadHeaderCell")
        collectionView.register(GalleryCell.self, forCellWithReuseIdentifier: "GalleryCell")
        
        nameArray = [
            [
            "tmpi_fever",
            "tmpi_cough",
            "tmpi_breathingdiff",
            "tmpi_tiredness",
            "tmpi_sorethroat",
            "tmpi_bodyache",
            "tmpi_chestpain",
            "tmpi_diarrhea",
            "tmpi_othersymptoms",
            "tmpi_otherpertinent",
            ],
            [
            "tmpi_heartdisease",
            "tmpi_highbp",
            "tmpi_diabetes",
            "tmpi_copdasthma",
            "tmpi_transplant",
            "tmpi_cancer",
            "tmpi_recenttravel",
            "tmpi_covid",
            ],
        [
            "tmpi_pollen",
            "tmpi_rhinitis",
            "tmpi_dust",
            "tmpi_drug",
            "tmpi_pet",
            "tmpi_eye",
            "tmpi_food",
            "tmpi_insect",
            "tmpi_cosmetic",
            "tmpi_aspirin",
            "tmpi_latex",
            "tmpi_sinus",
            "tmpi_otherallergies"
            ]
    ]
        
        dataArray = [
            [
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "",
                "",
            ],[
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
               
            ],
            [""],
            [""],
            [
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                ""
            ],
        [""]]


    }
    
    private func setupConstraint(){
        navView.frame = CGRect(x: 0, y: SCREEN.statusBarHeight, width: SCREEN.WIDTH, height: 55)
        backBtn.frame = CGRect(x: 0, y: 0, width: 45, height: 55)
        titlelbl.frame = CGRect(x: backBtn.frame.width + 5, y: 0, width: navView.frame.width - 45 - 45 - 5 - 5, height: 55)
                
        collectionView.frame = CGRect(x: 0, y: navView.frame.origin.y + navView.frame.height, width: SCREEN.WIDTH, height: SCREEN.HEIGHT - navView.frame.origin.y - navView.frame.height)

//        nextBtn.frame = CGRect(x: 0, y: SCREEN.HEIGHT - 50, width: SCREEN.WIDTH, height: 50)
    }
    
    private func loadSymp(){
        self.collectionView.isHidden = false
        self.activityIndicatorEnd()
        titles = [
            [
                "Fever (ज्वरो)",
                "Cough (खोकी)",
                "Breathing Difficulty (सास फेर्न गाह्रो)",
                "Tiredness (थकान)",
                "Sore Throat (घाँटी दुखेको)",
                "Bodyache (जीउ दुखेको)",
                "Chest Pain (छाती दुखेको)",
                "Diarrhea (पखाला)",
                "Any other symptoms:",
                "Other Pertinent Information:"
            ],
            
            [
                "Heart Disease (मुटु रोग)",
                "High Blood Pressure (उच्च रक्तचाप)",
                "Diabetes (मधुमेह)",
                "COPD/Asthama (दम)",
                "Transplant",
                "Cancer (क्यान्सर)",
                "Recent Travel (भर्खरको यात्रा)",
                "Exposure to Covid Patient (Covid-19 बिरामी संग संक्रमण)",
            ],
            
            [""],
            
            [""],

            [
                "Pollen (परागकण)",
                "Rhinitis (नासिकाशोथ)",
                "Dust (धुलो)",
                "Drug (लागु पदार्थ)",
                "Pet (घरपालुवा जनावर)",
                "Eye (आखा)",
                "Food (खाना)",
                "Insect (कीरा)",
                "Cosmetic (प्रसाधन सामाग्री)",
                "Aspirin (एस्पिरिन)",
                "Latex (ल्याटेक्स)",
                "Sinus (साइनस)",
                "Other Allergies:"
            ],
            [""]
        ]
        print("titles count :: \(titles.count)")
        collectionView.reloadData()
    }
    
    @objc private func backBtnPress() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    //Next button press
    @objc private func nextBtnPress(){
        view.endEditing(true)
        removeArray()
                
//        let vc = HealthPortfolioUploadVC()
//        vc.patientArray = self.patientArray
//
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    */
    
    func removeArray(){
        let array = Array(dataArray.joined())
        checkNetworkConnection(data: array)
    }
    
    private func checkNetworkConnection(data: Array<String>){
        if Reachability.isConnectedToNetwork(){
            self.activityIndicatorBegin()
            ApiCall(data: data)
        }else{
            self.activityIndicatorEnd()
            self.toastMessage(message: String.NO_INTERNET_CONNECTION_MESSAGE, toastType: .message)
        }
    }
    
    fileprivate func ApiCall(data: Array<String>){
        /*
        ApiManager.sendRequest(toApi: Api.Endpoint.savetelemedinfo(data: data,
                                                                   name: nameArray,
                                                                   timeid: "",
                                                                   midasid: patientArray[0].midasid,
                                                                   appid: patientArray[0].appid,
                                                                   hosid: users.orgid ?? "")) { (status, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                self.toastMessage(message: data["message"].string, toastType: .success)
                
                let vc = HealthPortfolioUploadVC()
                vc.patientArray = self.patientArray
//                vc.selectedTimeslot = self.selectedTimeslot
//                vc.queueno = self.queueno
//                vc.scheduleid = self.scheduleid
//                vc.appid = self.appid

                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
                self.toastMessage(message: message, toastType: .message)

            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
        */
    }
}

//MARK:- HealthPortfolio
extension HealthPortfolio{
    func getHealthPortfolio(){
        ApiManager.sendRequest(toApi: Api.Endpoint.getPatientTelemedicineInfo(midasid: patientArray[0].midasid, queueno: patientArray[0].appid, orgid: users.orgid ?? "")) { (status, data) in
            self.activityIndicatorEnd()
            
            if data["type"].string == "success"{
                if let responseData = data["response"].dictionary{
                    self.dataArray = [
                        [
                            responseData["tmpi_fever"]?.string ?? "",
                            responseData["tmpi_cough"]?.string ?? "",
                            responseData["tmpi_breathingdiff"]?.string ?? "",
                            responseData["tmpi_tiredness"]?.string ?? "",
                            responseData["tmpi_sorethroat"]?.string ?? "",
                            responseData["tmpi_bodyache"]?.string ?? "",
                            responseData["tmpi_chestpain"]?.string ?? "",
                            responseData["tmpi_diarrhea"]?.string ?? "",
                            responseData["tmpi_othersymptoms"]?.string ?? "",
                            responseData["tmpi_otherpertinent"]?.string ?? ""
                        ],
                        [
                            responseData["tmpi_heartdisease"]?.string ?? "",
                            responseData["tmpi_highbp"]?.string ?? "",
                            responseData["tmpi_diabetes"]?.string ?? "",
                            responseData["tmpi_copdasthma"]?.string ?? "",
                            responseData["tmpi_transplant"]?.string ?? "",
                            responseData["tmpi_cancer"]?.string ?? "",
                            responseData["tmpi_recenttravel"]?.string ?? "",
                            responseData["tmpi_covid"]?.string ?? ""
                        ],
                        [""],
                        [""],
                    [
                        responseData["tmpi_pollen"]?.string ?? "",
                        responseData["tmpi_rhinitis"]?.string ?? "",
                        responseData["tmpi_dust"]?.string ?? "",
                        responseData["tmpi_drug"]?.string ?? "",
                        responseData["tmpi_pet"]?.string ?? "",
                        responseData["tmpi_eye"]?.string ?? "",
                        responseData["tmpi_food"]?.string ?? "",
                        responseData["tmpi_insect"]?.string ?? "",
                        responseData["tmpi_cosmetic"]?.string ?? "",
                        responseData["tmpi_aspirin"]?.string ?? "",
                        responseData["tmpi_latex"]?.string ?? "",
                        responseData["tmpi_sinus"]?.string ?? "",
                        responseData["tmpi_otherallergies"]?.string ?? ""
                    ],
                        [""],
                        
                    ]
                    
                    print("dataArray \(self.dataArray)")
                    self.collectionView.reloadData()
                }
            }
            /*
             [
                 [
                 "tmpi_fever",
                 "tmpi_cough",
                 "tmpi_breathingdiff",
                 "tmpi_tiredness",
                 "tmpi_sorethroat",
                 "tmpi_bodyache",
                 "tmpi_chestpain",
                 "tmpi_diarrhea",
                 "tmpi_othersymptoms",
                 "tmpi_otherpertinent",
                 ],
                 [
                 "tmpi_heartdisease",
                 "tmpi_highbp",
                 "tmpi_diabetes",
                 "tmpi_copdasthma",
                 "tmpi_transplant",
                 "tmpi_cancer",
                 "tmpi_recenttravel",
                 "tmpi_covid",
                 ],
             [
                 "tmpi_pollen",
                 "tmpi_rhinitis",
                 "tmpi_dust",
                 "tmpi_drug",
                 "tmpi_pet",
                 "tmpi_eye",
                 "tmpi_food",
                 "tmpi_insect",
                 "tmpi_cosmetic",
                 "tmpi_aspirin",
                 "tmpi_latex",
                 "tmpi_sinus",
                 "tmpi_otherallergies"
                 ]
         ]
             */
            
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)

        }
    }
    
    func getHistoryApiCall(){
        self.activityIndicatorBegin()
        ApiManager.sendRequest(toApi: Api.Endpoint.gethistory(appid: patientArray[0].appid)) { (status, data) in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                
            }else{
//                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
//                self.toastMessage(message: message, toastType: .message)
            }
        } onError: { (error) in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }
    }
    func getChiefComplaintsApiCall() {
        
    }
    
//    func getChiefComplaintsApiCall(){
//        ApiManager.sendRequest(toApi: Api.Endpoint.getchiefcomplaints(appid: patientArray[0].appid)) { (status, data) in
//            self.activityIndicatorEnd()
//            if data["type"].string == "success"{
//                print(data)
//            }else{
////                let message = data["message"].string ?? ApiError.invalidData.localizedDescription
////                self.toastMessage(message: message, toastType: .message)
//            }
//        } onError: { (error) in
//            self.activityIndicatorEnd()
//            self.toastMessage(message: error.localizedDescription, toastType: .message)
//        }
//    }
    
    func getTelemedicineattachmentApiCall(){
        ApiManager.sendRequest(toApi: Api.Endpoint.gettelemedicineattachment(userid: patientArray[0].midasid, appid: patientArray[0].appid, orgid: users.orgid ?? "")) { status, data in
            self.activityIndicatorEnd()
            if data["type"].string == "success"{
                if let responseArray = data["response"].array{
                    for responseDic in responseArray{
                        
                        let categoryType = responseDic["tmat_category"]
                        if categoryType == "pathology" {
                            self.pathologyImages.append(responseDic["tmat_file"].string!)
                        }else if categoryType == "ecg" {
                            self.ecgImages.append(responseDic["tmat_file"].string!)
                        }else if categoryType == "prescription" {
                            self.prescriptionImages.append(responseDic["tmat_file"].string!)
                        }
                    }
                    self.collectionView.reloadData()
                }
                
//                selectedImages.append(contentsOf: pathologyImages)
                
            }else{
                
            }
        } onError: { error in
            self.activityIndicatorEnd()
            self.toastMessage(message: error.localizedDescription, toastType: .message)
        }

    }
    
    //MARK:- Save API Call
    @objc private func saveBtnTap(sender: UIButton){
        let tag = sender.tag
        print("save index path \(sender.tag)");
        
        
        if tag == 2 {
            
        }else if tag == 3{
            let indexPath = Utility.getCollectionViewCellIndexForInfobtnFromSubview(view: sender, inCollection: collectionView)
            historyCell = collectionView.cellForItem(at: indexPath!) as! HealthPortfolioHistoryCell

            ApiManager.sendRequest(toApi: Api.Endpoint.savehistory(appid: patientArray[0].appid, hede_history: historyCell.complaintTextField.text ?? "", hede_detailid: "")) { (status, data) in
                self.activityIndicatorEnd()
                self.toastMessage(message: data["message"].string, toastType: .message)
                
            } onError: { (error) in
                self.toastMessage(message: error.localizedDescription, toastType: .message)
            }
        }
    }
    
    
}
extension HealthPortfolio: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return headerTitles.count + headerTitleArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if section == 2 || section == 3 || section == 5 {
//            return 1
//        }
        if section == 5{
            return 0
        }else if section == 6{
            return pathologyImages.count
        }else if section == 7{
            return ecgImages.count
        }else if section == 8{
            return prescriptionImages.count
        }
        
        else{
            return titles[section].count
        }
        
        
        
//        if section == 5 || section == 6 || section == 7 {
//
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.section == 0 && indexPath.row == 8) || (indexPath.section == 0 && indexPath.row == 9) || (indexPath.section == 4 && indexPath.row == 12){

            footerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HealthPortfolioFooterCell", for: indexPath) as! HealthPortfolioFooterCell
            footerCell.titlelbl.text = titles[indexPath.section][indexPath.row]
            footerCell.infoTxtField.text = dataArray[indexPath.section][indexPath.row]
            footerCell.infoTxtField.delegate = self
            footerCell.infoTxtField.tag = indexPath.row
            
//            footerCell.titlelbl.frame = CGRect(x: 10, y: 8, width: view.frame.width - 20, height: 20)
//            footerCell.infoTxtField.frame = CGRect(x: 10, y: footerCell.titlelbl.frame.origin.y + footerCell.titlelbl.frame.height, width: view.frame.width - 20, height: 36)
            footerCell.infoTxtField.addTarget(self, action: #selector(textChanging(textField:)), for: .editingChanged)

            
            if indexPath.section == 0 && indexPath.row == 8 {
                footerCell.infoTxtField.placeholder = "Any other symptoms for testing"
            }else if indexPath.section == 0 && indexPath.row == 9{
                footerCell.infoTxtField.placeholder = "Other pertinent information for testing"
            }else if indexPath.section == 4 && indexPath.row == 12 {
                footerCell.infoTxtField.placeholder = "Other any allergies for testing"
            }
            
            
            return footerCell
            
        }else if  indexPath.section == 2 || indexPath.section == 3 {
            
            historyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HealthPortfolioHistoryCell", for: indexPath) as! HealthPortfolioHistoryCell
//            historyCell.complaintTextField .text = titles[indexPath.section][indexPath.row]
//            historyCell.infoTxtField.text = dataArray[indexPath.section][indexPath.row]
            
            historyCell.complaintTextField.delegate = self
            historyCell.complaintTextField.tag = indexPath.section
            historyCell.saveBtn.tag = indexPath.section
            
            if indexPath.section == 2 {
                historyCell.complaintTextField.placeholder = "Please enter chief complaints"
            }else{
                historyCell.complaintTextField.placeholder = "Please enter history."
            }
            
            historyCell.saveBtn.addTarget(self, action: #selector(saveBtnTap(sender:)), for: .touchUpInside)

            
            return historyCell
            
        }else if indexPath.section == 6 || indexPath.section == 7 || indexPath.section == 8 {
            galleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
            galleryCell.deleteBtn.isHidden = true
            if indexPath.section == 6 {
                galleryCell.profileImage.sd_setImage(with: URL(string: pathologyImages[indexPath.row]), placeholderImage: Images.ic_picture)
//                cell.profileImage.image = UIImage(named: pathologyImages[indexPath.row])
            }else if indexPath.section == 7 {
                galleryCell.profileImage.sd_setImage(with: URL(string: ecgImages[indexPath.row]), placeholderImage: Images.ic_picture)
//                cell.profileImage.image = UIImage(named: ecgImages[indexPath.row])
            }else if indexPath.section == 8 {
                galleryCell.profileImage.sd_setImage(with: URL(string: prescriptionImages[indexPath.row]), placeholderImage: Images.ic_picture)
//                cell.profileImage.image = UIImage(named: prescriptionImages[indexPath.row])
            }
            
            return galleryCell
        }else{
            healthPortfolioCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HealthPortfolioCell", for: indexPath) as! HealthPortfolioCell
            
            let tagText = titles[indexPath.section][indexPath.row]
            healthPortfolioCell.tagLabel.text = tagText
            
            let tagWidth = tagText.getWidth(with: view.frame.width - 8 - 25 - 25, font: UIFont.systemFont(ofSize: 15, weight: .regular))
            let tagHeight = tagText.getHeight(with: tagWidth, font: UIFont.systemFont(ofSize: 15, weight: .regular))
            
            healthPortfolioCell.checkBtn.frame = CGRect(x: 8, y: 10, width: 20, height: 20);
            healthPortfolioCell.tagLabel.frame = CGRect(x: 33, y: 0, width: tagWidth + 30, height: tagHeight + 20)
            healthPortfolioCell.tagLabel.preferredMaxLayoutWidth = tagWidth + 30
            
            
            if dataArray[indexPath.section][indexPath.row] == "0" {
                healthPortfolioCell.checkBtn.setImage(Images.TICK_UNSELECTED, for: .normal)
            }else{
                healthPortfolioCell.checkBtn.setImage(Images.TICK_GRAY_SELECTED, for: .normal)
            }
            return healthPortfolioCell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var reusableView : UICollectionReusableView? = nil
        if indexPath.section == 6 || indexPath.section == 7 || indexPath.section == 8 {
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HealthPortfolioUploadHeaderCell", for: indexPath) as! HealthPortfolioUploadHeaderCell
            
            reusableView = cell
            
            cell.titlelbl.text = headerTitleArray[indexPath.section - 6]
            cell.chooseBtn.tag = indexPath.section - 6
            cell.chooseBtn.addTarget(self, action: #selector(chooseBtnTap(sender:)), for: .touchUpInside)

        }else{
            if kind == UICollectionView.elementKindSectionHeader {
                
                
                let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CausesHeaderCell", for: indexPath) as! CausesHeaderCell
                reusableView = headerCell
                headerCell.tagHeaderLabel.text = headerTitles[indexPath.section]
            }
            
        }
        return reusableView!
    }
    
//    referenceSizeForHeaderInSectio
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if section == 6 || section == 7 || section == 8 {
            return CGSize(width: SCREEN.WIDTH, height: 60)
        }
        return CGSize(width: SCREEN.WIDTH, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (indexPath.section == 0 && indexPath.row == 8) || (indexPath.section == 0 && indexPath.row == 9) || (indexPath.section == 4 && indexPath.row == 12){
            return CGSize(width: view.frame.width - 20, height: 85)
        }else if  indexPath.section == 2 || indexPath.section == 3{
            return CGSize(width: view.frame.width - 20, height: 115)
        }else if indexPath.section == 5{
            return CGSize(width: view.frame.width - 20, height: 0)
        }else if indexPath.section == 6 || indexPath.section == 7 || indexPath.section == 8 {
            return CGSize(width: 70, height: 90)
        }else{
            let tagText = titles[indexPath.section][indexPath.row]
            
            let tagWidth = tagText.getWidth(with: view.frame.width - 25, font: UIFont.systemFont(ofSize: 15, weight: .regular))
            let tagHeight = tagText.getHeight(with: tagWidth, font: UIFont.systemFont(ofSize: 15, weight: .regular))
            healthPortfolioCell.checkBtn.frame = CGRect(x: 0, y: 10, width: 20, height: 20);
            healthPortfolioCell.tagLabel.frame = CGRect(x: 25, y: 0, width: tagWidth + 30, height: tagHeight + 20)

            return CGSize(width: 25 + tagWidth, height: tagHeight + 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    if indexPath.section == 6 || indexPath.section == 7 || indexPath.section == 8 {
        
        galleryCell = collectionView.cellForItem(at: indexPath) as! GalleryCell
        let vc = ImageVC()
        vc.selectedImageView = galleryCell.profileImage.image!
        
        if indexPath.section == 6{
            vc.selectedImageString = pathologyImages[indexPath.row]
        }else if indexPath.section == 7{
            vc.selectedImageString = ecgImages[indexPath.row]
        }else{
            vc.selectedImageString = prescriptionImages[indexPath.row]
        }
        
        navigationController?.pushViewController(vc, animated: true)
    
    }
        /*
        if (indexPath.section == 0 && indexPath.row == 8) || (indexPath.section == 0 && indexPath.row == 9) || indexPath.section == 2 || indexPath.section == 3 || (indexPath.section == 4 && indexPath.row == 12){

        }else{
            healthPortfolioCell = collectionView.cellForItem(at: indexPath) as! HealthPortfolioCell
            
            if (healthPortfolioCell.checkBtn.image(for: .normal) == Images.TICK_SELECTED) {
                healthPortfolioCell.checkBtn.setImage(Images.TICK_UNSELECTED, for: .normal)
                dataArray[indexPath.section][indexPath.row] = "0"
            }else{
                healthPortfolioCell.checkBtn.setImage(Images.TICK_GRAY_SELECTED, for: .normal)
                dataArray[indexPath.section][indexPath.row] = "1"
            }
        }*/
    }
    
    @objc func textChanging(textField: UITextField){
        let indexPath = Utility.getCollectionViewCellIndexForInfobtnFromSubview(view: textField, inCollection: collectionView)
        footerCell = collectionView.cellForItem(at: indexPath!) as! HealthPortfolioFooterCell
        dataArray[indexPath!.section][indexPath!.row] = footerCell.infoTxtField.text ?? ""
    }
    
    @objc private func chooseBtnTap(sender: UIButton){
        print("index path \(sender.tag)");
//        chooseBtnTag = sender.tag
        
    }
}


//MARK:- HealthPortfolioUploadHeaderCell
class HealthPortfolioUploadHeaderCell: UICollectionViewCell {
    
    fileprivate lazy var backview = BackViewProperties()

    fileprivate lazy var titlelbl = UILabel()
//    lazy var subtitlelbl = UILabel()
    lazy var chooseBtn = UIButton()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupFrame()
    }
    
    fileprivate func setupView() {
        contentView.addSubview(backview)
        
        backview.addSubview(titlelbl)
//        backview.addSubview(subtitlelbl)
        backview.addSubview(chooseBtn)
        
        titlelbl.font = UIFont.medium(ofSize: 14)
        titlelbl.textColor = .black
        
//        subtitlelbl.font = UIFont.regular(ofSize: 13)
//        subtitlelbl.textColor = .darkGray
        
        chooseBtn.setImage(Images.ic_folder, for: .normal)
        chooseBtn.backgroundColor = .clear
        chooseBtn.clipsToBounds = true
        chooseBtn.layer.borderWidth = 1.5
        chooseBtn.layer.borderColor = Theme.Color.lightGray.cgColor
        chooseBtn.layer.cornerRadius = 15
    }
    
    fileprivate func setupFrame(){
        backview.frame = CGRect(x: 10, y: 5, width: SCREEN.WIDTH - 20, height: 50)

        chooseBtn.frame = CGRect(x: backview.frame.width - 10 - 30, y: 10, width: 30, height: 30)
        
        let titleWidth: CGFloat = backview.frame.width - 10 - 5 - chooseBtn.frame.width - 10
        titlelbl.frame = CGRect(x: 10, y: 15, width: titleWidth, height: 20)
//        subtitlelbl.frame = CGRect(x: 10, y: 30, width: titleWidth, height: 20)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
