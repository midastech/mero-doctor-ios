//
//  Api.swift
//  MiDasHMIS
//
//  Created by Ramesh on 7/6/17.
//  Copyright © 2017 sunil. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyUserDefaults

enum ApiResponse: String {
    case success = "success"
    case failure = "error"
}



struct Api  {
    
    static let API_KEY = "0ee4198537b966818a4fbc1e81d7494d"
    static let API_VERSION = "v1/"
    static let HMIS_BASE_URL = "https://hmis.mero.doctor/api/"
    static let BASE_URL_MERO = "https://api.mero.doctor/api/" + API_VERSION
    static let BASE_URL_LOCAL = DataManager.getUserDetail()[0].hospitaldomain ?? ""
    
    enum Endpoint {
        case login(username: String, password: String)
        case getdochospital(gdocid: String)
        case getDocStatus(docid: String, orgid: String)
        case telemedicinepatientlist(docid: String, orgid: String, type: String, fromdate: String, todate: String)
        case getrevenue(fromdate: String, todate: String, orgid: String, gdocid: String, iscategory: String)
        case patientlist_local(docid: String, fromdate: String, todate: String)
        case getrevenuebycategory(fromdate: String, todate: String, orgid: String, gdocid: String, category: String)
        case getzoomcredentials
        case changeDocStatus(orgid: String, gdocid: String, delaytime: String, frmdate: String, todate: String, reason: String, status: String)
        case getpersonalnote(patientid: String, appid: String, midasid: String)
      
        case saveComplaints(appid: String, complaintId: String, complaintText: String)
        case updatepersonalnote(opdid: String, personalnote: String)
        case outPatientApi(appid: String, midasid: String, status: String)
        
        case getSchedule(orgid: String, gdepid: String, gdocid: String, fromdate: String, todate: String, type: String)
        case getslotinfo(orgid: String, gdocid: String)
        case getPatientTelemedicineInfo(midasid: String, queueno: String, orgid: String)
        case savetelemedinfo(data: [String], name: [String], timeid: String, midasid: String, appid: String, hosid: String)
        case gethistory(appid: String)
        case savehistory(appid: String, hede_history: String, hede_detailid: String)
        case getchiefcomplaints(appid: String)
        case updateChieftComplatints(appid: String, complaintId: String, complaintText: String)
        case updateObservations(appid: String, observationId: String, observationText: String)
        case updateDiagnosis(appid: String, diagnosisId: String, diagnosisText: String)
        case updateDoctorNote(appid: String, doctornoteId: String, doctornoteText: String)
        case getExaminationFullData(appid: String)
        case gettelemedicineattachment(userid: String, appid: String, orgid: String)
        case getPrescriptionData(userid: String, appid: String, orgid: String, category: String)
        case delattachment(id: String, hosid: String)
        case changePassword(oldpassword: String, newpassword: String, username: String)
        /*
         id ra hosid
        id is file id
        hosid is orgid
         
         */
//        userid=1000038338&appid=50949&orgid=614

        
        /*
         (@Field("appid") String appointmentId,
         @Field("hede_history")String history,
         @Field("hede_detailid")String historyId)
         */
        
        
        var url: String {
            switch self {
            case .login:
                return BASE_URL_MERO + "doctor/loginv1"
                
            case .getdochospital:
                return BASE_URL_MERO + "doctor/getdochospital"
                
            case .getDocStatus:
                return BASE_URL_MERO + "doctor/getDocStatus"
                
            case .telemedicinepatientlist:
                return BASE_URL_MERO + "appointments/telemedicinepatientlist"
                
            case .patientlist_local:
                return BASE_URL_LOCAL + "doctor/patientlist"
                
            case .getrevenue:
                return BASE_URL_MERO + "doctor/getrevenue"
                
            case .getrevenuebycategory:
                return BASE_URL_MERO + "doctor/getrevenuebycategory"
                
            case .getzoomcredentials:
                return BASE_URL_MERO + "doctor/getzoomcredentials"
//            case .doctorProfile:
//                return BASE_URL_MERO + "doctor/doctorProfile"
            
            case .getExaminationFullData:
                return HMIS_BASE_URL + "appointments/getexaminationdata"
            case .changeDocStatus:
                return BASE_URL_MERO + "doctor/changeDocStatus"
                
            case .getpersonalnote:
                return BASE_URL_MERO + "appointments/getpersonalnote"
                
            case .updatepersonalnote:
                return BASE_URL_MERO + "appointments/updatepersonalnote"
                
            case .getSchedule:
                return BASE_URL_MERO + "doctor/getSchedule"
                
            case .getslotinfo:
                return BASE_URL_MERO + "doctor/getslotinfo"
                
            case .getPatientTelemedicineInfo:
                return BASE_URL_MERO + "appointments/getPatientTelemedicineInfo"
                
            case .savetelemedinfo:
                return BASE_URL_MERO + "appointments/savetelemedinfo"
                
            case .gethistory:
                return BASE_URL_MERO + "appointments/gethistory"
            case .saveComplaints:
                return HMIS_BASE_URL + "appointments/savechiefcomplaints"
            case .updateChieftComplatints:
                return HMIS_BASE_URL + "appointments/savechiefcomplaints"
            case .updateObservations:
                return HMIS_BASE_URL + "appointments/saveexamination"
            case .updateDiagnosis:
                return HMIS_BASE_URL + "appointments/savediagnosis"
            case .updateDoctorNote:
                return HMIS_BASE_URL + "appointments/savedoctornote"
            case .savehistory:
                return BASE_URL_MERO + "appointments/savehistory"
                
            case .getchiefcomplaints:
                return BASE_URL_MERO + "appointments/getchiefcomplaints"
                
            case .gettelemedicineattachment:
                return BASE_URL_MERO + "appointments/gettelemedicineattachment"
                
            case .getPrescriptionData:
                return BASE_URL_MERO + "appointments/gettelemedicineattachment"
                
            case .delattachment:
                return BASE_URL_MERO + "appointments/delattachment"

            case .changePassword:
                return BASE_URL_MERO + "doctor/changepassword"
            case .outPatientApi:
                return BASE_URL_MERO + "appointments/checkcallstatus"
            default:
                break
            }
            
            return ""
        }
        
        
        
        var method: HTTPMethod {

            switch self {
            case .login, .getdochospital, .getDocStatus, .telemedicinepatientlist, .getrevenue, .patientlist_local, .getrevenuebycategory, .getzoomcredentials, .changeDocStatus, .getpersonalnote, .updatepersonalnote, .getSchedule, .getslotinfo, .getPatientTelemedicineInfo, .savetelemedinfo, .gethistory, .savehistory, .getchiefcomplaints, .gettelemedicineattachment, .getPrescriptionData, .delattachment, .changePassword, .outPatientApi, .getExaminationFullData, .updateChieftComplatints, .updateDiagnosis, .updateDoctorNote, .updateObservations, .saveComplaints:
                return .post
                
//            case .banner:
//                return .get
                
//            default:
//                return .connect// .get//

            }
        }

        var parameters: [String:Any]? {
            
            switch self {

            case .login(let username, let password):
                return["username": username, "password": password]
            
            case .getdochospital(let gdocid):
                return["gdocid": gdocid]
                
            case .getDocStatus(let docid, let orgid):
                return["docid": docid, "orgid": orgid]
                
            case .telemedicinepatientlist(let docid, let orgid, let type, let fromdate, let todate):
                return["docid": docid, "orgid": orgid, "type": type, "fromdate": fromdate, "todate": todate]
                
            case .getrevenue(let fromdate, let todate, let orgid, let gdocid, let iscategory):
                return["fromdate": fromdate, "todate": todate, "orgid": orgid, "gdocid": gdocid, "iscategory": iscategory]
            
            case .patientlist_local(let docid, let fromdate, let todate):
                return ["docid": docid, "fromdate": fromdate, "todate": todate]
                
            case .getrevenuebycategory(let fromdate, let todate, let orgid, let gdocid, let category):
                return ["fromdate": fromdate, "todate": todate, "orgid": orgid, "gdocid": gdocid, "category": category]
                
            case .getzoomcredentials:
                return nil
            case .getExaminationFullData(let appid):
                return ["appid": appid]
                
            case .changeDocStatus(let orgid, let gdocid, let delaytime, let frmdate, let todate, let reason, let status):
                return ["orgid": orgid, "gdocid": gdocid, "delaytime": delaytime, "frmdate": frmdate, "todate": todate, "reason": reason, "status": status]
                
            case .getpersonalnote(let patientid, let appid, let midasid):
                return ["patientid" : patientid,"appid" : appid,"midasid" : midasid]
                
            case .updatepersonalnote(let opdid, let personalnote):
                return ["opdid": opdid, "personalnote": personalnote]
                
            case .getSchedule(let orgid, let gdepid, let gdocid, let fromdate, let todate, let type):
                return ["orgid": orgid, "gdepid": gdepid, "gdocid": gdocid, "fromdate": fromdate, "todate": todate, "type": type]

            case .getslotinfo(let orgid, let gdocid):
                return ["orgid": orgid, "gdocid": gdocid]
                
            case .getPatientTelemedicineInfo(let midasid, let queueno, let orgid):
                return ["midasid" : midasid, "appid" : queueno, "orgid" : orgid]
                
            case .savetelemedinfo(let data, let name, let timeid, let midasid, let appid, let hosid):
                return ["data": data, "name": name, "timeid": timeid, "midasid": midasid, "appid": appid, "hosid": hosid]

            case .gethistory(let appid):
                return["appid": appid]
                
            case .savehistory(let appid, let hede_history, let hede_detailid):
                return["appid": appid , "hede_history": hede_history, "hede_detailid": hede_detailid]
            
            case .getchiefcomplaints(let appid):
                return["appid": appid]
                
            case .gettelemedicineattachment(let userid, let appid, let orgid):
                return["userid": userid, "appid": appid, "orgid": orgid]
            
            case .getPrescriptionData(let userid, let appid, let orgid, let category):
                return["userid": userid, "appid": appid, "orgid": orgid, "category": category]
                
            case .delattachment(let id, let hosid):
                return["id": id, "hosid": hosid]
            
            case .changePassword(let oldpassword, let newpassword, let username):
                return["oldpassword": oldpassword, "newpassword": newpassword, "username": username]
            case .outPatientApi(let appid, let midasid, let status):
                return["appid" : appid, "midasid": midasid, "callstatus": status]
            case .saveComplaints(let appid, let complaintId, let complaintText):
                return ["appid": appid, "HITA_COMPLAINID": complaintId, "HITA_COMPLAINT": complaintText]
            case .updateChieftComplatints(let appid, let complaintId, let complaintText):
                return["appid":appid,"HITA_COMPLAINT":complaintText,"HITA_COMPLAINID":complaintId]
            case .updateObservations(let appid, let observationId, let observationText):
                return ["appid": appid, "EXFO_EXID": observationId, "EXFO_DESCRIPTION": observationText]
            case .updateDiagnosis(let appid, let diagnosisId, let diagnosisText):
            return ["appid": appid, "ICLO_DIAGNOSIS": diagnosisText, "ICLO_FINALDIAGONOSISID": diagnosisId]
            case .updateDoctorNote(let appid, let doctornoteId, let doctornoteText):
                return ["appid": appid, "HEDE_DETAILID": doctornoteId, "HEDE_REMARKS": doctornoteText]
                
            default:
                return nil
            }
        }

        /*changePassword(oldpassword: String, password: String)


         */
        

        var headers: HTTPHeaders {
            switch self {
            case .login, .getdochospital, .getDocStatus, .telemedicinepatientlist, .getrevenue, .patientlist_local, .getrevenuebycategory, .getzoomcredentials, .changeDocStatus, .getpersonalnote, .updatepersonalnote, .getSchedule, .getslotinfo, .getPatientTelemedicineInfo, .savetelemedinfo, .gethistory, .savehistory, .getchiefcomplaints, .gettelemedicineattachment, .getPrescriptionData, .delattachment, .changePassword, .outPatientApi, .getExaminationFullData, .updateDiagnosis, .updateObservations, .updateDoctorNote, .saveComplaints:
                
                var defaultHeader = HTTPHeaders()
                defaultHeader.add(name: "Apikey", value: API_KEY)
                defaultHeader.add(name: "Machinetype", value: "Android")

            return defaultHeader
            case .updateChieftComplatints:
                var defaultHeader = HTTPHeaders()
                defaultHeader.add(name: "Content-Type", value: "application/x-www-form-urlencoded")
                defaultHeader.add(name: "Apikey", value: API_KEY)
                defaultHeader.add(name: "Machinetype", value: "Android")
                defaultHeader.add(name: "Host", value: "hmis.mero.doctor")
                defaultHeader.add(name: "Connection", value: "Keep-Alive")
                defaultHeader.add(name: "Accept-Encoding", value: "gzip")
                defaultHeader.add(name: "User-Agent", value: "okhttp/4.3.0")
                defaultHeader.add(name: "Apiversion", value: "v1/")
                defaultHeader.add(name: "Device", value: "OnePlusIN2021")
                defaultHeader.add(name: "Appversion", value: "1.12")
                defaultHeader.add(name: "Appversioncode", value: "12")
                defaultHeader.add(name: "IsDebug", value: "true")
                defaultHeader.add(name: "Applicationid", value: "np.com.midas.merodoctor_doctor")
               // defaultHeader.add(name: "Accept", value: "application/json")
              
                




                

            return defaultHeader
                
            default:
                break
            }
        }
        
        var encoding: ParameterEncoding {
            
            return URLEncoding.default
        }
    }
}
