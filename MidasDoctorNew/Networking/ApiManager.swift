//
//  ApiManager.swift
//  MiDasHMIS
//
//  Created by Nishan-82 on 7/6/17.
//  Copyright © 2017 sunil. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class ApiManager {
    
    class func sendRequest(toApi api:Api.Endpoint, onSuccess:@escaping (_ statusCode:Int,_ data:JSON)->(),onError:@escaping (ApplicationError)->()) {
//        Alamofire.SessionManager.default.session.
//        Alamofire.Session.default.request(api.url, method: api.method, parameters: api.parameters, encoding: api.encoding, headers: api.headers).response{(dataResponse) in
//
//        }
        print(api.headers,api.url,api.parameters)
        Alamofire.Session.default.request(api.url, method: api.method, parameters: api.parameters, encoding: api.encoding, headers: api.headers).response { (dataResponse) in
            /*
            Log.Print(api.url)
            Log.add(info: "Header: \(String(describing: api.headers))")
            Log.add(info: "parameter: \(String(describing: api.parameters))")
            Log.add(info: "Error: \(String(describing: dataResponse.error))")
                  */
           
            
            if let error = dataResponse.error {
                let message = error.localizedDescription
                onError(ApiError.invalidResponse(message: message))
                return
            }

            // If there is no HTTP related Error
            if let data = dataResponse.data, let statusCode = dataResponse.response?.statusCode {
               /*
                Log.add(info: "Response Data : \(String(describing: try? JSON(data:dataResponse.data!)))")
                print("statusCode = ",dataResponse.response?.statusCode as Any)
                */
                let jsonData = JSON(data)
                onSuccess(statusCode,jsonData)
            }
            Log.clear()
        }
    }
    
    
    
    
    
    
    class func cancelRequest(){
        Alamofire.Session.default.session.invalidateAndCancel()
//        SessionManager.default.request
//        Alamofire.cancelRequest()
    }

    
    class func sendRequestWithMultipart(toApi api:Api.Endpoint, withMultiPartDataBlock: @escaping (MultipartFormData)->(), onSuccess:@escaping (_ statusCode:Int,_ data:JSON)->(),onError:@escaping (ApplicationError)->()) {


        Alamofire.Session.default.request(api.url, method: api.method, parameters: api.parameters, encoding: api.encoding, headers: api.headers).response { (dataResponse) in
        /*
        Log.Print(api.url)
        Log.add(info: "Header: \(String(describing: api.headers))")
        Log.add(info: "parameter: \(String(describing: api.parameters))")

        Log.add(info: "Error: \(String(describing: dataResponse.error))")
        Log.add(info: "Response Data :: \(String(describing: try? JSON(data:dataResponse.data!)))")
            */
        Log.clear()
        
        
        if let error = dataResponse.error {
            
            let message = error.localizedDescription
            onError(ApiError.invalidResponse(message: message))
            return
        }
        
        // If there is no HTTP related Error
        if let data = dataResponse.data, let statusCode = dataResponse.response?.statusCode {

            /*
            print("dataResponse.data = ",dataResponse.data as Any)
 */
//            print("stat/usCode = ",dataResponse.response?.statusCode as Any)
            
            
            let jsonData = JSON(data)
            print("json data == ",jsonData)
            onSuccess(statusCode,jsonData)
        }
        }
    }
}
