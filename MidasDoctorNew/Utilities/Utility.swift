//
//  Utility.swift
//  MiDasHMIS
//
//  Created by ramesh prajapati on 3/16/18.
//  Copyright © 2020 Midas. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

import CommonCrypto
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class Utility {

    
    
    
//    class func deviceModelName() -> String {
//        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
//        var sysinfo = utsname()
//        uname(&sysinfo) // ignore return value
//        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
//    }
    
    class func deviceModelName() -> String {
//        static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
//        }()

    }
    
    class func iosVersion() -> String {
        let deviceIdentifier = UIDevice.current.systemVersion
            return deviceIdentifier
    }
    
    class func getCurrentAppVersion() -> String {
        
        if let currentAppVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return currentAppVersion
        }
        return ""
    }
    
    class func getDeviceUniqueIdentifier() -> String {
        
        if let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString {
            return deviceIdentifier
        }
        return ""
    }
    
    // NOTE: #include<ifaddrs.h> add this in bridging header if not working
    
    class func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    if let ifaName = interface?.ifa_name {
                        let name = String(cString: ifaName)
                        if name == "en0" {
                            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                            getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                            address = String(cString: hostname)
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    class func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    class func getTableViewCellIndexForInfobtnFromSubview(view: UIView,
                                                 inTableView tableView:UITableView) -> IndexPath? {
        let buttonPosition:CGPoint = view.convert(CGPoint.zero, to:tableView)
        let indexPath = tableView.indexPathForRow(at: buttonPosition)
        
        return indexPath
    }
    
    class func getCollectionViewCellIndexForInfobtnFromSubview(view: UIView,
                                                 inCollection collectionView:UICollectionView) -> IndexPath? {
        let buttonPosition:CGPoint = view.convert(CGPoint.zero, to:collectionView)
        let indexPath = collectionView.indexPathForItem(at: buttonPosition)
//        indexPathForRow(at: buttonPosition)
        
        return indexPath
    }
    
    
    class func getNepaliDay(day : String) -> String {
        var daystr = String()
        if day == "Sunday" || day == "SUNDAY" {
            daystr = "आइतबार";
        }else if day == "Monday" || day == "MONDAY" {
            daystr = "सोमबार";
        }else if day == "Tuesday" || day == "TUESDAY" {
            daystr = "मंगलबार";
        }else if day == "Wednesday" || day == "WEDNESDAY" {
            daystr = "बुधवार";
        }else if day == "Thursday" || day == "THURSDAY" {
            daystr = "बिहीबार";
        }else if day == "Friday" || day == "FRIDAY" {
            daystr = "शुक्रबार";
        }else if day == "Saturday" || day == "SATURDAY" {
            daystr = "शनिबार";
        }
        
        return daystr;
    }
    
    
    class func todayDate() -> String{
        let todayDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = String.DATE_FORMATE
        let dateString: String = formatter.string(from: todayDate) as String
//        print("dateString",dateString)
        
        return dateString
    }
    class func getDayofWeek(engDate: String) -> String {
            let weekdays = [
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            ]
        
//        let startEnglishDate :String = "1943/04/14"

        let formatter = DateFormatter()
        formatter.dateFormat = String.DATE_FORMATE

//        let startDate : NSDate = formatter.date(from: startEnglishDate)! as NSDate
//        print("startDate",startDate)
        
        let toDate : NSDate = formatter.date(from: engDate)! as NSDate

        
        let gregorianCalendar :NSCalendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
        let components : NSDateComponents = gregorianCalendar.components(.weekday, from: toDate  as Date) as NSDateComponents
        
//        (NSCalendar.Unit.day, from: startDate as Date, to: toDate as Date, options: []) as NSDateComponents

        

//            let calendar: NSCalendar = NSCalendar.currentCalendar()
//            let components: NSDateComponents = calendar.components(.Weekday, fromDate: self)
            return weekdays[components.weekday - 1]
        }
    
    class func stringToDate(_ value: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = String.DATE_FORMATE
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.date(from: value)
    }
    
    class func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
//            dateFormatter.calendar = Calendar(identifier: .persian)
        dateFormatter.dateFormat = String.DATE_FORMATE
        let str = dateFormatter.string(from: date)
        return str
    }
    
    
}
