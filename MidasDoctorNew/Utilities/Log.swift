//
//  Log.swift
//  MiDasHMIS
//
//  Created by AndMine on 10/17/16.
//  Copyright © 2016 AndMine. All rights reserved.
//

import Foundation

class Log {
  
    
    class func Print(_ info:String) {
        
        #if DEBUG
            print("\n---------------- \(info) ------------------- ")
        #endif
    }
    
    class func clear() {
        
        #if DEBUG
            print("\n--------------------------------------------\n")
        #endif
    }
    
    class func add(info:Any, fileName:String = #file, methodName:String = #function) {
        
        #if DEBUG
            print("› Log: [\(fileName.components(separatedBy: "/").last!.components(separatedBy: ".").first!).\(methodName)] : \(info)")
        #endif
    }
    
    class func error(info:Any, fileName:String = #file, methodName:String = #function) {
        
        #if DEBUG
            print("• Error: [\(fileName.components(separatedBy: "/").last!.components(separatedBy: ".").first!).\(methodName)] : \(info)")
        #endif
    }
    
    class func warn(info:Any, fileName:String = #file, methodName:String = #function) {
        
        #if DEBUG
            print("⚠ Warning: [\(fileName.components(separatedBy: "/").last!.components(separatedBy: ".").first!).\(methodName)] : \(info)")
        #endif
    }
    
    class func checkpoint(fileName:String = #file, methodName:String = #function) {
        
        #if DEBUG
            print("\n√ Checkpoint: [\(fileName.components(separatedBy: "/").last!.components(separatedBy: ".").first!).\(methodName)]\n")
        #endif
    }
    
    class func memoryDeallocation(fileName:String = #file, methodName:String = #function) {
        
        #if DEBUG
            print("♻ Deallocated: [\(fileName.components(separatedBy: "/").last!.components(separatedBy: ".").first!).\(methodName)]")
        #endif
    }
    
}
