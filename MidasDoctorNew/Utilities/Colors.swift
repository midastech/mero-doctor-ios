//
//  Colors.swift
//  MidaseClass
//
//  Created by ramesh prajapati on 7/28/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import Foundation
import UIKit

struct Theme {
    struct Color {
        
        static let white: UIColor = .white
        static let black: UIColor = .black
        static let lightGray: UIColor = .lightGray
        static let darkGray: UIColor = .darkGray
//        static let WHITE_DISABLE_COLOR: UIColor = UIColor.from(hex: "#EBEBEB")
        
        static let backgroundGray: UIColor = UIColor.from(hex: "#f7f7f7")

        static let colorPrimary: UIColor = UIColor.from(hex: "#429B92")
        static let colorSecondary: UIColor = UIColor.from(hex: "#78C697")
        static let colorAccent: UIColor = UIColor.from(hex: "#51C2E0")
        
//        static let black: UIColor = UIColor.from(hex: "#FF000000")
//        static let white: UIColor = UIColor.from(hex: "#FFFFFFFF")
        static let colorLightWhite: UIColor = UIColor.from(hex: "#EEEEEE")
        static let colorLightGray: UIColor = UIColor.from(hex: "#ddd")
        static let colorNormalGray: UIColor = UIColor.from(hex: "#aaa")
        static let colorGray: UIColor = UIColor.from(hex: "#777")
        static let colorDarkGray: UIColor = UIColor.from(hex: "#333")
        static let colorWarning: UIColor = UIColor.from(hex: "#E65D31")
        static let colorMidas: UIColor = UIColor.from(hex: "#F57C00")
        
        static let colorLightRed: UIColor = UIColor.from(hex: "#EFB5B5")
        static let Dark_Red_Color: UIColor = UIColor.from(hex: "#D9534F")

        static let colorGreen: UIColor = UIColor.from(hex: "#4CAF50")
        static let colorLightGreen: UIColor = UIColor.from(hex: "#94E4A9")
        static let colorLightDarkGreen: UIColor = UIColor.from(hex: "#78c597")
        
        static let colorTransparent: UIColor = UIColor.from(hex: "#00000000")

        static let colorBackground: UIColor = UIColor.from(hex: "#fafafa")
        static let colorDarkBlue: UIColor = UIColor.from(hex: "#0199D5")
        static let colorBlue: UIColor = UIColor.from(hex: "#20BDF5")
        static let colorLightBlue: UIColor = UIColor.from(hex: "#6CD0F3")
        
        static let textFieldBorder: UIColor = UIColor.from(hex: "#A9A9A9")
        
        static let Light_Orange_Color: UIColor = UIColor.from(hex: "#E2BA47")
        static let Orange_Color: UIColor = UIColor.from(hex: "#FB9902")
        static let Black_Orange_Color: UIColor = UIColor.from(hex: "#ee6d57")
        
        static let dark_Green_Color: UIColor = UIColor.from(hex: "#429B92")

    }
    
    struct Font {
        
    }
}

struct SCREEN {
    static let WIDTH = UIScreen.main.bounds.width
    static let HEIGHT = UIScreen.main.bounds.height
    static let statusBarHeight = UIApplication.shared.statusBarFrame.height
//    static let safeAreaBottom = UIApplication.shared.keyWindow!.safeAreaInsets.bottom

//    static let safeAreaBottom = UIApplication.shared.keyWindow!.safeAreaInsets.bottom
        
//    verticalSafeAreaInset = self.view.safeAreaInsets.bottom + self.view.safeAreaInsets.top

}

struct date {
    static let formate = "yyyy/MM/dd"
}

extension UIColor {
    class func from(r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
    /// Converts Hex format string to UIColor
    /// - Parameter hex: hexadecimal representation of color. eg, #cccccc
    /// - Returns: UIColor representing the format
    class func from (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
