//
//  AppError.swift
//  MiDasHMIS
//
//  Created by ramesh prajapati on 3/16/18.
//  Copyright © 2020 Midas. All rights reserved.
//

import Foundation

protocol ApplicationError: Error {
    
    var localizedDescription: String { get }
}



enum UnknownError: ApplicationError {
    
    case standard
    case custom(message:String)
    
    var localizedDescription: String {
        switch self {
        case .standard:
            return "Something went wrong. Try again later"
        case .custom(let message):
            return message
        }
    }
}

enum ApiError: ApplicationError {
    
    case invalidRequestParameter(message:String)
    case invalidResponse(message:String)
    case invalidData
    
    var localizedDescription: String {
        
        switch self {
        case .invalidData:
            return "Something went wrong. Try again later"
        case .invalidResponse(let message):
            return message
        case .invalidRequestParameter(let message):
            return message
        }
    }
}

enum LoginError: ApplicationError {
    
    case invalidUsername(message: String)
    case invalidPassword(message: String)
    case invalidInputs(message: String)
    
    var localizedDescription: String {
        switch self {
        case .invalidInputs(let message),.invalidPassword(let message),.invalidUsername(let message):
            return message
        }
    }
}

enum LocationError: ApplicationError {
    
    case locationServiceError(message: String)
    
    var localizedDescription: String {
        switch self {
        case .locationServiceError(let message):
            return message
        }
    }
}
