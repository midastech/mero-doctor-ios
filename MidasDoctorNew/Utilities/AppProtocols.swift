//
//  AppProtocols.swift
//  MiDasHMIS
//
//  Created by ramesh prajapati on 3/12/18.
//  Copyright © 2020 Midas. All rights reserved.
//

import Foundation

protocol BasePresentation: class {
    func setupViews()
    func setupConstraint()
}
