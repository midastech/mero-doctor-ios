//
//  Extension.swift
//  MidaseClass
//
//  Created by ramesh prajapati on 7/28/20.
//  Copyright © 2020 Ramesh. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func regular(ofSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.regular)
    }
    class func medium(ofSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.medium)
    }
    
    class func semibold(ofSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
    }
    class func bold(ofSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.bold)
    }
}

extension String {
    
    static let MOBILE_NUMBER_CANNOT_BE_EMPTY: String = String("Mobile number cannot be empty")
    static let INVALID_MOBILE_NUMBER: String = String("Invalid Mobile number")
    
    static let USERNAME_CANNOT_BE_EMPTY: String = String("Username cannot be empty")
    static let PASSWORD_CANNOT_BE_EMPTY: String = String("Password cannot be empty")
    static let SOMETHING_WENT_WRONG_TRY_AGIAN_LATER: String = String("Something went wrong. Try again later")
    static let DATE_FORMATE: String = String("yyyy/MM/dd")
    static let DATE_FORMATE_DAY: String = String("EEEE")

    static let NO_INTERNET_CONNECTION_MESSAGE: String = String("No Internet Connection.")
    static let INSERT_VALID_MOBILE_NUMBER: String = String("Please insert a valid mobile number")
    static let CURRENT_PASSWORD_EMPTY: String = String("Current Password cannot be empty")
    static let NEW_PASSWORD_EMPTY: String = String("New Password cannot be empty")
    static let CONFIRM_PASSWORD_EMPTY: String = String("Confirm Password cannot be empty")
    
    static let CURRENT_PASSWORD_NOT_LESS: String = String("Current Password must be atlest 6 characters")
    static let NEW_PASSWORD_NOT_LESS: String = String("New Password must be atlest 6 characters")
    static let CONFIRM_PASSWORD_NOT_LESS: String = String("Confirm Password must be atlest 6 characters")
    
    
    static let PATHOLOGY: String = String("pathology")
    static let ECG: String = String("ecg")
    static let PRESCRIPTION: String = String("prescription")

    
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    func getHeight(with width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [NSAttributedString.Key.font: font], context: nil)
        return actualSize.height
    }
    
    
    func getWidth(with height: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [NSAttributedString.Key.font: font], context: nil)
        return actualSize.width
    }
}





extension UIViewController {
    func toastMessage(message: String?, toastType: ToastType){
        guard let window = UIApplication.shared.keyWindow else {return}
        let messageLbl = UILabel()
        messageLbl.text = message
        messageLbl.textAlignment = .center
        messageLbl.font = toastType.font
        messageLbl.textColor = toastType.textColor
        messageLbl.backgroundColor = toastType.backgroundColor?.withAlphaComponent(1.0)
        
        let textSize:CGSize = messageLbl.intrinsicContentSize
        let labelWidth = min(textSize.width, window.frame.width - 40)
        
        messageLbl.frame = CGRect(x: 20, y: window.frame.height - 120, width: labelWidth + 30, height: textSize.height + 20)
        messageLbl.center.x = window.center.x
        messageLbl.layer.cornerRadius = messageLbl.frame.height/2
        messageLbl.layer.masksToBounds = true
        window.addSubview(messageLbl)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseIn, animations: {
                messageLbl.alpha = 0
            }, completion: { _ in
                UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                   // messageLbl.alpha = 1.0
                }, completion: {_ in
                    messageLbl.removeFromSuperview()
                })
            })
        }
    }
    
    func popupAlert(title: String, message: String, actionTitles: [String], perferredType:UIAlertController.Style, actionStyle:[UIAlertAction.Style], action:[((UIAlertAction) -> Void)]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: perferredType)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: actionStyle[index], handler: action[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHud(withTitle title: String, and Description:String) {
        let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        Indicator.label.text = title
        Indicator.isUserInteractionEnabled = false
        Indicator.detailsLabel.text = Description
        Indicator.show(animated: true)
    }
    func hideHud() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
   
    func convertEnglishDay(day: String) -> String{
//        var selectedDay: String = ""
        
        if day == "Sunday" {
            return "आईतबार"
        }else if day == "Monday" {
            return "सोमबार"
        }else if day == "Tuesday" {
            return "मंगलबार"
        }else if day == "Wednesday" {
            return "बुधाबार"
        }else if day == "Thursday" {
            return "बिहिबार"
        }else if day == "Friday" {
            return "शुक्रबार"
        }else if day == "Saturday" {
            return "शनिबार"
        }else{
            return ""
        }
        
//        return selectedDay
//
//        switch day {
//        case "sunday":
//            return "आईतबार"
//        case "monday":
//            return "सोमबार"
//        default:
//            break
//        }
    }
}

extension Date {
    
    func todayDate(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    

    
}
extension UIViewController {
    func getNepaliDate(engDate: String) -> String {
        let startEnglishDate :String = "1943/04/14"
        let startNepaliYear :Int  = 2000

        let data : [[Int]] = monthArray() as! [[Int]]
        
        let formatter = DateFormatter()
        formatter.dateFormat = String.DATE_FORMATE
        let startDate : NSDate = formatter.date(from: startEnglishDate)! as NSDate
        
        let toDate  : NSDate = formatter.date(from: engDate)! as NSDate
        
        let gregorianCalendar :NSCalendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
        let components : NSDateComponents = gregorianCalendar.components(NSCalendar.Unit.day, from: startDate as Date, to: toDate as Date, options: []) as NSDateComponents

        var days : Int = components.day + 1
        print("days",days)
        
        for i:Int in 0..<data.count{
            for j:Int in 0..<12 {
//                print("value i = ",data[i])
//                print("value j = ",data[j])
//                print("value i,j = ",data[i][j])

                if days > data[i][j]{
                    days = days - data[i][j]
                }else{
                    let nepaliDate :String = String(format: "%d/%02d/%02d", i+startNepaliYear,j+1,days)
                    return nepaliDate
                }
            }
        }
        return ""
    }
    
    func getNepaliFirstDateFromTodayDate(todayNepaliDate: String) -> String{
        let getDay: NSArray = todayNepaliDate.components(separatedBy: "/") as NSArray
        let firstDate: String = String(format: "%@/%@/%@", getDay[0] as! CVarArg, getDay[1] as! CVarArg, "01")// getDay[0] as! String + getDay[1] as! String
        return firstDate
    }
    
    func monthArray() -> NSArray {
            let data:NSArray = [
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            
            [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30],
            
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [30, 32, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
            
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
            
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            
            [31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
            [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
            [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
            
            [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
            [31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
            [31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30],
            [31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30],
            [31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
            [31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30],
            [30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30],
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
            
            [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
            ]
            return data
        }

}
enum ToastType{
    
    case success
    case failure
    case message
    
    var backgroundColor: UIColor? {
        switch self {
        case .success:
            return UIColor.from(hex: "#70D4B4")
        case .failure:
            return UIColor.from(hex: "#DD3E3E")
        case .message:
            return UIColor.from(hex: "#EBEBEB")
        }
    }
    
    var textColor: UIColor? {
        switch self {
        case .failure,.success:
            return Theme.Color.white
        case .message:
            return Theme.Color.darkGray
        }
    }
    
    var font: UIFont? {
        return UIFont.medium(ofSize: 17) //systemFont(ofSize: 17)
    }
    
    var blurEffect: UIBlurEffect? {
        return nil
    }
}


extension Notification.Name {
    static let Get_Profile_Image = Notification.Name("GET_PROFILE_IMAGE")
}


extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    func alignTextBelow(spacing: CGFloat = 10.0) {
        if let image = self.imageView?.image {
            let imageSize: CGSize = image.size
            self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: self.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel?.font as Any])
            self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height - 5), left: 0.0, bottom: 0.0, right: -titleSize.width)
        }
    }
    
    func leftImage(image: UIImage, renderMode: UIImage.RenderingMode) {
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width / 2)
        self.contentHorizontalAlignment = .left
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    func rightImage(image: UIImage, renderMode: UIImage.RenderingMode){
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left:image.size.width / 2, bottom: 0, right: 0)
        self.contentHorizontalAlignment = .right
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    
    
    
}


extension UIView {
    
    
    func statusBarHeight() -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        return Swift.min(statusBarSize.width, statusBarSize.height)
    }
    
    class var stringIdentifier: String {
        return String(describing: self)
    }
    
    func addRoundedCorner(corner: UIRectCorner,radius: CGFloat) {
        
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corner, cornerRadii: CGSize(width: radius, height: radius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        
        self.layer.mask = maskLayer
    }
    
    func addRoundedCorner(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func addBorder(color:UIColor) {
        
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
    }
    
    public func addGradient(color1: CGColor, color2: CGColor) {
        
        //        let gradientView = UIView(frame: .zero)
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =
            [color1,color2]
        //Use diffrent colors
        self.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func dropShadow(scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = Theme.Color.black.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowRadius = 3
        
        //self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // Embedded Message Label
    
    func showMessageLabel(embeddedLabel:UILabel, message:String, textColor:UIColor = Theme.Color.black) {
        
        //self.backgroundColor = UIColor.white
        
        embeddedLabel.text = message
        embeddedLabel.numberOfLines = 0
        embeddedLabel.translatesAutoresizingMaskIntoConstraints = false
        embeddedLabel.textAlignment = .center
        embeddedLabel.font = UIFont.systemFont(ofSize: 16)
        embeddedLabel.textColor = textColor
        
        self.addSubview(embeddedLabel)
        
        NSLayoutConstraint(item: embeddedLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: embeddedLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 16).isActive = true
        NSLayoutConstraint(item: embeddedLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -16).isActive = true
        NSLayoutConstraint(item: embeddedLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        
        self.bringSubviewToFront(embeddedLabel)
    }
    
    func hideMessageLabel(embeddedLabel:UILabel?) {
        embeddedLabel?.removeFromSuperview()
    }
    
    // Embedded Activity Indicator
    
    func showLoadingIndicator(activityIndicator: UIActivityIndicatorView) {
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        
        NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        
        activityIndicator.startAnimating()
        self.bringSubviewToFront(activityIndicator)
    }
    
    func hideLoadingIndicator(activityIndicator: UIActivityIndicatorView?) {
        
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
    }
    
    
    func applyGradient(colors: [UIColor], gradient orientation: GradientOrientation) {
        //        self.backgroundColor = nil
        self.layoutIfNeeded()
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors.map { $0.cgColor }

//        print("startPoint \(orientation.startPoint)")
//        print("endPoint \(orientation.endPoint)")
        
        gradientLayer.startPoint = orientation.startPoint
        gradientLayer.endPoint = orientation.endPoint
        gradientLayer.frame = self.bounds
        gradientLayer.masksToBounds = false
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
   
    func addCellGradient (colorArray: [CGColor], startPoint: CGPoint, endpoint: CGPoint){
        let gradient: CAGradientLayer = CAGradientLayer()
        let colors = colorArray
        gradient.frame = bounds
        gradient.colors = colors
        gradient.startPoint = startPoint
        gradient.endPoint = endpoint
        //        gradient.locations = [0.0,0.2]
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func drawLinearGradient(inside path:UIBezierPath, start:CGPoint, end:CGPoint, colors:[UIColor])
    {
        guard let ctx = UIGraphicsGetCurrentContext() else { return }

        ctx.saveGState()
        defer { ctx.restoreGState() } // clean up graphics state changes when the method returns

        path.addClip() // use the path as the clipping region

        let cgColors = colors.map({ $0.cgColor })
        guard let gradient = CGGradient(colorsSpace: nil, colors: cgColors as CFArray, locations: nil)
            else { return }

        ctx.drawLinearGradient(gradient, start: start, end: end, options: [])
    }
    
   
}


typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint: CGPoint {
        return points.startPoint
    }
    
    var endPoint: CGPoint {
        return points.endPoint
    }
    
    var points: GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0, y: 1.0), CGPoint(x: 1.0, y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 1, y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0, y: 0.5), CGPoint(x: 1.0, y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 0.0, y: 1.0))
        }
    }
}


extension UIApplication {
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 38482
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
                let statusBarView = UIView(frame: statusBarFrame)
                statusBarView.tag = tag
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        } else {
            return nil
        }
    }
}
