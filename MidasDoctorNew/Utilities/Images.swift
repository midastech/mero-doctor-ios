//
//  Images.swift
//  HealthPartner
//
//  Created by ramesh prajapati on 1/11/21.
//

import Foundation
import UIKit

struct Images {
    static let meroDoctor_icon: UIImage = UIImage(named: "logo1")!
    static let pwdhide: UIImage = UIImage(named: "ic_eye@3x")!
    static let pwdShow: UIImage = UIImage(named: "ic_eye_show@3x")!
    static let ic_textfield_background: UIImage = UIImage(named: "img-textfield-bar@3x")!
    static let ic_lock: UIImage = UIImage(named: "ic-lock")!
    static let single_user: UIImage = UIImage(named: "single-user@3x")!
    static let Back_Icon: UIImage = UIImage(named: "ic_back@3x")!
    static let ic_next: UIImage = UIImage(named: "next-ic@3x")!
    static let ic_user: UIImage = UIImage(named: "ic_user@2x")!
    static let ic_notification: UIImage = UIImage(named: "ic_notification@2x")!
    static let ic_hospital: UIImage = UIImage(named: "ic_hospital")!
    static let ic_alert: UIImage = UIImage(named: "ic_alert")!
    static let ic_video: UIImage = UIImage(named: "ic_video@3x")!
    static let ic_calendar: UIImage = UIImage(named: "ic_calendar")!
    static let ic_filter: UIImage = UIImage(named: "ic_filter@3x")!
    static let ic_previous: UIImage = UIImage(named: "previous-ic@3x")!
    static let ic_radio: UIImage = UIImage(named: "ic_radio@3x")!

    static let ic_menu: UIImage = UIImage(named: "ic_menu@3x")!
    static let ic_document: UIImage = UIImage(named: "ic_document@3x")!
    static let ic_upload: UIImage = UIImage(named: "ic_upload@3x")!
    static let placeholder_male: UIImage = UIImage(named: "placeholder_male@3x")!
    static let placeholder_clinic: UIImage = UIImage(named: "placeholder_clinic")!

    static let ic_video_room: UIImage = UIImage(named: "ic_video_room")!
    static let ic_no_internet: UIImage = UIImage(named: "ic_no_internet")!

    static let ic_downfill: UIImage = UIImage(named: "downfill@3x")!

    static let TICK_SELECTED: UIImage = UIImage(named: "tick-icon@2x")!
    static let TICK_GRAY_SELECTED: UIImage = UIImage(named: "tick-icon_gray@3x")!
    
    static let TICK_UNSELECTED: UIImage = UIImage(named: "untick-icon@3x")!
    static let Add_Image: UIImage = UIImage(named: "ic_add@3x")!
    static let Add_ImageXX: UIImage = UIImage(named: "ic_addxx@2x")!

    static let ic_remove: UIImage = UIImage(named: "ic_remove@3x")!

    static let ic_folder: UIImage = UIImage(named: "ic_folder@3x")!
    static let ic_image: UIImage = UIImage(named: "ic_image@3x")!
    
    static let ic_Dashboard_background_Image: UIImage = UIImage(named: "ic_Dashboard_background_Image")!
    static let ic_empty_patient: UIImage = UIImage(named: "ic_empty_patient")!
 
    static let ic_calendar_empty: UIImage = UIImage(named: "ic_calendar_empty@3x")!
    static let ic_picture: UIImage = UIImage(named: "picture")!

    static let ic_close: UIImage = UIImage(named: "ic_close")!
    static let ic_close_red: UIImage = UIImage(named: "ic_close_red@3x")!

    static let ic_female: UIImage = UIImage(named: "ic-female")!
    static let ic_male: UIImage = UIImage(named: "ic-male")!
    static let ic_change_password: UIImage = UIImage(named: "ic_change_password.jpg")!

    
}
