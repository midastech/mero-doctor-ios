//
//  UserDefaults+Extension.swift
//  MiDasHMIS
//
//  Created by ramesh prajapati on 3/16/18.
//  Copyright © 2020 Midas. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

let defaults = UserDefaults.standard

struct Keys {
    static let isLoggedIn       =      "is_logged_in"
    static let UserHospital       =      "USER_HOSPITAL"
    static let isTouchID       =      "is_Touch_ID_Enable"
}

public  extension UserDefaults{
    
    func setUserLogin(value: Bool) {
        defaults.set(value, forKey: Keys.isLoggedIn)
    }
    
    func isUserLoggedIn() -> Bool? {
        let login = defaults.bool(forKey: Keys.isLoggedIn)
        if login {
            return true
        }else{
            return false
        }
    }
    
    func setUserHospitalStatus(value: Bool) {
        defaults.set(value, forKey: Keys.UserHospital)
    }
    
    func isUserHospitalStatus() -> Bool? {
        let login = defaults.bool(forKey: Keys.UserHospital)
        if login {
            return true
        }else{
            return false
        }
    }
    
    
    
    func settouchID(value: Bool) {
        defaults.set(value, forKey: Keys.isTouchID)
    }

    func isTouchID() -> Bool {
        let touch = defaults.bool(forKey: Keys.isTouchID)
        if touch {
            return true
        }else{
            return false
        }
    }    
}
