//
//  BaseNavigationViewController.swift
//  MidasDoctorSwift
//
//  Created by ramesh prajapati on 4/23/19.
//  Copyright © 2019 Midas. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Theme.Color.backgroundGray
        
        // Navigation Bar Color
        self.navigationController?.navigationBar.barTintColor = Theme.Color.colorLightWhite
        self.navigationController?.navigationBar.tintColor = Theme.Color.colorPrimary
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Theme.Color.black]

        // Navigation Bar Opaque
        self.navigationController?.navigationBar.isOpaque = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        // Removing back button
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        /*
        // Navigation Bar Shadow
        self.navigationController?.navigationBar.layer.shadowColor =  Theme.Color.navigationBarShadow.cgColor
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.7
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
*/
    }
    
    var greyView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func activityIndicatorBegin() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50)) //UIActivityIndicatorView(frame: CGRectMake(0,0,50,50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.isUserInteractionEnabled = false
        
        greyView = UIView()
        greyView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        greyView.backgroundColor = .black
        greyView.alpha = 0.1
        self.view.addSubview(greyView)
    }

    func activityIndicatorEnd() {
        self.activityIndicator.stopAnimating()
        activityIndicator.isUserInteractionEnabled = true
        self.greyView.removeFromSuperview()
    }
}
